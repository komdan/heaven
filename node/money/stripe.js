const {
  STRIPE_SECRET_KEY,
} = process.env

const stripe = require('stripe')(STRIPE_SECRET_KEY)

exports.default = stripe
