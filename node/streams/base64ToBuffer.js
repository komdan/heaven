const base64ToBuffer = base64data => Buffer.from(base64data, 'base64')

exports.default = base64ToBuffer
