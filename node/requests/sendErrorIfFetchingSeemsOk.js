const fetch = require('node-fetch')
const {
  equal,
} = require('assert')

const sendErrorIfFetchingSeemsOk = async (
  url = '',
  timeout = 2000,
) => {
  const resolveStatus200AfterTimeout = () => new Promise(resolve => setTimeout(
    () => resolve({ status: 200 }),
    timeout,
  ))

  const { status } = await Promise.race([
    fetch(url),
    resolveStatus200AfterTimeout(),
  ])

  equal(status !== 200, true, 'sendErrorIfFetchingSeemsOk::fetched url resulted in OK (200)')
}

exports.default = sendErrorIfFetchingSeemsOk
