const {
  pick,
} = require('lodash')
const {
  log,
  error,
  postSlackNotification,
} = require('../../../heaven/js')

const sendMail = require('./sendMail').default
const getDocument = require('../firebase.firestore/getDocument').default
const getCollectionWhere = require('../firebase.firestore/getCollectionWhere').default
const analytics = require('../monitoring/analytics').default

const removePlusModifier = mailString => mailString.replace(/\+.*@/i, '@').trim()

const sendOptOutMail = async (mailData) => {
  const {
    mailType = '', // used only here in sendOptOutMail
    toEmail = '', // already used by heaven js sendMail()
    bccEmails = [],
    ccEmails = [],
  } = mailData || {}
  const {
    unsubscribedMailTypes = [],
  } = await (
    getDocument('optInOptOutParams', encodeURIComponent(toEmail))
      .catch(() => ({ }))
  )

  if (!unsubscribedMailTypes.includes(mailType)) {
    await sendMail({
      ...mailData,
      // toEmail: toEmail.replace('+manager@', '@').trim(),
      toEmail: removePlusModifier(toEmail),
      ccEmails: ccEmails.map(removePlusModifier),
      bccEmails: bccEmails.map(removePlusModifier),
    })
  } else {
    log(`sendOptOutMail.js\\${toEmail} opted out for "${mailType}" mails`)
  }

  try {
    const [{
      id: clientId = '',
    } = {}] = await getCollectionWhere('clients', 'email', '==', toEmail)
    if (clientId) {
      analytics.track({
        [clientId ? 'userId' : 'anonymousId']: clientId || 'unauth_client',
        userId: clientId,
        event: 'Email sent',
        properties: {
          toEmail,
          mailType,
          ...pick(mailData, ['fromEmail', 'fromName', 'toEmail', 'toName', 'subject', 'bccEmails']),
        },
      })
    }
  } catch (e) {
    error(e)
    await postSlackNotification(`(sendOptOutMail.js) error tracking mail analytics: ${e.message}`)
  }
}

exports.default = sendOptOutMail
