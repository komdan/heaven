const {
  log,
  error,
} = require('../../../heaven/js')
const sendMailMailjet = require('./sendMailMailjet').default
const sendMailSendgrid = require('./sendMailSendgrid').default
const sendMailNodeMailer = require('./sendMailNodeMailer').default

const WORKING_DOMAINS = [
  'yopmail.com',
  'komdab.net',
]

// ======================== Functions ========================
const sendMail = async ({
  fromEmail = 'matthieu.marielouise@komdab.net',
  fromName = 'Komdab',
  toEmail,
  toName,
  subject,
  textBody = 'Veuillez activer HTML pour visualiser ce courrier',
  htmlBody,
  bccEmails = [],
  ccEmails = [],
  attachments = [],
} = {}) => {
  const attachmentsNodeMailer = attachments.map(({
    ContentType,
    Filename,
    Base64Content,
  }) => ({
    filename: Filename,
    content: Base64Content,
    encoding: 'base64',
    contentType: ContentType,
  }))
  const attachmentsSendGrid = attachments.map(({
    ContentType,
    Filename,
    Base64Content,
  }) => ({
    content: Base64Content,
    filename: Filename,
    type: ContentType,
    disposition: 'attachment',
  }))
  if (
    WORKING_DOMAINS.filter(domain => (
      toEmail.includes(domain)
    )).length > 0
  ) {
    await sendMailNodeMailer({
      fromEmail,
      fromName,
      toEmail,
      toName,
      subject,
      textBody,
      htmlBody,
      bccEmails,
      ccEmails,
      attachments: attachmentsNodeMailer,
    })
  } else {
    try {
      log('Sending with Sendgrid')
      await sendMailSendgrid({
        fromEmail,
        fromName,
        toEmail,
        toName,
        subject,
        textBody,
        htmlBody,
        bccEmails,
        ccEmails,
        attachments: attachmentsSendGrid,
      })
    } catch (e) {
      error(new Error('Error with Sendgrid'))
      error(e)
      error(`sendMail() arguments are : ${JSON.stringify({
        fromEmail,
        fromName,
        toEmail,
        toName,
        subject,
        textBody,
        htmlBody,
        bccEmails,
        ccEmails,
        attachments,
      })}`)
      log('Sending with Mailjet')
      try {
        await sendMailMailjet({
          fromEmail,
          fromName,
          toEmail,
          toName,
          subject,
          textBody,
          htmlBody,
          bccEmails,
          ccEmails,
          attachments,
        })
      } catch (err) {
        error(new Error('Error with sendMailMailjet'))
        error(err)
      }
    }
  }
}

exports.default = sendMail
