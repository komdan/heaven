const nodeMailerPooledTransporter = require('./nodeMailerPooledTransporter').default

const {
  NODE_ENV,
} = process.env

const devString = NODE_ENV === 'development' ? '_' : ''
const sendMailNodeMailer = ({
  fromEmail,
  fromName,
  toEmail,
  toName,
  subject,
  textBody,
  htmlBody,
  bccEmails,
  ccEmails,
  attachments,
}) => nodeMailerPooledTransporter.sendMail({
  from: {
    name: fromName,
    address: fromEmail, // sender address
  },
  replyTo: {
    name: fromName,
    address: fromEmail,
  },
  to: {
    name: toName,
    address: toEmail, // list of receivers
  },
  subject: devString + subject, // Subject line
  text: textBody, // plain text body
  html: htmlBody, // html body
  bcc: ['dev@komdan.com', ...bccEmails],
  cc: ccEmails,
  attachments,
})

exports.default = sendMailNodeMailer
