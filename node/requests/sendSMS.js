// USAGE:
// npx babel-node heaven/js/requests/sendSMS.js

const {
  post,
} = require('axios')
const postSlackNotification = require('../../js/logging/postSlackNotification').default

const log = require('../../js/logging/log').default
const error = require('../../js/logging/error').default

const {
  MAILJET_SMS_TOKEN,
  NODE_ENV,
} = process.env

const sendSMS = async ({ To, Text }) => {
  try {
    if (
      NODE_ENV === 'production'
      // || NODE_ENV === 'development' // should be commented while live
    ) {
      const url = 'https://api.mailjet.com/v4/sms-send'
      const data = {
        From: 'Komdab',
        To,
        Text,
      }
      const options = {
        headers: {
          Authorization: `Bearer ${MAILJET_SMS_TOKEN}`,
          'Content-Type': 'application/json',
        },
      }
      const {
        status,
        statusText,
        // headers,
        // config,
        // request,
        data: postData,
      } = await post(url, data, options)
      log(`sendSMS.js\\sms-sent:${JSON.stringify({ status, statusText, postData })}`)
    } else { // NODE_ENV === 'development'
      log(`sendSMS.js\\sms-not-sent-decause-dev:${JSON.stringify({ To, Text })}`)
      await postSlackNotification(`__SMS to:${To} text:${Text}`)
    }
  } catch (e) {
    const {
      message: errorMessage = '',
      response: {
        data: {
          message = '',
        } = {},
      } = {},
    } = e
    error(e)
    error(new Error(`sms.js\\sendSMS(): ${errorMessage || message}`))
    error(new Error('sms.js\\sendSMS(): ERROR FOLLOWS'))
  }
}

// await sendSMS({
//   To: `+${mobilePhoneNumber}`,
//   Text: smsText,
// })

exports.default = sendSMS
