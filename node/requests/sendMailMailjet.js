const {
  mailJetAPIURL,
  mailJetPK,
  mailJetSK,
  NODE_ENV,
} = process.env

const myFetchPostAuthenticated = require('../../js/requests/myFetchPostAuthenticated').default

// ======================== Functions ========================
const devString = NODE_ENV === 'development' ? '__DEV__' : ''
const sendMailMailjet = ({
  fromEmail = 'matthieu.marielouise@komdab.net',
  fromName = 'Komdab',
  toEmail,
  toName,
  subject,
  textBody = 'Veuillez activer HTML pour visualiser ce courrier',
  htmlBody,
  bccEmails = [],
  ccEmails = [],
  attachments = [],
} = {}) => myFetchPostAuthenticated(
  mailJetAPIURL,
  {
    basicAuth: {
      username: mailJetPK,
      password: mailJetSK,
    },
    data: {
      Messages: [{
        From: {
          Email: fromEmail,
          Name: fromName,
        },
        To: [
          {
            Email: toEmail,
            Name: toName,
          },
        ],
        Cc: [
          ...ccEmails.map(email => ({ Email: email })),
        ],
        Bcc: [
          {
            Email: 'dev@komdan.com',
          },
          ...bccEmails.map(email => ({ Email: email })),
        ],
        Subject: devString + subject,
        TextPart: textBody,
        HTMLPart: htmlBody,
        Attachments: attachments,
      }],
    },
  },
)

exports.default = sendMailMailjet
