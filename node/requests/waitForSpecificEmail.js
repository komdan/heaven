const Imap = require('node-imap')
const _ = require('lodash')
const {
  decodeQuotedPrintable,
} = require('../../../heaven/js')

const waitForSpecificEmail = async ({
  user = '',
  password = '',
  host = 'imap.ionos.fr',
  port = 993,
  tls = true,
  subjectToMatch = '',
  retryDelay = 30 * 1000, // 30 seconds
  onMailReceived = (emailBody) => console.log(emailBody),
  sinceDateTimestamp = Date.now(),
}) => {
  console.log('waitForSpecificEmail executing')
  const imap = new Imap({
    user,
    password,
    host,
    port,
    tls,
  })

  const openInbox = () => new Promise((resolve, reject) => {
    imap.openBox('INBOX', true, (err, box) => {
      if (err) {
        reject(err)
      } else {
        resolve(box)
      }
    })
  })

  const searchForEmail = () => new Promise((resolve, reject) => {
    const sinceDate = new Date(sinceDateTimestamp)
    const sinceDateString = sinceDate.toUTCString()
    imap.search(['UNSEEN', ['SINCE', sinceDateString], ['SUBJECT', subjectToMatch]], (err, results) => {
      if (err) {
        reject(err)
      } else {
        resolve(results)
      }
    })
  })

  const fetchEmail = (uid) => new Promise((resolve, reject) => {
    const f = imap.fetch(uid, { bodies: '' })
    f.on('message', (msg) => {
      msg.on('body', (stream) => {
        let body = ''
        stream.on('data', (chunk) => {
          body += chunk.toString('utf8')
        })
        stream.on('end', () => {
          const decodedBody = decodeQuotedPrintable(
            body.replace(/=F/g, ''),
          )
          resolve(decodedBody)
        })
      })
    })
    f.on('error', (err) => {
      reject(err)
    })
  })

  const checkForEmail = async () => {
    try {
      await new Promise((resolve, reject) => {
        imap.once('ready', resolve)
        imap.once('error', reject)
        imap.connect()
      })

      await openInbox()
      const results = await searchForEmail()

      if (results.length === 0) {
        return false
      }

      const emailUid = results[0]
      const emailBody = await fetchEmail(emailUid)
      console.log('Matching email found!')
      await onMailReceived(emailBody)

      return true
    } finally {
      imap.removeAllListeners() // avoids memory leaks
    }
  }

  const waitForEmail = async () => {
    const emailFound = await checkForEmail()

    if (!emailFound) {
      console.log(`No matching emails found. Waiting for ${+(retryDelay / 1000).toFixed(0)} seconds...`)
      await new Promise((resolve) => setTimeout(resolve, retryDelay)) // Wait for xx seconds
      await waitForEmail()
    } else {
      // nothing, all listeners should already be cleared
    }
  }

  await waitForEmail()
}

exports.default = waitForSpecificEmail
