// PREREQUISITES:
// Tor nodes should be launched from 9050 to 9149

// inspiration : https://medium.com/@paynoattn/using-a-socks-proxy-to-make-requests-in-node-js-ba5125c7999f
// inspiration : https://github.com/axios/axios/issues/535

const {
  post,
} = require('axios')
const {
  random,
} = require('lodash')
const {
  SocksProxyAgent,
} = require('socks-proxy-agent')
// const {
//   LEGIT_USER_AGENTS,
// } = require('../../constants')
// const {
//   log,
// } = require('../../js')

const {
  TOR_BASE_PORT,
  DISABLE_TOR = 'yes',
} = process.env
const TOR_NODE_NB = 10

const anonymousPost = (
  url = '',
  data,
  {
    headers = {},
  } = {},
) => {
  const socksUrl = `socks5://localhost:${+random(TOR_NODE_NB - 1) + +TOR_BASE_PORT}` // 9050-9149
  console.log({
    ...headers,
  })
  // log(`anonymousPost.js\\socksUrl:${socksUrl}`)
  const payload = {
    headers: {
      'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; rv:31.0) Gecko/20100101 Firefox/31.0', // tor user agent
      // 'User-Agent': LEGIT_USER_AGENTS[random(LEGIT_USER_AGENTS.length - 1)],
      'Connection': 'keep-alive',
      ...headers,
    },
  }
  if (DISABLE_TOR === 'no') {
    payload.httpsAgent = new SocksProxyAgent(socksUrl)
  }
  return post(
    url,
    data,
    payload,
  )
}
exports.default = anonymousPost
