const {
  readFileSync,
} = require('fs')
const nodemailer = require('nodemailer')

const {
  MAILSERVER_HOST,
  MAILSERVER_PORT,
  MAILSERVER_DOMAIN,
  MAILSERVER_DKIM_SELECTOR,
  MAILSERVER_USERNAME,
  MAILSERVER_PASS,
  MAILSERVER_DKIM_PRIVATE_KEY_PATH,
  MAILSERVER_DKIM_CACHE_FOLDER_PATH,
} = process.env

const nodeMailerPooledTransporter = nodemailer.createTransport({
  pool: true,
  host: MAILSERVER_HOST,
  port: MAILSERVER_PORT,
  secure: true, // use TLS
  auth: {
    user: MAILSERVER_USERNAME,
    pass: MAILSERVER_PASS,
  },
  dkim: {
    domainName: MAILSERVER_DOMAIN,
    keySelector: MAILSERVER_DKIM_SELECTOR,
    privateKey: readFileSync(MAILSERVER_DKIM_PRIVATE_KEY_PATH, 'utf8'),
    cacheDir: MAILSERVER_DKIM_CACHE_FOLDER_PATH,
    cacheTreshold: 100 * 1024,
  },
})

exports.default = nodeMailerPooledTransporter
