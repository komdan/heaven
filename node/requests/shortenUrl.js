const uuidv4 = require('uuid/v4')
const getDocument = require('../firebase.firestore/getDocument').default
const setDocument = require('../firebase.firestore/setDocument').default

const {
  NODE_ENV,
} = process.env

const GO_URL = NODE_ENV === 'development'
  ? 'https://dev.go.komdab.net'
  : 'https://go.komdab.net'

const isSlugUsed = async (slug) => {
  let isInShortLinksData = true
  try {
    await getDocument('shortLinksData', slug)
  } catch {
    isInShortLinksData = false
  }
  return isInShortLinksData
}

const shortenUrl = async ({
  longLink = '',
  forcedSlug = '',
  temporary = true,
  params = {},
  data = {},
  expiresAtTimestamp = Number.MAX_SAFE_INTEGER,
} = {}) => {
  let slug = uuidv4().substr(0, 8)
  while (await isSlugUsed(slug)) { // eslint-disable-line
    slug = uuidv4().substr(0, 8)
  }
  if (forcedSlug) {
    slug = forcedSlug
  }

  await setDocument('shortLinksData', slug, {
    redirectToUrl: longLink,
    type: 'genericShortenedUrl',
    shortenedAt: new Date(),
    shortenedAtTimestamp: Date.now(),
    expiresAtTimestamp,
    temporary,
    params,
    ...data,
  })
  return `${GO_URL}/${slug}`
}

// // USAGE:
// // echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/node/requests/shortenUrl.js" | /bin/bash
// // echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/node/requests/shortenUrl.js" | /bin/bash
// const main = async () => {
//   const url = await shortenUrl({
//     longLink: 'https://onedrive.live.com/download?resid=2C7D6B4AAE16253F!241506',
//     forcedSlug: 'dlkp',
//   })
//   console.log(url)
// }
// main()

exports.default = shortenUrl
