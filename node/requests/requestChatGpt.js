const {
  post,
} = require('axios')

const {
  CHAT_GPT_API_KEY = '',
} = process.env

const requestChatGpt = async ({
  temperature = 0.3,
  role = 'user',
  model = 'gpt-3.5-turbo',
  content = '',
} = {}) => {
  const {
    data: {
      choices: [
        {
          message: {
            content: result = '',
          } = {},
        } = {},
      ] = [],
    } = {},
  } = await post(
    'https://api.openai.com/v1/chat/completions',
    {
      model,
      messages: [
        {
          role,
          content,
        },
      ],
      temperature,
    },
    {
      headers: {
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${CHAT_GPT_API_KEY}`,
      },
    },
  ) || {}
  return result
}

exports.default = requestChatGpt
