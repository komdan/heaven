const {
  SENDGRID_API_KEY,
  NODE_ENV,
} = process.env
const sgMail = require('@sendgrid/mail')

sgMail.setApiKey(SENDGRID_API_KEY)

// ======================== Functions ========================
const devString = NODE_ENV === 'development' ? '__DEV__' : ''
const sendMailSendgrid = ({
  fromEmail = 'matthieu.marielouise@komdab.net',
  fromName = 'Komdab',
  toEmail,
  toName,
  subject,
  textBody = 'Veuillez activer HTML pour visualiser ce courrier',
  htmlBody,
  bccEmails = [],
  ccEmails = [],
  attachments = [],
  bccDevAtKomdab = true,
} = {}) => sgMail
  .send({
    personalizations: [
      {
        to: [
          {
            email: toEmail,
            name: toName,
          },
        ],
        cc: [
          ...ccEmails.map(email => ({ email })),
        ],
        bcc: bccDevAtKomdab
          ? [
            {
              email: 'dev@komdan.com',
            },
            ...bccEmails.map(email => ({ email })),
          ]
          : [
            ...bccEmails.map(email => ({ email })),
          ],
      },
    ],
    from: {
      email: fromEmail,
      name: fromName,
    },
    subject: devString + subject,
    text: textBody,
    html: htmlBody,
    attachments,
  })

exports.default = sendMailSendgrid
