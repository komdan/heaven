function send(res, code, body) {
  const response = {
    statusCode: code,
    body,
  }
  // log('utils.js\\send()::Sending back to client: ', response)
  res.status(code).send(response)
}

exports.default = send
