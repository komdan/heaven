const Analytics = require('analytics-node')

const {
  SEGMENT_WRITE_KEY,
} = process.env

const analytics = SEGMENT_WRITE_KEY
  ? (new Analytics(SEGMENT_WRITE_KEY))
  : ({
    track: () => { },
    setup: () => { },
  })

exports.default = analytics
