const getBucketFiles = require('./getBucketFiles').default

const getBucketFileNames = async () => {
  const bucketFiles = await getBucketFiles()
  return bucketFiles.map(({ name }) => name)
}

exports.default = getBucketFileNames
