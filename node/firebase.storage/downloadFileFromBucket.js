const {
  firebaseAdmin,
} = require('../firebase.firestore')

const downloadFileFromBucket = async (bucketPath, writeFileRelativePath) => {
  const file = firebaseAdmin.storage().bucket().file(bucketPath)
  await file.download({
    destination: writeFileRelativePath,
  })
}

exports.default = downloadFileFromBucket
