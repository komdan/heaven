const {
  firebaseAdmin,
} = require('../firebase.firestore')

const deleteBucketFile = path => new Promise((resolve, reject) => {
  firebaseAdmin.storage().bucket().file(path)
    .delete((e, response) => (e ? reject(e) : resolve(response)))
})

exports.default = deleteBucketFile
