const {
  firebaseAdmin,
} = require('../firebase.firestore')

const bucketFileExists = async (path) => {
  const file = firebaseAdmin.storage().bucket().file(path);
  const exists = await file.exists();
  return exists[0];
}

exports.default = bucketFileExists
