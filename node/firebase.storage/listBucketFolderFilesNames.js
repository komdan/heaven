const {
  firebaseAdmin,
} = require('../firebase.firestore')

const listBucketFolderFilesNames = async (folderPath) => {
  const options = {
    prefix: `${folderPath}/`,
  };
  const [files] = await firebaseAdmin.storage().bucket().getFiles(options);

  const fileList = [];
  files.forEach(file => {
    fileList.push({
      ...file,
    });
  });

  return fileList;
}

exports.default = listBucketFolderFilesNames
