const {
  firebaseAdmin,
} = require('../firebase.firestore')
const {
  error,
} = require('../../js')

const uploadStreamToBucket = (path, stream) => new Promise(async (resolve, reject) => {
  const file = firebaseAdmin.storage().bucket().file(path)
  try {
    await file.makePublic()
  } catch (e) {
    error(e)
  }
  // console.log(`saving file ${path}`)
  const remoteStream = file.createWriteStream()
  stream.pipe(remoteStream)
    .on('error', (e) => {
      reject(e)
      throw e
    })
    .on('finish', async () => {
      await file.makePublic()
      const [{ mediaLink }] = await file.getMetadata()
      // console.log(mediaLink)
      resolve(mediaLink)
    })
})

exports.default = uploadStreamToBucket
