const {
  firebaseAdmin,
} = require('../firebase.firestore')

const uploadBufferToBucket = async (localFilePath, destination) => {
  await firebaseAdmin.storage().bucket().upload(localFilePath, { destination })
  const file = firebaseAdmin.storage().bucket().file(destination)
  await file.makePublic()
  const [{ mediaLink }] = await file.getMetadata()
  return mediaLink
}

exports.default = uploadBufferToBucket
