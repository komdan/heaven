const {
  firebaseAdmin,
} = require('../firebase.firestore')

const getBucketFiles = async () => {
  const [files] = await firebaseAdmin.storage().bucket().getFiles()
  return files
}

exports.default = getBucketFiles
