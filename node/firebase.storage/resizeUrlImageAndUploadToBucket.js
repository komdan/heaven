const Jimp = require('jimp')
const listBucketFolderFilesNames = require('./listBucketFolderFilesNames').default
const uploadBufferToBucket = require('./uploadBufferToBucket').default
const {
  slugify,
} = require('../../js')

const resizeUrlImageAndUploadToBucket = async ({
  width = 0,
  // height = 0,
  path,
  fileName: orginalFileName,
  url,
  quality = 100,
  computeExtension = true,
}) => {
  try {
    const fileName = slugify(orginalFileName)
    const fileNames = await listBucketFolderFilesNames(`${path}`)

    const filesOnBucketMatchingRemoteFile = fileNames.filter(({
      name,
    }) => (
      name.includes(`${path}/${fileName}`)
    ))

    const isFileAlreadyUploadedInBucket = (filesOnBucketMatchingRemoteFile.length > 0)

    if (
      isFileAlreadyUploadedInBucket
      && filesOnBucketMatchingRemoteFile[0].metadata
      && filesOnBucketMatchingRemoteFile[0].metadata.mediaLink
    ) {
      return filesOnBucketMatchingRemoteFile[0].metadata.mediaLink
    }

    else {
      const image = await Jimp.read(url)
      if (width > 0) {
        image.resize(width, Jimp.AUTO)
      }
      image.quality(quality)

      const mime = image.getMIME()
      const mimeSplitted = mime.split('/')

      let extension = ''

      if (computeExtension && (mimeSplitted.length > 1)) {
        extension = `.${mimeSplitted[mimeSplitted.length - 1]}`
      }

      const buffer = await image.getBufferAsync(Jimp.AUTO)

      const mediaLink = await uploadBufferToBucket(`${path}/${fileName}${extension}`, buffer)

      return mediaLink
    }
  } catch (e) {
    console.error(e)
    return url
  }
}

exports.default = resizeUrlImageAndUploadToBucket
