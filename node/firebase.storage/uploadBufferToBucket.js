const {
  firebaseAdmin,
} = require('../firebase.firestore')

const uploadBufferToBucket = async (bucketPath, buffer) => {
  const file = firebaseAdmin.storage().bucket().file(bucketPath)
  await file.save(buffer)
  await file.makePublic()
  const [{ mediaLink }] = await file.getMetadata()
  return mediaLink
}

exports.default = uploadBufferToBucket
