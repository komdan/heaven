const executeConvert = require('./executeConvert').default

const placeQRCodeOnTemplate = ({
  qrCodeFilePath,
  inputFilePath,
  outputFilePath,
  gravity,
  size,
  marginHorizontal,
  marginVertical,
}) => executeConvert({
  inputFilePaths: [
    inputFilePath,
    qrCodeFilePath,
  ],
  options: `-gravity ${gravity} -geometry ${size}${marginHorizontal}${marginVertical} -composite`,
  outputFilePath,
})

exports.default = placeQRCodeOnTemplate
