// eslint-disable

const fs = require('fs')
const {
  parse: toCSV,
} = require('json2csv')
const {
  omitBy,
  isUndefined,
} = require('lodash')

// Write the CSV string to a file
function saveCsvToFile(csv, filePath) {
  fs.writeFileSync(filePath, csv)
}

// Convert a JSON object to a CSV string
function jsonToCsv(
  json,
  filePath,
  {
    fields = undefined,
    excelStrings = false,
    quote = '"',
    delimiter = ';',
    withBOM = true,
  } = {},
) {
  // const columns = Object.keys(json[0])
  // const rows = json.map(row => columns.map(column => row[column]).join(','))
  // const csv = [columns.join(','), ...rows].join('\n')
  const csv = toCSV(
    json,
    omitBy({
      quote, // default value: "
      delimiter, // default: ,
      withBOM,
      excelStrings,
      fields,
    }, isUndefined),
  )
  saveCsvToFile(csv, filePath)
}


exports.default = jsonToCsv
