const {
  readFile,
} = require('fs')

const JSONParseFile = (
  inputPath,
  options = undefined,
) => new Promise((resolve, reject) => readFile(
  inputPath,
  options,
  (e, data) => (e ? reject(e) : resolve(JSON.parse(data.toString()))),
))

exports.default = JSONParseFile
