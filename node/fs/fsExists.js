const {
  access,
  constants: {
    F_OK,
  },
} = require('fs')

const fsExists = filePath => new Promise(resolve => access(
  filePath,
  F_OK,
  e => (e ? resolve(false) : resolve(true)),
))

exports.default = fsExists
