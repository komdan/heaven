const {
  exec,
} = require('child_process')

const executeBashCommand = ({
  command = 'ls',
  showExecution = false,
  hideOutput = false,
}) => new Promise((resolve, reject) => {
  if (showExecution) console.log(`> Executing: ${command}`)
  exec(
    command,
    (err, stdout, stderr) => {
      // log(>> TERMINATED: ${command})
      if (err) {
        console.error(err)
        reject(new Error(stderr))
      } else {
        if (!hideOutput) {
          console.log(stdout)
        }
        resolve(stdout)
      }
    },
  )
})

exports.default = executeBashCommand
