const executeBashCommand = require('./executeBashCommand').default

const executeConvert = ({
  inputFilePaths = [],
  outputFilePath = '',
  options = '',
}) => executeBashCommand({
  command: `convert ${inputFilePaths.join(' ')} ${options} ${outputFilePath}`,
})

exports.default = executeConvert
