const {
  writeFile,
} = require('fs')
const {
  dirname,
} = require('path')
const mkdirp = require('mkdirp')

const JSONStringifyToFile = (
  object,
  outputPath,
  options,
) => new Promise(async (resolve, reject) => {
  await mkdirp(dirname(outputPath)).catch(reject)
  writeFile(
    outputPath,
    JSON.stringify(object),
    options,
    e => (e ? reject(e) : resolve()),
  )
})

exports.default = JSONStringifyToFile

// // TESTS
// const functionToTest = JSONStringifyToFile
// const test = async () => {
//   console.log('Testing with various data types and edge cases')

//   // Test with empty object
//   console.log('Empty object -> check /tmp/TEST_emptyObject.json')
//   await functionToTest({}, '/tmp/TEST_emptyObject.json')

//   // Test with empty array
//   console.log('Empty array -> check /tmp/TEST_emptyArray.json')
//   await functionToTest([], '/tmp/TEST_emptyArray.json')

//   // Test with empty string
//   console.log('Empty string -> check /tmp/TEST_emptyString.json')
//   await functionToTest("", '/tmp/TEST_emptyString.json')

//   // Test with null
//   console.log('Null value -> check /tmp/TEST_null.json')
//   await functionToTest(null, '/tmp/TEST_null.json')

//   // // Test with undefined
//   // console.log('Undefined value -> check /tmp/TEST_undefined.json')
//   // await functionToTest(undefined, '/tmp/TEST_undefined.json')

//   // Test with a very large number
//   console.log('Very large number -> check /tmp/TEST_largeNumber.json')
//   await functionToTest(1e308, '/tmp/TEST_largeNumber.json')

//   // Test with a Date object
//   console.log('Date object -> check /tmp/TEST_dateObject.json')
//   await functionToTest(new Date(), '/tmp/TEST_dateObject.json')

//   // Existing tests
//   console.log('Collection [{ a: 1 }, { b: 2, field: "value" }] -> check /tmp/TEST_collection.json')
//   await functionToTest([{ a: 1 }, { b: 2, field: "value" }], '/tmp/TEST_collection.json')

//   console.log('Array [1, 2, "a"] -> check /tmp/TEST_array.json')
//   await functionToTest([1, 2, "a"], '/tmp/TEST_array.json')

//   console.log('String "mystring is cool and \n has a retour à la ligne" -> check /tmp/TEST_string.json')
//   await functionToTest("mystring is cool and \n has a retour à la ligne", '/tmp/TEST_string.json')

//   console.log('Object { b: 2, field: "value" } -> check /tmp/TEST_object.json')
//   await functionToTest({ b: 2, field: "value" }, '/tmp/TEST_object.json')

//   // // Test with circular reference
//   // console.log('Object with circular reference -> check /tmp/TEST_circular.json')
//   // const circularObject = {}
//   // circularObject.self = circularObject
//   // try {
//   //   await functionToTest(circularObject, '/tmp/TEST_circular.json')
//   // } catch (e) {
//   //   console.log('Handled circular reference:', e.message)
//   // }
// }

// test()
