const executeBashCommand = require('./executeBashCommand').default

const downloadFromUrl = async ({
  url = 'https://komdab.net/favicon.ico',
  outputFolderPath = '/tmp/komdab-useless-folder',
  extension = 'png',
  fileName = 'dummy-file-name',
  isAuthorizedDirectoryPath = false,
}) => {
  if (!isAuthorizedDirectoryPath) {
    throw new Error(`Error - downloadFromUrl - Invalid output folder path (${outputFolderPath})`)
  }
  await executeBashCommand({
    command: `curl "${url}" --output "${outputFolderPath}/${fileName}.${extension}"`,
    hideOutput: true,
  })
  return `${outputFolderPath}/${fileName}.${extension}`
}

exports.default = downloadFromUrl
