const fs = require('fs')
const { dirname } = require('path')
const mkdirp = require('mkdirp')
const { Transform } = require('stream')

const JSONStringifyToLargeFile = (object, outputPath, options) => new Promise(async (resolve, reject) => {
  try {
    await mkdirp(dirname(outputPath))

    const outputStream = fs.createWriteStream(outputPath, options)
    const transformStream = new Transform({
      writableObjectMode: true,
      transform(chunk, encoding, callback) {
        let json
        try {
          // Convertir l'élément de l'objet en JSON
          json = JSON.stringify(chunk)
          if (this.isArray && !this.firstChunkWritten) {
            this.push(`[${json}`)
            this.firstChunkWritten = true
          } else if (this.isArray) {
            this.push(`,${json}`)
          } else {
            this.push(json)
          }
          callback()
        } catch (error) {
          callback(error)
        }
      },
      flush(callback) {
        if (this.isArray && !this.firstChunkWritten) {
          // Gérer le cas du tableau vide
          this.push('[]')
        } else if (this.isArray) {
          this.push(']') // Fermer le tableau JSON dans le flux
        }
        callback()
      },
    })

    transformStream.firstChunkWritten = false
    transformStream.isArray = Array.isArray(object)

    if (transformStream.isArray && object.length > 0) {
      object.forEach(item => transformStream.write(item))
    } else if (!transformStream.isArray) {
      transformStream.write(object)
    }

    transformStream.end()

    transformStream.pipe(outputStream)

    outputStream.on('finish', () => {
      resolve()
    })

    outputStream.on('error', (err) => {
      reject(err)
    })

    transformStream.on('error', (err) => {
      reject(err)
    })
  } catch (err) {
    reject(err)
  }
})

exports.default = JSONStringifyToLargeFile


// // TESTS
// const functionToTest = JSONStringifyToLargeFile
// const test = async () => {
//   console.log('Testing with various data types and edge cases')

//   // Test with empty object
//   console.log('Empty object -> check /tmp/TEST_emptyObject_big.json')
//   await functionToTest({}, '/tmp/TEST_emptyObject_big.json')

//   // Test with empty array
//   console.log('Empty array -> check /tmp/TEST_emptyArray_big.json')
//   await functionToTest([], '/tmp/TEST_emptyArray_big.json')

//   // Test with empty string
//   console.log('Empty string -> check /tmp/TEST_emptyString_big.json')
//   await functionToTest("", '/tmp/TEST_emptyString_big.json')

//   // // Test with null
//   // console.log('Null value -> check /tmp/TEST_null_big.json')
//   // await functionToTest(null, '/tmp/TEST_null_big.json')

//   // // Test with undefined
//   // console.log('Undefined value -> check /tmp/TEST_undefined_big.json')
//   // await functionToTest(undefined, '/tmp/TEST_undefined_big.json')

//   // Test with a very large number
//   console.log('Very large number -> check /tmp/TEST_largeNumber_big.json')
//   await functionToTest(1e308, '/tmp/TEST_largeNumber_big.json')

//   // Test with a Date object
//   console.log('Date object -> check /tmp/TEST_dateObject_big.json')
//   await functionToTest(new Date(), '/tmp/TEST_dateObject_big.json')

//   // Existing tests
//   console.log('Collection [{ a: 1 }, { b: 2, field: "value" }] -> check /tmp/TEST_collection_big.json')
//   await functionToTest([{ a: 1 }, { b: 2, field: "value" }], '/tmp/TEST_collection_big.json')

//   console.log('Array [1, 2, "a"] -> check /tmp/TEST_array_big.json')
//   await functionToTest([1, 2, "a"], '/tmp/TEST_array_big.json')

//   console.log('String "mystring is cool and \n has a retour à la ligne" -> check /tmp/TEST_string_big.json')
//   await functionToTest("mystring is cool and \n has a retour à la ligne", '/tmp/TEST_string_big.json')

//   console.log('Object { b: 2, field: "value" } -> check /tmp/TEST_object_big.json')
//   await functionToTest({ b: 2, field: "value" }, '/tmp/TEST_object_big.json')

//   // // Test with circular reference
//   // console.log('Object with circular reference -> check /tmp/TEST_circular.json')
//   // const circularObject = {}
//   // circularObject.self = circularObject
//   // try {
//   //   await functionToTest(circularObject, '/tmp/TEST_circular.json')
//   // } catch (e) {
//   //   console.log('Handled circular reference:', e.message)
//   // }
// }

// test()
