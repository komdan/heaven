const fs = require('fs')

const getFileTimestamp = async (filePath) => {
  try {
    const { mtimeMs } = await fs.promises.stat(filePath)
    return mtimeMs
  } catch (error) {
    return null
  }
}

const fileExists = async (filePath) => {
  try {
    await fs.promises.access(filePath)
    return true
  } catch (error) {
    return false
  }
}

const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms))

const waitForFileToChange = async (filePath, timeout = 3600 * 1000) => {
  const startTime = Date.now()
  const previousTimestamp = await getFileTimestamp(filePath)

  while (true) { // eslint-disable
    await sleep(1000) // Wait for 1 second

    const currentTimestamp = await getFileTimestamp(filePath)
    if (currentTimestamp !== previousTimestamp) {
      return
    }

    const elapsedTime = Date.now() - startTime
    if (elapsedTime >= timeout) {
      throw new Error(`Timeout waiting for file ${filePath} to change or be created`)
    }
  }
}

const waitForFileToExist = async (filePath, timeout = 3600 * 1000) => {
  const startTime = Date.now()

  while (true) { // eslint-disable
    await sleep(1000) // Wait for 1 second
    if (await fileExists(filePath)) {
      return
    }

    const elapsedTime = Date.now() - startTime
    if (elapsedTime >= timeout) {
      throw new Error(`Timeout waiting for file ${filePath} to change or be created`)
    }
  }
}

const waitForFileToChangeOrToBeCreated = async (filePath, timeout = 3600 * 1000) => {
  const fileAlreadyExists = await fileExists(filePath)

  if (fileAlreadyExists) {
    await waitForFileToChange(filePath, timeout)
  } else {
    await waitForFileToExist(filePath, timeout)
  }
}

exports.default = waitForFileToChangeOrToBeCreated
