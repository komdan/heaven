const {
  find,
} = require('lodash')
const JSONParseFile = require('../fs/JSONParseFile').default
const JSONStringifyToFile = require('../fs/JSONStringifyToFile').default

// ====== USAGE
// Used to handle heavy treatment.
// Pushed objects will be treated.
// the queue can be saved into a file for fail-safe recovery
//
// ====== EXAMPLE
// const VideoCutter = new TreatingQueue({
//   treatOne: downloadCutAndUploadVideo,
//   queueFilePath: TEMP_QUEUE_JSONFILE,
// })
//
// const onError = console.error
//
// RUN
// VideoCutter.push({ youtubeVideoSource, youtubeVideoStart, youtubeVideoDuration })
//
// JOB
// setInterval(
//   () => VideoCutter.popAndTreatOne().catch(onError),
//   1000,
// )
// setInterval(
//   () => VideoCutter.backupQueueIntoFile().catch(onError),
//   2000,
// )

class TreatingQueue {
  constructor({
    treatOne,
    queueFilePath,
  }) {
    this.queue = []
    this.treatOne = treatOne
    this.queueFilePath = queueFilePath

    this.isVideoCutterBusy = false
    this.appendQueueFile()
  }

  async appendQueueFile() {
    const queueInFile = await JSONParseFile(this.queueFilePath).catch(() => [])
    this.queue = [
      ...queueInFile,
      ...this.queue,
    ]
  }

  async backupQueueIntoFile() {
    await JSONStringifyToFile(this.queue, this.queueFilePath)
  }

  push(object) {
    if (!find(this.queue, object)) {
      this.queue.push(object)
    }
  }

  async popAndTreatOne() {
    if (this.isVideoCutterBusy || !this.queue.length) return
    this.isVideoCutterBusy = true

    const [first] = this.queue
    this.queue = this.queue.slice(1)

    try {
      await this.treatOne(first)
    } catch (e) {
      await this.backupQueueIntoFile()
      throw e
    }

    this.isVideoCutterBusy = false
  }
}

exports.default = TreatingQueue
