const client = require('./twilioClient').default

const triggerFlow = async ({
  flowId = 'FW476c55ff987b93d3127ad299efa1a40e',
  to = '',
  from = '+33186472718',
  parameters = {},
}) => client
  .studio
  .v2
  .flows(flowId)
  .executions
  .create({
    to,
    from,
    parameters: {
      ...parameters,
    },
  })

exports.default = triggerFlow
