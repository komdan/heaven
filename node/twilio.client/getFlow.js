const client = require('./twilioClient').default

const getFlow = (flowSid = '') => (
  client
    .studio
    .flows(flowSid)
    .fetch()
)

exports.default = getFlow
