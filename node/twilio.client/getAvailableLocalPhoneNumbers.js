const client = require('./twilioClient').default

const getAvailableLocalPhoneNumbers = ({
  contains = '+331',
  country = 'FR',
  limit = 100,
} = {
  contains: '+331',
  country: 'FR',
  limit: 100,
}) => {
  return client
    .availablePhoneNumbers(country)
    .local
    .list({ contains, limit })
}

exports.default = getAvailableLocalPhoneNumbers
