const client = require('./twilioClient').default

const {
  TWILIO_BUNDLE_SID,
  TWILIO_ADDRESS_SID,
} = process.env

const buyPhoneNumber = ({
  phoneNumber,
  bundleSid = TWILIO_BUNDLE_SID,
  addressSid = TWILIO_ADDRESS_SID,
}) => (
  client
    .incomingPhoneNumbers
    .create({ phoneNumber, addressSid, bundleSid })
)

exports.default = buyPhoneNumber
