const client = require('./twilioClient').default

const updatePhoneNumber = ({ phoneNumberSid, phoneNumberProps }) => (
  client
    .incomingPhoneNumbers(phoneNumberSid)
    .update({
      ...phoneNumberProps,
    })
)

exports.default = updatePhoneNumber
