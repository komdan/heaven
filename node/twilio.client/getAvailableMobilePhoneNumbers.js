const client = require('./twilioClient').default

const getAvailableMobilePhoneNumbers = ({ country = 'FR', limit = 100 } = { country: 'FR', limit: 100 }) => {
  return client
    .availablePhoneNumbers(country)
    .mobile
    .list({ limit })
}

exports.default = getAvailableMobilePhoneNumbers
