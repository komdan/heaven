const client = require('./twilioClient').default

const createFlow = ({ flow }) => (
  client
    .studio
    .flows
    .create({
      ...flow,
    })
)

exports.default = createFlow
