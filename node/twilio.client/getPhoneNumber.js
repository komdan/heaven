const client = require('./twilioClient').default

const getPhoneNumber = phoneNumber => (
  client
    .incomingPhoneNumbers
    .list({ phoneNumber, limit: 1 })
)

exports.default = getPhoneNumber
