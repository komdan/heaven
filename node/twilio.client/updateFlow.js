const client = require('./twilioClient').default

const updateFlow = ({ flowSid, flowProps }) => (
  client
    .incomingPhoneNumbers(flowSid)
    .update({
      ...flowProps,
    })
)

exports.default = updateFlow
