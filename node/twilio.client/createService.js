const client = require('./twilioClient').default

const createService = friendlyName => (
  client
    .messaging
    .services
    .create({ friendlyName })
)

exports.default = createService
