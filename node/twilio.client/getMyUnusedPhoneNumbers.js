const client = require('./twilioClient').default

const getMyUnusedPhoneNumbers = async () => {
  const phoneNumbers = await client
    .incomingPhoneNumbers
    .list({ limit: 1000 })

  return phoneNumbers.filter(({ voiceUrl }) => voiceUrl === '')
}

exports.default = getMyUnusedPhoneNumbers
