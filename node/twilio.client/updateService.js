const client = require('./twilioClient').default

const updateService = ({ serviceSid, serviceProps }) => (
  client
    .messaging
    .services(serviceSid)
    .update({
      ...serviceProps,
    })
)

exports.default = updateService
