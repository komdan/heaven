const client = require('./twilioClient').default

const createAlphaSender = ({ serviceSid, alphaSender }) => (
  client
    .messaging
    .services(serviceSid)
    .alphaSenders
    .create({ alphaSender })
)

exports.default = createAlphaSender
