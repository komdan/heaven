const {
  chunk,
} = require('lodash')
const {
  error,
  assertNonEmptyArray,
  assertNonEmptyString,
} = require('../../js')

const sendMulticastNotificationsLimitedTo100 = require('./sendMulticastNotificationsLimitedTo100').default

async function sendMulticastNotifications({
  pushTokens,
  title,
  body,
  ...rest
}) {
  try {
    assertNonEmptyArray(pushTokens, 'pushTokens')
    assertNonEmptyString(title, 'title')
    assertNonEmptyString(body, 'body')

    const pushTokensChunks = chunk(pushTokens, 100)

    // 4.RetUrN
    const response = await Promise.all(
      pushTokensChunks.map(
        // TODO: remove 'async' below, not needed
        async tokens => sendMulticastNotificationsLimitedTo100({
          pushTokens: tokens,
          title,
          body,
          ...rest,
        }),
      ),
    )

    return response
  } catch (e) {
    error(new Error('sendMulticastNotifications.js/ERROR FOLLOWS'))
    error(e)
    return []
  }
}

exports.default = sendMulticastNotifications
