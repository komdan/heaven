const {
  postSlackNotification,
  error,
} = require('../../js')
const {
  createDocument,
} = require('../firebase.firestore')

const sendPushNotificationsHandleErrors = async (responses) => {
  // == responses object:
  // [
  //     {
  //         "pushToken": "xxxxxxxxxxx",
  //         "canonicalRegistrationTokenCount": 0,
  //         "failureCount": 0,
  //         "multicastId": 4359606424888973300,
  //         "results":
  //         [
  //             {
  //                 "messageId": "1651052654093284"
  //             }
  //         ],
  //         "successCount": 1
  //     }
  // ]
  const allErrorCodes = []
  await Promise.all(responses.map(async (response) => {
    const {
      pushToken,
      results = [],
    } = response || {}

    await Promise.all(results.map(async ({ error: err }) => {
      if (err) {
        const {
          code: firebaseMessagingErrorCode = '',
        } = err
        allErrorCodes.push(firebaseMessagingErrorCode)
        await postSlackNotification(`sendPushNotificationsHandleErrors.js ERROR`)
        error(new Error(`(sendPushNotificationsHandleErrors.js) error "${firebaseMessagingErrorCode}" sending to ${JSON.stringify(pushToken)}`))

        await createDocument('expiredPushTokens', {
          pushToken,
          expiredAtTimestamp: Date.now(),
          expiredAt: new Date(),
          firebaseMessagingErrorCode,
        })
      }
    }))
  }))

  if (allErrorCodes.length) {
    throw new Error(JSON.stringify(allErrorCodes))
  }
}

exports.default = sendPushNotificationsHandleErrors
