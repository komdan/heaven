const wasSendPushNotificationsSuccessful = (responses = []) => {
  // returns true if at least one push notification was sent correctly
  return !!responses.length
  // Before FCM migration :
  // // == responses object:
  // // [
  // //     {
  // //         "pushToken": "xxxxxxxxxxx",
  // //         "canonicalRegistrationTokenCount": 0,
  // //         "failureCount": 0,
  // //         "multicastId": 4359606424888973300,
  // //         "results":
  // //         [
  // //             {
  // //                 "messageId": "1651052654093284"
  // //             }
  // //         ],
  // //         "successCount": 1
  // //     }
  // // ]
  // let wasItSuccessful = true
  // responses.forEach(async ({
  //   results = [],
  // } = {}) => {
  //   results.forEach(async ({ error: err }) => {
  //     if (err) {
  //       wasItSuccessful = false
  //     }
  //   })
  // })

  // return wasItSuccessful
}

exports.default = wasSendPushNotificationsSuccessful
