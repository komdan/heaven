const sendPushNotifications = require('./sendPushNotifications').default

const {
  error,
  assertNonEmptyArray,
  assertNonEmptyString,
} = require('../../js')

async function sendMulticastNotificationsLimitedTo100({
  pushTokens: pushTokensInArgs,
  title,
  body,
  ...rest
}) {
  try {
    const pushTokens = pushTokensInArgs.filter(pushToken => !!pushToken)

    assertNonEmptyArray(pushTokens, 'pushTokens')
    assertNonEmptyString(title, 'title')
    assertNonEmptyString(body, 'body')

    const response = await sendPushNotifications({
      pushTokens,
      title,
      body,
      ...rest,
    })

    return response
  } catch (e) {
    console.error(e)
    error(new Error('sendMulticastNotificationsLimitedTo100/error: ', e))
    error(e)
    return []
  }
}

exports.default = sendMulticastNotificationsLimitedTo100
