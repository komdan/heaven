const {
  pickBy,
  compact,
  uniq,
} = require('lodash')
const {
  log,
  error,
  assertNonEmptyArray,
  assertNonEmptyString,
  isNonEmptyString,
  postSlackNotification,
} = require('../../js')
const {
  createDocument,
  firebaseAdmin,
} = require('../firebase.firestore')

async function sendPushNotifications({
  pushTokens,
  title,
  body,
  sound = 'default',
  ...rest
}) {
  try {
    assertNonEmptyArray(pushTokens, 'pushTokens')
    assertNonEmptyString(title, 'title')
    assertNonEmptyString(body, 'body')
    const restStrings = pickBy(rest, isNonEmptyString)

    const responses = await Promise.all(uniq(compact(pushTokens)).map(async (pushToken) => {
      try {
        const firebaseResponse = await firebaseAdmin.messaging().send({
          token: pushToken,
          notification: {
            title,
            body,
          },
          data: {
            title,
            body,
            ...restStrings,
          },
          android: {
            notification: {
              sound,
            },
          },
          apns: {
            payload: {
              aps: {
                sound,
              },
            },
          },
        })
        return ({
          firebaseResponse,
          pushToken,
        })
      } catch (e) {
        await createDocument('expiredPushTokens', {
          pushToken,
          expiredAtTimestamp: Date.now(),
          expiredAt: new Date(),
          firebaseMessagingErrorCode: e.message,
        })
        console.log(pushToken)
        console.log(e.message)
        return null
      }
    }))

    console.log(`sendPushNotifications.js\\responses:${JSON.stringify(responses)}`)
    log(`sendPushNotifications.js\\responses:${JSON.stringify(responses)}`)

    return compact(responses)
  } catch (e) {
    // this global try catch is legacy. It should not be useful now that there is a try catch below
    // this is after the firebase.messaging.sendToDevice -> firebase.messaging.send migration 20.06.2024
    await postSlackNotification(`sendPushNotifications.js/e.message:"${e.message}"`)
    error(new Error('sendPushNotifications.js/ERROR FOLLOWS'))
    error(e)
    return []
  }
}

exports.default = sendPushNotifications
