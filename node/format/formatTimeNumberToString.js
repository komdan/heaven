const formatTimeNumberToString = time => (time < 10
  ? `0${time}`
  : `${time}`)

exports.default = formatTimeNumberToString
