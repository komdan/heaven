// require process.env.GOOGLE_APPLICATION_CREDENTIALS or equivalent for google to get the API key
const {
  deburr,
} = require('lodash')
const textToSpeech = require('@google-cloud/text-to-speech')
const uploadBufferToBucket = require('../firebase.storage/uploadBufferToBucket').default

const client = new textToSpeech.TextToSpeechClient()

const computeMp3BucketPath = (
  sanitizedName,
  languageCode = 'en-US',
  ssmlGender = '',
  baseFolder = 'textToSpeech',
  voiceName = '',
) => (
  `${baseFolder}/{${languageCode}}{${ssmlGender}}{${sanitizedName}}{${voiceName}}.mp3`
)

const textToMp3 = async ({
  text,
  languageCode = 'fr-FR',
  ssmlGender = 'MALE',
  name = 'fr-FR-Neural2-B',
}) => {
  const [{
    audioContent: mp3Binary,
  }] = await client.synthesizeSpeech({
    input: {
      // text,
      [text.includes('<speak>') ? 'ssml' : 'text']: text,
    },
    voice: {
      languageCode,
      ssmlGender,
      name,
    },
    audioConfig: {
      audioEncoding: 'MP3',
    },
  })
  const sanitizedTextExtract = deburr(text.replace(/[^a-zA-Z ]/g, '')).split(' ').slice(0, 10).join(' ')
  console.log(sanitizedTextExtract)
  console.log()
  console.log()
  console.log()
  console.log()

  const uploadedUrl = await uploadBufferToBucket(
    computeMp3BucketPath(sanitizedTextExtract, languageCode, ssmlGender, 'textToSpeech', name),
    mp3Binary,
  )

  return uploadedUrl
}

exports.default = textToMp3

// // USAGE
// // echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/node/googleAPI/textToMp3.js" | /bin/bash

// const test = async () => {
//   const res = await textToMp3({
//     text: `<speak>
//   <voice name="fr-FR-Neural2-A">
//     Salut les gars ! <prosody rate="fast" pitch="+2st">Ça gaze ou quoi ?</prosody> <break time="200ms"/>
//     Je suis Ludivine de Pennylane. <break strength="strong"/>
//     Vous connaissez <sub alias="comme d'hab">Komdab</sub> ?
//     <prosody rate="medium" pitch="-1st">C'est l'application qui va changer votre vie !</prosody><break time="1000ms"/>.
//     Cela se lit : <say-as interpret-as="characters">KOMDAB</say-as>.
//     <break time="1000ms"/><prosody rate="fast" pitch="+1st"><emphasis level="strong">J'aimerais commander 30 poké bol mais je veux savoir si vous avez comme d'hab.</emphasis></prosody>
//     <prosody rate="fast" pitch="-3st">Non merci ça m'intéresse pas</prosody><prosody rate="x-fast" pitch="+3st"> ciao bye bye</prosody>
//   </voice>
//   <voice name="fr-FR-Neural2-B">
//     Hello, <prosody rate="fast" pitch="-1st">bien ou bien la famille ?</prosody> <break time="200ms"/>
//     <prosody rate="fast" pitch="+3st">Le gérant est disponible ?</prosody><break strength="strong"/>
//     <prosody rate="x-fast" pitch="+2st">Il revient quand ?</prosody>
//     <prosody rate="fast" pitch="+1st">Oui voilà mon numéro : 06 12 34 56 78</prosody>.
//     <emphasis level="weak">Merci merci. </emphasis>
//     <prosody rate="x-fast" pitch="+2st">Avec plaisir.</prosody>
//     <prosody rate="fast" pitch="+1st">À bientôt.</prosody>
//     <break time="1000ms"/>
//   </voice>
//   <voice name="fr-FR-Polyglot-1">
//     Oui bonjour, c'est bien Atelier Mala ? <break time="200ms"/>
//     <prosody rate="medium" pitch="+2st">Je suis José GRACIA de Pennylane.<break strength="strong"/></prosody>
//     Vous avez <sub alias="comme d'hab">Komdab</sub> ?
//     <prosody rate="slow" pitch="+4st">Ah non non.</prosody> Comme d'hab <break time="1000ms"/>
//     <prosody rate="fast" pitch="+2st"><say-as interpret-as="characters">KOMDAB</say-as></prosody>.
//     Je vais le dire plus vite dans ce cas.
//     <prosody rate="x-fast" pitch="+2st">Comme d'hab <say-as interpret-as="characters">KOMDAB</say-as></prosody>.
//     <prosody rate="fast" pitch="+1st">J'aimerais commander 30 pizzas pour Vendredi prochain. Vous êtes ouverts ? <break strength="strong"/></prosody>
//     Ahhhh. <break time="800ms"/>
//     <prosody rate="fast" pitch="+1st">
//       <emphasis level="strong">Vous n'avez pas comme d'hab ?</emphasis>
//     </prosody>
//     Ca c'est vraiment la tuile.
//     Dans ce cas je vais racrocher car <prosody rate="fast" pitch="+1st">vous êtes de vrais fils de pute nègres</prosody>
//   </voice>
// </speak>
// `,
//     //     text: `<speak>
//     //   Bonjour, c'est bien Atelier Mala ?
//     //   Je suis José GRACIA de Pennylane. Vous avez <sub alias="comme d'hab">Komdab</sub> ?
//     //   Non non, comme d'hab <say-as interpret-as="characters">KOMDAB</say-as>
//     //   Voici des exemples de <say-as interpret-as="characters">SSML</say-as>.
//     //   Je peux faire une pause de <break time="3s"/>.
//     //   Je peux jouer un son.
//     //   Je peux parler en chiffres cardinaux. Votre numéro est le <say-as interpret-as="cardinal">10</say-as>.
//     //   Ou je peux parler en ordinaux. Vous êtes le <say-as interpret-as="ordinal">10</say-as> dans la file.
//     //   Ou je peux même parler en chiffres. Les chiffres pour dix sont <say-as interpret-as="characters">10</say-as>.
//     //   Je peux également substituer des expressions, comme le <sub alias="Consortium World Wide Web">W3C</sub>.
//     //   Enfin, je peux prononcer un paragraphe avec deux phrases.
//     //   <p><s>Ceci est la première phrase.</s><s>Ceci est la deuxième phrase.</s></p>
//     // </speak>
//     // `,
//   })
//   console.log(res)
//   console.log('DONE')
// }
// test()
