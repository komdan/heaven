const dotenv = require('dotenv')

const loadEnvironmentVariables = () => {
  const { NODE_ENV } = process.env

  const dotenvConfig = (NODE_ENV !== 'production')
    ? ({ path: './.env.development' })
    : ({ path: './.env.production' })

  dotenv.config(dotenvConfig)
}

exports.default = loadEnvironmentVariables
