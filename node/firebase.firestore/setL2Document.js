const firebaseAdmin = require('./firebaseAdmin').default

const setL2Document = (collection1, docId1, collection2, docId2, documentToCreate) => (
  docId2
    ? (
      firebaseAdmin.firestore()
        .collection(`${collection1}`)
        .doc(`${docId1}`)
        .collection(`${collection2}`)
        .doc(docId2)
        .set(documentToCreate || {}, { merge: true })
    )
    : (
      firebaseAdmin.firestore()
        .collection(`${collection1}`)
        .doc(`${docId1}`)
        .collection(`${collection2}`)
        .add(documentToCreate || {})
    )
)

exports.default = setL2Document
