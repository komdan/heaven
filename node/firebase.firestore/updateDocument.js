const firebaseAdmin = require('./firebaseAdmin').default

function updateDocument(collection, documentId, updatedObject) {
  return (
    firebaseAdmin.firestore()
      .collection(`${collection}`)
      .doc(`${documentId}`)
      .update(updatedObject)
  )
}

exports.default = updateDocument
