const firebaseAdmin = require('./firebaseAdmin').default

const getCollectionObjectIds = (collection) => {
  const ids = []

  // returns the firestore promise
  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .get()
      .then((snapshot) => {
        if (snapshot === null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            ids.push(doc.id)
          })
        return ids
      })
  )
}

exports.default = getCollectionObjectIds
