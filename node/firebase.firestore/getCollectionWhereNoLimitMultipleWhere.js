const {
  chunk,
  flatten,
} = require('lodash')
const getCollectionMultipleWhere = require('./getCollectionMultipleWhere').default

const FIRESTORE_IN_FILTER_LENGTH_LIMIT = 10 // imposed by firestore

const getCollectionWhereNoLimitMultipleWhere = async (collection, field, operator, value, conditions) => {
  const valueChunks = chunk(value, FIRESTORE_IN_FILTER_LENGTH_LIMIT)

  const collectionChunks = await Promise.all(valueChunks.map(async (valueChunk) => {
    const collectionChunk = await getCollectionMultipleWhere(
      collection,
      [
        {
          field,
          operator,
          value: valueChunk,
        },
        ...conditions,
      ],
    )
    return collectionChunk
  }))

  return flatten(collectionChunks)
}

exports.default = getCollectionWhereNoLimitMultipleWhere
