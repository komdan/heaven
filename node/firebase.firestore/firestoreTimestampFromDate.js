const firebaseAdmin = require('./firebaseAdmin').default

const firestoreTimestampFromDate = date => firebaseAdmin.firestore.Timestamp.fromDate(date)

exports.default = firestoreTimestampFromDate
