const firebaseAdmin = require('./firebaseAdmin').default

function deleteDocument(path, docId) {
  return (
    firebaseAdmin.firestore()
      .collection(`${path}`)
      .doc(`${docId}`)
      .delete()
  )
}

exports.default = deleteDocument
