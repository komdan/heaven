const {
  equal, // eslint-disable-line
} = require('assert')
const postSlackNotification = require('../../../heaven/js/logging/postSlackNotification').default
const getCollectionWhere = require('./getCollectionWhere').default

const getDocumentWhere = async (
  collection,
  field,
  operator,
  value,
) => {
  const documentsFound = await getCollectionWhere(
    collection,
    field,
    operator,
    value,
  )
  const nbDocs = documentsFound.length
  const multipleDocsErrorText = `(getDocumentWhere.js) Multiple documents (${nbDocs}) found with same '${field}' ('${value}') in '${collection}' : ${documentsFound.map(({ id }) => id)}`
  if (nbDocs > 1) {
    await postSlackNotification(multipleDocsErrorText)
  }

  // equal(nbDocs > 1, false, multipleDocsErrorText)

  return documentsFound[0]
}

exports.default = getDocumentWhere
