const firebaseAdmin = require('./firebaseAdmin').default

const firestoreServerTimestamp = () => firebaseAdmin.firestore.FieldValue.serverTimestamp()

exports.default = firestoreServerTimestamp
