const firebaseAdmin = require('./firebaseAdmin').default

function getCollection(collection) {
  // Will putt data here
  const data = []

  // return the firestore promise
  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
            })
          })
        return data
      })
  )
}

exports.default = getCollection
