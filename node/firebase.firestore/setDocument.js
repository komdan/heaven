const firebaseAdmin = require('./firebaseAdmin').default

function setDocument(path, docId, document, merge = true) {
  return (
    firebaseAdmin.firestore()
      .collection(`${path}`)
      .doc(`${docId}`)
      .set(document || {}, { merge })
  )
}

exports.default = setDocument
