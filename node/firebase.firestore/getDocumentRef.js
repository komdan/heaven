const firebaseAdmin = require('./firebaseAdmin').default

function getDocumentRef(collection, documentId) {
  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .doc(documentId)
  )
}

exports.default = getDocumentRef
