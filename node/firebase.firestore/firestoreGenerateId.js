const firebaseAdmin = require('./firebaseAdmin').default

const firestoreGenerateId = path => firebaseAdmin.firestore()
  .collection(path)
  .doc()
  .id

exports.default = firestoreGenerateId
