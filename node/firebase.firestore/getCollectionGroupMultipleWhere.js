const firebaseAdmin = require('./firebaseAdmin').default

function getCollectionGroupMultipleWhere(
  collection,
  conditions = [],
) {
  const data = []

  let query = firebaseAdmin.firestore().collectionGroup(collection)
  conditions.forEach(({ field, operator, value }) => {
    query = query.where(field, operator, value)
  })

  return (
    query
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
            })
          })
        return data
      })
  )
}

exports.default = getCollectionGroupMultipleWhere
