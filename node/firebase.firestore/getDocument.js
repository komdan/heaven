const firebaseAdmin = require('./firebaseAdmin').default

function getDocument(collection, documentId) {
  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .doc(documentId)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          throw new Error(`getDocument.js/document-does-not-exist: ${collection}/${documentId}`)
          // return {}
        }
        const obj = doc.data()
        return { ...obj, id: doc.id }
      })
  )
}

exports.default = getDocument
