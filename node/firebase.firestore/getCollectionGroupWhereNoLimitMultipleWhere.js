const {
  chunk,
  flatten,
} = require('lodash')
const getCollectionGroupMultipleWhere = require('./getCollectionGroupWhere').default

const FIRESTORE_IN_FILTER_LENGTH_LIMIT = 10 // imposed by firestore

const getCollectionGroupWhereNoLimitMultipleWhere = async (collection, field, operator, value, conditions) => {
  const valueChunks = chunk(value, FIRESTORE_IN_FILTER_LENGTH_LIMIT)

  const collectionGroupChunks = await Promise.all(valueChunks.map(async (valueChunk) => {
    const collectionGroupChunk = await getCollectionGroupMultipleWhere(collection, field, operator, valueChunk, conditions)
    return collectionGroupChunk
  }))

  return flatten(collectionGroupChunks)
}

exports.default = getCollectionGroupWhereNoLimitMultipleWhere
