const {
  isNumber,
} = require('lodash')
const firebaseAdmin = require('./firebaseAdmin').default

const getCollectionMultipleWhere = async (
  collection,
  conditions,
  {
    limit,
  } = {},
) => {
  // Will putt data here
  const data = []

  // return the firestore promise
  let query = firebaseAdmin.firestore().collection(collection)
  conditions.forEach(({ field, operator, value }) => {
    query = query.where(field, operator, value)
  })
  if (isNumber(limit)) {
    query = query.limit(limit)
  }

  return query
    .get()
    .then((snapshot) => {
      if (snapshot == null) {
        return []
      }
      snapshot
        .docs
        .forEach((doc) => {
          // Add the id, on each object, easier for referencing
          const obj = doc.data()
          data.push({
            ...obj,
            id: doc.id,
          })
        })
      return data
    })
}

exports.default = getCollectionMultipleWhere
