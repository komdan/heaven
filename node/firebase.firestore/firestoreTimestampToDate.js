const firestoreTimestampToDate = ({ _seconds, _nanoseconds }) => new Date(
  _seconds * 1000 + _nanoseconds / 1000000,
)

exports.default = firestoreTimestampToDate
