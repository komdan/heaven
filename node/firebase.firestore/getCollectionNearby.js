const {
  getDistance,
} = require('geolib')
const {
  neighbors,
  decode,
} = require('ngeohash')
const {
  flattenDeep,
  orderBy,
} = require('lodash')

const {
  computeAdequateGeohashLength,
} = require('../../js')
const getCollectionInGeohashBox = require('./getCollectionInGeohashBox').default

// Postulat : les éléments de la collection ont l'attribut "geohash"
const getCollectionNearby = async (collection, baseGeohash, maxDistanceInMeter) => {
  const baseGeohashTruncated = baseGeohash.substr(
    0,
    computeAdequateGeohashLength(maxDistanceInMeter),
  )

  const baseGeohashes = [
    baseGeohashTruncated,
    ...neighbors(baseGeohashTruncated),
  ]

  const documentsInBigBox = flattenDeep(await Promise.all([
    getCollectionInGeohashBox(collection, baseGeohashes[0]),
    getCollectionInGeohashBox(collection, baseGeohashes[1]),
    getCollectionInGeohashBox(collection, baseGeohashes[2]),
    getCollectionInGeohashBox(collection, baseGeohashes[3]),
    getCollectionInGeohashBox(collection, baseGeohashes[4]),
    getCollectionInGeohashBox(collection, baseGeohashes[5]),
    getCollectionInGeohashBox(collection, baseGeohashes[6]),
    getCollectionInGeohashBox(collection, baseGeohashes[7]),
    getCollectionInGeohashBox(collection, baseGeohashes[8]),
  ]))

  const documentsInDistanceCircle = documentsInBigBox.filter(({ geohash = '' }) => (
    getDistance(decode(geohash), decode(baseGeohash)) <= maxDistanceInMeter
  ))

  const documentsSortedByDistance = orderBy(
    documentsInDistanceCircle,
    ({ geohash = '' }) => getDistance(decode(geohash), decode(baseGeohash)),
  )

  // Adding a filter to retrieve only items in the distance circle
  return documentsSortedByDistance
}

exports.default = getCollectionNearby
