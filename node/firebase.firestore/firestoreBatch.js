const firebaseAdmin = require('./firebaseAdmin').default

const firestoreBatch = () => firebaseAdmin.firestore().batch()

exports.default = firestoreBatch
