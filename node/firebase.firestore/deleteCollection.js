// Inspired from https://firebase.google.com/docs/firestore/manage-data/delete-data

const firebaseAdmin = require('./firebaseAdmin').default

const db = firebaseAdmin.firestore()

async function deleteQueryBatch(query, resolve) {
  const snapshot = await query.get()

  if (snapshot.size === 0) {
    // When there are no documents left, we are done
    resolve()
    return
  }

  // Delete documents in a batch
  const batch = db.batch()
  snapshot.docs.forEach((doc) => {
    batch.delete(doc.ref)
  })
  await batch.commit()

  // Recurse on the next process tick, to avoid
  // exploding the stack.
  process.nextTick(() => {
    deleteQueryBatch(query, resolve)
  })
}

async function deleteCollection(collectionPath, batchSize = 100) {
  const query = db
    .collection(collectionPath)
    .orderBy('__name__')
    .limit(batchSize)

  return new Promise((resolve, reject) => {
    deleteQueryBatch(query, resolve).catch(reject)
  })
}

exports.default = deleteCollection
