const firebaseAdmin = require('./firebaseAdmin').default

function getCollectionWhereAndWhere(
  collection,
  field1,
  operator1,
  value1,
  field2,
  operator2,
  value2,
  orderBy = '',
  orderType = 'desc',
  limitNumber = -1,
) {
  // Will putt data here
  const data = []

  let ref = firebaseAdmin.firestore()
    .collection(collection)
    .where(field1, operator1, value1)
    .where(field2, operator2, value2)

  if (orderBy !== '') {
    ref = ref
      .orderBy(orderBy, orderType)
  }

  if (limitNumber > -1) {
    ref = ref
      .limit(limitNumber)
  }

  // return the firestore promise
  return (
    ref
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
            })
          })
        return data
      })
  )
}

exports.default = getCollectionWhereAndWhere
