const {
  incrementAlphabetical,
} = require('../../js')
const firebaseAdmin = require('./firebaseAdmin').default

const getCollectionInGeohashBox = (collection, geohash) => {
  const data = []

  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .where('geohash', '>=', geohash)
      .where('geohash', '<', incrementAlphabetical(geohash))
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
            })
          })
        return data
      })
  )
}

exports.default = getCollectionInGeohashBox
