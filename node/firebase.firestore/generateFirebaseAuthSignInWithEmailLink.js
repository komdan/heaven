const firebaseAdmin = require('./firebaseAdmin').default

const {
  KOMDAB_MANAGER_V2_URL,
} = process.env

const generateFirebaseAuthSignInWithEmailLink = ({
  email = '',
  actionCodeSettings = {
    // URL you want to redirect back to. The domain (www.example.com) for this
    // URL must be in the authorized domains list in the Firebase Console.
    url: `${KOMDAB_MANAGER_V2_URL}/fr/accounts/login/link`,
    // This must be true.
    handleCodeInApp: true,
    // iOS: {
    //   bundleId: 'com.example.ios'
    // },
    // android: {
    //   packageName: 'com.example.android',
    //   installApp: true,
    //   minimumVersion: '12'
    // },
    // dynamicLinkDomain: 'localhost'
  },
}) => (
  firebaseAdmin.auth().generateSignInWithEmailLink(
    `${email}`.toLowerCase().trim(),
    actionCodeSettings,
  )
);

exports.default = generateFirebaseAuthSignInWithEmailLink
