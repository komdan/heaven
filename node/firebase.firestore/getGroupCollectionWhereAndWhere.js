const firebaseAdmin = require('./firebaseAdmin').default

function getGroupCollectionWhereAndWhere(
  collection,
  field1,
  operator1,
  value1,
  field2,
  operator2,
  value2,
) {
  // Will putt data here
  const data = []

  // return the firestore promise
  return (
    firebaseAdmin.firestore()
      .collectionGroup(collection)
      .where(field1, operator1, value1)
      .where(field2, operator2, value2)
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
              _path: doc.ref._path.segments.slice(0, -1).reduce((acc, path) => `${acc}/${path}`, ''), // eslint-disable-line
            })
          })
        return data
      })
  )
}

exports.default = getGroupCollectionWhereAndWhere
