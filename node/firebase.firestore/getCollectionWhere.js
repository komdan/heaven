const firebaseAdmin = require('./firebaseAdmin').default

const getCollectionWhere = async (collection, field, operator, value) => {
  // Will putt data here
  const data = []

  // return the firestore promise
  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .where(field, operator, value)
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
            })
          })
        return data
      })
  )
}

exports.default = getCollectionWhere
