const {
  equal,
} = require('assert')
const postSlackNotification = require('../../../heaven/js/logging/postSlackNotification').default
const getCollectionWhereAndWhere = require('./getCollectionWhereAndWhere').default

const getDocumentWhereAndWhere = async (
  collection,
  field1,
  operator1,
  value1,
  field2,
  operator2,
  value2,
  orderBy = '',
  orderType = 'desc',
  limitNumber = -1,
) => {
  const documentsFound = await getCollectionWhereAndWhere(
    collection,
    field1,
    operator1,
    value1,
    field2,
    operator2,
    value2,
    orderBy,
    orderType,
    limitNumber,
  )
  const nbDocs = documentsFound.length
  const multipleDocsErrorText = `(getDocumentWhereAndWhere.js) Multiple documents (${nbDocs}) found with same '${field1}' ('${value1}') and '${field2}' ('${value2}') in '${collection}' : ${documentsFound.map(({ id }) => id)}`
  if (nbDocs > 1) {
    await postSlackNotification(multipleDocsErrorText)
  }

  equal(nbDocs > 1, false, multipleDocsErrorText)

  return documentsFound[0]
}

exports.default = getDocumentWhereAndWhere
