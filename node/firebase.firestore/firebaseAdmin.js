const firebaseAdmin = require('firebase-admin')

const {
  FIRESTORE_EMULATOR_HOST,
  NODE_ENV,
} = process.env
// console.log('Initializing firebase admin with : ', process.env.FIREBASE_DATABASE_URL, process.env.FIREBASE_STORAGE_BUCKET_NAME)

// const serviceAccount = require('../../../ubar-firebase-dev.service-account-file.json')

firebaseAdmin.initializeApp({
  credential: firebaseAdmin.credential.applicationDefault(), // will fetch info in the path provided by process.env.GOOGLE_APPLICATION_CREDENTIALS
  // credential: firebaseAdmin.credential.cert(serviceAccount),
  databaseURL: process.env.FIREBASE_DATABASE_URL,
  storageBucket: process.env.FIREBASE_STORAGE_BUCKET_NAME,
})

if (NODE_ENV === 'development' && FIRESTORE_EMULATOR_HOST) {
  // Point Firestore to the local emulator
  firebaseAdmin.firestore().settings({
    host: FIRESTORE_EMULATOR_HOST,
    ssl: false,
    timestampsInSnapshots: true,
  })
} else {
  firebaseAdmin.firestore().settings({
    timestampsInSnapshots: true,
  })
}

exports.default = firebaseAdmin
