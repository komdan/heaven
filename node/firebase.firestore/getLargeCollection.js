/* eslint-disable */
const firebaseAdmin = require('./firebaseAdmin').default

const limit = 500
let docs = []

const getLargeCollection = async (collectionName) => {
  const results = []

  const collectionRef = firebaseAdmin.firestore().collection(collectionName)
  let query = collectionRef.limit(limit)

  do {
    // console.log('Fetching results')
    const querySnapshot = await query.get() // eslint-disable-line
    docs = querySnapshot.docs // thanks to @prahack answer // eslint-disable-line
    for (const doc of docs) { // eslint-disable-line
      await results.push({
        id: doc.id,
        ...doc.data(),
      })
    }
    if (docs.length > 0) {
      // Get the last visible document
      query = collectionRef.startAfter(docs[docs.length - 1]).limit(limit)
    }
  } while (docs.length > 0)

  return results
}

exports.default = getLargeCollection
