const firebaseAdmin = require('./firebaseAdmin').default

function createDocument(collection, doc) {
  return (
    firebaseAdmin.firestore()
      .collection(collection)
      .doc()
      .set(doc || {})
  )
}

exports.default = createDocument
