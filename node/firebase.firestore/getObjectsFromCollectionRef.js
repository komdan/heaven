function getObjectsFromCollectionRef(collectionRef) {
  const results = []
  return new Promise((resolve, reject) => {
    collectionRef
      .get()
      .then((docsRef) => {
        docsRef
          .docs
          .forEach((doc) => {
            if (doc.exists) {
              const data = doc.data()
              const { id } = doc
              results.push({ id, ...data })
            }
          })
        resolve(results)
      })
      .catch(e => reject(e))
  })
}

exports.default = getObjectsFromCollectionRef
