const {
  chunk,
  flatten,
} = require('lodash')
const getCollectionGroupWhere = require('./getCollectionGroupWhere').default

const FIRESTORE_IN_FILTER_LENGTH_LIMIT = 10 // imposed by firestore

const getCollectionGroupWhereNoLimit = async (collection, field, operator, value) => {
  const valueChunks = chunk(value, FIRESTORE_IN_FILTER_LENGTH_LIMIT)

  const collectionGroupChunks = await Promise.all(valueChunks.map(async (valueChunk) => {
    const collectionGroupChunk = await getCollectionGroupWhere(collection, field, operator, valueChunk)
    return collectionGroupChunk
  }))

  return flatten(collectionGroupChunks)
}

exports.default = getCollectionGroupWhereNoLimit
