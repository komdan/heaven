const getCollection = require('../firebase.firestore/getCollection').default

const getBarFromString = async (string) => {
  const [bar] = (await getCollection('bars'))
    .filter(({ name }) => name.includes(string))

  return bar
}

exports.default = getBarFromString
