/* eslint camelcase: 0 */
const setDocument = require('../firebase.firestore/setDocument').default
const getBarFromString = require('./getBarFromString').default

const {
  WEBAPP_URL,
  ENJOY_URL,
  GO_URL,
} = process.env

const generateShortLink = async ({
  barId: barIdInArgs = '',
  qid = '',
  table = '',
  barNameIncludes = '',
  enterpriseCode = '',
  shouldSignUpForCompetitionGames = '',
  email = '',
  password = '',
  promo = '',
  shortPostfix,
  utm_source,
  utm_medium,
  utm_campaign,
  utm_term,
  utm_content,
  type,
  redirectToUrl = '',
  shouldGoFirstOnWebApp = false,
  shouldSendSlackNotif = false,
  useGoUrl = false,
  temporary = false,
  appStorePackageId = 'com.komdan.ubar',
  appStoreId = '1474295770',
  shouldGoToWhitelabelWebsite = false,
  shouldGoOnlyOnWebApp = false,
  websiteWhitelabelByKomdabUrl = '',
  slotLocalId = '',
  enterpriseOfferId = '',
} = {}) => {
  let barId = ''
  if (barIdInArgs) {
    barId = barIdInArgs
  } else if (barNameIncludes) {
    const {
      id,
    } = await getBarFromString(barNameIncludes)
    barId = id
  }

  let getParams = ''
  if (enterpriseCode) getParams += `&enterpriseCode=${enterpriseCode}`
  if (barId) getParams += `&barId=${barId}`
  if (promo) getParams += `&promo=${promo}`
  if (table) getParams += `&table=${table}`
  if (qid) getParams += `&qid=${qid}`
  if (shouldSignUpForCompetitionGames) getParams += `&shouldSignUpForCompetitionGames=${shouldSignUpForCompetitionGames}`
  if (email) getParams += `&email=${email}`
  if (password) getParams += `&password=${password}`
  if (slotLocalId) getParams += `&slotLocalId=${slotLocalId}`
  if (enterpriseOfferId) getParams += `&enterpriseOfferId=${enterpriseOfferId}`

  const unencodedLink = `${shouldGoToWhitelabelWebsite ? websiteWhitelabelByKomdabUrl : WEBAPP_URL}/?${getParams.substr(1)}`
  const link = encodeURIComponent(unencodedLink)
  const defaultRedirectToUrl = shouldGoOnlyOnWebApp
    ? unencodedLink
    : (
      shouldGoFirstOnWebApp
        ? `https://ubar.page.link/?link=${link}&ifl=${link}&afl=${link}&apn=${appStorePackageId}&isi=${appStoreId}&ibi=${appStorePackageId}`
        : `https://ubar.page.link/?link=${link}&apn=${appStorePackageId}&isi=${appStoreId}&ibi=${appStorePackageId}`
    )
  console.log(defaultRedirectToUrl)

  await setDocument('shortLinksData', shortPostfix, {
    redirectToUrl: redirectToUrl === '' ? defaultRedirectToUrl : redirectToUrl,
    type,
    params: {
      enterpriseCode,
      utm_source,
      utm_medium,
      utm_campaign,
      utm_term,
      utm_content,
      shouldSendSlackNotif,
      type,
    },
    utm_source,
    temporary,
    utm_campaign,
    utm_medium,
    utm_content,
    shouldSendSlackNotif,
    utm_term,
    enterpriseCode, // in double (here and in params) to allow db queries
    lastUpdateTimestamp: Date.now(),
    lastUpdate: new Date(),
  })

  const domainToUse = useGoUrl ? GO_URL : ENJOY_URL

  return `${domainToUse}/${shortPostfix}`
}

exports.default = generateShortLink

// // USAGE:
// // echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/node/komdab/generateShortLink.js" | /bin/bash
// // echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/node/komdab/generateShortLink.js" | /bin/bash

// const main = async () => {
//   console.log('THE SCRIPT generateShortLink.js is NOT OK, comment me')
//   const url = await generateShortLink({
//     barId: 'OZzloA4WDLSv8fNZ9bmw',
//     qid: '',
//     table: '',
//     barNameIncludes: '',
//     enterpriseCode: 'addactis',
//     shouldSignUpForCompetitionGames: '',
//     promo: '',
//     utm_source: '',
//     utm_medium: '',
//     utm_campaign: '',
//     utm_term: '',
//     utm_content: '',
//     type: '',
//     // redirectToUrl: 'https://tribunali.bykomdab.com/?order=true',
//     shouldGoFirstOnWebApp: false,
//     shouldSendSlackNotif: false,
//     useGoUrl: true,
//     shortPostfix: 'addactiskibonoki117',
//     enterpriseOfferId: 'rCMDKFHK0gGcLajMbQLKOk',
//     slotLocalId: '4f0fe0cc-5d7d-4b0b-ab3e-d89f7bae1866',
//     // email: 'sunmitest+manager@yopmail.com',
//     // password: 'sunmitest@yopmail.com',
//     // temporary: true,
//     // appStorePackageId: 'com.komdan.komdabprosunmi',
//     // appStoreId: '1623156686',
//   })
//   console.log(url)
// }
// main()
