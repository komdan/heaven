import {
  useState,
  useEffect,
} from 'react'

const useDelayOver = (delay) => {
  const [isDelayOver, setIsDelayOver] = useState(!delay)

  useEffect(() => {
    const timeout = setTimeout(
      () => setIsDelayOver(true),
      delay,
    )
    return () => clearTimeout(timeout)
  }, []) // eslint-disable-line

  return ({
    isDelayOver,
  })
}

export default useDelayOver
