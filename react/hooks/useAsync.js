import {
  useState,
  useEffect,
} from 'react'
import {
  error as logError,
} from '../../js-web'

const useAsync = (
  asyncFunction = () => { },
  {
    immediate = true,
    resetOnExecution = false,
  } = {},
) => {
  const [isLoaded, setIsLoaded] = useState(false)
  const [pending, setPending] = useState(false)
  const [result, setResult] = useState(undefined)
  const [error, setError] = useState(undefined)

  const execute = async (...args) => {
    setPending(true)
    if (resetOnExecution) {
      setResult(undefined)
      setError(undefined)
      setIsLoaded(false)
    }
    try {
      const response = await asyncFunction(...args)
      setResult(response)
      setPending(false)
      setIsLoaded(true)
    } catch (e) {
      setError(e)
    }
  }

  useEffect(() => {
    if (error) logError(error)
  }, [error])

  useEffect(() => {
    if (immediate) {
      execute()
    }
  }, [immediate])

  return ([
    result,
    isLoaded,
    execute,
    error,
    pending,
  ])
}

export default useAsync
