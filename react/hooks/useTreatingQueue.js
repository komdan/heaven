import {
  useEffect,
  useState,
} from 'react'
import {
  find as lodashFind,
  reject,
  uniq,
  uniqBy,
} from 'lodash'
import {
  log,
  error,
} from '../../js-web'

const useTreatingQueue = ({
  treatOne = async () => { },
  find = lodashFind,
  getUniqueId = JSON.stringify,
  // debugEnableLogs = false,
  shouldBeTreating = true,
}) => {
  const [queue, setqueue] = useState([])
  const [alreadyTreatedIds, setalreadyTreatedIds] = useState([])
  const [alreadyPushedIds, setalreadyPushedIds] = useState([])
  const [isTreatingQueueBusy, setisTreatingQueueBusy] = useState(false)
  const [lastPushAttemptTimestamp, setlastPushAttemptTimestamp] = useState(Date.now())

  const reset = () => {
    setqueue([])
    setalreadyTreatedIds([])
    setalreadyPushedIds([])
    setisTreatingQueueBusy(false)
    setlastPushAttemptTimestamp(Date.now())
  }

  const push = (object) => {
    setlastPushAttemptTimestamp(Date.now())
    const uid = getUniqueId(object)
    if (!find(queue, object) && !alreadyPushedIds.includes(uid)) {
      log(`⤵ Pushing ${uid}`)
      console.log(`⤵ Pushing ${uid}`)

      setalreadyPushedIds(oldalreadyPushedIds => uniq([
        ...oldalreadyPushedIds,
        uid,
      ]))
      setqueue(oldQueue => uniqBy([
        ...oldQueue,
        object,
      ], getUniqueId))
    }
  }

  useEffect(() => {
    let timeout
    const asyncMethod = async () => {
      if (queue.length && !isTreatingQueueBusy && shouldBeTreating) {
        setisTreatingQueueBusy(true)
        const [object] = queue
        const uid = getUniqueId(object)

        if (!alreadyTreatedIds.includes(uid)) {
          log(`⤵ Treating ${uid}`)
          console.log(`⤵ Treating ${uid}`)

          try {
            await treatOne(object)
            setalreadyTreatedIds(oldalreadyTreatedIds => uniq([
              ...oldalreadyTreatedIds,
              uid,
            ]))
            setqueue(oldQueue => reject(oldQueue, object))
            setisTreatingQueueBusy(false)
          } catch (e) {
            error(new Error('(useTreatingQueue.js) - error in treatOne FOLLOWS'))
            error(e)
            timeout = setTimeout( // this timeout reduce loops when printer is in error
              () => setisTreatingQueueBusy(false),
              10 * 1000,
            )
          }
        } else {
          setqueue(oldQueue => reject(oldQueue, object))
          setisTreatingQueueBusy(false)
        }
      }
    }
    asyncMethod()
    return () => clearTimeout(timeout)
  }, [JSON.stringify(queue), isTreatingQueueBusy, shouldBeTreating])

  useEffect(() => {
    console.log()
    console.log('📲 useTreatingQueue.js queue', queue.map(getUniqueId))
    console.log('📲 useTreatingQueue.js alreadyTreatedIds', alreadyTreatedIds)
    console.log('📲 useTreatingQueue.js alreadyPushedIds', alreadyPushedIds)
    console.log('📲 useTreatingQueue.js isTreatingQueueBusy', isTreatingQueueBusy)
    console.log('📲 useTreatingQueue.js lastPushAttemptTimestamp', lastPushAttemptTimestamp)
    console.log('📲 useTreatingQueue.js shouldBeTreating', shouldBeTreating)
    console.log()
    const timeout = setTimeout(() => {
      if (queue.length === 0) {
        setalreadyTreatedIds([])
        setalreadyPushedIds([])
        setisTreatingQueueBusy(false)
      }
    }, 30 * 1000) // 30 secs after nothing happens
    return () => clearTimeout(timeout)
  }, [alreadyTreatedIds, alreadyPushedIds, isTreatingQueueBusy, lastPushAttemptTimestamp, shouldBeTreating])

  return ({
    push,
    reset,
  })
}

export default useTreatingQueue
