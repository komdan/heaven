import {
  useEffect,
  useState,
} from 'react'
import {
  max,
  min,
  ceil,
} from 'lodash'
import {
  networkStatusListener,
} from '../../js-web'

const useNetworkStatus = ({
  optimalLatency = 400, // very optional. used to calculate network score
  worstLatency = 1000, // very optional. used to calculate network score
} = {}) => {
  const [isNetworkUp, setisNetworkUp] = useState(false)
  const [latency, setlatency] = useState(0)

  useEffect(() => {
    const listener = networkStatusListener(({ status, latency: ltcy }) => {
      if (status === 'UP') {
        setisNetworkUp(true)
        setlatency(ltcy)
      } else {
        setisNetworkUp(false)
        setlatency(0)
      }
    })

    return () => listener()
  }, [])

  const networkScore = isNetworkUp
    ? ceil(
      max([
        min([
          (worstLatency - latency) / (worstLatency - optimalLatency),
          1,
        ]),
        0,
      ]) * 100,
    )
    : 0

  return [
    isNetworkUp,
    latency,
    networkScore,
  ]
}

export default useNetworkStatus
