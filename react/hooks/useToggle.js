import { useState } from 'react'
import { useInterval } from 'react-use'

const useToggle = (defaultValue, {
  autoToggleInterval = 0,
} = {}) => {
  const [toggle, setToggle] = useState(!!defaultValue)
  const switchToggle = () => setToggle(!toggle)

  useInterval(
    () => {
      switchToggle()
    },
    autoToggleInterval || null,
  )

  return ([
    toggle,
    switchToggle,
  ])
}

export default useToggle
