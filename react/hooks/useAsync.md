🔄 heaven / react / hooks / useAsync
==============================
⌛ Hook handling asynchronous  ⌛

Definition
-----
useAsync takes two arguments:
  - **asyncFunction**
    - an async method returning the value that will be used as state(s)
  - options
    - **immediate**: trigger the async function and the state initialisation on first call
    - **resetOnExecution**: useful when you want to fail hard -> state will be set back to its default value
```js
const useAsync = (
  asyncFunction,
  {
    immediate = true,
    resetOnExecution = false,
  } = {},
) => {
  //...
}
```

Returns
-----
useAsync returns an array:
* result
* isLoading (!pending)
* execute
* error
* pending

```js
  return ([
    result,
    !pending,
    execute,
    error,
    pending,
  ])
```

Usage
-----
Multiple example in Kombab
```js
const useBarPage = (props) => {
  const [
    localities = [],
    isLocalitiesLoaded,
  ] = useAsync(() => getLocalities(localityIds))
  const [
    {
      result: {
        rating: googleRating,
        user_ratings_total: googleRatingsTotal,
      },
    } = { result: {} },
  ] = useAsync(() => fetchGoogleRating(googlePlaceId))

  const [
    prebuildStates,,
    prebuildOrderSync,, // <- erreur ici, on ne doit pas utiliser "Sync". La fonction est bien une promesse et on peut faire un await dessus. ça à l'air juste "sync" car les states sont gérés automatiquement
    prebuildingOrderScreen,
  ] = useAsync(prebuildOrder, { immediate: false })
  const openOrderScreen = () => prebuildOrderSync(props)

  return ({
    localities,
    isLocalitiesLoaded,
    googleRating,
    googleRatingsTotal,
    prebuildStates,
    prebuildOrderSync,
    prebuildingOrderScreen,
    openOrderScreen,
  })
```

Used in Squeeze
```js
const FileBrowser = () => {
  const {
    handleFilePress,
    currentLootDirectory,
  } = useContext(SqueezeAppContext)
  const [
    {
      directoryItems,
      pathItems,
    } = { directoryItems: [], path: '' },,
    lsDirectory,,
    pending,
  ] = useAsync(path => lsDirectoryWeb(path || currentLootDirectory))

  return (
    <React.Fragment>
      <Loader active={pending} />
      <Button onClick={() => lsDirectory('/')}>/</Button>
      <Button onClick={() => lsDirectory('.')}>cwd</Button>
      <Button onClick={() => lsDirectory('./public')}>$public</Button>
      <Browser>
        <Browser.NavBar
          items={pathItems}
          onFolderClick={({ absolutePath }) => lsDirectory(absolutePath)}
        />
        {directoryItems.map((item, index) => (
          <Browser.Item
            key={JSON.stringify({ item, index })}
            onFolderClick={(itm) => {
              handleFilePress(itm)
              lsDirectory(itm.absolutePath)
            }}
            onFileClick={handleFilePress}
            item={item}
          />
        ))}
      </Browser>
    </React.Fragment>
  )
}
```