/* ====== USAGE
Use this hook when you want to refresh a component using the 'key' props
Example (below, a delay of 400ms allows to minimise re-renders on multiple errors)

const {
  componentKey: videoKey,
  triggerKeyRefresh,
} = useRefreshingKey(videoSourceUrl, 400)

<Video
  key={videoKey}
  onError={triggerKeyRefresh}
/>
*/

import {
  useState,
  useEffect,
} from 'react'
import {
  key,
} from '../../js-web'
import useToggle from './useToggle'

const useRefreshingKey = (
  keyValueOrObject,
  delay = 0,
) => {
  const [errorToggle, switchErrorToggle] = useToggle()
  const [componentKey, setComponentKey] = useState(key(keyValueOrObject, errorToggle))

  useEffect(() => {
    const timeout = setTimeout(
      () => setComponentKey(key(keyValueOrObject, errorToggle)),
      delay,
    )
    return () => clearTimeout(timeout)
  }, [errorToggle]) // eslint-disable-line

  const triggerKeyRefresh = () => switchErrorToggle()

  return ({
    componentKey,
    triggerKeyRefresh,
  })
}

export default useRefreshingKey
