import {
  useState,
} from 'react'

const useFullPageModal = ({
  autoDisplay = false,
} = {}) => {
  const [isModalVisible, setIsModalVisible] = useState(autoDisplay)
  const onShowModalPress = () => setIsModalVisible(true)
  const onCancelPress = () => setIsModalVisible(false)
  const onClosePress = () => setIsModalVisible(false)

  return ({
    isModalVisible,
    onCancelPress,
    onClosePress,
    onShowModalPress,
    setIsModalVisible,
  })
}

export default useFullPageModal
