const replaceAllSlashes = string => string.replace(/\//g, '')

exports.default = replaceAllSlashes
