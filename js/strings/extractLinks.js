const extractLinks = (str) => {
  try {
    const regex = /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()'@:%_\+.~#?!&//=]*)/gi
    const links = str.match(regex) || []
    return links
  } catch (error) {
    console.error('Error extracting links:', error)
    throw error
  }
}

exports.default = extractLinks
