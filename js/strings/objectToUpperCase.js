const updateAllObjectValues = require('./updateAllObjectValues').default

const objectToUpperCase = obj => updateAllObjectValues(
  obj,
  value => value.toUpperCase(),
)

exports.default = objectToUpperCase
