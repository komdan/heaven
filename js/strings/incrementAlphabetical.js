const incrementCharacter = require('./incrementCharacter').default

const incrementAlphabetical = string => string.replace(/.$/, incrementCharacter)

exports.default = incrementAlphabetical
