const generateRandomPassword = () => Math.random().toString(36).substr(2, 8)

exports.default = generateRandomPassword
