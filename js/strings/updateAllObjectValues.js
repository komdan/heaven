const {
  fromPairs,
  toPairs,
} = require('lodash')

const updateAllObjectValues = (obj, method) => fromPairs(
  toPairs(obj).map(
    ([key, value]) => [key, method(value)],
  ),
)

exports.default = updateAllObjectValues
