const parseGETParams = require('./parseGETParams').default

const parseDeepLink = (longDeepLink) => {
  const { link } = parseGETParams(longDeepLink)
  return parseGETParams(link)
}

exports.default = parseDeepLink
