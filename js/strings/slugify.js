const {
  kebabCase,
} = require('lodash')
const emojiStrip = require('./emojiStrip').default

const slugify = text => {
  const preSlug = emojiStrip(kebabCase(text).replace(/&/g, '-and-').replace(/\//g, '-'))
  return preSlug[0] === '-'
    ? preSlug.slice(1)
    : preSlug
}

exports.default = slugify
