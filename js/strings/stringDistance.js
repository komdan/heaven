const levenshtein = require('fast-levenshtein')
const normalize = require('./normalize').default

const stringDistance = (
  string1 = '',
  string2 = '',
) => {
  const s1 = normalize(
    `${string1}`
      .replace(/ /g, ''),
  ).replace(/[^a-zA-Z0-9\s]/g, '')
  const s2 = normalize(
    `${string2}`
      .replace(/ /g, ''),
  ).replace(/[^a-zA-Z0-9\s]/g, '')

  // console.log(JSON.stringify(s1))
  // console.log(JSON.stringify(s2))

  return levenshtein.get(s1, s2)
}

exports.default = stringDistance

// // TEST
// // echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/strings/stringDistance.js" | /bin/bash
// const test = () => {

//   console.log(
//     stringDistance('Quattro mori 🚨', 'Quattro Mori'),
//   )
// }
// test()
