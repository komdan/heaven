const extractStringsMatchingRegex = ({ regex, text }) => {
  const matches = regex.exec(text)
  try {
    return matches[0]
  } catch (e) {
    return ''
  }
}

exports.default = extractStringsMatchingRegex
