const quotedPrintable = require('quoted-printable')
const utf8 = require('utf8')

const decodeQuotedPrintable = string => utf8.decode(quotedPrintable.decode(string))

exports.default = decodeQuotedPrintable

// npx babel-node heaven/js/strings/decodeQuotedPrintable.js" | /bin/bash
// const main = async () => {
//   try {
//     const body = "s=s1; bh=FEaENI5Vwa/QvRsFn/rk36Q8ecrcVL00Lcvr5hrhJqs=;\r"
//     const decodedBody = decodeQuotedPrintable(body)
//     console.log(decodedBody)
//   } catch (e) {
//     console.error(e)
//   }
// }
// main()
