const {
  capitalize,
  toUpper,
} = require('lodash')

const capitalizeIfUpperCase = s => {
  if (!s || !s.length || s !== toUpper(s)) {
    return s
  }
  return capitalize(s)
}

exports.default = capitalizeIfUpperCase
