const nbSpecialCharacters = string => string.replace(/[a-zA-Z0-9 ]/g, '').length

exports.default = nbSpecialCharacters
