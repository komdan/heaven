const emojify = require('./emojify').default
const updateAllObjectValues = require('./updateAllObjectValues').default

const emojifyObject = obj => updateAllObjectValues(
  obj,
  value => emojify(value),
)

exports.default = emojifyObject
