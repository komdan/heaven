const cleanHtmlString = (htmlString = '') => {
  // Remove HTML tags
  let cleanedString = htmlString.replace(/<\/?[^>]+(>|$)/g, "")

  // Manually replace common HTML entities
  const entities = {
    '&nbsp;': ' ',
    '&amp;': '&',
    '&quot;': '"',
    '&lt;': '<',
    '&gt;': '>',
    // Add more entities here as needed
  }

  // Replace entities in the string
  cleanedString = cleanedString.replace(/&[^;]+;/g, match => entities[match] || match)

  return cleanedString
}

exports.default = cleanHtmlString
