const zwidge = '‍' // not an empty string but a zero width emoji, copied here https://emojipedia.org/zero-width-joiner/

const emojify = string => `${zwidge}${string}`

exports.default = emojify
