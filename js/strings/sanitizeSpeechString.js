const {
  words,
  trim,
} = require('lodash')

const sanitizeSpeechString = exerciseName => words(trim(exerciseName.toLowerCase())).join(' ')

exports.default = sanitizeSpeechString
