const hardNormalize = str => str.normalize('NFD').replace(/ /g, '').replace(/[^a-zA-Z0-9]/g, '').toLowerCase()

exports.default = hardNormalize
