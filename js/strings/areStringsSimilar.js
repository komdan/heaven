const stringDistance = require('./stringDistance').default

const areStringsSimilar = (string1, string2, stringMaxDistance = 1) => {
  return stringDistance(string1, string2) <= stringMaxDistance
}

exports.default = areStringsSimilar
