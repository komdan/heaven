const safeTrim = string => (
  (string && string.trim)
    ? string.trim()
    : ''
)

exports.default = safeTrim
