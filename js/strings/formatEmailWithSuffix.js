const formatEmailWithSuffix = ({ email, suffix }) => {
  const emailSplitted = email.split('@', 2)
  return `${emailSplitted[0]}+${suffix}@${emailSplitted[1]}`
}

exports.default = formatEmailWithSuffix
