/* eslint-disable */
// MML : autre possibilité : .replace(/[^\p{L}\p{N}\p{P}\p{Z}]/gu, '') trouvée ici https://stackoverflow.com/questions/10992921/how-to-remove-emoji-code-using-javascript

const emojiRegexRGI = require('emoji-regex/RGI_Emoji.js')

const emojiStrip = (string) => {
  const emojiRegexRGIRegex = emojiRegexRGI()
  return string.replace(emojiRegexRGIRegex, '')
}

exports.default = emojiStrip
