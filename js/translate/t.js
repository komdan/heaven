const {
  post,
} = require('axios')
const LANGUAGE = require('./LANGUAGE').default
const translations = require('./translations').default

const {
  BACKEND_URL,
  BACKEND_SECRET_KEY,
} = process.env

// console.log('LANGUAGE.locale in t.js:', LANGUAGE.locale)

const replaceBracketsContent = (string, params) => {
  let res = string
  params.forEach((value, index) => {
    const re = new RegExp(`\\{${index}\\}`, 'g') // "{1}", "{2}", etc.
    res = res.replace(re, value)
  })
  return res
}

const t = (
  string = '',
  locale = LANGUAGE.locale,
  ...params // HAS TO BE USED
) => {
  if (string === '') return ''

  try {
    if (!(string in (translations[locale] || {}))) {
      post(`${BACKEND_URL}/translationsAddNewString`, {
        inputString: string,
        outputLocale: locale,
        secretKey: BACKEND_SECRET_KEY,
      })
      return replaceBracketsContent(string, params)
    }
    return replaceBracketsContent(translations[locale][string], params)
  } catch (e) {
    return string
  }
}

exports.default = t

// USAGE:
// echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/js/translate/t.js" | /bin/bash
// echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/translate/t.js" | /bin/bash

// // t("Omg what happened 😱 ?", 'en')
// console.log(t("Bravo 🙌 Vous avez gagné AAA !", 'fr'))
// // Bravo 🙌 Vous avez gagné {0} !
// console.log(t("Bravo 🙌 Vous avez gagné AAA !", 'fr', 'blabla'))
// // Bravo 🙌 Vous avez gagné blabla !

// // OTHER TEST
// // string = "je veux {0} et {1}"
// // params = ['toi', 'moi']
// // replaceBracketsContent(string, params)
// // 'je veux toi et moi'
