const LANGUAGE = require('./LANGUAGE').default
const translations = require('./translations').default

const setLanguage = (lang) => {
  if (lang.toLowerCase() in translations) {
    LANGUAGE.locale = lang.toLowerCase()
  }
}

exports.default = setLanguage
