// chunk array (or collection) by calculating an accumulator with an iteratee function while going through the collection
// and passing the result to the predicate function. While true, it pushes docs in current chunk
// if predicate gets fale, we reset the accumulator, and the next docs will be pushed in a new chunk

const chunkBy = (
  collection = [],
  iteratee = (accu, doc) => 1, // eslint-disable-line
  initialAccumulatorValue = 0,
  predicate = () => true, // when it gets false, creates new chunk and reset accumulator
  {
    inclusivePredicate = false, // by default, when predicates get false, current object is added to next chunk. with inclusive predicate, chunk is ended AFTER adding current object
  } = {},
) => {
  const chunks = []
  let accumulator = initialAccumulatorValue

  let currentChunk = []

  collection.forEach((doc) => {
    accumulator = iteratee(accumulator, doc)
    const endThisChunk = !predicate(accumulator, doc)

    if (endThisChunk) {
      if (!currentChunk.length) { // nothing in currentChunk it means the doc alone does not validate the predicate. We push it alone
        chunks.push([doc])
        currentChunk = []
        accumulator = initialAccumulatorValue
      } else {
        if (inclusivePredicate) {
          chunks.push([...currentChunk, doc])
          currentChunk = []
          accumulator = iteratee(initialAccumulatorValue, doc)
        } else {
          chunks.push(currentChunk)
          currentChunk = [doc]
          accumulator = iteratee(initialAccumulatorValue, doc)
        }
      }
    } else {
      currentChunk.push(doc)
    }
  })
  if (currentChunk.length) {
    chunks.push(currentChunk)
  }

  return chunks
}

exports.default = chunkBy

// // TEST
// //
// // INPUT
// ordersOfSlots = [{ nbPersonsInOrder: 1 }, { nbPersonsInOrder: 7 }, { nbPersonsInOrder: 1 }, { nbPersonsInOrder: 1 }, { nbPersonsInOrder: 4 }, { nbPersonsInOrder: 1 }, { nbPersonsInOrder: 1 }]
// //
// // OUTPUT if chunk with 10
// res = chunkBy(
//   ordersOfSlots,
//   (nbPersonsInSlot, { nbPersonsInOrder = 1 } = {}) => nbPersonsInSlot + +nbPersonsInOrder,
//   0,
//   nbPersonsInSlot => nbPersonsInSlot <= 10,
//   { inclusivePredicate: true }
// )
// -> [
//   [{ nbPersonsInOrder: 1 }, { nbPersonsInOrder: 7 }, { nbPersonsInOrder: 1 }, { nbPersonsInOrder: 1 }],
//   [{ nbPersonsInOrder: 4 }, { nbPersonsInOrder: 1 }, { nbPersonsInOrder: 1 }]
// ]
