const range = (start, end, step = 1) => (
  start < end
    ? Array.from(Array.from(Array(Math.ceil((end - start) / step)).keys()), x => start + x * step)
    : Array.from(Array.from(Array(Math.ceil((start - end) / step)).keys()), x => end + x * step)
)

exports.default = range

// > range(0,10,1)
// [
//   0, 1, 2, 3, 4,
//   5, 6, 7, 8, 9
// ]

// > range(0,10,2)
// [ 0, 2, 4, 6, 8 ]
