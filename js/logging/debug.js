/*
  Created by Matthieu MARIE-LOUISE
*/
const log = require('./log').default

const {
  NODE_ENV,
} = process.env

const debug = (NODE_ENV === 'development')
  ? log
  : () => { }

exports.default = debug
