/*
  Created by Matthieu MARIE-LOUISE
*/
const pushLog = require('./pushLog').default
const red = require('../terminal/red').default
const LOG_SESSION = require('./LOG_SESSION').default
const postSlackNotification = require('./postSlackNotification').default

const {
  NODE_ENV,
} = process.env
const isDevelopment = NODE_ENV === 'development'

// ======================== Logging ====================================
const error = (err, properties) => {
  try {
    const {
      stack = '',
      message = '',
      response: {
        data,
      } = {},
    } = err || {}
    const now = (new Date()).toLocaleString('en-US')

    // Regular logging
    if (isDevelopment) console.warn(red(message))
    if (isDevelopment && data) console.warn(red(JSON.stringify(data)))

    // Send to custom logger
    pushLog(`"${now}";"${red(message)}  ${JSON.stringify(properties)}";"${LOG_SESSION}";"${stack}"`, 'error')
    if (data) {
      pushLog(`"${now}";"${red(JSON.stringify(data))}";"${LOG_SESSION}";"${stack}"`, 'error')
    }
    if (
      message.includes('firestore')
      && !(message.includes('The caller does not have permission to execute the specified operation'))
    ) {
      postSlackNotification(`heaven/js:: error.message includes "firestore" - ${message}`)
    }
    if (
      message.includes('connect ETIMEDOUT')
    ) {
      postSlackNotification(`heaven/js:: error.message includes "connect ETIMEDOUT" 🚨🚨🚨🚨 THIS IS MOST LIKELY A CATASTROPHE ! Server is lagging, has not performed the required task, AND WILL NOT !!- ${message}`)
    }
    // postSlackNotification(`"${now}";"${red(message)}  ${JSON.stringify(properties)}";"${LOG_SESSION}";"${stack}"`)
  } catch (e) {
    error(e)
  }
}

// ======================== Exports ========================
exports.default = error
