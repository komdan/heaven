/*
  Created by Matthieu MARIE-LOUISE
*/
const uuid = require('uuid')

const LOG_SESSION = uuid.v4()

exports.default = LOG_SESSION
