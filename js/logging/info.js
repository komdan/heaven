/*
  Created by Matthieu MARIE-LOUISE
*/
const blue = require('../terminal/blue').default

const pushLog = require('./pushLog').default
const LOG_SESSION = require('./LOG_SESSION').default

const {
  NODE_ENV,
} = process.env

const isDevelopment = NODE_ENV === 'development'
const enableConsoleLog = false

// ======================== Logging ====================================
const info = (event, properties) => {
  const now = (new Date()).toLocaleString('en-US')

  if (typeof properties !== 'undefined') {
    // Regular logging
    if (enableConsoleLog && isDevelopment) console.log(blue(event), properties)

    // Send to custom logger
    pushLog(`"${now}";"${blue(event)} ${JSON.stringify(properties)}";"${LOG_SESSION}"`, 'info')
  } else {
    // Regular logging
    if (enableConsoleLog && isDevelopment) console.log(blue(event))

    // Send to custom logger
    pushLog(`"${now}";"${blue(event)}";"${LOG_SESSION}"`, 'info')
  }
}

// ======================== Exports ========================
exports.default = info
