/*
  Created by Matthieu MARIE-LOUISE
*/
const axios = require('axios')

const {
  LOGGER_URL,
  REACT_APP_LOGGER_URL,
} = process.env

const THROTTLE_PERIOD = 600

// ====== Send log that has been push
let logs = []
setInterval(
  async () => {
    try {
      if (logs.length) {
        // console.log('logging.js\\sending: logs', logs)
        await axios.post(
          LOGGER_URL || REACT_APP_LOGGER_URL,
          { logs },
        )
      }
    } catch (e) {
      console.warn('logging.js\\error sending : ', logs)
      console.warn(e.message, e.stack)
    }
    logs = []
  },
  THROTTLE_PERIOD,
)

// ====== Functions
const pushLog = async (message, level = 'info') => {
  const logObject = {
    msg: message,
    level,
    stacktrace: '',
  }
  logs.push(logObject)
}

exports.default = pushLog
