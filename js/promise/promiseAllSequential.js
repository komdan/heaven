async function promiseAllSequential(promises) {
  for (const promise of promises) {
    await promise
  }
}

exports.default = promiseAllSequential
