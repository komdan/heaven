async function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

exports.default = sleep

// await sleep(2000)
