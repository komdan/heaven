const neverEndingPromise = () => new Promise(() => null)

exports.default = neverEndingPromise
