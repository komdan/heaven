const numberToHexString = number => number.toString(16)

exports.default = numberToHexString
