function btoa(string) {
  return Buffer.from(string).toString('base64')
}

exports.default = btoa

// > btoa('allo')
// 'YWxsbw=='
