const displayPrice = price => (+price / 100).toFixed(2)

exports.default = displayPrice

// > displayPrice(159)
// '1.59'
// > displayPrice('159')
// '1.59'
