const {
  Buffer,
} = require('buffer')

const toBase64 = string => Buffer.from(string).toString('base64')

exports.default = toBase64

// > toBase64('allo')
// 'YWxsbw=='
