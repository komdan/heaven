const convertCollectionToObject = (collection) => {
  const res = {}

  // Parcourir la collection
  collection.forEach((objet) => {
    // Récupérer l'ID de l'objet
    const {
      id,
    } = objet

    // Ajouter l'objet à l'objet global avec l'ID comme clé
    res[id] = objet
  })

  return res
}

exports.default = convertCollectionToObject
