const revertObject = (obj) => {
  const revertedObj = {}

  Object.keys(obj).forEach((key) => {
    // eslint-disable-next-line
    if (obj.hasOwnProperty(key)) {
      const value = obj[key]

      // eslint-disable-next-line
      if (!revertedObj.hasOwnProperty(value)) {
        revertedObj[value] = [key]
      } else {
        revertedObj[value].push(key)
      }
    }
  })

  return revertedObj
}

exports.default = revertObject
