const { orderBy } = require('lodash');

const deepSortObject = (obj, sortProperty = 'name') => {
  if (Array.isArray(obj)) {
    if (obj.every(item => typeof item === 'object')) {
      const sortedArray = orderBy(obj, [sortProperty], ['asc']);
      return sortedArray.map(item => deepSortObject(item, sortProperty));
    } else if (obj.every(item => typeof item === 'string')) {
      return obj.slice().sort();
    }
    return obj; // or handle mixed types if needed
  }

  if (typeof obj === 'object' && obj !== null) {
    const sortedKeys = Object.keys(obj).sort();
    const sortedObj = {};
    for (const key of sortedKeys) {
      sortedObj[key] = deepSortObject(obj[key], sortProperty);
    }
    return sortedObj;
  }

  return obj; // return primitive types or null as-is
};

exports.default = deepSortObject;
