/* eslint-disable */
const promiseAll = async (array, asyncCallback) => {
  const results = await Promise.all(array.map(asyncCallback))
  return results
}

exports.default = promiseAll;
