/* eslint-disable */
async function asyncForEach(arrayInArgs, callback) {
  const results = []
  const array = [...arrayInArgs]
  for (let index = 0; index < array.length; index++) {
    const result = await callback(array[index], index, array);
    results.push(result);
  }
  return results;
}

exports.default = asyncForEach;
