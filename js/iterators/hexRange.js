const {
  range,
} = require('lodash')

const hexRange = (hex1, hex2) => range(hex1, hex2 + 1)
  .map(hexInDec => hexInDec.toString(16).padStart(4, '0'))

exports.default = hexRange

// > hexRange(0x0019, 0x001a)
// [ '0019', '001a' ]

// > hexRange(25,26)
// [ '0019', '001a' ]
