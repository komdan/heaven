// Bin packing problem : https://en.wikipedia.org/wiki/Bin_packing_problem
// Used for grouping orders in stuart jobs
// if possible, it will choose the repartitions that best fill every bins

const {
  shuffle,
  chunk,
  max,
  min,
} = require('lodash')
const FACTORIALS = require('./FACTORIALS').default
const chunkBy = require('../arrays/chunkBy').default

// binPackingChunk will :
//   STEP 1 - (shuffle) randomly shuffle 'collection'
//   STEP 2 - (chunkBy) go through the shuffled collection and :
//     * put each time the first doc in the opened bin. If not possible, definitively close the bin, open a new one and put the doc inside it
//     * iterate untill the end
//   STEP 3 - update optimal result
//     * update if new nbBins is better
//     * update if same nbBins and gap between most and worst filled bin is better

const MAX_NB_LOOPS = 500 * 1000

const binPackingChunk = ({
  collection = [],
  binSize = 10,
  getObjectSize = () => 1,
}) => {
  const loopTime = 5 * 1000 // 5 seconds
  const startTimestamp = Date.now()
  const loopWillStopAtTimestamp = startTimestamp + loopTime

  const totalNbCombinations = (FACTORIALS[collection.length] && FACTORIALS[collection.length] < MAX_NB_LOOPS)
    ? FACTORIALS[collection.length]
    : MAX_NB_LOOPS

  let nbShuffles = 0
  let optimalNbBins = collection.length // worst-case possible : each doc in one bin
  let optimalFilledBins = chunk(collection, 1) // worst-case possible : each doc in one bin
  let optimalGapBetweenMostAndLessFilledBin = binSize - 1 // worst-case possible : each doc in one bin

  while (
    nbShuffles < 2 * totalNbCombinations
    && Date.now() < loopWillStopAtTimestamp
  ) {
    const shuffledCollection = shuffle(collection) // STEP 1
    const filledBins = chunkBy( // STEP 2
      shuffledCollection,
      (accu, doc) => accu + +(getObjectSize(doc)),
      0,
      accu => accu <= binSize,
    )

    // console.log('filledBins')
    // console.log(JSON.stringify(filledBins.map(bin => bin.map(getObjectSize))))

    // STEP 3
    const nbBins = filledBins.length
    const nbDocsByBins = filledBins.map(filledBin => filledBin.reduce(
      (accu, doc) => accu + getObjectSize(doc),
      0,
    ))
    // console.log(JSON.stringify(nbDocsByBins))
    const gapBetweenMostAndLessFilledBin = max(nbDocsByBins) - min(nbDocsByBins)
    if (
      nbBins < optimalNbBins
      || (nbBins === optimalNbBins && gapBetweenMostAndLessFilledBin < optimalGapBetweenMostAndLessFilledBin)
    ) {
      optimalNbBins = nbBins
      optimalFilledBins = filledBins
      optimalGapBetweenMostAndLessFilledBin = gapBetweenMostAndLessFilledBin
    }

    nbShuffles += 1
  }

  return optimalFilledBins
}

exports.default = binPackingChunk

// // === TESTS
// /* eslint no-redeclare: 0 */
// /* eslint no-var: 0 */
// /* eslint vars-on-top: 0 */
// // npx babel-node ./heaven/js/algos/binPackingChunk.js
// const {
//   find,
//   flattenDeep,
// } = require('lodash') // eslint-disable-line
// const isEmptyArray = require('../types/isEmptyArray').default

// const runAlgo = async ({
//   collection,
//   binSize,
//   getObjectSize,
// }) => {
//   const outChunks = await binPackingChunk({
//     collection,
//     binSize,
//     getObjectSize,
//   })
//   const nbBins = outChunks.length
//   const nbDocsByBins = outChunks.map(filledBin => filledBin.reduce(
//     (accu, doc) => accu + getObjectSize(doc),
//     0,
//   ))
//   const gapBetweenMostAndLessFilledBin = max(nbDocsByBins) - min(nbDocsByBins) || 0
//   return ({
//     inCollection: collection,
//     outChunks,
//     nbBins,
//     nbDocsByBins,
//     gapBetweenMostAndLessFilledBin,
//   })
// }
// const generateDataSet = ({ minOrders = 1, maxOrders = 20, minNbPersonsInOrder = 1, maxNbPersonsInOrder = 3 } = {}) => {
//   const nbOrdersOfSlot = minOrders + Math.floor(Math.random() * maxOrders) // x à y commandes au total
//   const ordersOfSlot = [...Array(nbOrdersOfSlot).keys()].map(index => ({
//     nbPersonsInOrder: minNbPersonsInOrder + Math.floor(Math.random() * maxNbPersonsInOrder), // pour chaque commande : 1 personne + 0 à n-1 amis
//     id: index,
//   }))

//   return ordersOfSlot
// }

// const test = async () => {
//   var {
//     inCollection,
//     outChunks,
//     nbBins,
//     nbDocsByBins,
//     gapBetweenMostAndLessFilledBin,
//   } = await runAlgo({
//     collection: generateDataSet(),
//     binSize: 10,
//     getObjectSize: ({ nbPersonsInOrder = 1 }) => nbPersonsInOrder,
//   })

//   console.log('===============')
//   console.log('IN COLLECTION')
//   console.log(JSON.stringify(inCollection))
//   console.log()
//   console.log(JSON.stringify(inCollection.map(order => order.nbPersonsInOrder)))
//   console.log()

//   console.log('===============')
//   console.log('OUT CHUNKS')
//   console.log()

//   console.log(JSON.stringify(outChunks))
//   console.log()
//   console.log(JSON.stringify(outChunks.map(ordersChunk => ordersChunk.map(order => order.nbPersonsInOrder))))
//   console.log()
//   console.log(JSON.stringify(nbDocsByBins))
//   console.log()
//   console.log(JSON.stringify({ nbBins, gapBetweenMostAndLessFilledBin }))
//   console.log('===============')
//   console.log('TESTS')
//   console.log()

//   console.log('All docs are in chunks')
//   const allDocAreInChunks = inCollection.reduce(
//     (accu, { id }) => accu && find(flattenDeep(outChunks), { id }),
//     true,
//   )
//   console.log(allDocAreInChunks ? '✅' : '❌')

//   console.log('Same number of docs at the end')
//   const sameNumberOfOrdersAtTheEnd = flattenDeep(outChunks).length === inCollection.length
//   console.log(sameNumberOfOrdersAtTheEnd ? '✅' : '❌')

//   console.log('In collection is empty')
//   var {
//     inCollection,
//     outChunks,
//     nbBins,
//     nbDocsByBins,
//     gapBetweenMostAndLessFilledBin,
//   } = await runAlgo({
//     collection: [],
//     binSize: 10,
//     getObjectSize: ({ nbPersonsInOrder = 1 }) => nbPersonsInOrder,
//   })

//   console.log(isEmptyArray(outChunks) ? '✅' : '❌', 'outChunksEmptyArray')

//   console.log('In collection has one order and one person')
//   var {
//     inCollection,
//     outChunks,
//     nbBins,
//     nbDocsByBins,
//     gapBetweenMostAndLessFilledBin,
//   } = await runAlgo({
//     collection: generateDataSet({ maxOrders: 1, maxNbPersonsInOrder: 1 }),
//     binSize: 10,
//     getObjectSize: ({ nbPersonsInOrder = 1 }) => nbPersonsInOrder,
//   })

//   console.log(JSON.stringify(outChunks) === '[[{"nbPersonsInOrder":1,"id":0}]]' ? '✅' : '❌', JSON.stringify(outChunks), 'outChunkOk')

//   return outChunks
// }
// test()
