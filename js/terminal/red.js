const {
  SHELL_COLORS,
} = require('../../constants')

const red = string => `${SHELL_COLORS.FgRed}${string}${SHELL_COLORS.Reset}`

exports.default = red
