const {
  SHELL_COLORS,
} = require('../../constants')

const blue = string => `${SHELL_COLORS.FgCyan}${string}${SHELL_COLORS.Reset}`

exports.default = blue
