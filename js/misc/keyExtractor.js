const keyExtractor = (
  { // item
    id = '',
    ...rest
  } = {},
  indexOrString = '',
) => (id
  ? `${id}${indexOrString || ''}`
  : JSON.stringify(`${rest}${indexOrString}`)
)

exports.default = keyExtractor
