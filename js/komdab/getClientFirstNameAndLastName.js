const getClientFirstNameAndLastName = (displayName) => {
  const splittedFullName = `${displayName}`.split(' ')
  const firstName = splittedFullName[0]
  const lastName = splittedFullName.slice(1).join(' ')
  return {
    firstName,
    lastName,
  }
}

exports.default = getClientFirstNameAndLastName
