const moment = require('moment-timezone')

const dateToMoment = (
  date,
  timeZone = 'Europe/Paris',
) => {
  const time = moment.tz(
    date,
    timeZone,
  )
  const day = time.day()
  const month = time.month()
  const year = time.year()
  const hours = `${time.hour()}:${time.minute()}`

  return ({
    time,
    day,
    hours,
    month,
    year,
  })
}

exports.default = dateToMoment
