const {
  minBy,
} = require('lodash')
const msToWait = require('./msToWait').default

const closestTimeFromNow = (arrayOfTime, timeZone) => {
  // Usage:
  // inputs:
  //   arrayOfTime: ['18:00', '21:00', '00:00']
  //   timeZone: ''
  //
  // output:
  //   if the current time is 18:30, return '21:00'
  //
  // creates an array with 'time' and the 'delay' fromnow,
  // removes when delay < 0
  // returns the minimum

  const { time: closestTime } = minBy(
    arrayOfTime
      .map((time) => {
        const [hour, minute] = time.split(':').map(Number)
        return {
          time,
          delay: msToWait(hour, minute, timeZone),
        }
      })
      .filter(({ delay }) => delay > 0),
    'delay',
  ) || {}
  return closestTime
}

exports.default = closestTimeFromNow
