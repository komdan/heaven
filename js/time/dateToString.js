const moment = require('moment-timezone')

const dateToString = (date, timeZone) => moment.tz(
  date,
  timeZone,
).format('DD.MM.YYYY-HH:mm')

exports.default = dateToString
