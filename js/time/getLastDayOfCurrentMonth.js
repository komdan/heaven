const getLastDayOfCurrentMonth = (date = new Date()) => {
  const currentMonthNumber = date.getMonth()
  const currentYear = date.getFullYear()
  const lastDay = new Date(currentYear, currentMonthNumber + 1, 0)
  return {
    lastDayNumber: lastDay.getDate(),
    currentMonthNumber,
    currentYear,
  }
}

exports.default = getLastDayOfCurrentMonth
