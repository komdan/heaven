const msToWait = require('./msToWait').default

const executeTodayAt = (hour, minute, timeZone, callBack) => {
  const delay = msToWait(hour, minute, timeZone)
  return setTimeout(
    () => delay > 0 && callBack(),
    delay,
  )
}

exports.default = executeTodayAt
