const moment = require('moment-timezone')

const msToWait = (hour, minute, timeZone) => {
  // how many millyseconds to wait fromnow to 'hour:minute'
  const currentTime = moment.tz(
    new Date(),
    timeZone,
  )
  const callTime = moment.tz(
    new Date(
      currentTime.year(),
      currentTime.month(),
      currentTime.date(),
      hour,
      minute,
      0,
      0,
    ),
    timeZone,
  )
  return callTime - currentTime
}

exports.default = msToWait
