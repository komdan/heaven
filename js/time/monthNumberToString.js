const monthNumberToString = (
  month,
  t = s => s,
) => t(
  [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ][month],
)

exports.default = monthNumberToString
