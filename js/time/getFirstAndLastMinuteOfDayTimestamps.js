const getFirstAndLastMinuteOfDayTimestamps = (timestamp) => {
  const date = new Date(timestamp)
  const year = date.getFullYear()
  const month = date.getMonth()
  const day = date.getDate()
  
  const firstMinuteOfDayTimestamp = new Date(year, month, day, 0, 0, 0, 0).getTime()
  const lastMinuteOfDayTimestamp = new Date(year, month, day, 23, 59, 59, 999).getTime()
  
  return {
    firstMinuteOfDayTimestamp,
    lastMinuteOfDayTimestamp,
  }
}

exports.default = getFirstAndLastMinuteOfDayTimestamps
