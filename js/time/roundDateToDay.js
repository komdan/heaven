const roundDateDownTo = require('./roundDateDownTo').default

const roundDateToDay = date => roundDateDownTo(
  24 * 3600 * 1000,
  date,
)

exports.default = roundDateToDay
