const soonerThan = require('./soonerThan').default

const isBetween = (begin, time1, end) => (
  // input:
  //    begin: '19:00'
  //    end: '20:00'
  //    time1: '19:30'
  // output:
  //    true
  soonerThan(begin, time1) && soonerThan(time1, end)
)

exports.default = isBetween
