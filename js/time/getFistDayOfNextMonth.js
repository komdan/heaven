const getFirstDayOfNextMonth = () => {
  const date = new Date()

  return new Date(date.getFullYear(), date.getMonth() + 1, 1, 15)
}

exports.default = getFirstDayOfNextMonth
