const getLastDayOfCurrentMonth = require('./getLastDayOfCurrentMonth').default

const getLastSundayOfCurrentMonth = () => {
  const {
    lastDayNumber,
    currentMonthNumber,
    currentYear,
  } = getLastDayOfCurrentMonth()
  const date = new Date()
  date.setFullYear(currentYear, currentMonthNumber, lastDayNumber)
  date.setDate(date.getDate() - date.getDay())
  return date.getDate()
}

exports.default = getLastSundayOfCurrentMonth
