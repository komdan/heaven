const getMaxQuarterHours = (num) => {
  const hours = Math.floor(num / 60);
  const minutes = num % 60;
  const totalQuarterHours = (hours * 4) + Math.floor(minutes / 15);
  return totalQuarterHours;
}

exports.default = getMaxQuarterHours
