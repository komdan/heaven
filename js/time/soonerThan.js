const soonerThan = (time1, time2) => {
  // is time1 (e.g. "08:12") sooner than time2 (e.g "09:17")
  const [hours1, minutes1] = time1.split(':').map(Number)
  const [hours2, minutes2] = time2.split(':').map(Number)
  if (hours1 === hours2) {
    return minutes1 <= minutes2
  }
  return hours1 < hours2
}

exports.default = soonerThan
