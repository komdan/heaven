const timestampToLocalDateString = timestamp => timestamp.toDate().toLocaleDateString()

exports.default = timestampToLocalDateString
