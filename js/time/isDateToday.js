const {
  isEqual,
} = require('lodash')
const roundDateToDay = require('./roundDateToDay').default

const isDateToday = date => isEqual(
  roundDateToDay(date),
  roundDateToDay(Date.now()),
)

exports.default = isDateToday
