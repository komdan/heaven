const getNextXQuarterHours = (time24h, x) => {
  const [hours, minutes] = time24h.split(':').map(Number)
  let currentHour = hours
  let currentMinute = minutes
  const results = []

  for (let i = 0; i < x; i++) {
    currentMinute += 15
    if (currentMinute >= 60) {
      currentMinute -= 60
      currentHour += 1
      if (currentHour >= 24) {
        currentHour -= 24
      }
    }
    results.push(`${currentHour.toString().padStart(2, '0')}:${currentMinute.toString().padStart(2, '0')}`)
  }

  return results
}

exports.default = getNextXQuarterHours
