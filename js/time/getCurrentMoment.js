const moment = require('moment-timezone')

const getCurrentMoment = (
  timeZone,
  timestamp = Date.now(),
) => {
  const time = moment.tz(timestamp, timeZone)
  const day = time.day()
  const month = time.month()
  const year = time.year()
  const hours = `${time.hour()}:${time.minute()}`
  const hHmM = hours

  return ({
    time,
    day,
    hours,
    month,
    year,
    hHmM,
  })
}

exports.default = getCurrentMoment
