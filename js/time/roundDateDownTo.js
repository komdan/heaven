const roundDateDownTo = (
  roundTo,
  date = new Date(),
) => new Date(Math.floor(date / roundTo) * roundTo)

exports.default = roundDateDownTo

// Example. Round down to 5 min
// > roundDateDownTo(5*60*1000)
// 2021-01-31T09:25:00.000Z
// Example. roundDateToDay -> Aujourd'hui à minuit
// > roundDateDownTo(24 * 3600 * 1000)
// 2023-03-23T00:00:00.000Z
