// USAGE
// objectToCollection({
//   'Yanntanguy1981@gmail.com': 2,
//   'chaimaa-benallal@hotmail.fr': 4,
// }, {
//   key: 'email',
//   value: 'orderCount',
// })
// returns ->
// [
//   {
//     email: 'Yanntanguy1981@gmail.com',
//     orderCount: 2,
//   },
//   {
//     email: 'chaimaa-benallal@hotmail.fr',
//     orderCount: 4,
//   },
// ]

const objectToCollection = (obj, {
  key = 'key',
  value = 'value',
} = {}) => Object.keys(obj).map(k => ({
  [key]: k,
  [value]: obj[k],
}))

exports.default = objectToCollection
