// Used in ExerciseSliders
// -> roundup a distance to 5 secs
const round5 = require('./round5').default

const roundDistance500 = x => (
  x < 400
    ? round5(x)
    : Math.ceil((x - 400) / 500) * 500
)

exports.default = roundDistance500
