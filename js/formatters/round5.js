// Used in ExerciseSliders
// -> roundup a duration to 5 secs

const round5 = x => (
  x > 4
    ? Math.ceil((x - 4) / 5) * 5
    : 0
)

exports.default = round5
