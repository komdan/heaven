const moment = require('moment')
require('moment/min/locales') // Import all moment-locales -- it's just 400kb
require('moment-timezone')

const calendarDate = (
  timestamp,
  {
    locale = 'fr',
  } = {},
) => {
  moment.locale(locale)
  return moment(timestamp).calendar()
}

exports.default = calendarDate
