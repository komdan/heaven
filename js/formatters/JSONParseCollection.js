// Apply JSON.parse to values of the collection.
// Useful to automatically convert string 'false' or arrays '[]' to false and the array object []

const JSONParseCollection = (collection, preTreat = string => string) => collection.map(element => {
  const res = {}
  Object.keys(element).forEach(field => {
    try {
      res[field] = JSON.parse(preTreat(element[field])) // will convert boolean and arrays
    } catch {
      res[field] = element[field]
    }
  })
  return res
})

exports.default = JSONParseCollection
