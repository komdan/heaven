const moment = require('moment')
require('moment/min/locales') // Import all moment-locales -- it's just 400kb
require('moment-timezone')
const {
  capitalize,
} = require('lodash')

const formatDate = (
  timestamp,
  format = 'll',
  locale = 'fr',
) => {
  moment.locale(locale)
  return capitalize(moment(timestamp).format(format))
}

exports.default = formatDate
// USAGE:
// echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/js/formatters/formatDate.js" | /bin/bash
// echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/formatters/formatDate.js" | /bin/bash
// const ONE_WEEK = 7 * 24 * 3600 * 1000
// console.log(formatDate(new Date(Date.now() + ONE_WEEK), undefined, 'de'))
