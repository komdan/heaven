const getCurrentMoment = require('../time/getCurrentMoment').default

const convertTimestampToHhMm = (
  timestamp = Date.now(),
  timeZone = 'Europe/Paris',
) => {
  const {
    hHmM,
  } = getCurrentMoment(timeZone, timestamp)

  return hHmM
}

exports.default = convertTimestampToHhMm

// // USAGE:
// // echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/js/formatters/convertTimestampToHhMm.js" | /bin/bash
// // echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/formatters/convertTimestampToHhMm.js" | /bin/bash
// const test = () => {
//   console.log(
//     convertTimestampToHhMm(1658851388484)
//   )
//   console.log(
//     convertTimestampToHhMm(1658844188484)
//   )
// }
// test()
