const convertHhMmToTimestamp = (
  hhMM = ':', // ex '12:50'
  day = new Date(), // timestamp or date can be passed, whatever, we will convert to date here
) => {
  const [hours, minutes] = hhMM.split(':').map(Number)

  const resultingDate = new Date(day)
  resultingDate.setHours(hours, minutes)

  const resultingTimestamp = +resultingDate

  return resultingTimestamp
}

exports.default = convertHhMmToTimestamp
