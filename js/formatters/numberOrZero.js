const numberOrZero = variable => Number(variable) || 0

exports.default = numberOrZero

// > numberOrZero(0)
// 0
// > numberOrZero(1)
// 1
// > numberOrZero('1')
// 1
// > numberOrZero(NaN)
// 0
// > numberOrZero(undefined)
// 0
// > numberOrZero(null)
// 0
// > numberOrZero('string')
