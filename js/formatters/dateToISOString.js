const moment = require('moment')
require('moment/min/locales') // Import all moment-locales -- it's just 400kb
require('moment-timezone')

const dateToISOString = (
  dateOrTimestamp,
) => {
  return moment(dateOrTimestamp).toISOString()
}

exports.default = dateToISOString

// USAGE:
// echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/js/formatters/dateToISOString.js" | /bin/bash
// echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/formatters/dateToISOString.js" | /bin/bash
// const ONE_WEEK = 7 * 24 * 3600 * 1000
// console.log(dateToISOString(new Date(Date.now() + ONE_WEEK)))
