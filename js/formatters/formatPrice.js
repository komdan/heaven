const formatPrice = price => +(price / 100).toFixed(2)

exports.default = formatPrice
