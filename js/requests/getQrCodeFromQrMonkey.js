const {
  post,
} = require('axios')

const getQrCodeFromQrMonkey = async ({ qrCodeLink }) => {
  const res = await post(
    'https://api.qrcode-monkey.com//qr/custom',
    {
      data: `${qrCodeLink}`,
      config: {
        body: 'circular',
        eye: 'frame2',
        eyeBall: 'ball14',
        erf1: [
          'fv',
        ],
        erf2: [],
        erf3: [],
        brf1: [],
        brf2: [],
        brf3: [],
        bodyColor: '#000000',
        bgColor: '#FFFFFF',
        eye1Color: '#000000',
        eye2Color: '#000000',
        eye3Color: '#000000',
        eyeBall1Color: '#000000',
        eyeBall2Color: '#000000',
        eyeBall3Color: '#000000',
        gradientColor1: '',
        gradientColor2: '',
        gradientType: 'linear',
        gradientOnEyes: 'true',
        logo: '',
        logoMode: 'default',
      },
      size: 1000,
      download: 'imageUrl',
      file: 'svg',
    },
  )
  const {
    data: {
      imageUrl,
    },
  } = res
  return `https:${imageUrl}`
}

exports.default = getQrCodeFromQrMonkey

// USAGE:
// echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/js/requests/getQrCodeFromQrMonkey.js" | /bin/bash
// echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/requests/getQrCodeFromQrMonkey.js" | /bin/bash

// getQrCodeFromQrMonkey({ qrCodeLink: 'https://enjoy.komdab.net/jellyfish-gagne-une-semaine-de-dejeuners' })
//   .then(console.log)
