const fetch = require('node-fetch')
const error = require('../logging/error').default

const {
  GOOGLE_API_KEY = 'AIzaSyAJo-_l2Z2L7s3NVEIZENKwGGQPnodz08Y',
} = process.env

const getGoogleReviews = (googlePlaceId) => {
  const apiKey = GOOGLE_API_KEY
  const url = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${googlePlaceId}&fields=reviews&key=${apiKey}`

  return (
    fetch(url)
      .then((response) => {
        if (response.status === 200) {
          return response.json()
        }
        return {}
      })
      .catch((e) => {
        error(e)
        console.warn(`common.js\\getGoogleReviews('${googlePlaceId}') ERROR: `, JSON.stringify(e))
      })
  )
}

exports.default = getGoogleReviews
