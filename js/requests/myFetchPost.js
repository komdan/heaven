const fetch = require('node-fetch')

const error = require('../logging/error').default

function myFetchPost(url = '', postData) {
  return new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(
        url,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(postData),
        },
      )
      res.json().then((data) => {
        const { body } = data
        resolve(body)
      })
    } catch (e) {
      error(e)
      console.warn('backendRequests/myFetchPost::error: ', JSON.stringify(e))
      reject(e)
    }
  })
}

exports.default = myFetchPost
