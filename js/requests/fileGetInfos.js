const fileGetInfos = ({
  req: {
    file: {
      originalname = '',
      mimetype = '',
      size = 0,
      buffer = Buffer.alloc(0),
    } = {},
  } = {},
} = {}) => {
  const extensionArray = mimetype.split('/');
  const hasFiletype = (extensionArray.length > 0)
    && (extensionArray[0].length > 0);
  const hasExtension = (extensionArray.length > 1);
  const type = hasFiletype
    ? extensionArray[0]
    : '';
  const extension = hasExtension
    ? extensionArray[1]
    : '';
  return {
    extensionArray,
    hasFiletype,
    hasExtension,
    type,
    extension,
    size,
    buffer,
  };
};

exports.default = fileGetInfos;
