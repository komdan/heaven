/*
* Created by Matthieu MARIE-LOUISE
* Node style
*/
// USAGE
// networkStatusListener(status => console.log(status))
// -> 'UP' or 'DOWN'
// DETAILS
// This will poll url located at process.env.NETWORK_POLLING_URL

const EventEmitter = require('events')
const nodeFetch = require('node-fetch')

const fetch = nodeFetch

const {
  NETWORK_POLLING_URL,
} = process.env

const eventEmitter = new EventEmitter()
eventEmitter.setMaxListeners(50)
const REQUEST_TIMEOUT = 5 * 1000
const POLLING_INTERVAL = 10 * 1000

const requestNetwork = () => Promise.race([
  fetch(`${NETWORK_POLLING_URL}?timestamp=${Date.now()}`) // timestamp is used to avoid possible caching
    .then(() => true)
    .catch(() => false),
  new Promise(resolve => setTimeout(
    () => resolve(false),
    REQUEST_TIMEOUT,
  )),
])

const networkStatusListener = (callBack) => {
  const poll = setInterval(
    async () => {
      const start = Date.now()
      const isNetworkUp = await requestNetwork()
      const latency = Date.now() - start

      eventEmitter.emit('isNetworkUp updated', {
        status: isNetworkUp ? 'UP' : 'DOWN',
        latency,
        isNetworkUp,
      })
    },
    POLLING_INTERVAL,
  )

  eventEmitter.addListener(
    'isNetworkUp updated',
    ({ status, latency, isNetworkUp }) => callBack({ status, latency, isNetworkUp }),
  )

  return () => {
    clearInterval(poll)
    eventEmitter && eventEmitter.remove && eventEmitter.remove( // eslint-disable-line
      'isNetworkUp updated',
      ({ status, latency, isNetworkUp }) => callBack({ status, latency, isNetworkUp }),
    )
  }
}

exports.default = networkStatusListener
