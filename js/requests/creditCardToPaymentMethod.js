const {
  post,
} = require('axios')

const {
  REACT_APP_BACKEND_URL,
  BACKEND_URL,
} = process.env

const creditCardToPaymentMethod = async ({
  card,
}) => {
  const {
    data: {
      paymentMethod,
    },
  } = await post(`${BACKEND_URL || REACT_APP_BACKEND_URL}/stripeCreatePaymentMethod`, {
    card,
  })

  return paymentMethod
}

exports.default = creditCardToPaymentMethod
