const {
  post,
} = require('axios')

// const USECASES = require('../../../src/endpoints/dispatchNotifications/dispatchNotifications.useCases')

const {
  BACKEND_URL,
  BACKEND_SECRET_KEY,
} = process.env

const callDispatchNotifications = async (useCase, useCaseInputs) => {
  await post(`${BACKEND_URL}/dispatchNotifications`, {
    useCase,
    useCaseInputs: {
      backendSecretKey: BACKEND_SECRET_KEY,
      ...useCaseInputs,
    },
  })
  // // (TO BE TESTED) IF WE DO NOT WANT TO DO A BACKEND HTTP REQUEST, call dispatch directly. If you want to do this, importing USECASES will cause exports circular dependency issue
  // const body = {
  //   useCaseInputs: {
  //     backendSecretKey: BACKEND_SECRET_KEY,
  //     ...useCaseInputs,
  //   },
  // }
  // const {
  //   assert = () => { },
  //   dbRequests = () => { },
  //   verify = () => { },
  //   execute = () => { },
  //   analytics = () => { },
  // } = USECASES[useCase]

  // await assert({ body })
  // const dbResults = await dbRequests({ body })
  // await verify({ body, dbResults })
  // const { responseToClient = '', analyticsData = {} } = await execute({ body, dbResults })
  // await analytics({ body, dbResults, responseToClient, analyticsData })
}

exports.default = callDispatchNotifications
