const {
  get,
} = require('axios')

const searchGoogle = ({
  query,
}) => get(
  `https://www.google.com/search?q=${query}`,
)

exports.default = searchGoogle
