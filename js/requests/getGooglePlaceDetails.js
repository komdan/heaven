const {
  post,
} = require('axios')

const {
  GOOGLE_API_KEY = 'AIzaSyAJo-_l2Z2L7s3NVEIZENKwGGQPnodz08Y',
} = process.env

const getGooglePlaceDetails = async (googlePlaceId) => {
  const {
    data,
  } = await post(`https://maps.googleapis.com/maps/api/place/details/json?placeid=${googlePlaceId}&key=${GOOGLE_API_KEY}`)

  return data
}

// USAGE:
// echo "$(cat .env.development | tr '\n' ' ') npx babel-node heaven/js/requests/getGooglePlaceDetails.js" | /bin/bash
// echo "$(cat .env.production | tr '\n' ' ') npx babel-node heaven/js/requests/getGooglePlaceDetails.js" | /bin/bash

// getGooglePlaceDetails('ChIJF2vM9Upu5kcRttnjILcrqbo')
//   .then(console.log)

exports.default = getGooglePlaceDetails
