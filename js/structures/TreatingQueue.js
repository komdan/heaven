const {
  find: lodashFind,
  uniqBy,
  debounce,
  random,
} = require('lodash')
const log = require('../logging/log').default
const error = require('../logging/error').default

// ====== USAGE
// Used to handle heavy treatment.
// Pushed objects will be treated.
// the queue can be saved into a file for fail-safe recovery
//
// ====== EXAMPLE
// -- Definition file : OrderPrintingQueue.js
// const OrderPrintingQueue = new TreatingQueue({
//   treatOne: printOrder,
//   find: findSameOrderIdInQueue,
//   getUniqueId: ({
//     order: {
//       id = '',
//     } = {},
//   } = {}) => id,
// })
// -- Caller file :
// import OrderPrintingQueue from './OrderPrintingQueue'
// OrderPrintingQueue.push(olderOrder)

class TreatingQueue {
  constructor({
    treatOne,
    getUniqueId = JSON.stringify,
    find = lodashFind,
    popAndTreatOneInterval = 1000, // 1 second
    resetAlreadyTreatedIdsInterval = 60 * 1000, // 60 secondes
    resetAlreadyPushedIdsInterval = 60 * 1000, // 60 secondes
    debounceInterval = 600, // ms
    cleanQueueInterval = 700, // ms
  }) {
    this.queue = []
    this.alreadyTreatedIds = []
    this.alreadyPushedIds = []
    this.treatOne = debounce(treatOne, debounceInterval)
    this.find = find
    this.getUniqueId = getUniqueId
    this.debounceInterval = debounceInterval

    this.isTreatingQueueBusy = false
    this.treatingQueueId = random(1000)

    setInterval(
      () => this.popAndTreatOne(),
      popAndTreatOneInterval,
    )
    setInterval(
      () => {
        this.alreadyTreatedIds = []
      },
      resetAlreadyTreatedIdsInterval,
    )
    setInterval(
      () => {
        this.alreadyPushedIds = []
      },
      resetAlreadyPushedIdsInterval,
    )
    setInterval(
      () => {
        this.cleanQueue()
      },
      cleanQueueInterval,
    )
  }

  push = debounce(this.pushNotDebounced, this.debounceInterval)

  popAndTreatOne = debounce(this.popAndTreatOneNotDebounced, this.debounceInterval)
  // popAndTreatOne = this.popAndTreatOneNotDebounced

  async cleanQueue() {
    // remove duplicates
    this.queue = uniqBy(
      this.queue,
      this.getUniqueId,
    )
    // remove already treated
    this.queue = this.queue
      .filter(object => !this.alreadyTreatedIds.includes(
        this.getUniqueId(object),
      ))
  }

  async pushNotDebounced(object) {
    const uid = this.getUniqueId(object)
    if (
      !this.find(this.queue, object)
      && !this.alreadyPushedIds.includes(uid)
      // && !this.alreadyTreatedIds.includes(uid)
    ) {
      log(`⤵ Pushing ${uid}`)
      this.queue.push(object)
      this.alreadyPushedIds.push(uid)
      setTimeout(() => {
        this.cleanQueue() // seems useless but it is not : allow to avoid duplicates when pushing multiple identical object on the same tick
      }, 400)
    }
  }

  async popAndTreatOneNotDebounced() {
    // console.log('📝 this.alreadyTreatedIds', this.alreadyTreatedIds)
    // console.log('📝 this.alreadyPushedIds', this.alreadyPushedIds)
    // console.log()
    if (this.isTreatingQueueBusy || !this.queue.length) return
    this.isTreatingQueueBusy = true
    this.cleanQueue()

    const [first] = this.queue
    this.queue = this.queue.slice(1)
    const uid = this.getUniqueId(first)

    if (!this.alreadyTreatedIds.includes(uid)) {
      log(`⏩ Treating ${uid} - ${this.treatingQueueId}`)
      try {
        await this.treatOne(first)
        this.alreadyTreatedIds.push(uid)
      } catch (e) {
        error(e)
        error(new Error('TreatingQueue.js error in treatOne'))
      }
    }

    this.isTreatingQueueBusy = false
  }
}

exports.default = TreatingQueue
