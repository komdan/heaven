const sleep = require('../promise/sleep').default

// ====== USAGE
// Execute functions one after another using a 'isBusy' semaphore
// synchronous, asynchronous
// different from TreatingQueue because each function will be executed (no found)

class CallbackQueue {
  constructor({
    synchronousDelayMs = 500,
    pollingIntervalMs = 200,
    queueMaxLength = 100,
  }) {
    this.queue = []
    this.isBusy = false
    this.synchronousDelayMs = synchronousDelayMs
    this.pollingIntervalMs = pollingIntervalMs
    this.queueMaxLength = queueMaxLength

    setInterval(() => this.nextTick(), pollingIntervalMs)
  }

  push(func) {
    if (this.queue.length < this.queueMaxLength) {
      this.queue.push(func)
    } else {
      console.warn(`⚠️ OVERFLOWCallback Queue size : ${this.queue.length}`)
    }
  }

  async nextTick() {
    if (!this.isBusy && this.queue.length) {
      this.isBusy = true

      const [firstFunc] = this.queue
      this.queue = this.queue.slice(1)

      switch (firstFunc.constructor.name) {
        case 'Function': {
          firstFunc()
          await sleep(this.synchronousDelayMs)
          break
        }
        case 'Promise':
        case 'AsyncFunction': {
          await firstFunc()
          break
        }
        default: {
          console.error(`ERROR IN CALLBACKQUEUE, constructor.name is ${firstFunc.constructor.name} of type ${typeof firstFunc.constructor.name}`)
          break
        }
      }

      this.isBusy = false
    } else {
      // console.log(`CallbackQueue : no tick ! isBusy:${this.isBusy} queue.length:${this.queue.length}`)
    }
  }
}

exports.default = CallbackQueue
