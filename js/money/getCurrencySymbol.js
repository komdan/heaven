const {
  CURRENCY_SYMBOLS,
} = require('../../constants')

const getCurrencySymbol = (currency) => {
  if (typeof currency === 'string') {
    return (CURRENCY_SYMBOLS[currency.toUpperCase()] || currency)
  }
  return currency || '€'
}

exports.default = getCurrencySymbol
