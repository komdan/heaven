const firestoreTimestampToDate = ({
  _seconds = 0,
  _nanoseconds = 0,
} = {}) => new Date(
  _seconds * 1000 + _nanoseconds / 1000000,
)

exports.default = firestoreTimestampToDate
