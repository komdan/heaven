const CryptoJS = require('crypto-js')

// Inspired from https://www.npmjs.com/package/crypto-js
const encrypt = (message, key) => (
  message && message.length
    ? CryptoJS.AES.encrypt(message, key).toString()
    : ''
)

exports.default = encrypt

// > encrypt('I am your father', 's3cr3t')
// 'U2FsdGVkX19SY0MB+nj6t9cyFJ/7bb6jYKu9v0Q/EgmpHD3NePBh+mk3TJ0AyfEA'
