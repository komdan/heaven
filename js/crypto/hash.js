const CryptoJS = require('crypto-js')

const hash = string => CryptoJS.SHA3(string).toString(CryptoJS.enc.Hex)

exports.default = hash
