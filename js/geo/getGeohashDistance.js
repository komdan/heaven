const {
  getDistance,
} = require('geolib')
const {
  decode,
} = require('ngeohash')

const getGeohashDistance = (geohash1, geohash2) => {
  const {
    latitude: latitude1,
    longitude: longitude1,
  } = decode(geohash1)
  const {
    latitude: latitude2,
    longitude: longitude2,
  } = decode(geohash2)
  return getDistance(
    { latitude: latitude1, longitude: longitude1 },
    { latitude: latitude2, longitude: longitude2 },
  )
}

exports.default = getGeohashDistance

// // USAGE:
// // npx node heaven/js/geo/getGeohashDistance.js
// const main = async () => {
//   try {
//     const distance = getGeohashDistance('u09whutvm', 'u09whue8fzfy')
//     console.log(distance)
//   } catch (e) {
//     console.error(e)
//   }
// }
// main()
