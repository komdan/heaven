const computeAdequateGeohashLength = (distance) => {
  // https://www.movable-type.co.uk/scripts/geohash.html
  // distance should be below the smaller side of geohash boxes
  if (distance <= 0.0186) return 12
  if (distance <= 0.149) return 11
  if (distance <= 0.596) return 10
  if (distance <= 4.77) return 9
  if (distance <= 19.1) return 8
  if (distance <= 153) return 7
  if (distance <= 610) return 6
  if (distance <= 4890) return 5
  if (distance <= 19500) return 4
  if (distance <= 156000) return 3
  if (distance <= 625000) return 2
  if (distance <= 5000000) return 1
  return 0
}

exports.default = computeAdequateGeohashLength
