// WARNING : we want to allow every international phone number.
// This function should be called "isValidPhoneNumber"

// const isNonEmptyString = require('./isNonEmptyString').default
// const formatFrenchPhoneNumber = require('../formatters/formatFrenchPhoneNumber').default
const { parsePhoneNumber } = require('awesome-phonenumber')

const isFrenchPhoneNumber = (input, reservationModalFormattedPhoneNumberCountry = 'FR') => {
  const pn = parsePhoneNumber(input, { regionCode: reservationModalFormattedPhoneNumberCountry.toUpperCase() })
  return pn.valid
}

// WAS BEFORE :
// const isFrenchPhoneNumber = input => (
//   isNonEmptyString(input)
//   && input === formatFrenchPhoneNumber(input)
//   && (input.replace(/[^\d]/g, '').length === 10)
// )

exports.default = isFrenchPhoneNumber

// > isFrenchPhoneNumber('07 61 78 25 38')
// true
// > isFrenchPhoneNumber('+337 61 78 25 38')
// true
// > isFrenchPhoneNumber('+33761782538')
// true
// > isFrenchPhoneNumber('+33761782538 8')
// true
