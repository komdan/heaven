const isTruthy = value => !!value

exports.default = isTruthy
