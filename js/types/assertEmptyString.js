const { equal } = require('assert')

const assertEmptyString = (input, variableName) => {
  equal(
    input === '',
    true,
    `${variableName} should be an empty string and not: ${input} of type ${typeof input}`,
  )
}

exports.default = assertEmptyString
