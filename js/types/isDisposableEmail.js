const {
  DISPOSABLE_EMAIL_DOMAIN_LIST,
} = require('../../constants')

const isDisposableEmail = email => DISPOSABLE_EMAIL_DOMAIN_LIST.reduce(
  (accumulator, currentDisposableDomain) => accumulator || email.includes(currentDisposableDomain),
  false,
)

exports.default = isDisposableEmail
