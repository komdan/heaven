const isValidDate = variable => (
  !!variable
  && Object.prototype.toString.call(variable) === '[object Date]'
  && !Number.isNaN(+variable)
)

exports.default = isValidDate
