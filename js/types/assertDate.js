const {
  isDate,
} = require('lodash')
const {
  equal,
} = require('assert')

const assertDate = (variable, variableName) => {
  equal(isDate(variable), true, `${variableName} should be a date and not ${variable} of type ${typeof variable}`)
}

exports.default = assertDate
