const { equal } = require('assert')
const assertString = require('./assertString').default
const isEmail = require('./isEmail').default
const isDisposableEmail = require('./isDisposableEmail').default

const assertNonDisposableEmail = (input, variableName) => {
  assertString(input, variableName)
  equal(
    isEmail(input),
    true,
    `${variableName} should be an email and not: ${input}`,
  )
  equal(
    isDisposableEmail(input),
    false,
    `${variableName} should not be a disposable email: ${input}`,
  )
}

exports.default = assertNonDisposableEmail
