const { equal } = require('assert')

const iban = require('iban')
const assertString = require('./assertString').default

// Helpers
const isIbanValid = iban.isValid

const assertIban = (input, variableName) => {
  assertString(input, variableName)
  equal(
    isIbanValid(input),
    true,
    `${variableName} should be an IBAN and not: ${input}`,
  )
}

exports.default = assertIban
