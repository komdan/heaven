const assert = require('assert')
const {
  isObject, // returns false for undefined and null
} = require('lodash')

const assertObject = (variable, variableName) => assert(
  isObject(variable),
  `${variableName} is not an object. variable: ${variable} of type ${typeof variable}`,
)

exports.default = assertObject
