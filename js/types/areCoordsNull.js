const {
  isEqual,
} = require('lodash')

const {
  NULL_COORDS,
} = require('../../constants')

const areCoordsNull = devicePosition => isEqual(devicePosition, NULL_COORDS.coords)

exports.default = areCoordsNull
