const { equal } = require('assert')
const isValidDate = require('./isValidDate').default

const assertValidDate = (input, variableName) => equal(
  isValidDate(input),
  true,
  `${variableName} should be a valid date and not: ${input} of type ${typeof input} - ${Object.prototype.toString.call(input)}`,
)

exports.default = assertValidDate
