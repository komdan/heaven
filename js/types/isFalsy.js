const isFalsy = value => !value

exports.default = isFalsy
