const isDate = variable => (
  Object.prototype.toString.call(variable) === '[object Date]'
)

exports.default = isDate
