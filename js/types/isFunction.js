const isFunction = variable => (typeof variable === 'function')

exports.default = isFunction
