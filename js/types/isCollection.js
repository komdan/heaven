const isObject = require('./isObject').default
const isArray = require('./isArray').default

const isCollection = variable => (
  isArray(variable)
  && variable.reduce(
    (accumulator, elementOfArray) => accumulator && isObject(elementOfArray),
    true,
  )
)

exports.default = isCollection
