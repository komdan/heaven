const { equal } = require('assert')
const assertString = require('./assertString').default
const isFrenchPhoneNumber = require('./isFrenchPhoneNumber').default

const assertFrenchPhoneNumber = (input, reservationModalFormattedPhoneNumberCountry, variableName = '') => {
  const errorMessage = `${variableName} should be a valid phone number and not: ${input}`

  assertString(input, variableName)
  equal(isFrenchPhoneNumber(input, reservationModalFormattedPhoneNumberCountry), true, errorMessage)
}

exports.default = assertFrenchPhoneNumber
