const {
  isUndefined,
} = require('lodash')

exports.default = isUndefined
