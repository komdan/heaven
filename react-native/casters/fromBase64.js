import base64 from 'react-native-base64'

const fromBase64 = string => base64.decode(string)

export default fromBase64
