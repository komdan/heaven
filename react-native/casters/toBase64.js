import base64 from 'react-native-base64'

const toBase64 = string => base64.encode(string)

export default toBase64
