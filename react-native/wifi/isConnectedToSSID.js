import WifiManager from 'react-native-wifi-reborn'

const isConnectedToSSID = async ssidInArgs => WifiManager.getCurrentWifiSSID()
  .then(
    ssid => ssid === ssidInArgs,
    () => false, // error
  )

export default isConnectedToSSID
