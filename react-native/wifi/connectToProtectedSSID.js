import WifiManager from 'react-native-wifi-reborn'
import {
  Alert,
} from 'react-native'
import {
  log,
  error,
  copyString,
} from '@heaven/react-native'

let nbRetry = 0
const MAX_RETRY = 10

const connectToProtectedSSID = async ({
  ssid,
  passphrase,
  t = string => string,
}) => WifiManager.connectToProtectedSSID(ssid, passphrase, false).then(
  () => {
    log(`wifi.js\\connectToProtectedSSID::Connected successfully to ${ssid}`)
  },
  async (e) => {
    error(`wifi.js\\connectToProtectedSSID::Connection FAILED: ${ssid}. ERROR FOLLOWS`)
    error(e)

    if (nbRetry < MAX_RETRY) {
      nbRetry++
      await connectToProtectedSSID(ssid, passphrase)
    } else {
      Alert.alert(
        t('autoWifiConnectionFailed'),
        `${t('tryToConnectManuallyToWifi')}\n\n${t('Wifi')}:\n${ssid}\n\n${t('passphrase')}:\n${passphrase}`,
        [
          { text: t('ok') },
          { text: t('copyPassphrase'), onPress: () => copyString(passphrase) },
        ],
      )
      nbRetry = 0
    }
  },
)

export default connectToProtectedSSID
