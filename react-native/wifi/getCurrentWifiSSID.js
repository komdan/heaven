import WifiManager from 'react-native-wifi-reborn'
import {
  error,
} from '@heaven/react-native'

let nbRetry = 0
const MAX_RETRY = 3

const getCurrentWifiSSID = async ({
  maxRetry = MAX_RETRY,
} = {}) => {
  try {
    const currentWifiSSID = await WifiManager.getCurrentWifiSSID()
    return currentWifiSSID
  } catch (e) {
    error(`wifi.js\\getCurrentWifiSSID::Connection FAILED. ERROR FOLLOWS`)
    error(e)

    if (nbRetry < maxRetry) {
      nbRetry++
      const currentWifiSSID = await WifiManager.getCurrentWifiSSID()
      return currentWifiSSID
    }
    nbRetry = 0
    throw e
  }
}

export default getCurrentWifiSSID
