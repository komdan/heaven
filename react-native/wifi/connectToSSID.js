import WifiManager from 'react-native-wifi-reborn'
import {
  error,
} from '@heaven/react-native'

let nbRetry = 0
const MAX_RETRY = 3

const connectToSSID = async ({
  ssid = '',
  maxRetry = MAX_RETRY,
} = {}) => {
  try {
    await WifiManager.connectToSSID(ssid)
  } catch (e) {
    error(`wifi.js\\connectToSSID::Connection FAILED: ${ssid}. ERROR FOLLOWS`)
    error(e)

    if (nbRetry < maxRetry) {
      nbRetry++
      await connectToSSID(ssid)
    } else {
      nbRetry = 0
      throw e
    }
  }
}

export default connectToSSID
