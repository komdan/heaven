import {
  Alert,
} from 'react-native'

import {
  localCache,
} from '@heaven/react-native'

import connectToProtectedSSID from './connectToProtectedSSID'

const requestWifiConnection = async ({ ssid, passphrase, t }) => {
  const { stopRequesting } = await localCache.get('wifiConnection')

  if (!stopRequesting) {
    Alert.alert(
      t('slowNetwork'),
      `${t('tryWifiAutoConnectTo')} '${ssid}'`,
      [
        { text: t('no') },
        { text: t('neverAskAgain'), onPress: () => localCache.set('wifiConnection', { stopRequesting: true }) },
        { text: t('ok'), onPress: () => connectToProtectedSSID(ssid, passphrase) },
      ],
    )
  }
}

export default requestWifiConnection
