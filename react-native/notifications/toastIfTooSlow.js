import {
  isUndefined,
} from '@heaven/js'
import showFlashMessage from './showFlashMessage'

const UNUSUAL_LOADING_TIME = 7 * 1000

const toastIfTooSlow = async (promise, message) => {
  let promiseResult
  const toastAfterDelayIfPromiseIsUnresolved = () => new Promise(resolve => setTimeout(
    () => {
      if (isUndefined(promiseResult)) showFlashMessage(message)
      resolve(true)
    },
    UNUSUAL_LOADING_TIME,
  ))

  toastAfterDelayIfPromiseIsUnresolved()
  promiseResult = await promise
  return promiseResult
}

export default toastIfTooSlow
