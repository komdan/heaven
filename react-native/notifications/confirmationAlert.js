import {
  Alert,
} from 'react-native'

const confirmationAlert = ({
  confirmTitle,
  confirmMessage,
  onPress,
  t,
}) => Alert.alert(
  t(confirmTitle),
  t(confirmMessage),
  [
    { text: t('confirmAlertNo'), style: 'destructive' },
    { text: t('confirmAlertYes'), onPress },
  ],
)

export default confirmationAlert
