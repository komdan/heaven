import {
  StatusBar,
  Platform,
} from 'react-native'
import {
  showMessage,
} from 'react-native-flash-message'
import executeAfterAnimation from '../alerts/executeAfterAnimation'
import Alertalert from '../alerts/Alertalert'

const {
  FLASH_MESSAGE_BACKGROUND_COLOR,
  FLASH_MESSAGE_TEXT_COLOR,
  FLASH_MESSAGE_POSITION,
} = process.env

const showFlashMessage = (
  title,
  message = '',
  {
    onPress: onPressInArgs,
    position: positionInArgs,
    duration: durationInArgs,
    autoHide = true,
  } = {},
) => {
  const onPress = onPressInArgs || null
  const position = positionInArgs || (
    Platform.OS === 'android'
      ? (
        {
          top: StatusBar.currentHeight,
          left: 0,
          right: 0,
        }
      )
      : 'top'
  )
  const duration = durationInArgs || 'long'

  let flashMessageDuration
  if (duration === 'veryShort') { flashMessageDuration = 1000 }
  if (duration === 'short') { flashMessageDuration = 4000 }
  if (duration === 'long') { flashMessageDuration = 8000 }

  executeAfterAnimation(() => {
    showMessage({
      onPress,
      // hideOnPress: !onPress,
      hideOnPress: true,
      message: title,
      description: message,
      type: 'default',
      floating: 'true',
      duration: flashMessageDuration,
      backgroundColor: FLASH_MESSAGE_BACKGROUND_COLOR || 'black',
      color: FLASH_MESSAGE_TEXT_COLOR || 'white',
      position: FLASH_MESSAGE_POSITION || position,
      autoHide,
    })
  })
}

export default showFlashMessage
