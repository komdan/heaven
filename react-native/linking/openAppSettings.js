import {
  Linking,
  Platform,
} from 'react-native'

const openAppSettings = () => {
  if (Platform.OS === 'ios') {
    Linking.openURL('app-settings:')
  } else {
    Linking.openSettings()
  }
}

export default openAppSettings
