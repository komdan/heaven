import {
  Linking,
} from 'react-native'

const callPhoneNumber = (phoneNumber) => {
  Linking.openURL(`tel:${phoneNumber}`)
}

export default callPhoneNumber
