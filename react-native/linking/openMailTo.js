import {
  Linking,
} from 'react-native'

const openMailTo = (toAddress) => {
  Linking.openURL(`mailto:${toAddress}`)
}

export default openMailTo
