import {
  Linking,
} from 'react-native'

const openWebUrl = (URL) => {
  Linking.openURL(URL)
}

export default openWebUrl
