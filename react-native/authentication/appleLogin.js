// source: https://github.com/invertase/react-native-apple-authentication/blob/master/docs/FIREBASE.md

import firebase from '@react-native-firebase/app'
import { appleAuth } from '@invertase/react-native-apple-authentication'
import {
  error,
} from '@heaven/react-native'
// import {
//   showFlashMessage,
// } from '@heaven/react-native'

const appleLogin = async () => {
  try {
    if (!appleAuth.isSupported) return {}

    const appleAuthRequestResponse = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    })

    const {
      identityToken,
      nonce,
      fullName: {
        givenName = '',
        familyName = '',
        // middleName = '',
        // namePrefix = '',
        // nameSuffix = '',
        // nickname = '',
      } = {},
    } = appleAuthRequestResponse

    if (!identityToken) { throw new Error('identityToken is null, try again') }

    const appleCredential = firebase.auth.AppleAuthProvider.credential(identityToken, nonce)
    const firebaseUserCredential = await firebase.auth().signInWithCredential(appleCredential)
    const appleDisplayName = (givenName || familyName)
      ? `${givenName} ${familyName}`
      : ''

    return {
      ...firebaseUserCredential,
      appleDisplayName,
    }
  } catch (e) {
    error(new Error('appleLogin.js\\ERROR FOLLOWS'))
    error(e)
    // showFlashMessage(e.message)
    return {}
  }
}

export default appleLogin
