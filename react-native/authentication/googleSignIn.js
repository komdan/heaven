import {
  GoogleSignin,
  statusCodes,
} from '@react-native-community/google-signin'
import {
  // log,
  error,
} from '@heaven/react-native'

const googleSignIn = async () => {
  try {
    // log('SignUp.js\\googleSignIn()::EXECUTED')
    // this.setState({ googleSignInInProgress: true })
    await GoogleSignin.hasPlayServices()
    // log('SignUp.js\\googleSignIn()::GoogleSignin.hasPlayServices()::DONE')

    // const userInfo = await GoogleSignin.signIn()
    await GoogleSignin.signIn()

    // log('SignUp.js\\googleSignIn()::userInfo:', userInfo)
    // this.setState({ googleSignInInProgress: false })
    // this.setState({ userInfo })

    // TODO: signin user with firebase
  } catch (err) {
    error(new Error('SignUp.js\\googleSignIn()::ERROR FOLLOWS'), err)
    error(err)
    if (err.code === statusCodes.SIGN_IN_CANCELLED) {
      // user cancelled the login flow
    } else if (err.code === statusCodes.IN_PROGRESS) {
      // operation (e.g. sign in) is in progress already
    } else if (err.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      // play services not available or outdated
    } else {
      // some other error happened
    }
  }
}

export default googleSignIn
