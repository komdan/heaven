import {
  AccessToken,
  LoginManager,
} from 'react-native-fbsdk-next'
import firebase from '@react-native-firebase/app'

import {
  // log,
  error,
  showFlashMessage,
} from '@heaven/react-native'

const facebookLogin = async () => {
  try {
    console.log('🗽🗽🗽🗽🗽🗽🗽🗽🗽🗽🗽')
    const {
      isCancelled,
      // grantedPermissions,
    } = await LoginManager.logInWithPermissions(['public_profile', 'email'])

    if (isCancelled) {
      // handle this however suites the flow of your app
      throw new Error('User cancelled request')
    }
    console.log('🚦🚦🚦🚦🚦🚦🚦🚦🚦🚦🚦🚦')

    // log(`Login success with permissions: ${grantedPermissions.toString()}`)

    // get the access token
    const data = await AccessToken.getCurrentAccessToken()

    if (!data) {
      // handle this however suites the flow of your app
      throw new Error('Something went wrong obtaining the users access token')
    }

    // create a new firebase credential with the token
    const credential = firebase.auth.FacebookAuthProvider.credential(data.accessToken)

    // login with credential
    const firebaseUserCredential = await firebase.auth().signInWithCredential(credential)

    // log(JSON.stringify(firebaseUserCredential.user.toJSON()))

    return firebaseUserCredential
  } catch (e) {
    error(new Error('socialAuthentication.js\\facebookLogin::ERROR FOLLOWS'))
    error(e)
    showFlashMessage(e.message)

    return {}
  }
}

export default facebookLogin
