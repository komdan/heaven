import {
  LoginManager,
} from 'react-native-fbsdk-next'

const facebookLogout = () => LoginManager.logOut()

export default facebookLogout
