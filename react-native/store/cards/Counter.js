/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native & Expo
import React from 'react'
import {
  TouchableOpacity,
  View,
  StyleSheet,
  // Platform,
} from 'react-native'

// Community
import {
  Icon,
  Text,
} from 'react-native-elements'
import Lottie from 'lottie-react-native'

// Helpers & Config
import { myTheme } from '@styles'

const confettis = require('@assets/animation/confettis.json')

// ======================== Main component ========================
class Counter extends React.Component {
  decrement = () => {
    const {
      onChange = () => { },
      count = 0,
      incrementAmount = 1,
      minimum = 1,
      formatCounterNumber = (counter) => +(counter).toFixed(2),
    } = this.props

    const newValue = formatCounterNumber(
      count - incrementAmount >= minimum
        ? count - incrementAmount
        : count,
    )

    onChange(newValue)
  }

  increment = () => {
    const {
      onChange = () => { },
      count = 0,
      incrementAmount = 1,
      formatCounterNumber = (counter) => +(counter).toFixed(2),
    } = this.props

    const newValue = formatCounterNumber(
      count + incrementAmount,
    )

    onChange(newValue)
    this.playAnimation()
  }

  playAnimation() {
    if (this.animationRef) {
      this.animationRef.reset()
      this.animationRef.play()
    }
  }

  render() {
    const {
      animated,
      count = 0,
      suffix = '',
      title = '',
      containerStyle,
      formatCounterString = (counter) => (counter).toFixed(2),
    } = this.props

    const countString = formatCounterString(count)

    return (
      <View style={StyleSheet.flatten([styles.container, containerStyle])}>
        {/* COUNTER TITLE */}
        {!!title && (
          <Text style={styles.title}>{title}</Text>
        )}

        {/* COUNTER */}
        <View style={styles.wrapper}>

          {/* MINUS ICON */}
          <TouchableOpacity
            style={[styles.minusButton, { flex: 2 }]}
            onPress={() => this.decrement()}
            testID="minusOneTouchableOpacity"
          >
            <Icon
              name="remove"
              size={35}
              color={styles.secondaryColor}
              type="material"
            />
          </TouchableOpacity>

          {/* ITEM COUNTER */}
          <Text style={[styles.counter, { flex: 1 }]}>
            {`${countString}${suffix}`}
          </Text>

          {/* PLUS ICON */}
          <TouchableOpacity
            style={[styles.plusButton, { flex: 2 }]}
            onPress={() => this.increment()}
            testID="plusOneTouchableOpacity"
          >
            <Icon
              name="add"
              size={40}
              color={styles.primaryColor}
              type="material"
            />
            {!!animated && (
              <View style={styles.animationContainer}>
                <Lottie
                  loop={false}
                  ref={(ref) => { this.animationRef = ref }}
                  style={styles.animation(150)}
                  source={confettis}
                  speed={2}
                />
              </View>
            )}
          </TouchableOpacity>
        </View>
      </View>
    )
  }
}

// ======================== Styles ========================
const styles = {
  ...myTheme,
  ...StyleSheet.create({
    container: {
      backgroundColor: 'transparent',
      width: '100%',
      marginTop: 5,
      paddingBottom: 10,
    },
    wrapper: {
      flexDirection: 'row',
      alignItems: 'center',
    },
    minusButton: {
      alignItems: 'center',
    },
    plusButton: {
      alignItems: 'center',
    },
    counter: {
      textAlign: 'center',
      fontFamily: myTheme.boldFont,
      fontSize: myTheme.bigFontSize,
      color: myTheme.black,
    },
    title: {
      textAlign: 'center',
      fontFamily: myTheme.upperCaseFont,
      textTransform: 'uppercase',
      fontSize: myTheme.h3FontSize,
      color: myTheme.black,
      marginTop: 0,
    },
    animationContainer: {
      position: 'absolute',
      top: -55,
      // left: '50%',
      // marginTop: -50,
      // marginLeft: -50,
    },
  }),
}

// ======================== Exports ========================
export default Counter
