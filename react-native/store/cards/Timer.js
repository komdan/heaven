import React, {
  useEffect,
  // forwardRef,
  Fragment,
} from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native'

import {
  myTheme,
} from '@styles'
import {
  msToString,
} from '@heaven/js'

import CenteredText from '../typography/CenteredText'

const Timer = ({
  value, // in ms
  onEnd,
  play,
  inverted,
  title,
  onTimerAboutToEnd,
  onTimerLastSecond,
  onTimerValueChange,
}) => {
  useEffect(() => {
    const timeout = setTimeout(
      () => play && (
        value > 0
          ? onTimerValueChange(value - 1000)
          : onEnd()
      ),
      1000,
    )
    return () => clearTimeout(timeout)
  }, [value, play]) // eslint-disable-line

  useEffect(() => {
    if (value === 7000) onTimerAboutToEnd()
    if (value === 1000) onTimerLastSecond()
  }, [value]) // eslint-disable-line

  return !!value && (
    <Fragment>
      {!!title && (
        <CenteredText
          h1
          h1Style={styles.titleStyle(inverted)}
        >
          {title}
        </CenteredText>
      )}
      <View style={styles.container}>
        <View style={styles.wrapper}>
          <Text
            style={inverted ? styles.invertedCounterText : styles.valueText}
          >
            {msToString(value)}
          </Text>
        </View>
      </View>
    </Fragment>
  )
}

const styles = {
  titleStyle: inverted => ({
    fontSize: styles.giganticFontSize,
    color: inverted ? styles.white : styles.primaryColor,
  }),
  ...myTheme,
  ...StyleSheet.create({
    container: {
      flex: 1,
    },
    wrapper: {
      alignItems: 'center',
    },
    valueText: {
      alignItems: 'center',
      color: myTheme.primaryColor,
      fontSize: myTheme.giganticallyHugeFontSize,
    },
    invertedCounterText: {
      alignItems: 'center',
      color: myTheme.white,
      fontSize: myTheme.giganticallyHugeFontSize,
    },
  }),
}

export default Timer
// export default forwardRef(Timer)
