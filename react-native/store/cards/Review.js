/*
  Created by Matthieu MARIE-LOUISE
*/
// React-native
import React from 'react'
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Platform,
} from 'react-native'

// Community
// import StarRating from 'react-native-star-rating'

// Custom components
import { withTranslation } from 'react-multi-lang'

// Helpers & Config
import { myTheme } from '@styles'

import ScoreCircle from './ScoreCircle'

// ======================== Main Component ========================
const Review = (props) => {
  const {
    displayMore,
    onMoreReviewsPress,
    t,
    review: {
      rating,
      relative_time_description: relativeTimeDescription,
      text,
      author_name: authorName,
    },
  } = props

  return (
    <View>
      {/* SCORE CIRCLE */}
      <ScoreCircle
        score={rating}
        containerStyle={styles.scoreCircle}
      />

      {/* REVIEW CONTENT */}
      <View style={styles.contentContainer}>
        <View style={styles.ratingAndTimeDescription}>
          {/*
          <StarRating
            {...starRatingStyle}
            rating={rating}
            fullStarColor={styles.primaryColor}
          />
          */}
          <View style={{ flex: 3 }}>
            <Text style={styles.timeDescription}>{` - ${relativeTimeDescription}`}</Text>
          </View>
        </View>
        <Text style={styles.textContent}>{text}</Text>
        <Text style={styles.authorName}>{authorName}</Text>
        {!!displayMore && (
          <TouchableOpacity onPress={onMoreReviewsPress}>
            <View style={styles.moreReviewsContainer}>
              <Text style={styles.moreReviews}>{t('moreReviews')}</Text>
            </View>
          </TouchableOpacity>
        )}
      </View>
    </View>
  )
}

// ======================== Helpers ========================
const styles = {
  ...myTheme,
  ...StyleSheet.create({
    scoreCircle: {
      ...Platform.select({
        ios: {
          zIndex: 1,
        },
        android: {
          elevation: 2,
        },
      }),
    },
    contentContainer: {
      backgroundColor: myTheme.black,
      borderWidth: 1,
      paddingTop: 40,
      paddingLeft: 10,
      paddingRight: 10,
      paddingBottom: 10,
      marginTop: -25,
      marginLeft: 15,
      marginRight: 15,
      marginBottom: 15,
      borderRadius: 5,
      borderColor: myTheme.greyD,
      ...Platform.select({
        android: {
          elevation: 1,
        },
        default: {
          shadowColor: 'rgba(0,0,0, .2)',
          shadowOffset: { height: 0, width: 0 },
          shadowOpacity: 1,
          shadowRadius: 1,
          backgroundColor: myTheme.black, // used with shadow
        },
      }),
    },
    ratingAndTimeDescription: {
      flex: 1,
      flexDirection: 'row',
    },
    timeDescription: {
      color: myTheme.grey2,
    },
    textContent: {
      fontFamily: myTheme.boldFont,
      color: myTheme.grey2,
      textAlign: 'left',
    },
    authorName: {
      marginBottom: 5,
      fontFamily: myTheme.boldFont,
      color: myTheme.grey3,
      textAlign: 'right',
    },
    moreReviewsContainer: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      marginLeft: 5,
      marginRight: 5,
      // fontFamily: myTheme.regularFont,
      backgroundColor: myTheme.primaryColor,
      borderWidth: 1,
      borderColor: myTheme.greyD,
      marginTop: -2,
      borderRadius: 5,
    },
    moreReviews: {
      fontSize: myTheme.regularFontSize,
      color: myTheme.black,
      marginTop: 10,
      marginBottom: 10,
    },
  }),
}

// const starRatingStyle = {
//   disabled: false,
//   maxStars: 5,
//   starSize: 15,
// }

// ======================== Exports ========================
export default withTranslation(Review)
