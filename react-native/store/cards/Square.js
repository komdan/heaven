/*
* Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native imports
import React from 'react'
import {
  Text,
  View,
  Dimensions,
  Platform,
  StyleSheet,
  // Image,
} from 'react-native'
import LinearGradient from 'react-native-linear-gradient'

// Community
import Image from 'react-native-fast-image'
import {
  Divider,
  Badge,
} from 'react-native-elements'
// Helpers & Config
import { withTranslation } from 'react-multi-lang'

import { myTheme } from '@styles'

// ======================== Globals ========================
// Do not display distance greater than 3km
const MAX_DISPLAYING_DISTANCE = 3000

// ======================== Main Component ========================
const Square = ({
  halfSized = false,
  item,
  distance: distanceInProps = null,
  t,
  badgeText = undefined,
}) => {
  const {
    thumbnail,
    name: itemName,
    // TODELETE
    image,
  } = item

  const name = t(itemName || '')
  const distance = (!!distanceInProps)
    && (distanceInProps <= MAX_DISPLAYING_DISTANCE)
    && (
      (distanceInProps >= 1000)
        ? `${Math.round(distanceInProps / 1000)}km`
        : `${distanceInProps}m`
    )

  return (
    <View style={halfSized ? styles.tagContainer : styles.barContainer}>
      <View style={halfSized ? styles.tagWrapper : styles.barWrapper}>
        <Image
          source={{ uri: thumbnail || image }}
          style={halfSized
            ? styles.tagImage
            : styles.barImage}
        >
          <LinearGradient
            colors={styles.shadowGradientColors}
            locations={styles.shadowGradientLocations}
            style={{ flex: 1 }}
          />
        </Image>
        {!!badgeText && (
          <Badge value={badgeText} {...styles.badge()} />
        )}
      </View>
      <Text
        style={styles.squareTitle}
        numberOfLines={2}
      >
        {name}
      </Text>

      {!!distance && (
        <Text style={styles.distanceText}>
          {`${distance}`}
        </Text>
      )}
      <Divider style={styles.invisibleDivider} />
    </View>
  )
}

// ======================== Local helpers ========================
const { width } = Dimensions.get('window')
const barWidth = width * 68 / 100
const barHeight = width * 57 / 100
const tagWidth = width * 30 / 100
const borderRadius = 10

const styles = {
  ...myTheme,
  shadowGradientColors: [
    'transparent',
    'transparent',
    'transparent',
  ],
  shadowGradientLocations: [
    0,
    0.50,
    1,
  ],
  ...StyleSheet.create({
    tagContainer: {
      flex: 1,
      width: tagWidth,
      margin: 10,
    },
    tagWrapper: {
      borderRadius,
      margin: 8,
      ...Platform.select({
        ios: {
          shadowColor: myTheme.black,
          shadowOpacity: 0.2,
          shadowRadius: 8,
          shadowOffset: { width: 0, height: 3 },
          backgroundColor: myTheme.white, // used with shadow
        },
        android: {
          // elevation: 4,
        },
      }),
    },
    tagImage: {
      borderRadius,
      width: tagWidth * 0.95,
      height: tagWidth * 1.5 * 0.95,
    },
    barContainer: {
      flex: 1,
      width: barWidth,
      margin: 8,
    },
    barWrapper: {
      borderRadius,
      margin: 10,
      ...Platform.select({
        ios: {
          shadowColor: myTheme.black,
          shadowOpacity: 0.2,
          shadowRadius: 8,
          shadowOffset: { width: 0, height: 3 },
          backgroundColor: myTheme.white, // used with shadow
        },
        android: {
          // elevation: 1,
        },
      }),
    },
    barImage: {
      width: barWidth * 0.95,
      height: barHeight * 0.95,
      borderRadius,
    },
    invisibleDivider: {
      height: 15,
      backgroundColor: 'transparent',
    },
    squareTitle: {
      marginTop: 3,
      marginLeft: 10,
      fontFamily: myTheme.boldFont,
      fontSize: myTheme.small,
      textAlign: 'center',
      color: myTheme.black,
    },
    distanceText: {
      marginTop: 3,
      marginLeft: 10,
      fontFamily: myTheme.lightFont,
      textAlign: 'center',
    },
  }),
}

// ======================== Exports ========================
export default withTranslation(Square)
