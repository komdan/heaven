/*
* Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React & React Native & Expo
import React from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native'
// Helpers & config
// import StarRating from 'react-native-star-rating'
import { myTheme } from '@styles'

// Custom components

// ======================== Main Component ========================
const Rating = ({
  score = undefined,
  showText,
  subtitle,
  containerStyle,
}) => !!score && (
  <View
    style={StyleSheet.flatten([
      styles.container,
      containerStyle,
    ])}
  >
    <View style={styles.filler} />
    <View style={styles.wrapper}>
      {!!showText && (
        <View style={styles.titleContainer}>
          <Text style={styles.scoreText}>{score}</Text>
          <Text style={styles.totalText}>/5</Text>
        </View>
      )}
      {/*
      <StarRating
        disabled={false}
        maxStars={5}
        rating={score}
        fullStarColor={myTheme.secondaryColor}
        starSize={20}
      />
      */}

      {!!subtitle && (
        <Text style={styles.subtitle}>{subtitle}</Text>
      )}
    </View>
    <View style={styles.filler} />
  </View>
)

// ======================== Local Helpers ========================
const styles = {
  ...myTheme,
  filler: {
    flex: 1,
  },
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  wrapper: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'transparent',
    borderColor: 'transparent',
    borderRadius: 5,
    borderWidth: 1,
  },
  titleContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  scoreText: {
    fontSize: 20,
    fontFamily: myTheme.boldFont,
    color: myTheme.black,
  },
  totalText: {
    fontSize: 10,
    fontFamily: myTheme.regularFont,
  },
  subtitle: {
    color: myTheme.greyA,
  },
}

// ======================== Exports ========================
export default Rating
