/*
  Created by Matthieu MARIE-LOUISE
*/
// React-native
import React from 'react'
import {
  View,
  Text,
  StyleSheet,
} from 'react-native'

// Helpers & Config
import { myTheme } from '@styles'

// ======================== Main Component ========================
const ScoreCircle = ({ score, containerStyle }) => (
  <View style={StyleSheet.flatten([styles.container, containerStyle])}>
    <View style={styles.scoreCircle}>
      <Text style={styles.scoreText}>{score}</Text>
      <Text style={styles.totalText}>/5</Text>
    </View>
  </View>
)

// ======================== Styles ========================
const styles = {
  container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  scoreCircle: {
    flexDirection: 'row',
    borderRadius: 25,
    borderWidth: 1,
    borderColor: myTheme.primaryColor,
    marginTop: 20,
    width: 50,
    height: 50,
    backgroundColor: myTheme.lightgrey,
    alignItems: 'center',
    justifyContent: 'center',
  },
  scoreText: {
    fontSize: 25,
    textAlign: 'center',
    color: myTheme.greyE,
  },
  totalText: {
    fontSize: 14,
    color: 'grey',
    textAlign: 'center',
  },
}

// ======================== Exports ========================
export default ScoreCircle
