import React from 'react'
import * as Animatable from 'react-native-animatable'

const {
  IS_TESTING,
} = process.env

const Pulse = ({ children, iterationDelay, ...props }) => (
  <Animatable.View
    useNativeDriver
    animation="pulse"
    iterationCount="infinite"
    iterationDelay={IS_TESTING ? 1000 : iterationDelay}
    {...props}
  >
    {children}
  </Animatable.View>
)

export default Pulse
