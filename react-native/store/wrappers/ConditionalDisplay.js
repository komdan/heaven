import React from 'react'
import {
  View,
} from 'react-native'

const ConditionalDisplay = ({
  condition = true,
  children = <View />,
}) => (condition ? children : <View />)

export default ConditionalDisplay
