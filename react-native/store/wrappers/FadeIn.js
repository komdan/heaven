import React from 'react'
import * as Animatable from 'react-native-animatable'

const FadeIn = ({ children, ...props }) => (
  <Animatable.View
    useNativeDriver
    animation="fadeIn"
    {...props}
  >
    {children}
  </Animatable.View>
)

export default FadeIn
