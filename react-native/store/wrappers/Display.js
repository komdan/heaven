import React from 'react'
import RNDisplay from 'react-native-display'

const Display = ({
  children,
  ...props
}) => (
  <RNDisplay enter="fadeIn" exit="fadeOut" {...props}>
    {children}
  </RNDisplay>
)

export default Display
