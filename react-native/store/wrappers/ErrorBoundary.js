import React, { Component } from 'react'
import { View } from 'react-native'

import {
  error,
} from '@heaven/react-native'

class ErrorBoundary extends Component {
  state = { // eslint-disable-line
    hasError: false,
  }

  // eslint-disable-next-line
  static getDerivedStateFromError(err) {
    // Update state so the next render will show the fallback UI.
    error(err)
    return { hasError: true }
  }

  // eslint-disable-next-line
  componentDidCatch(err, info) {
    // log the error to our server with loglevel
    error(err, info)
  }

  render() {
    const { hasError } = this.state
    const { children } = this.props

    if (hasError) {
      // You can render any custom fallback UI
      return <View />
    }

    return children
  }
}

export default ErrorBoundary
