import React, {
  useState,
} from 'react'
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native'
import {
  Icon,
} from 'react-native-elements'

import {
  keyExtractor,
} from '@heaven/js'
import {
  myTheme,
} from '@styles'

const styles = {
  ...myTheme,
  masterTitleContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  masterTitle: {
    marginLeft: 15,
    fontSize: 24,
    fontFamily: myTheme.boldFont,
    color: myTheme.primaryColor,
  },
  rightIconContainer: {
    padding: 10,
    paddingLeft: 50,
  },
  rightCategoryTitle: {
    marginRight: 15,
    fontSize: myTheme.biggishFontSize,
    fontFamily: myTheme.boldFont,
    color: myTheme.primaryColor,
  },
}

const MasterTitle = ({
  title,
  rightTitle = '',
  rightIcon,
  onRightTitlePress = () => null,
  onRightIconPress = () => null,
  data,
  renderItem,
}) => {
  const [isRightIconLoading, setIsRightIconLoading] = useState(false)

  const onRightIconPressLoader = async () => {
    setIsRightIconLoading(true)
    await onRightIconPress()
    setIsRightIconLoading(false)
  }

  return (
    data.length
      ? (
        <View style={{ flex: 1 }}>
          <View style={styles.masterTitleContainer}>
            <Text style={styles.masterTitle}>{title}</Text>
            {!!rightIcon && !!rightIcon.name && !isRightIconLoading && (
              <TouchableOpacity
                onPress={onRightIconPressLoader}
                style={styles.rightIconContainer}
              >
                <Icon {...rightIcon} />
              </TouchableOpacity>
            )}
            {!!rightIcon && !!rightIcon.name && isRightIconLoading && (
              <ActivityIndicator />
            )}
            {!!rightTitle && (
              <Text style={styles.rightCategoryTitle} onPress={onRightTitlePress}>{rightTitle}</Text>
            )}
          </View>
          <FlatList
            showsHorizontalScrollIndicator={false}
            data={data}
            keyExtractor={keyExtractor}
            renderItem={renderItem}
            horizontal
          />
        </View>
      )
      : (
        <View />
      )
  )
}

export default MasterTitle
