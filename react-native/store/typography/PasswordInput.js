import React, {
  useState,
} from 'react'
import {
  Icon,
} from 'react-native-elements'
import {
  myTheme,
} from '@styles'
import Input from './Input'

const PasswordInput = ({ testID, ...otherProps }) => {
  const [isPasswordShowing, setisPasswordShowing] = useState(false)
  const rightIcon = isPasswordShowing
    ? myTheme.eyeIcon()
    : myTheme.eyeOffIcon()

  return (
    <Input
      testID={testID}
      secureTextEntry={!isPasswordShowing}
      autoCorrect={false}
      rightIcon={(
        <Icon
          onPress={() => setisPasswordShowing(!isPasswordShowing)}
          {...rightIcon}
        />
      )}
      {...otherProps}
    />
  )
}
export default PasswordInput
