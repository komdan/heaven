import React from 'react'
import {
  Text,
} from 'react-native'

const Strong = ({ children, ...props }) => (
  <Text style={{ fontWeight: 'bold' }} {...props}>{children}</Text>
)

export default Strong
