import React from 'react'
import {
  Text,
} from 'react-native'

const Underline = ({ children, ...props }) => (
  <Text style={{ textDecorationLine: 'underline' }} {...props}>{children}</Text>
)

export default Underline
