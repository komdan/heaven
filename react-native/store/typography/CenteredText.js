/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native
import React from 'react'
import {
  StyleSheet,
  // Text as RNText,
} from 'react-native'
// Community
import {
  Text,
} from 'react-native-elements'

import {
  populateStyle,
} from '@styles'

// ======================== Main Component ========================
const CenteredText = ({
  children,
  style: styleInArgs = {},
  numberOfLines = 2,
  ...props
}) => {
  const style = populateStyle(styleInArgs, props)
  return (
    <Text
      style={StyleSheet.flatten([{ textAlign: 'center' }, style])}
      h1Style={StyleSheet.flatten([{ textAlign: 'center' }, style])}
      h2Style={StyleSheet.flatten([{ textAlign: 'center' }, style])}
      h3Style={StyleSheet.flatten([{ textAlign: 'center' }, style])}
      h4Style={StyleSheet.flatten([{ textAlign: 'center' }, style])}
      numberOfLines={numberOfLines}
      {...props}
    >
      {children}
    </Text>
  )
}

// ======================== Exports ========================
export default CenteredText
