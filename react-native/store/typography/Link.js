import React, {
  Fragment,
} from 'react'
import {
  TouchableOpacity,
  Linking,
} from 'react-native'
import {
  withTranslation,
} from 'react-multi-lang'

import CenteredText from './CenteredText'

const Link = ({
  to,
  name: nameInArgs,
  children,
  label = '',
  t,
}) => {
  const name = nameInArgs || to
  const onPress = to
    ? () => Linking.openURL(to)
    : () => null

  return (
    <TouchableOpacity onPress={onPress}>
      {children}
      {!children && (
        <Fragment>
          {!!label && (<CenteredText h4>{t(label)}</CenteredText>)}
          <CenteredText bold>{t(name)}</CenteredText>
        </Fragment>
      )}
    </TouchableOpacity>
  )
}

export default withTranslation(Link)
