import React, {
  useCallback,
  forwardRef,
} from 'react'
import {
  TextInput,
} from 'react-native'
import {
  Input as RNEInput,
} from 'react-native-elements'

const Input = ({ testID, ...otherProps }) => {
  const MyTextInput = useCallback((props) => (
    <TextInput
      {...props}
      testID={testID}
    />
  ), []) // eslint-disable-line

  return (
    <RNEInput
      {...otherProps}
      inputComponent={MyTextInput}
    />
  )
}
// export default forwardRef(Input) // was like this before seeing a warning "forwardRef" takes 2 args..
export default forwardRef((props, ref) => (<Input {...props} ref={ref} />)) // should be this ?
// export default Input // if it works like this, let's simplify
