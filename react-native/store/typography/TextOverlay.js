import React from 'react'
import {
  Text,
} from 'react-native'

import {
  screenHeight,
} from '@styles'

const TextOverlay = ({
  text,
}) => (
  <Text style={{
    backgroundColor: '#000000CC',
    color: 'white',
    padding: 25,
    position: 'absolute',
    top: screenHeight / 2,
    // top: 30,
    // left: 0,
    // bottom: 0,
    textAlign: 'center',
    fontSize: 18,
  }}
  >
    {text}
  </Text>
)

export default TextOverlay
