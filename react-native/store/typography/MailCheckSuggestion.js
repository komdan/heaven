import React from 'react'
import MailCheck from 'react-mailcheck'
import {
  Text,
} from 'react-native-elements'
import {
  useTranslation,
} from 'react-multi-lang'
import {
  myTheme,
} from '@styles'

const MailCheckSuggestion = ({
  email = '',
} = {}) => {
  const t = useTranslation()

  return (
    <MailCheck email={email}>
      {emailSuggestion => !!emailSuggestion && !!emailSuggestion.full && (
        <Text style={{ fontSize: 13, marginLeft: 10, textAlign: 'right', color: myTheme.orange }}>
          {'⚠️'} {t('Did you mean')} &quot;{emailSuggestion.full}&quot;
        </Text>
      )}
    </MailCheck>
  )
}

export default MailCheckSuggestion
