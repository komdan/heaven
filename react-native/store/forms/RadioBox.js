import React, {
  useState,
} from 'react'
import {
  View,
} from 'react-native'
import {
  CheckBox,
} from 'react-native-elements'

import {
  myTheme,
} from '@styles'

const RadioBox = ({
  containerStyle,
  items,
  onItemPress,
  checkedColor = myTheme.lime,
  selectedItem: selectedItemInProps = null,
}) => {
  const [selectedItem, setselectedItem] = useState(selectedItemInProps)
  const onPress = (title) => {
    if (selectedItem === title) {
      setselectedItem(null)
      onItemPress(null)
    } else {
      setselectedItem(title)
      onItemPress(title)
    }
  }

  return (
    <View style={containerStyle}>
      {items.map((title, index) => (
        <CheckBox
          key={`${title}-${index}`}
          left
          iconRight
          checkedIcon="circle"
          uncheckedIcon="circle-o"
          checkedColor={checkedColor}
          title={title}
          titleProps={{
            style: {
              fontFamily: myTheme.regularFont,
              color: myTheme.black,
            },
          }}
          checked={selectedItem === title}
          onPress={() => onPress(title)}
          wrapperStyle={{ justifyContent: 'space-between' }}
          // key={keyExtractor(choice)}
          style={{ padding: 10 }}
          containerStyle={{
            backgroundColor: myTheme.grey2,
            borderColor: myTheme.grey3,
          }}
        />
      ))}
    </View>
  )
}

export default RadioBox
