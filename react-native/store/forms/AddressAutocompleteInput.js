import React, {
  useState,
  useEffect,
  useCallback,
  Fragment,
} from 'react'
import {
  GooglePlacesAutocomplete,
} from 'react-native-google-places-autocomplete'
import {
  isNonEmptyObject,
} from '@heaven/js'
import {
  OneMarkerMapView,
} from '@heaven/react-native'

const GOOGLE_MAPS_API_KEY = 'AIzaSyAJo-_l2Z2L7s3NVEIZENKwGGQPnodz08Y'

const AddressAutocompleteInput = ({
  placeholder = 'Rechercher une adresse',
  onAddressDetailsChange = () => { },
  addressDetails: addressDetailsInProps = {},
  onEditingStatusChange = () => { },
} = {}) => {
  const [isEditing, setisEditing] = useState(false)
  const [addressDetails, setAddressDetails] = useState(addressDetailsInProps)
  const [addressDisplayText, setaddressDisplayText] = useState('')

  const onAddressTextChange = (input) => {
    if (
      (input !== '') // When address changes via the ref, this function is not triggered. User is typing.
      || (input === '' && addressDisplayText !== '') // user is deleting everything. new input is empty, old addressDetails is not
    ) {
      setisEditing(true)
    }
    setaddressDisplayText(input)
  }

  const onAddressPress = (data, details = null) => {
    onAddressDetailsChange(details)
    setAddressDetails(details)
    setisEditing(false)
  }

  const googlePlacesAutocompleteRef = useCallback((current) => { // eslint-disable-line
    const currentFieldValue = current?.getAddressText()
    console.log('currentFieldValue', currentFieldValue)
    if (!isEditing && !currentFieldValue) {
      setaddressDisplayText(addressDetails?.formatted_address || '')
      current?.setAddressText(addressDetails?.formatted_address || '') // eslint-disable-line
    }
  })

  useEffect(() => {
    onEditingStatusChange(isEditing)
  }, [isEditing]) // eslint-disable-line

  return (
    <Fragment>
      <GooglePlacesAutocomplete
        ref={googlePlacesAutocompleteRef}
        autoFocus
        selectTextOnFocus
        placeholder={placeholder}
        onPress={onAddressPress}
        fetchDetails
        onNotFound={console.warn}
        onFail={console.warn}
        onTimeout={console.warn}
        query={{
          key: GOOGLE_MAPS_API_KEY,
          language: 'fr',
          components: 'country:fr',
        }}
        textInputProps={{
          onChangeText: onAddressTextChange,
          clearButtonMode: 'never',
          selectTextOnFocus: true,
        }}
        debounce={400}
        enableHighAccuracyLocation
        enablePoweredByContainer={false}
        // currentLocation
      />
      {/*
      */}
      {!!isNonEmptyObject(addressDetails) && !!addressDetails.geometry && !!addressDetails.geometry.location && (
      <OneMarkerMapView
        latitude={addressDetails.geometry.location.lat}
        longitude={addressDetails.geometry.location.lng}
        latitudeDelta={0.008}
        longitudeDelta={0.004}
        title="Votre addresse"
        height={120}
        mapViewProps={{
          pitchEnabled: true,
          rotateEnabled: true,
          scrollEnabled: true,
          zoomEnabled: true,
        }}
      />

      )}
    </Fragment>
  )
}

export default AddressAutocompleteInput
