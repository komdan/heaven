import React from 'react'
// import {
//   useTranslation,
// } from 'react-multi-lang'

import {
  Alert,
} from 'react-native'
import {
  Icon,
} from 'react-native-elements'
import {
  Input,
} from '@heaven/react-native'
import {
  myTheme,
} from '@styles'

const CustomInput = ({
  value,
  onChangeText,
  placeholder,
  label,
  alertTitle,
  alertMessage,
  rightIcon = {
    ...myTheme.infoIcon(myTheme.greyA),
  },
  ...rest
}) => (
  <Input
    value={value}
    onChangeText={onChangeText}
    placeholderTextColor={myTheme.grey}
    placeholder={placeholder}
    label={label}
    rightIcon={(alertTitle || alertMessage) && (
      <Icon
        {...rightIcon}
        onPress={() => Alert.alert(alertTitle, alertMessage)}
      />
    )}
    {...rest}
  />
)

export default CustomInput
