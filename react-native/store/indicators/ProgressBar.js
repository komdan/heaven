/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native
import React from 'react'
import {
  StyleSheet,
} from 'react-native'
// Community
import {
  View,
} from 'react-native-animatable'

// Helpers & Config
import { myTheme } from '@styles'

// ======================== Main component ========================
const ProgressBar = (props) => {
  const {
    progress,
    animated,
  } = props

  return (
    <View style={styles.container}>
      {animated && (
        <View
          useNativeDriver
          style={[styles.left, { flex: progress }]}
          animation="rubberBand"
          iterationCount="infinite"
          iterationDelay={2000}
        />
      )}
      {!animated && (
        <View
          useNativeDriver
          style={[styles.left, { flex: progress }]}
          animation="bounceInLeft"
        />
      )}
      <View
        useNativeDriver
        style={[styles.right, { flex: (1 - progress) }]}
        animation="bounceInRight"
      />
    </View>
  )
}

// ======================== Styles ========================
const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    margin: 2,
  },
  left: {
    backgroundColor: myTheme.primaryColor,
    height: 10,
    borderRadius: 20,
  },
  right: {
    backgroundColor: myTheme.transparent,
    height: 10,
    borderRadius: 20,
  },
})

// ======================== Exports ========================
export default ProgressBar
