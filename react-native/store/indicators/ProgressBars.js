/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native
import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'

// Custom components
import { keyExtractor } from '@heaven/js'

import ProgressBar from './ProgressBar'

// Helpers

// ======================== Main component ========================
const ProgressBars = ({ progress, step, totalSteps }) => (
  <View style={styles.container}>
    {[...Array(totalSteps)].map((_, index) => (
      <View
        style={styles.wrapper}
        key={keyExtractor(step, index)}
      >
        <ProgressBar
          progress={index === step ? progress : compare(step, index)}
          animated={index === step}
        />
      </View>
    ))}
  </View>
)

// ======================== Micro helpers ========================
const compare = (x, y) => ((x < y) ? 0 : 1)

// ======================== Styles ========================
const styles = StyleSheet.create({
  container: {
    margin: 5,
    flexDirection: 'row',
  },
  wrapper: {
    flex: 1,
  },
})

// ======================== Exports ========================
export default ProgressBars
