import React from 'react'
import {
  ActivityIndicator,
} from 'react-native'
import {
  myTheme,
} from '@styles'

const MyActivityIndicator = props => (
  <ActivityIndicator
    color={myTheme.black}
    {...props}
  />
)

export default MyActivityIndicator
