import React from 'react'
import {
  View,
  ActivityIndicator,
  StyleSheet,
} from 'react-native'
import {
  myTheme,
  screenWidth,
} from '@styles'

const TaintedIndicator = () => (
  <View style={styles.container}>
    <View style={styles.activityContainer}>
      <ActivityIndicator
        style={styles.activityIndicator}
        size="large"
        color={styles.white}
      />
    </View>
  </View>
)

const indicatorSize = 800
const containerSize = screenWidth / 5
const styles = {
  ...myTheme,
  ...StyleSheet.create({
    container: {
      position: 'absolute',
      width: '100%',
      height: '100%',
      zIndex: 1,
    },
    activityContainer: {
      position: 'absolute',
      width: containerSize,
      height: containerSize,
      zIndex: 1,
      borderRadius: 10,
      backgroundColor: myTheme.lightlyTainted,
      left: '50%',
      marginLeft: -containerSize / 2,
      top: '50%',
      marginTop: -containerSize / 2,
    },
    activityIndicator: {
      position: 'absolute',
      height: indicatorSize,
      width: indicatorSize,
      left: '50%',
      marginLeft: -indicatorSize / 2,
      top: '50%',
      marginTop: -indicatorSize / 2,
    },
  }),
}

export default TaintedIndicator
