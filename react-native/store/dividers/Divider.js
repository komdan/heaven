import React from 'react'
import {
  View,
  Text,
  Alert,
} from 'react-native'
import {
  Divider as RNEDivider,
} from 'react-native-elements'
import {
  LightDivider,
} from '@heaven/react-native'
import {
  myTheme,
} from '@styles'

const Divider = ({
  children,
  title = '',
  invisible = false,
  light = false,
  ...props
} = {}) => {
  try {
    if (invisible) return (<InvisibleDivider />)
    if (light) return (<LightDivider />)
    if (!title) return (<RNEDivider {...props}>{children}</RNEDivider>)
    return <DividerWithText title={title} />
  } catch (e) {
    Alert.alert(e.message)
    return <View />
  }
}

const InvisibleDivider = () => <Divider style={myTheme.invisibleDivider} />

const DividerWithText = ({ title }) => (
  <View
    style={{
      marginTop: 10,
      marginBottom: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    }}
  >
    <View
      style={{
        flex: 0.2,
      }}
    />
    <View
      style={{
        borderBottomColor: myTheme.grey,
        borderBottomWidth: 1,
        flex: 1,
      }}
    />
    <View
      style={{
        flex: 0.05,
      }}
    />
    <Text style={{
      textAlign: 'center',
      color: myTheme.grey,
      fontSize: myTheme.regularFontSize,
      fontFamily: myTheme.boldFont,
    }}
    >
      {title}
    </Text>
    <View
      style={{
        flex: 0.05,
      }}
    />
    <View
      style={{
        borderBottomColor: myTheme.grey,
        borderBottomWidth: 1,
        flex: 1,
      }}
    />
    <View
      style={{
        flex: 0.2,
      }}
    />
  </View>
)

export default Divider
