import React from 'react'
import {
  View,
  Text,
} from 'react-native'

import {
  myTheme,
} from '@styles'

const DividerWithText = ({ title }) => (
  <View
    style={{
      marginTop: 10,
      marginBottom: 10,
      flex: 1,
      flexDirection: 'row',
      alignItems: 'center',
    }}
  >
    <View
      style={{
        flex: 0.2,
      }}
    />
    <View
      style={{
        borderBottomColor: myTheme.grey,
        borderBottomWidth: 1,
        flex: 1,
      }}
    />
    <View
      style={{
        flex: 0.05,
      }}
    />
    <Text style={{
      height: 25,
      textAlign: 'center',
      color: myTheme.grey,
      fontSize: myTheme.regularFontSize,
    }}
    >
      {title}
    </Text>
    <View
      style={{
        flex: 0.05,
      }}
    />
    <View
      style={{
        borderBottomColor: myTheme.grey,
        borderBottomWidth: 1,
        flex: 1,
      }}
    />
    <View
      style={{
        flex: 0.2,
      }}
    />
  </View>
)

export default DividerWithText
