import React from 'react'
import {
  Divider,
} from '@heaven/react-native'
import {
  myTheme,
} from '@styles'

const LightDivider = () => (
  <Divider style={{ backgroundColor: myTheme.grey2 }} />
)

export default LightDivider
