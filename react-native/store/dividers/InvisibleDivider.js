import React from 'react'
import {
  myTheme,
} from '@styles'

import Divider from './Divider'

const InvisibleDivider = ({
  height,
}) => {
  const margin = height || myTheme.headerHeight * 0.4

  return (
    <Divider style={{
      backgroundColor: 'transparent',
      height: margin,
    }}
    />
  )
}

export default InvisibleDivider
