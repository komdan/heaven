/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native & Expo
import React from 'react'
import LinearGradient from 'react-native-linear-gradient'
// Helpers & Config
import { myTheme } from '@styles'

// ======================== Main component ========================
const ShadowGradient = ({ children, inverted, ...props }) => (
  <LinearGradient
    colors={inverted ? styles.darkGradientColors : styles.shadowGradientColors}
    locations={inverted ? styles.darkGradientLocations : styles.shadowGradientLocations}
    style={{ flex: 1 }}
    {...props}
  >
    {children}
  </LinearGradient>
)

// ======================== Styles ========================
const styles = {
  shadowGradientColors: [
    myTheme.lightgrey,
    myTheme.white,
    myTheme.lightgrey,
  ],
  darkGradientColors: [
    myTheme.greyD,
    myTheme.black,
    myTheme.black,
  ],
  shadowGradientLocations: [
    0,
    0.98,
    1,
  ],
  darkGradientLocations: [
    0,
    0.02,
    1,
  ],
}

// ======================== Exports ========================
export default ShadowGradient
