import React from 'react'
import {
  View,
} from 'react-native'
import {
  myTheme,
} from '@styles'

const BasicSegment = ({
  children,
  style,
}) => (
  <View style={{
    borderRadius: 5,
    borderWidth: 1,
    backgroundColor: myTheme.grey3,
    borderColor: myTheme.grey2,
    margin: 20,
    marginTop: 10,
    marginBottom: 10,
    justifyContent: 'center',
    padding: 5,
    ...style,
  }}
  >
    {children}
  </View>
)

export default BasicSegment
