import React, {
// useRef,
} from 'react'
import {
  FlatList as RNFlatList,
} from 'react-native'

const FlatList = ({ children, data, ...props }) => (
  <RNFlatList
    initialNumToRender={4} // default: 10
    maxToRenderPerBatch={3} // default: 10
    windowSize={2} // default: 21
    data={data}
    key={JSON.stringify(data)}
    keyExtractor={(item, index) => JSON.stringify({ item, index })}
    updateCellsBatchingPeriod={100} // default: 50
    {...props}
    // viewabilityConfig={viewabilityConfig}
  >
    {children}
  </RNFlatList>
)

// const viewabilityConfig = useRef({
//   minimumViewTime: 2000,
//   viewAreaCoveragePercentThreshold: 50,
//   itemVisiblePercentThreshold: 50,
//   waitForInteraction: true,
// })

export default FlatList
