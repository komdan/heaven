/*
  Created by Matthieu MARIE-LOUISE
*/
// React-native
import React from 'react'
import { View } from 'react-native'

import {
  // log,
  getGoogleReviews,
} from '@heaven/js'

// Custom components
import Review from '../cards/Review'

// ======================== Main Component ========================
class GoogleReviews extends React.Component {
  state = { // eslint-disable-line
    showMoreReviews: false,
    reviews: [],
  }

  componentDidMount() {
    const { googlePlaceId } = this.props
    getGoogleReviews(googlePlaceId)
      .then(({ result }) => {
        // log('GoogleReviews.js\\componentDidMount()::response.result: ', JSON.stringify(result))
        const {
          reviews,
        } = result

        this.setState({
          reviews: (reviews && reviews.length)
            ? reviews
            : [],
        })
      })
  }

  renderReview(review) {
    const {
      showMoreReviews,
    } = this.state

    if (
      !!review
      && !!review.rating
      && !!review.relative_time_description
      && !!review.author_name
      && !!review.text
    ) {
      return (
        <Review
          review={review}
          displayMore={!showMoreReviews}
          onMoreReviewsPress={() => this.setState({ showMoreReviews: true })}
        />
      )
    }
    return (<View />)
  }

  render() {
    const {
      renderTitle,
    } = this.props

    const {
      showMoreReviews,
      reviews,
    } = this.state

    // log('\nGoogleReviews.js\\render()::reviews: ', JSON.stringify(reviews))

    const [
      googleFirstReview,
      googleSecondReview,
      googleThirdReview,
      googleFourthReview,
      googleFifthReview,
    ] = reviews

    if (!reviews.length) {
      return <View />
    }

    return (
      <View>
        {renderTitle()}
        {this.renderReview(googleFirstReview)}
        {showMoreReviews && this.renderReview(googleSecondReview)}
        {showMoreReviews && this.renderReview(googleThirdReview)}
        {showMoreReviews && this.renderReview(googleFourthReview)}
        {showMoreReviews && this.renderReview(googleFifthReview)}
      </View>
    )
  }
}
// ======================== Helpers ========================

// ======================== Exports ========================
export default GoogleReviews
