/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native & Expo
import React from 'react'
import {
  View,
  Text,
} from 'react-native'

// Community
import {
  ListItem,
} from 'react-native-elements'
import _ from 'lodash'

import { PARIS_METRO_LINES } from '@heaven/constants'

import { myTheme } from '@styles'

// ======================== Main component ========================
const CustomListItem = ({
  leftIconType,
  subwayLines,
  ...props
}) => {
  if (leftIconType === 'station' && !!subwayLines.length) {
    return (
      <ListItem
        containerStyle={{
          padding: 5,
        }}
        leftElement={(
          <View style={styles.linesContainer}>
            {subwayLines.map((line) => {
              const {
                color,
                borderColor,
                textColor,
                shortName,
              } = _.find(PARIS_METRO_LINES, ({ lineName }) => lineName === line)
              return (
                <View
                  style={[
                    styles.sphere,
                    {
                      backgroundColor: color || 'white',
                      borderColor: borderColor || null,
                      borderWidth: borderColor ? 2 : 0,
                    },
                  ]}
                  key={line}
                >
                  <Text style={[
                    styles.leftBadgeText,
                    {
                      color: textColor || 'black',
                    },
                  ]}
                  >
                    {shortName}
                  </Text>
                </View>
              )
            })}
          </View>
        )}
        {...props}
      />
    )
  }
  if (leftIconType) {
    return (
      <ListItem
        leftIcon={{
          name: icons[leftIconType],
          type: 'material',
          size: 24,
          color: styles.primaryColor,
        }}
        {...props}
      />
    )
  }
  return <ListItem {...props} />
}

// ======================== Local Helpers ========================
const sphereSize = 30
const styles = {
  ...myTheme,
  linesContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    maxWidth: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  leftBadgeText: {
    fontFamily: myTheme.boldFont,
  },
  sphere: {
    alignItems: 'center',
    justifyContent: 'center',
    width: sphereSize,
    height: sphereSize,
    borderRadius: sphereSize / 2,
    margin: 3,
  },
}

const icons = {
  activity: 'play-arrow',
  atmosphere: 'fingerprint',
  bar: 'store-mall-directory',
  city: 'location-city',
  station: 'train',
  drink: 'local-drink',
  locality: 'place',
  phone: 'phone',
  closesAt: 'access-time',
  opensAt: 'block',
  aroundMe: 'near-me',
  default: 'stars',
}

// ======================== Exports ========================
export default CustomListItem
