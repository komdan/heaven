import React from 'react'
import {
  View,
  StatusBar,
} from 'react-native'
import {
  Text,
} from 'react-native-elements'

import {
  myTheme,
  statusBarHeight,
} from '@styles'

import OptionButton from '../buttons/OptionButton'

const MasterHeader = ({
  onOptionPress,
  headerTitle,
}) => (
  <View style={styles.customHeader.container}>
    <StatusBar barStyle={styles.headerBarStyle} backgroundColor={styles.white} />
    {/* OPTION / MENU BUTTON */}
    <View style={[styles.customHeader.leftArea, { flex: 1 }]}>
      <OptionButton
        onPress={onOptionPress}
        style={styles.optionButtonStyle}
      />
    </View>
    {/* CENTER COMPONENT */}
    <View style={[styles.customHeader.centerArea, { flex: 6 }]}>
      <Text h3>{headerTitle}</Text>
    </View>
  </View>
)

const styles = {
  optionButtonStyle: {
    marginTop: -20, // grow the icon to make it more clickable
    paddingTop: 20, // grow the icon to make it more clickable
    paddingBottom: 10,
  },
  customHeader: {
    borderColor: myTheme.grey,
    detailsTintColor: false,
    statusBarBackgroundColor: myTheme.black,
    tintColor: myTheme.black,
    tintColorInBar: myTheme.black,
    container: {
      height: statusBarHeight + 55,
      paddingTop: 0,
      backgroundColor: 'transparent',
      flexDirection: 'row',
      justifyContent: 'flex-start',
      paddingLeft: 5,
      paddingRight: 5,
    },
    leftArea: {
      // "flex":1,
      // marginTop: 38,
      marginTop: statusBarHeight + 18,
    },
    centerArea: {
      // "flex":6,
      marginTop: statusBarHeight,
      alignItems: 'center',
      justifyContent: 'center',
      // marginTop: 20,
    },
    rightArea: {
      // "flex":1,
      // paddingBottom: 10,
      // marginTop: 38,
      marginTop: statusBarHeight + 18,
    },
    navLogo: {
      height: 30,
      width: 100,
    },
  },
}

export default MasterHeader
