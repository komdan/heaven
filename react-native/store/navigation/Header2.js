/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native & Expo
import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'

// Community
import {
  Header,
  Text,
} from 'react-native-elements'
import {
  myTheme,
} from '@styles'

// Custom components
import BackButton from '../buttons/BackButton'

// ======================== Main component ========================
const Header2 = (props) => {
  const {
    subtitle,
    title,
    goBack,
    confirmMessage,
    containerStyle,
  } = props

  return (
    <Header
      containerStyle={StyleSheet.flatten([styles.header, containerStyle])}
      barStyle="light-content"
      leftComponent={(
        <BackButton
          confirmMessage={confirmMessage}
          onPress={() => goBack(null)}
          testID="backButton"
        />
      )}
      centerComponent={(
        <View>
          <Text h1 numberOfLines={1}>{title}</Text>
          <Text h2 numberOfLines={2} h2Style={{ fontSize: 16, color: myTheme.silver }}>{subtitle}</Text>
        </View>
      )}
      {...props}
    />
  )
}
// ======================== Helpers ========================
const styles = StyleSheet.create({
  header: {
    height: 120,
  },
})

// ======================== Exports ========================
export default Header2
