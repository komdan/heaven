/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native & Expo
import React, {
  Fragment,
} from 'react'

import {
  StyleSheet,
} from 'react-native'
import {
  Header,
  Text,
  // Icon,
} from 'react-native-elements'

import { myTheme } from '@styles'

import BackButton from '../buttons/BackButton'
import OptionButton from '../buttons/OptionButton'

// ======================== Main component ========================
const Header1 = (props) => {
  const {
    title = '',
    titleStyle = {},
    titleProps = {},
    goBack = undefined,
    openDrawer = undefined,
    goBackConfirmationMessage = undefined,
    goBackConfirmationTitle = undefined,
    rightComponent = undefined,
  } = props

  return (
    <Header
      barStyle={myTheme.headerBarStyle}
      leftComponent={(openDrawer
        ? (
          <OptionButton
            onPress={() => openDrawer()}
            color={styles.secondaryColor}
            style={styles.button}
          />
        )
        : goBack
          ? (
            <BackButton
              onPress={() => goBack(null)}
              confirmMessage={goBackConfirmationMessage}
              confirmTitle={goBackConfirmationTitle}
            />
          )
          : (
            <Fragment />
          )
      )}
      centerComponent={(
        <Text
          h2
          h2Style={StyleSheet.flatten([
            { color: styles.black },
            titleStyle,
          ])}
          numberOfLines={1}
          {...titleProps}
        >
          {title}
        </Text>
      )}
      {...props}
      rightComponent={(rightComponent || (<Fragment />))}
    />
  )
}
// ======================== Helpers ========================
const styles = {
  ...myTheme,
  button: {
    marginTop: -20, // grow the button to make it more clickable
    paddingTop: 20, // grow the button to make it more clickable
    paddingBottom: 10,
    paddingRight: 20, // grow the button to make it more clickable
  },
}
// ======================== Exports ========================
export default Header1
