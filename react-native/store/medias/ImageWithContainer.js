/*
* Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native imports
import React from 'react'
import {
  View,
  StyleSheet,
  // Image,
} from 'react-native'

// Community
import Image from 'react-native-fast-image'
// Helpers & Config
import { withTranslation } from 'react-multi-lang'
import { myTheme } from '@styles'

// ======================== Main Component ========================
const ImageWithContainer = ({ uri, containerWidth, imageStyle, containerStyle, wrapperStyle }) => (
  <View style={StyleSheet.flatten([
    styles.container,
    { width: containerWidth },
    containerStyle,
  ])}
  >
    <View style={StyleSheet.flatten([
      styles.wrapper,
      wrapperStyle,
    ])}
    >
      <Image
        source={{ uri }}
        style={imageStyle}
      />
    </View>
  </View>
)

// ======================== Local helpers ========================
const styles = {
  ...myTheme,
  ...StyleSheet.create({
    container: {
      flex: 1,
      // flexDirection: 'row',
      // width: barWidth,
      alignItems: 'center',
    },
    wrapper: {
      alignItems: 'center',
    },
  }),
}

// ======================== Exports ========================
export default withTranslation(ImageWithContainer)
