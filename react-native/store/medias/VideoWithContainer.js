import {
  Fragment,
} from 'react'

export default Fragment

// /*
// * Created by Matthieu MARIE-LOUISE
// */
// // ======================== Imports ========================
// // React-native imports
// import React from 'react'
// import {
//   View,
//   StyleSheet,
// } from 'react-native'

// // Community
// import Video from 'react-native-video'
// // Helpers & Config
// import { withTranslation } from 'react-multi-lang'
// import {
//   myTheme as styles,
// } from '@styles'
// import {
//   useRefreshingKey,
//   useDelayOver,
// } from '@heaven/react'

// import ImageWithContainer from './ImageWithContainer'

// const DELAY_BEFORE_VIDEORESET = 400

// // ======================== Main Component ========================
// const VideoWithContainer = ({
//   uri,
//   videoStyle,
//   containerWidth,
//   containerStyle,
//   delayBeforeLaunchingVideo,
//   poster,
//   hideVideo,
//   ...props
// }) => {
//   const {
//     componentKey: videoKey,
//     triggerKeyRefresh: handleError,
//   } = useRefreshingKey({ uri }, DELAY_BEFORE_VIDEORESET)
//   const { isDelayOver } = useDelayOver(delayBeforeLaunchingVideo || 2000)

//   return (
//     <View style={StyleSheet.flatten([
//       styles.centeredContainer(),
//       { width: containerWidth },
//       containerStyle,
//     ])}
//     >
//       {!!isDelayOver && !hideVideo && (
//         <Video
//           disableFocus
//           repeat
//           muted
//           useTextureView
//           ignoreSilentSwitch="obey"
//           // resizeMode="cover" // commented because proptypes caused compilation pbm
//           // posterResizeMode="cover" // commented because proptypes caused compilation pbm
//           volume={0.0}
//           selectedAudioTrack={{ type: 'disabled' }}
//           controls={false}
//           source={{ uri }}
//           style={videoStyle}
//           onError={handleError}
//           key={videoKey}
//           bufferConfig={{
//             minBufferMs: 1500,
//             maxBufferMs: 10000,
//             bufferForPlaybackMs: 250,
//             bufferForPlaybackAfterRebufferMs: 500,
//           }}
//           maxBitRate={500000} // 500ko
//           minLoadRetryCount={3}
//           progressUpdateInterval={10000}
//           onReadyForDisplay={() => { }}
//           poster={poster}
//           {...props}
//         />
//       )}
//       {(!isDelayOver || !!hideVideo) && (
//         <ImageWithContainer
//           uri={poster}
//           imageStyle={videoStyle}
//           containerStyle={containerStyle}
//         />
//       )}
//     </View>
//   )
// }

// // ======================== Exports ========================
// export default withTranslation(VideoWithContainer)
