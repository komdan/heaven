import React, { Component, Fragment } from 'react'
import {
  View,
  Text,
} from 'react-native'

import FastImage from 'react-native-fast-image'

export default class PlaceHolderFastImage extends Component {
  state = { // eslint-disable-line
    loaded: null,
  }

  onLoadEnd = () => this.setState({ loaded: true })

  onLoadStart = () => this.setState({ loaded: false })

  render() {
    const {
      source,
      style,
      placeholderText,
    } = this.props
    const {
      loaded,
    } = this.state

    const normalisedSource = source && typeof source.uri === 'string' && !source.uri.split('http')[1]
      ? null
      : source

    return (
      <Fragment>
        <View style={loaded ? style : { width: 0, height: 0 }}>
          <FastImage
            {...this.props}
            source={normalisedSource}
            style={style}
            onLoadStart={this.onLoadStart}
            onLoadEnd={this.onLoadEnd}
          />
        </View>
        <View style={style}>
          {!loaded && (
            <View style={styles.placeHolderContainerStyle}>
              <Text style={styles.placeHolderTextStyle}>{placeholderText}</Text>
            </View>
          )}
        </View>
      </Fragment>
    )
  }
}

const styles = {
  placeHolderContainerStyle: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  placeHolderTextStyle: {
    color: 'white',
  },
}

PlaceHolderFastImage.defaultProps = {
  style: {},
  placeholderText: '',
}
