import React from 'react'
import MapView from 'react-native-maps'

import {
  myTheme,
} from '@styles'

const OneMarkerMapView = ({
  latitude,
  longitude,
  title,
  latitudeDelta = 0.04,
  longitudeDelta = 0.02,
  height = 320,
  mapViewProps,
  mapViewMarkerProps,
}) => (
  <MapView
    style={myTheme.mapView({
      height,
    })}
    initialRegion={{
      latitude,
      longitude,
      latitudeDelta,
      longitudeDelta,
    }}
    region={{
      latitude,
      longitude,
      latitudeDelta,
      longitudeDelta,
    }}
    pitchEnabled={false}
    rotateEnabled={false}
    scrollEnabled={false}
    zoomEnabled={false}
    {...mapViewProps}
  >
    <MapView.Marker
      coordinate={{ latitude, longitude }}
      title={title}
      description=""
      {...mapViewMarkerProps}
    />
  </MapView>
)

export default OneMarkerMapView
