import React from 'react'
import {
  View,
  ScrollView,
} from 'react-native'

// Community
import Modal from 'react-native-modal'
import Image from 'react-native-fast-image'

import {
  useTranslation,
} from 'react-multi-lang'

import {
  myTheme,
} from '@styles'

import Divider from '../dividers/Divider'
import Button from '../buttons/Button'
import CenteredText from '../typography/CenteredText'
import ModalHeader from './ModalHeader'

const FullPageModal = ({
  isVisible,
  leftTitle,
  rightTitle,
  rightIcon,
  description,
  pictureUrl = null,
  onCancelPress,
  onClosePress,
  onValidatePress,
  children,
  cancelButtonText: cancelButtonTextInArgs = '',
  validateButtonText: validateButtonTextInArgs = '',
  noCancelButton,
}) => {
  const t = useTranslation()
  const cancelButtonText = cancelButtonTextInArgs || t('cancel')
  const validateButtonText = validateButtonTextInArgs || t('ok')

  return (
    <View>
      {/* MODAL */}
      <Modal
        isVisible={isVisible}
        backdropColor="rgba(0, 0, 0, 0.75)"
        backdropOpacity={1}
        animationIn="fadeInLeft"
        animationOut="fadeOutRight"
        animationInTiming={250}
        animationOutTiming={250}
        onBackdropPress={onClosePress}
        backdropTransitionInTiming={500}
        backdropTransitionOutTiming={500}
      >
        <View style={myTheme.modalWrapper}>
          {/* TITLE */}
          <ModalHeader
            leftTitle={leftTitle}
            rightTitle={rightTitle}
            rightIcon={rightIcon}
          />
          {!!description && (
            <CenteredText style={myTheme.modalDescription}>{description}</CenteredText>
          )}
          <ScrollView style={myTheme.width('100%')}>
            <Divider />
            {!!pictureUrl && (
              <Image
                source={{ uri: pictureUrl }}
                style={myTheme.image('30%', '100%', 5)}
              />
            )}
            {children}
          </ScrollView>
          {/* Bottom buttons */}
          <View style={myTheme.fullRow}>
            {!noCancelButton && (
            <Button
              title={cancelButtonText}
              onPress={onCancelPress}
              color={myTheme.secondaryColor}
              {...myTheme.modalBottomButton}
            />
            )}
            <Button
              title={validateButtonText}
              onPress={onValidatePress}
              {...myTheme.modalBottomButton}
            />
          </View>
        </View>
      </Modal>
    </View>
  )
}

export default FullPageModal
