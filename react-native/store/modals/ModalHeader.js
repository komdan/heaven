import React from 'react'
import {
  View,
  StyleSheet,
} from 'react-native'
import {
  Icon,
} from 'react-native-elements'
import {
  myTheme,
} from '@styles'

import CenteredText from '../typography/CenteredText'

const ModalHeader = ({
  leftTitle = '',
  rightTitle = '',
  rightIcon,
}) => (
  <View style={styles.header}>
    {!!leftTitle && (
    <CenteredText style={styles.left} numberOfLines={2}>{leftTitle}</CenteredText>
    )}
    {!!rightTitle && (
    <CenteredText style={styles.right}>{rightTitle}</CenteredText>
    )}
    {!!rightIcon && (
    <Icon style={{ flex: 1 }} {...rightIcon} />
    )}
  </View>
)
const styles = {
  ...myTheme,
  ...StyleSheet.create({
    header: {
      flexDirection: 'row',
      width: '100%',
      justifyContent: 'center',
      alignItems: 'center',
    },
    left: {
      flex: 3,
      alignSelf: 'flex-start',
      fontFamily: myTheme.upperCaseFont,
      textTransform: 'uppercase',
      color: myTheme.black,
      fontSize: 22,
      padding: 20,
      textAlign: 'left',
    },
    right: {
      flex: 1,
      alignSelf: 'flex-start',
      fontFamily: myTheme.boldFont,
      color: myTheme.black,
      fontSize: 14,
      padding: 20,
      textAlign: 'right',
    },
  }),
}

export default ModalHeader
