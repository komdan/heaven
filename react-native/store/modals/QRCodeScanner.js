import React from 'react'
import {
  View,
} from 'react-native'
// Community
import {
  Text,
} from 'react-native-elements'
import {
  t as t_,
} from 'react-multi-lang'
// TODO : migrate this
// import QRCodeScanner from 'react-native-qrcode-scanner'

// Custom
import {
  myTheme,
  screenHeight,
} from '@styles'
import ActionSheet from './ActionSheet'

class ScanScreen extends React.Component {
  show = () => this.ActionSheetRef && this.ActionSheetRef.show() // eslint-disable-line

  hide = () => this.ActionSheetRef && this.ActionSheetRef.hide() // eslint-disable-line

  render() {
    const {
      onRead,
    } = this.props
    return (
      <ActionSheet
        // autoShow
        ref={(ref) => { this.ActionSheetRef = ref }}
        height={screenHeight * 1.0}
        buttonBoxHeight="100%"
        // ref={(ref) => { this.PaymentChoiceActionSheet = ref }}
        title={(
          <View style={styles.actionSheetTitleContainer}>
            <Text style={styles.actionSheetTitle}>{t_('scanQRCode')}</Text>
          </View>
        )}
        options={[
          // <QRCodeScanner
          //   key={'QRCodeScanner'}
          //   onRead={({ data }) => onRead(data)}
          //   fadeIn={false}
          //   cameraProps={{ androidCameraPermissionOptions: null }} // BUG FIX: Blank modal before asking for permissions
          // />,
        ]}
      />
    )
  }
}

const styles = {
  actionSheetTitle: {
    color: myTheme.black,
    textTransform: 'uppercase',
    fontFamily: myTheme.upperCaseFont,
  },
  actionSheetTitleContainer: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: myTheme.primaryColor,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
}

export default ScanScreen
