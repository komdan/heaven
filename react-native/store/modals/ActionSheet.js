/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React
import React from 'react'
import {
  Text,
  View,
  Dimensions,
  Modal,
  TouchableHighlight,
  Animated,
  ScrollView,
  StyleSheet,
  Easing,
  FlatList,
} from 'react-native'

import {
  Divider,
} from 'react-native-elements'

import {
  myTheme,
  isIphoneX,
} from '@styles'
import {
  // log,
  keyExtractor,
} from '@heaven/js'

const ACTIONSHEET_ANIMATION_DURATION = 200
const ACTION_SHEET_SHOW_DELAY = 0

// ======================== Main Component ========================
class ActionSheet extends React.Component {
  constructor(props) {
    super(props)
    this.scrollEnabled = false
    this.translateY = this.calculateHeight(props)
    this.state = {
      visible: false,
      sheetAnim: new Animated.Value(this.translateY),
    }
  }

  componentDidMount() {
    const {
      autoShow,
    } = this.props
    if (autoShow) this.show()
  }

  componentDidUpdate(nextProps) {
    this.translateY = this.calculateHeight(nextProps)
  }

  hide = index => new Promise((resolve) => {
    const { onPress } = this.props
    const { sheetAnim } = this.state

    Animated.timing(
      sheetAnim,
      {
        toValue: this.translateY,
        duration: ACTIONSHEET_ANIMATION_DURATION,
        useNativeDriver: true,
      },
    ).start(
      // hide sheet
      () => this.setState(
        { visible: false },
        async () => {
          if (isSet(index)) await onPress(index)
          resolve(true)
        },
      ),
    )
  })

  show = (delay = ACTION_SHEET_SHOW_DELAY) => new Promise((resolve) => {
    setTimeout(
      () => {
        const { sheetAnim } = this.state
        const {
          selectedIndex,
          options,
        } = this.props

        this.setState(
          // STEP1: Set sheet visible
          { visible: true },
          () => {
            // STEP2: Show the sheet
            Animated.timing(
              sheetAnim,
              {
                toValue: 0,
                duration: ACTIONSHEET_ANIMATION_DURATION,
                easing: Easing.out(Easing.ease),
                useNativeDriver: true,
              },
            ).start(
              // STEP3: Scroll to selected index
              // () => this.ScrollView.scrollToEnd(),
              () => {
                this.ScrollView.scrollTo({
                  x: 0,
                  y: ((selectedIndex / options.length) * calculateScrollViewHeight(this.props))
                    - (this.calculateHeight(this.props) / 2),
                  animated: false,
                })
                resolve(true)
              },
            )
          },
        )
      },
      delay,
    )
  })

  /**
   * elements: titleBox, messageBox, buttonBox, cancelButtonBox
   * box size: height, marginTop, marginBottom
   */
  calculateHeight(props) {
    const height = props.height || calculateScrollViewHeight(props)
    if (height > maxHeight) {
      this.scrollEnabled = true
      return maxHeight
    }

    this.scrollEnabled = false
    return height
  }

  render() {
    const {
      title,
      message,
      options,
      cancelButtonIndex,
      // dividerColor,
      destructiveButtonIndex,
      fontColor,
      selectedIndex,
      buttonBoxHeight,
    } = this.props

    const { visible, sheetAnim } = this.state
    return (
      <Modal
        visible={visible}
        animationType="none"
        transparent
        onRequestClose={() => this.hide(selectedIndex)}
      >
        <View style={[styles.wrapper]}>
          <Text
            style={[styles.overlay]}
            onPress={() => this.hide(selectedIndex)}
          />
          <Animated.View
            style={[
              styles.body,
              { height: this.translateY, transform: [{ translateY: sheetAnim }] },
            ]}
          >
            {!!title && (
              <View style={styles.titleBox}>
                {React.isValidElement(title) ? title : (
                  <Text style={styles.titleText}>{title}</Text>
                )}
              </View>
            )}
            {!!message && (
              <View style={styles.messageBox}>
                {React.isValidElement(message) ? message : (
                  <Text style={styles.messageText}>{message}</Text>
                )}
              </View>
            )}
            <ScrollView
              scrollEnabled={this.scrollEnabled}
              ref={(ref) => { this.ScrollView = ref }}
            >
              <React.Fragment>
                <Divider />
                {/* // this was here before the FlatList below
                {options.map((title_, index) => (!isSet(cancelButtonIndex) || (cancelButtonIndex !== index)) && (
                  <ActionSheetButton
                    title={title_}
                    // eslint-disable-next-line
                    key={`${title_}${index}`}
                    // dividerColor={dividerColor}
                    fontColor={destructiveButtonIndex === index ? styles.errorColor : fontColor}
                    fontFamily={selectedIndex === index ? myTheme.boldFont : myTheme.regularFont}
                    containerStyle={StyleSheet.flatten([
                      cancelButtonIndex === index
                        ? styles.cancelButtonBox
                        : styles.buttonBox,
                      buttonBoxHeight ? { height: buttonBoxHeight } : {},
                    ])}
                    onPress={() => this.hide(index)}
                  />
                ))}
                */}
                <FlatList
                  data={options}
                  keyExtractor={keyExtractor}
                  renderItem={({
                    item: title_,
                    index,
                  }) => (!isSet(cancelButtonIndex) || (cancelButtonIndex !== index)) && (
                    <ActionSheetButton
                      title={title_}
                      // eslint-disable-next-line
                      key={`${title_}${index}`}
                      // dividerColor={dividerColor}
                      fontColor={destructiveButtonIndex === index ? styles.errorColor : fontColor}
                      fontFamily={selectedIndex === index ? myTheme.boldFont : myTheme.regularFont}
                      containerStyle={StyleSheet.flatten([
                        cancelButtonIndex === index
                          ? styles.cancelButtonBox
                          : styles.buttonBox,
                        buttonBoxHeight ? { height: buttonBoxHeight } : {},
                      ])}
                      onPress={() => this.hide(index)}
                    />
                  )}
                />
              </React.Fragment>
            </ScrollView>
            {isSet(cancelButtonIndex) && (
              <React.Fragment>
                <Divider />
                <ActionSheetButton
                  title={options[cancelButtonIndex]}
                  index={cancelButtonIndex}
                  // dividerColor={dividerColor}
                  fontColor={isSet(destructiveButtonIndex) && isSet(cancelButtonIndex) && destructiveButtonIndex === cancelButtonIndex
                    ? styles.errorColor
                    : fontColor}
                  containerStyle={styles.cancelButtonBox}
                  onPress={() => this.hide(cancelButtonIndex)}
                />
              </React.Fragment>
            )}
          </Animated.View>
        </View>
      </Modal>
    )
  }
}

// ======================== Locals ========================
// ==== Styles
const maxHeight = Dimensions.get('window').height * 0.7

const titleBoxHeight = 40
const titleBoxMarginTop = 0
const titleBoxMarginBottom = 0

const messageBoxHeight = 30
const messageBoxMarginTop = 0
const messageBoxMarginBottom = 0

const buttonBoxHeight = 50
// const buttonBoxMarginTop = StyleSheet.hairlineWidth
const buttonBoxMarginTop = 0
const buttonBoxMarginBottom = 0

const cancelButtonBoxHeight = 50
const cancelButtonBoxMarginTop = 0
const cancelButtonBoxMarginBottom = 0

const styles = {
  ...myTheme,
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    opacity: 0.4,
    backgroundColor: myTheme.white,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: isIphoneX ? 20 : 0,
  },
  body: {
    flex: 1,
    alignSelf: 'flex-end',
    // backgroundColor: myTheme.greyE,
    backgroundColor: 'transparent',
  },
  titleBox: {
    height: titleBoxHeight,
    marginTop: titleBoxMarginTop,
    marginBottom: titleBoxMarginBottom,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: myTheme.white,
    backgroundColor: 'transparent',
  },
  titleText: {
    color: myTheme.grey7,
    fontSize: 14,
  },
  messageBox: {
    height: messageBoxHeight,
    marginTop: messageBoxMarginTop,
    marginBottom: messageBoxMarginBottom,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 10,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: myTheme.greyF,
  },
  messageText: {
    color: myTheme.grey9,
    fontSize: 12,
  },
  buttonBox: {
    height: buttonBoxHeight,
    marginTop: buttonBoxMarginTop,
    marginBottom: buttonBoxMarginBottom,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: myTheme.black,
  },
  buttonText: {
    fontSize: 18,
  },
  cancelButtonBox: {
    height: cancelButtonBoxHeight,
    marginTop: cancelButtonBoxMarginTop,
    marginBottom: cancelButtonBoxMarginBottom,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: myTheme.black,
  },
}

// ==== Local Helpers
function isSet(prop) {
  return typeof prop !== 'undefined'
}

function calculateScrollViewHeight(props) {
  const {
    title,
    message,
    cancelButtonIndex,
    options,
  } = props

  const buttonBoxTotalHeight = (buttonBoxHeight + buttonBoxMarginTop + buttonBoxMarginBottom)
  const height = (
    Number(!!title && (titleBoxHeight + titleBoxMarginTop + titleBoxMarginBottom))
    + Number(!!message && (messageBoxHeight + messageBoxMarginTop + messageBoxMarginBottom))
    + Number(!!isSet(cancelButtonIndex)
        && (
          cancelButtonBoxHeight + cancelButtonBoxMarginTop + cancelButtonBoxMarginBottom
          + (options.length - 1) * buttonBoxTotalHeight
        ))
    + Number(!isSet(cancelButtonIndex) && (options.length * buttonBoxTotalHeight))
  )

  // Debug
  // log('ActionSheet.js\\calculateScrollViewHeight()::height: ', height)
  // log('ActionSheet.js\\calculateScrollViewHeight()::height >= maxHeight: ', height >= maxHeight)

  return height
}

// ==== Local Components
const ActionSheetButton = ({
  title,
  // dividerColor,
  fontColor,
  fontFamily = myTheme.defaultFont,
  containerStyle = {},
  onPress,
}) => (
  <React.Fragment>
    <TouchableHighlight
      activeOpacity={1}
      // underlayColor={dividerColor}
      style={containerStyle}
      onPress={onPress}
    >
      {React.isValidElement(title) ? title : (
        <Text
          style={[
            styles.buttonText,
            {
              color: fontColor,
              fontFamily,
            }]}
        >
          {title}
        </Text>
      )}
    </TouchableHighlight>
    <Divider />
  </React.Fragment>
)

// ======================== Exports ========================
export default ActionSheet
