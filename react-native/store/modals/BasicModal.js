import React from 'react'
import Modal from 'react-native-modal'
// import {
//   Modal,
// } from 'react-native'

const BasicModal = ({
  isVisible,
  onClosePress,
  children,
  animationIn = 'slideInUp',
  animationOut = 'slideOutDown',
  avoidKeyboard = true,
  ...props
}) => !!isVisible && ( 
  <Modal
    avoidKeyboard={avoidKeyboard}
    isVisible={isVisible}
    backdropColor="rgba(0, 0, 0, 0.93)"
    backdropOpacity={1}
    animationIn={animationIn}
    animationOut={animationOut}
    animationInTiming={250}
    animationOutTiming={250}
    onBackdropPress={onClosePress}
    backdropTransitionInTiming={250}
    backdropTransitionOutTiming={250}
    {...props}
  >
    {children}
  </Modal>
)

export default BasicModal
