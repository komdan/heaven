import React from 'react'
import {
  TouchableOpacity,
} from 'react-native'
import {
  myTheme,
} from '@styles'

const TouchableArea = ({
  onPress,
  children,
}) => (
  <TouchableOpacity
    onPress={onPress}
    style={myTheme.touchable()}
  >
    {children}
  </TouchableOpacity>
)

export default TouchableArea
