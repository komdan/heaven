import React from 'react'
import {
  View,
} from 'react-native'
import {
  useTranslation,
} from 'react-multi-lang'
import {
  MyButton,
} from '@heaven/react-native'

import {
  myTheme,
  screenWidth,
} from '@styles'

const OkAndCancelButtonsInModal = ({
  onOkPress,
  onCancelPress,
  cancelText = 'Cancel',
  cancelTitle = '',
  okTitle = '',
  hideCancelButton = false,
}) => {
  const t = useTranslation()
  return (
    <View style={{
      flexDirection: 'row',
      justifyContent: 'space-between',
      // backgroundColor: 'pink',
    }}
    >
      {!hideCancelButton && (
        <MyButton
          containerStyle={{ width: screenWidth * 0.34, backgroundColor: myTheme.error }}
          title={cancelTitle || t(cancelText)}
          onPress={onCancelPress}
        />
      )}
      <MyButton
        containerStyle={{ width: screenWidth * (hideCancelButton ? 0.85 : 0.34) }}
        isFooter
        title={okTitle || t('Ok')}
        onPress={onOkPress}
      />
    </View>
  )
}

export default OkAndCancelButtonsInModal
