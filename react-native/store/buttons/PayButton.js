/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React native
import React from 'react'
import {
  Text,
  View,
  StyleSheet,
} from 'react-native'
// Community
import { withTranslation } from 'react-multi-lang'
// Helpers
import { myTheme } from '@styles'

// ======================== Main Component ========================
const PayButton = (props) => {
  const {
    t,
    // totalItemAmount,
    billAmount,
    currency,
  } = props

  return (
    <View style={styles.container}>
      {/*
      <View style={styles.itemAmountWrapper}>
        <Text style={styles.itemAmount}>
          {totalItemAmount}
        </Text>
      </View>
      */}

      <View style={styles.showOrderWrapper}>
        <Text style={styles.showOrder}>
          {t('showOrder')}
        </Text>
      </View>

      <View style={styles.priceWrapper}>
        <Text style={styles.price}>
          {billAmount + currency}
        </Text>
      </View>

    </View>
  )
}

// ======================== Helpers ========================
const height = 60
const styles = {
  // ...myTheme,
  ...StyleSheet.create({
    container: {
      height,
      backgroundColor: myTheme.primaryColor,
      flexDirection: 'row',
      alignItems: 'center',
      padding: 5,
      borderRadius: 15,
      margin: 10,
    },
    price: {
      fontFamily: myTheme.boldFont,
      fontSize: myTheme.bigFontSize,
      color: myTheme.white,
    },
    showOrder: {
      fontSize: myTheme.biggishFontSize,
      color: myTheme.white,
    },
    showOrderWrapper: {
      flex: 1,
      height: height - 10,
      flexDirection: 'row',
      paddingLeft: 5,
      justifyContent: 'center',
      alignItems: 'center',
    },
    priceWrapper: {
      paddingRight: 5,
      justifyContent: 'center',
      alignItems: 'center',
    },
  }),
}

// ======================== Exports ========================
export default withTranslation(PayButton)
