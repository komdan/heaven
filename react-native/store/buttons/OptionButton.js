// React-native
import React from 'react'
import {
  View,
  TouchableOpacity,
  StyleSheet,
} from 'react-native'

// Helpers & Config
import { Icon } from 'react-native-elements'
import { myTheme } from '@styles'
// Community

const OptionButton = (props) => {
  const {
    color,
    style,
    onPress,
  } = props

  return (
    <TouchableOpacity onPress={onPress} testID="OptionButton">
      <View style={StyleSheet.flatten([style, styles.button])}>
        <Icon
          type="feather"
          name="align-left"
          size={styles.bigIconSize}
          color={color}
        />
      </View>
    </TouchableOpacity>
  )
}

const styles = {
  ...myTheme,
  button: {},
}

OptionButton.defaultProps = {
  color: styles.silver,
  style: {},
}

export default OptionButton
