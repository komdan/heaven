import React, { useState, useEffect } from 'react'
import {
  StyleSheet,
} from 'react-native'

import {
  msToString,
} from '@heaven/js'
import {
  myTheme,
} from '@styles'

import MyButton from './MyButton'

const ChronoButton = ({ onValueChange, autoPlay, t }) => {
  const [counter, setCounter] = useState(0)
  const [isPlaying, setIsPlaying] = useState(!!autoPlay)

  const adequateValue = (pauseValue, resetValue, playValue) => (
    isPlaying
      ? pauseValue
      : (
        counter
          ? resetValue
          : playValue
      )
  )

  // componentDidMount
  useEffect(() => {
    const timeout = setTimeout(
      () => isPlaying && setCounter(counter + 1000),
      1000,
    )
    return () => clearTimeout(timeout)
  }, [counter, isPlaying])
  useEffect(() => {
    onValueChange(counter)
  }, [counter]) // eslint-disable-line

  const chronoButtonTitle = adequateValue(
    msToString(counter), // pause
    msToString(counter), // reset
    t('launchChrono'), // play
  )
  const chronoButtonTitleStyle = adequateValue(
    styles.color('white'), // pause
    styles.color('black'), // reset
    styles.color('white'), // play
  )
  const chronoButtonContainerStyle = adequateValue(
    styles.backgroundColor('black'), // pause
    styles.backgroundColor('white'), // reset
    styles.backgroundColor('black'), // play
  )
  const chronoButtonRightIcon = adequateValue(
    styles.pauseIcon, // pause
    styles.resetIcon, // reset
    styles.chronoButtonPlayIcon, // play
  )
  const chronoButtonLeftIcon = adequateValue(
    styles.pauseIcon, // pause
    styles.resetIcon, // reset
    styles.chronoButtonPlayIcon, // play
  )
  const onChronoButtonPress = () => adequateValue(
    () => setIsPlaying(false), // pause
    () => { setCounter(0); setIsPlaying(true) }, // reset
    () => setIsPlaying(true), // play
  )()

  return (
    <MyButton
      onPress={onChronoButtonPress}
      title={chronoButtonTitle}
      titleStyle={chronoButtonTitleStyle}
      containerStyle={chronoButtonContainerStyle}
      rightIcon={chronoButtonRightIcon}
      leftIcon={chronoButtonLeftIcon}
      isFooter
    />
  )
}

const styles = {
  ...myTheme,
  ...StyleSheet.create({
    container: {
      flex: 1,
    },
    wrapper: {
      alignItems: 'center',
    },
    counterText: {
      alignItems: 'center',
      color: myTheme.primaryColor,
      fontSize: myTheme.giganticallyHugeFontSize,
    },
  }),
}

export default ChronoButton
