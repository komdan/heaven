import React from 'react'
import {
  View,
} from 'react-native'

import {
  Text,
} from 'react-native-elements'
import { myTheme } from '@styles'

const ApplePayButton = ({ title }) => (
  <View style={styles.container} testID="ApplePayButton.Container">
    <View style={styles.wrapper}>
      <Text
        style={styles.title}
      >
        {title}
      </Text>
    </View>
  </View>
)

const styles = {
  container: {
    width: '90%',
    marginBottom: 5,
    backgroundColor: 'black',
  },
  wrapper: {
    height: '90%',
    backgroundColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 5,
  },
  title: {
    fontFamily: 'Open Sans',
    fontSize: myTheme.biggishFontSize,
    color: 'white',
  },
}

export default ApplePayButton
