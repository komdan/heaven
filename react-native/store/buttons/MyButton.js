/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native
import React, {
  useState,
} from 'react'
import {
  // Alert,
  View,
  TouchableOpacity,
  StyleSheet,
  Text,
} from 'react-native'
import Image from 'react-native-fast-image'
// Community
import {
  Icon,
  Card,
  Badge,
} from 'react-native-elements'
// import { Icon } from '@rneui/themed'
import {
  View as AnimatableView,
} from 'react-native-animatable'
import {
  myTheme,
  isIphoneX,
} from '@styles'
import {
  sleep,
} from '@heaven/js'
import {
  CenteredText,
  ActivityIndicator,
  log,
  error,
} from '@heaven/react-native'

// ======================== Main Component ========================
const MyButton = ({
  buttonStyle,
  color,
  containerStyle,
  disabled = false,
  enabled = true,
  introAnimation,
  introAnimationDuration,
  isFooter,
  leftElement,
  leftImageUri,
  leftImageSize = 40,
  leftIcon,
  loading,
  loopedAnimation,
  loopedAnimationIterationDelay,
  onPress: onPressInProps,
  onPressActionFinished,
  rightIcon,
  rightTitle,
  subtitle,
  subtitleStyle,
  subtitle2,
  subtitle2Style,
  subtitle3,
  subtitle3Style,
  testID,
  title,
  titleStyle,
  badgeValue = '',
}) => {
  const [hasPress, setHasPress] = useState(false)
  const onPress = async () => {
    if (!hasPress) {
      setHasPress(true)
      try {
        const onPressResult = await onPressInProps()
        await onPressActionFinished(onPressResult)
      } catch (e) {
        error(e)
      }
      await sleep(200) // disable button very quickly to improve perceipted UX responsiveness
      setHasPress(false)
    } else {
      log('MyButton.js\\onPress()::❌ TRIED TO PRESS ON DISABLED BUTTON')
    }
  }
  const buttonCustomColorStyle = color ? { backgroundColor: color } : {}

  return (
    <ConditionalAnimatableView
      animated={!!introAnimation}
      useNativeDriver
      animation={introAnimation}
      duration={introAnimationDuration}
    >
      <ConditionalAnimatableView
        animated={!!loopedAnimation}
        useNativeDriver
        animation={loopedAnimation}
        iterationCount="infinite"
        iterationDelay={loopedAnimationIterationDelay}
      >
        <TouchableOpacity disabled={disabled} onPress={() => !!enabled && onPress()} testID={testID}>
          <Card containerStyle={StyleSheet.flatten([
            styles.container,
            { marginBottom: isFooter ? (isIphoneX ? 20 : 5) : 5 },
            containerStyle,
            buttonStyle,
            buttonCustomColorStyle,
          ])}
          >
            <View style={StyleSheet.flatten([
              styles.wrapper,
            ])}
            >
              <View>
                {!!leftIcon && <Icon {...leftIcon} />}
                {!!leftElement && leftElement}
                {!!leftImageUri && (
                  <Image
                    source={{ uri: leftImageUri }}
                    style={myTheme.image(leftImageSize, leftImageSize, 5)}
                  />
                )}
              </View>
              <View>
                {!hasPress && !loading && (
                <Text
                  style={StyleSheet.flatten([styles.title, titleStyle])}
                  numberOfLines={1}
                >
                  {title}
                </Text>
                )}
                {!!hasPress && (<ActivityIndicator color={myTheme.white} />)}
                {!!loading && (<ActivityIndicator color={myTheme.white} />)}
              </View>
              <View>
                {!!rightIcon && <Icon {...rightIcon} />}
                {!!rightTitle && <Text style={styles.rightTitle}>{rightTitle}</Text>}
              </View>
            </View>
            {!hasPress && !!subtitle && (
              <CenteredText
                style={StyleSheet.flatten([styles.subtitle, subtitleStyle])}
              >
                {subtitle}
              </CenteredText>
            )}
            {!hasPress && !!subtitle2 && (
              <CenteredText
                style={StyleSheet.flatten([styles.subtitle2, subtitle2Style])}
              >
                {subtitle2}
              </CenteredText>
            )}
            {!hasPress && !!subtitle3 && (
              <CenteredText
                style={StyleSheet.flatten([styles.subtitle3, subtitle3Style])}
              >
                {subtitle3}
              </CenteredText>
            )}
          </Card>
          {!hasPress && !!badgeValue && (
            <Badge value={badgeValue} {...myTheme.badge()} />
          )}
        </TouchableOpacity>
      </ConditionalAnimatableView>
    </ConditionalAnimatableView>
  )
}

// ======================== Helpers ========================
const ConditionalAnimatableView = ({ animated, children, ...props }) => (
  animated
    ? <AnimatableView {...props}>{children}</AnimatableView>
    : <View {...props}>{children}</View>
)

const styles = {
  ...myTheme,
  ...StyleSheet.create({
    container: {
      backgroundColor: myTheme.primaryColor,
      borderWidth: 0,
    },
    wrapper: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      alignItems: 'center',
    },
    title: {
      color: myTheme.black,
      fontFamily: myTheme.upperCaseFont,
      // textTransform: 'uppercase',
      fontSize: 18,
      marginLeft: 5,
    },
    subtitle: {
      color: myTheme.grey,
      fontFamily: myTheme.regularFont,
      // textTransform: 'uppercase',
      fontSize: 18,
      marginTop: 5,
    },
    subtitle2: {
      color: myTheme.errorColor,
      fontFamily: myTheme.boldFont,
      // textTransform: 'uppercase',
      fontSize: 14,
      marginTop: 5,
    },
    subtitle3: {
      color: myTheme.errorColor,
      fontFamily: myTheme.boldFont,
      // textTransform: 'uppercase',
      fontSize: 14,
      marginTop: 5,
    },
    rightTitle: {
      fontFamily: myTheme.upperCaseFont,
      // textTransform: 'uppercase',
      fontSize: myTheme.bigFontSize,
      color: myTheme.black,
    },
  }),
}

ConditionalAnimatableView.defaultProps = {
  animated: '',
}

// ======================== Exports ========================
export default MyButton
