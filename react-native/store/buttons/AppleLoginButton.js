import React from 'react'
import {
  StyleSheet,
} from 'react-native'
import {
  myTheme,
} from '@styles'

import MyButton from './MyButton'

const AppleLoginButton = ({
  onPress = () => { },
  onPressActionFinished = () => { },
  title = '',
}) => (
  <MyButton
    title={title}
    onPress={() => onPress()}
    onPressActionFinished={onPressResult => onPressActionFinished(onPressResult)}
    containerStyle={myTheme.backgroundColor('black')}
    leftIcon={myTheme.appleLoginIcon}
    titleStyle={styles.titleStyle}
    testID="LoginWithApple.Button"
  />
)

const styles = {
  ...StyleSheet.create({
    titleStyle: {
      color: 'black',
      fontFamily: 'Open Sans',
      fontSize: 20,
    },
  }),
}

export default AppleLoginButton
