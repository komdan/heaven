import React from 'react'
import {
  View,
} from 'react-native'

import {
  Text,
} from 'react-native-elements'
import Image from 'react-native-fast-image'
import { myTheme } from '@styles'

const googleLogo = require('@assets/images/google.png')

const GooglePayButton = () => (
  <View style={styles.container}>
    <View style={styles.wrapper}>
      <Image source={googleLogo} style={styles.googleLogo} />
      <Text
        style={styles.title}
      >
        Pay
      </Text>
    </View>
  </View>
)

const logoSize = 25
const styles = {
  container: {
    flex: 1,
    width: '90%',
    marginBottom: 5,
  },
  wrapper: {
    flex: 1,
    flexDirection: 'row',
    height: '90%',
    backgroundColor: myTheme.black,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 7,
  },
  title: {
    fontFamily: 'Open Sans',
    fontSize: 22,
    color: myTheme.white,
  },
  googleLogo: {
    width: logoSize,
    height: logoSize,
    marginRight: 3,
    // position: 'absolute',
  },
}

export default GooglePayButton
