/*
*  Created by Matthieu MARIE-LOUISE
*/
// ======================== Imports ========================
// React-native
import React, { Component } from 'react'
import {
  Alert,
  View,
  TouchableOpacity,
} from 'react-native'
// Helpers & Config
import { withTranslation } from 'react-multi-lang'
// Community
import { Icon } from 'react-native-elements'
import { myTheme } from '@styles'
// import {
//   log,
// } from '@heaven/js'

// ======================== Main Component ========================
class BackButton extends Component {
  state = {} // eslint-disable-line

  alertUserWithConfirmationMessage = () => {
    const {
      t,
      onPress,
      confirmMessage,
      confirmTitle,
    } = this.props
    // CORE
    if (confirmMessage) {
      Alert.alert(
        // Title
        t(confirmTitle),
        // Message
        t(confirmMessage),
        // Buttons
        [
          { text: t('no') },
          { text: t('yesGoBack'), onPress },
        ],
      )
    } else {
      onPress()
    }
  }

  onPress = () => {
    const { hasPress } = this.state

    if (!hasPress) {
      this.setState(
        { hasPress: true },
        () => this.alertUserWithConfirmationMessage(),
      )
      setTimeout(() => this.setState({ hasPress: false }), 2000)
    } else {
      // log('BackButton.js\\onPress()::❌ TRIED TO PRESS ON DISABLED BUTTON')
    }
  }

  render() {
    const {
      color = myTheme.secondaryColor,
      style,
    } = this.props

    return (
      <TouchableOpacity
        onPress={() => this.onPress()}
        testID="BackButton"
      >
        <View style={style}>
          <Icon
            reverse
            type="font-awesome"
            name="chevron-left"
            size={styles.smallIconSize}
            color={styles.grey1}
            reverseColor={color}
          />
        </View>
      </TouchableOpacity>
    )
  }
}

// ======================== Locals ========================
const styles = {
  ...myTheme,
}

// ======================== Exports ========================
export default withTranslation(BackButton)
