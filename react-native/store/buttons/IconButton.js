import React from 'react'
import {
  TouchableOpacity,
} from 'react-native'
import {
  Icon,
} from 'react-native-elements'

const IconButton = ({
  onPress = () => null,
  buttonStyle = {},
  ...props
}) => (
  <TouchableOpacity onPress={onPress} style={buttonStyle}>
    <Icon {...props} />
  </TouchableOpacity>
)

export default IconButton
