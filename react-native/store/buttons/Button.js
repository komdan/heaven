import React from 'react'
import {
  StyleSheet,
} from 'react-native'
import {
  Button as RNEButton,
} from 'react-native-elements'

const Button = ({
  color,
  buttonStyle,
  children,
  ...props
}) => {
  const buttonCustomColorStyle = color ? { backgroundColor: color } : {}
  return (
    <RNEButton
      buttonStyle={StyleSheet.flatten([
        buttonStyle,
        buttonCustomColorStyle,
      ])}
      {...props}
    >
      {children}
    </RNEButton>
  )
}

export default Button
