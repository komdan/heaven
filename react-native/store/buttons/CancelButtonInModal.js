import React from 'react'
import {
  View,
} from 'react-native'
import {
  useTranslation,
} from 'react-multi-lang'
import {
  MyButton,
} from '@heaven/react-native'

import {
  myTheme,
  screenWidth,
} from '@styles'

const CancelButtonInModal = ({
  onCancelPress,
}) => {
  const t = useTranslation()
  return (
    <View style={{ flexDirection: 'row', justifyContent: 'center' }}>
      <MyButton
        containerStyle={{ width: screenWidth * 0.80, backgroundColor: myTheme.error }}
        title={t('Cancel')}
        onPress={onCancelPress}
      />
    </View>
  )
}

export default CancelButtonInModal
