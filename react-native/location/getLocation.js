/*
  Created by Matthieu MARIE-LOUISE
*/
// Community
import {
  Platform,
} from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import {
  debounce,
} from 'lodash'
import {
  error,
  hasLocationPermissionAndroid,
  localCache,
} from '@heaven/react-native'
import {
  NULL_COORDS,
} from '@heaven/constants'

const getCurrentPosition = () => new Promise((resolve) => {
  Geolocation.getCurrentPosition(
    (position) => {
      // log(`getLocation.js\\getCurrentPosition(): ${JSON.stringify(position)}`)
      localCache.set('last-location', position.coords)
      resolve(position)
    },
    async (err) => {
      if (Platform.OS === 'ios') {
        resolve(NULL_COORDS) // comment this line and uncomment below to constantly ask for location
        // await askUserToActivateLocation({
        //   callBack: () => resolve(NULL_COORDS),
        //   t,
        // })
      } else {
        resolve(NULL_COORDS)
        // error(new Error('getLocation.js\\getCurrentPosition(): ERROR in Geolocation.getCurrentPosition(..)'))
        error(err)
      }
    },
    { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 },
  )
})

// ======================== Location - Generic functions ====================================
const getLocation = async () => {
  console.log('getLocation.js\\EXECUTED')
  try {
    // Ask permissionss & get location permissions on the device
    if (Platform.OS === 'android') {
      if (await hasLocationPermissionAndroid()) {
        const currentPosition = await getCurrentPosition()
        return currentPosition
      }
    }
    if (Platform.OS === 'ios') {
      const currentPosition = await getCurrentPosition()
      return currentPosition
    }
    return NULL_COORDS
  } catch (e) {
    return NULL_COORDS
  }
}

const getLocationDebounced = debounce(getLocation, 2000, { leading: true, trailing: false })

export default getLocationDebounced
