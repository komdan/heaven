import {
  post,
} from 'axios'

const {
  SLACK_MONITORING_URL,
  NODE_ENV,
} = process.env

const DEV_PREFIX = NODE_ENV === 'development'
  ? '_'
  : ''

const postSlackNotification = async (
  text,
  slackUrl = SLACK_MONITORING_URL,
) => {
  await post(
    slackUrl,
    { text: DEV_PREFIX + text },
  )
    // .then(result => console.log(result))
    .catch(e => console.error(e))
}

export default postSlackNotification
