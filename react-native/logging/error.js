import crashlytics from '@react-native-firebase/crashlytics'

import pushLog from './pushLog'
import LOG_SESSION from './LOG_SESSION'
import postSlackNotification from './postSlackNotification'
import red from './red'

const {
  NODE_ENV,
} = process.env
const isDevelopment = NODE_ENV === 'development'

// ======================== Logging ====================================
const error = (err, properties) => {
  try {
    const {
      stack = '',
      message = '',
      response: {
        data = '',
      } = {},
    } = err || {}
    const now = (new Date()).toLocaleString('en-US')

    // Regular logging
    if (isDevelopment) console.error(red(message))
    if (isDevelopment && data) console.error(red(JSON.stringify(data)))

    // Send to custom logger
    pushLog(`"${now}";"${red(message)}  ${JSON.stringify(properties)}";"${LOG_SESSION}";"${stack}"`, 'error')
    if (data) {
      pushLog(`"${now}";"${red(JSON.stringify(data))}";"${LOG_SESSION}";"${stack}"`, 'error')
    }
    // postSlackNotification(`"${now}";"${red(message)}  ${JSON.stringify(properties)}";"${LOG_SESSION}";"${stack}"`)
    if (
      message.includes('firestore')
      && !(message.includes('The caller does not have permission to execute the specified operation'))
    ) {
      postSlackNotification(`heaven/react-native:: error.message includes "firestore" - ${message}`)
    }
    crashlytics().log(`Error catched with "error(..)": ${message} prop:${JSON.stringify(properties)} ${stack}`)
  } catch (e) {
    console.error(e)
  }
}

// ======================== Exports ========================
export default error
