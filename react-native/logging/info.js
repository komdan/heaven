/*
  Created by Matthieu MARIE-LOUISE
*/
import crashlytics from '@react-native-firebase/crashlytics'

import pushLog from './pushLog'
import LOG_SESSION from './LOG_SESSION'
import blue from './blue'

const {
  NODE_ENV,
} = process.env

const isDevelopment = NODE_ENV === 'development'
const enableConsoleLog = false

// ======================== Logging ====================================
const info = (event, properties) => {
  const now = (new Date()).toLocaleString('en-US')

  if (typeof properties !== 'undefined') {
    // Regular logging
    if (enableConsoleLog && isDevelopment) console.log(blue(event), properties)

    crashlytics().log(`${event} properties:${JSON.stringify(properties)}`)

    // Send to custom logger
    pushLog(`"${now}";"${blue(event)} ${JSON.stringify(properties)}";"${LOG_SESSION}"`, 'info')
  } else {
    // Regular logging
    if (enableConsoleLog && isDevelopment) console.log(blue(event))

    // Send to custom logger
    pushLog(`"${now}";"${blue(event)}";"${LOG_SESSION}"`, 'info')
  }
}

// ======================== Exports ========================
export default info
