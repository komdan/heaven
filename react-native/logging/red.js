const {
  SHELL_COLORS,
} = require('../../constants')

const red = string => `${SHELL_COLORS.FgRed}${string}${SHELL_COLORS.Reset}`

export default red
