import {
  useEffect,
  useState,
} from 'react'
import {
  sleep,
  isNonEmptyObject,
} from '@heaven/js'
import {
  localCache,
  error,
  log,
} from '@heaven/react-native'

const useDeepLinkAfterCodePush = ({
  contexts,
  treatLink,
  isReactLoaded,
  linkObjInLive,
}) => {
  const [linkObjInCache, setlinkObjInCache] = useState()
  const [triggerTreatLink, settriggerTreatLink] = useState(false)

  useEffect(() => {
    localCache.listen('linkObj', linkObj => {
      setlinkObjInCache(linkObj)
    })
  }, [isReactLoaded])

  useEffect(() => {
    if (
      isReactLoaded
      && (linkObjInCache && isNonEmptyObject(linkObjInCache))
      && !(linkObjInLive && isNonEmptyObject(linkObjInLive))
    ) {
      log('useDeepLinkAfterCodePush.js - APP HITS CODE PUSH - useEffect WILL triggerTreatLink')
      settriggerTreatLink(true)
    }
  }, [
    isReactLoaded,
    JSON.stringify(linkObjInCache),
  ])

  useEffect(() => {
    log('useDeepLinkAfterCodePush.js useEffect []')
    if (
      triggerTreatLink
      && isReactLoaded
    ) {
      const asyncTreatLink = async () => {
        try {
          log(`useDeepLinkAfterCodePush.js - TRIGGERING treatLink  with linkObj:${linkObjInCache}`)
          settriggerTreatLink(false)
          await treatLink({
            linkObj: linkObjInCache,
            contexts,
          })
        } catch (e) {
          error(new Error('useDeepLinkAfterCodePush.js\\error-follows'))
          error(e)
        }
        await sleep(10000) // wait a bit because code push update can happen right now : in that case the deep link will be found in linkObjInCache and will be treated again
        await localCache.remove('linkObj')
      }
      asyncTreatLink()
    }
  }, [
    triggerTreatLink,
    isReactLoaded,
    JSON.stringify(linkObjInCache),
  ])
}

export default useDeepLinkAfterCodePush
