import {
  useEffect,
  useState,
} from 'react'
import {
  // sleep,
  isNonEmptyObject,
} from '@heaven/js'
import {
  useAsync,
} from '@heaven/react'
import {
  getInitialDeepLink,
  // localCache,
  error,
  log,
} from '@heaven/react-native'

const useDeepLinkOnAppClosed = ({
  contexts,
  treatLink,
  isReactLoaded,
}) => {
  const [
    initialLinkObj,
    isinitialLinkObjLoaded,
  ] = useAsync(getInitialDeepLink)
  const [triggerTreatLink, settriggerTreatLink] = useState(false)

  useEffect(() => {
    const asyncMethod = async () => {
      log('useDeepLinkOnAppClosed.js useEffect [isinitialLinkObjLoaded, initialLinkObj]')
      if (isReactLoaded && isinitialLinkObjLoaded && initialLinkObj && isNonEmptyObject(initialLinkObj)) {
        log('useDeepLinkOnAppClosed.js - APP CLOSED - useEffect WILL triggerTreatLink')
        // await localCache.set('linkObj', initialLinkObj) // save for next time if interrupted by a code push flash
        settriggerTreatLink(true)
      }
    }
    asyncMethod()
  }, [
    isReactLoaded,
    isinitialLinkObjLoaded,
    JSON.stringify(initialLinkObj),
  ])

  useEffect(() => {
    log('useDeepLinkOnAppClosed.js useEffect [triggerTreatLink, isReactLoaded, initialLinkObj, liveLinkObj, linkObjInCache, isDeepLinkOnAppOpened]')
    if (
      triggerTreatLink
      && isReactLoaded
    ) {
      const asyncTreatLink = async () => {
        try {
          log(`useDeepLinkOnAppClosed.js - TRIGGERING treatLink  with linkObj:${initialLinkObj}`)
          settriggerTreatLink(false)
          await treatLink({
            linkObj: initialLinkObj,
            contexts,
          })
        } catch (e) {
          error(new Error('useDeepLinkOnAppClosed.js\\error-follows'))
          error(e)
        }
        // await sleep(10000) // wait a bit because code push update can happen right now : in that case the deep link will be found in linkObjInCache and will be treated again
        // await localCache.remove('linkObj')
      }
      asyncTreatLink()
    }
  }, [
    triggerTreatLink,
    isReactLoaded,
    JSON.stringify(initialLinkObj),
  ])
}

export default useDeepLinkOnAppClosed
