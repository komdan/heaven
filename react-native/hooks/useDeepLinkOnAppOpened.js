import {
  useEffect,
  useState,
} from 'react'
import {
  sleep,
  // isNonEmptyObject,
} from '@heaven/js'
import {
  localCache,
  error,
  log,
  onDeepLink,
} from '@heaven/react-native'

const useDeepLinkOnAppOpened = ({
  contexts,
  treatLink,
  isReactLoaded,
}) => {
  const [linkObjInLive, setlinkObjInLive] = useState()
  const [triggerTreatLink, settriggerTreatLink] = useState(false)

  useEffect(() => {
    log('useDeepLinkOnAppOpened.js useEffect [userId, isReactLoaded]')
    if (isReactLoaded) {
      const deepLinkListener = onDeepLink(async (linkObject) => {
        log(`useDeepLinkOnAppOpened.js/ "linkObj" (deepLinkListener-onDeepLink) is ${linkObjInLive}`)
        // cache is used here because code-push installMode IMMEDIATE interrupt treatLink
        // linkObj will be retrieved and treated in the following 'linkObj || linkObjInCache'

        if (linkObject) {
          log('useDeepLinkOnAppOpened.js - APP OPENED - useEffect WILL triggerTreatLink')
          await localCache.set('linkObj', linkObject) // save for next time if interrupted by a code push flash
          setlinkObjInLive(linkObject)
          settriggerTreatLink(true)
        }
      })
      return () => deepLinkListener()
    }
    return () => { }
  }, [
    isReactLoaded,
  ])

  useEffect(() => {
    log('useDeepLinkOnAppOpened.js useEffect [triggerTreatLink, isReactLoaded, initialLinkObj, linkObj, linkObjInCache, isDeepLinkOnAppOpened, ]')
    if (
      triggerTreatLink
      && isReactLoaded
    ) {
      const asyncTreatLink = async () => {
        try {
          log(`useDeepLinkOnAppOpened.js - TRIGGERING treatLink  with linkObj:${linkObjInLive}`)
          settriggerTreatLink(false)
          await treatLink({
            linkObj: linkObjInLive,
            contexts,
          })
        } catch (e) {
          error(new Error('useDeepLinkOnAppOpened.js\\error-follows'))
          error(e)
        }
        setlinkObjInLive(undefined)
        await sleep(20000) // wait a bit because code push update can happen right now : in that case the deep link will be found in linkObjInCache and will be treated again
        await localCache.remove('linkObj')
      }
      asyncTreatLink()
    }
  }, [
    triggerTreatLink,
    isReactLoaded,
    JSON.stringify(linkObjInLive),
  ])

  return ({
    linkObjInLive,
  })
}

export default useDeepLinkOnAppOpened
