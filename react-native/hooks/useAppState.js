import { useState, useEffect } from 'react'
import { AppState } from 'react-native'

import {
  isFunction,
} from '@heaven/js'

const useAppState = ({ onChange, onForeground, onBackground } = {}) => {
  const [appState, setAppState] = useState(AppState.currentState)

  // componentDidMount
  useEffect(() => {
    const handleAppStateChange = (nextAppState) => {
      if (nextAppState === 'active') {
        if (isFunction(onForeground)) onForeground()
      } else if (nextAppState.match(/inactive|background/)) {
        if (isFunction(onBackground)) onBackground()
      }
      setAppState(nextAppState)
      if (isFunction(onChange)) onChange(nextAppState)
    }

    AppState.addEventListener('change', handleAppStateChange)
    // componentWillUnmount
    return () => {
      AppState.removeEventListener('change', handleAppStateChange)
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  return { appState }
}

export default useAppState
