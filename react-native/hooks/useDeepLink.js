import {
  // useState,
  useContext,
  useEffect,
} from 'react'
import {
  NULL_COORDS,
} from '@heaven/constants'
import {
  isNonEmptyObject,
} from '@heaven/js'
import {
  log,
} from '@heaven/react-native'
import {
  useStoreState,
  GlobalContext,
} from '@contexts'

import useDeepLinkOnAppClosed from './useDeepLinkOnAppClosed'
import useDeepLinkOnAppOpened from './useDeepLinkOnAppOpened'
import useDeepLinkAfterCodePush from './useDeepLinkAfterCodePush'

// Usage:
// useDeepLink({ navigation, treatLink, t, user })
const useDeepLink = ({
  treatLink,
} = {}) => {
  const [user = {}] = useStoreState('user')
  const [isUserLoaded = false] = useStoreState('isUserLoaded')
  const [location = NULL_COORDS] = useStoreState('location')
  const [sessionId = ''] = useStoreState('sessionId')
  const [hasJustLaunched = false] = useStoreState('hasJustLaunched')
  const authContext = {
    user,
    location,
    sessionId,
  }

  const globalContext = useContext(GlobalContext)
  const {
    onEnterpriseCodeChange,
    popToTop,
    navigation: {
      navigate,
    } = {},
  } = globalContext
  const {
    user: {
      uid: userId,
    } = {},
  } = authContext
  const contexts = {
    authContext,
    globalContext,
  }

  const isReactLoaded = isNonEmptyObject(globalContext)
    && isNonEmptyObject(authContext)
    && !!(typeof navigate === 'function')
    && !!(typeof onEnterpriseCodeChange === 'function')
    && !hasJustLaunched
    && isUserLoaded

  useEffect(() => {
    log(`useDeepLink.js useEffect ${JSON.stringify({
      isReactLoaded,
      isNonEmptyObjectglobalContext: isNonEmptyObject(globalContext),
      isNonEmptyObjectauthContext: isNonEmptyObject(authContext),
      typeofnavigate: typeof navigate,
      typeofpopToTop: typeof popToTop,
      typeofonEnterpriseCodeChange: typeof onEnterpriseCodeChange,
      userId,
      isUserLoaded,
    })}`)
  })

  useDeepLinkOnAppClosed({
    contexts,
    treatLink,
    isReactLoaded,
  })
  const {
    linkObjInLive,
  } = useDeepLinkOnAppOpened({
    contexts,
    treatLink,
    isReactLoaded,
  })
  useDeepLinkAfterCodePush({
    contexts,
    treatLink,
    isReactLoaded,
    linkObjInLive,
  })
}

export default useDeepLink
