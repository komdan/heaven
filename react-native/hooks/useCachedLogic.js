import {
  useEffect,
} from 'react'

import {
  debounce,
} from 'lodash'

import {
  localCache,
} from '@heaven/react-native'

const saveLogicInStorage = debounce(
  (key, value) => localCache.set(key, value),
  250,
  { leading: true, trailing: false }, // no trailing: we don't want to delay saving after a cleanup
)

const useCachedLogic = ({
  logic,
  setLogic,
  storageKey,
}) => {
  useEffect(() => {
    const init = async () => {
      const initiallyCachedLogic = await localCache.get(storageKey)
      if (initiallyCachedLogic) {
        setLogic(initiallyCachedLogic)
      }
    }
    init()

    const initMultipleTimes = (frequency = 250, duration = 1000) => {
      const interval = setInterval(init, frequency)
      const timeout = setTimeout(() => clearInterval(interval), duration)
      return () => {
        clearInterval(interval)
        clearTimeout(timeout)
      }
    }
    const cancel = initMultipleTimes()

    return () => cancel()
  }, []) // eslint-disable-line

  useEffect(() => {
    saveLogicInStorage(storageKey, logic)
  }, [logic]) // eslint-disable-line

  // componentDidUnmount
  useEffect(() => () => localCache.remove(storageKey), []) // eslint-disable-line
}

export default useCachedLogic
