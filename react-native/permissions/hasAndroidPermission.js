import {
  PermissionsAndroid,
  Platform,
  ToastAndroid,
} from 'react-native'

import {
  // log,
  error,
} from '@heaven/react-native'

const hasAndroidPermission = async ({
  permission,
  t = string => string,
}) => {
  // below works for location permission. Check for other kind of permission
  if (Platform.OS === 'ios'
      || (Platform.OS === 'android' && Platform.Version < 23)) {
    return true
  }

  const permissionOK = await PermissionsAndroid.check(permission)

  if (permissionOK) {
    // log('hasAndroidPermission.js\\requestPermission():: Permission granted: ', permission)
    return true
  }

  const status = await PermissionsAndroid.request(permission)

  if (status === PermissionsAndroid.RESULTS.GRANTED) return true

  if (status === PermissionsAndroid.RESULTS.DENIED) {
    // log('hasAndroidPermission.js\\requestPermission():: Permission denied: ', permission)
    ToastAndroid.show(t('permissionDenied'), ToastAndroid.LONG)
  } else if (status === PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN) {
    // log('hasAndroidPermission.js\\requestPermission():: Permission revoked: ', permission)
    ToastAndroid.show(t('permissionRevoked'), ToastAndroid.LONG)
  } else {
    error(`hasAndroidPermission.js\\requestPermission():: Unidentified error while requesting permissions. status:${JSON.stringify(status)}`)
  }

  return false
}

export default hasAndroidPermission
