import {
  PermissionsAndroid,
} from 'react-native'
import hasAndroidPermission from './hasAndroidPermission'

const hasFineLocationPermission = async ({
  t = string => string,
} = {}) => hasAndroidPermission({
  permission: PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
  t,
})

export default hasFineLocationPermission
