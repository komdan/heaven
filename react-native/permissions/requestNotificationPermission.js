import {
  Platform,
} from 'react-native'

import askUserToActivateNotifications from '../alerts/askUserToActivateNotifications'

import requestNotificationPermissionIOS from './requestNotificationPermissionIOS'
import requestNotificationPermissionAndroid from './requestNotificationPermissionAndroid'

const requestNotificationPermission = async ({
  t,
}) => {
  try {
    if (Platform.OS === 'ios') {
      await requestNotificationPermissionIOS()
    } else {
      await requestNotificationPermissionAndroid()
    }
  } catch (err) {
    // User has rejected permissions
    askUserToActivateNotifications({ t })
    throw err
  }
}

export default requestNotificationPermission
