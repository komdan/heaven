import hasFineLocationPermission from './hasFineLocationPermission'

const hasLocationPermissionAndroid = hasFineLocationPermission

export default hasLocationPermissionAndroid
