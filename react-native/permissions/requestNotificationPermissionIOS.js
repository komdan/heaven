import firebase from '@react-native-firebase/app'

// import {
//   log,
// } from '@heaven/js'

const requestNotificationPermissionIOS = async () => {
  // log('permissions.js\\IOS:requestNotificationPermission(): Requesting permissions...')
  const result = await firebase.messaging().requestPermission()
  // log('permissions.js\\IOS:requestNotificationPermission(): Requesting permissions result:', result)
  return result
}

export default requestNotificationPermissionIOS
