import firebase from '@react-native-firebase/app'

// import {
//   log,
// } from '@heaven/js'

import hasNotificationEnabled from '../firebase/messaging/hasNotificationEnabled'

const requestNotificationPermissionAndroid = async () => {
  // log('permissions.js\\ANDROID:requestNotificationPermission(): Checking permissions...')
  if (!(await hasNotificationEnabled())) {
    // log('permissions.js\\ANDROID:requestNotificationPermission(): Requesting permissions 🔥🍾...')
    await firebase.messaging().requestPermission()
  }
  // log('permissions.js\\ANDROID:requestNotificationPermission(): PERMISSIONS GRANTED 🔥🍾...')
}

export default requestNotificationPermissionAndroid
