import TcpSocket from 'react-native-tcp-socket';

const RNSendTCPPayload = async ({
  host,
  port = 9100,
  payload,
  maxRetries = 5, // Nombre maximum de tentatives
}) => {
  for (let attempt = 1; attempt <= maxRetries; attempt++) {
    let client;
    try {
      console.log(`Attempt ${attempt}: Connecting to ${host}:${port}`);
      return await new Promise((resolve, reject) => {
        client = TcpSocket.createConnection({ host, port }, () => {
          console.log('Connected to the server.');
          try {
            client.write(payload);
          } catch (e) {
            reject(e);
            throw e;
          }
        });

        client.on('data', (data) => {
          console.log('Received data:', data.toString());
          resolve(data);
        });

        client.on('error', (error) => {
          console.error('An error occurred:', error);
          reject(error);
        });

        client.on('close', () => {
          console.log('Connection closed.');
          resolve(); // Si aucun data n'est reçu mais connexion fermée
        });
      });
    } catch (error) {
      console.error(`Attempt ${attempt} failed:`, error);
      if (attempt === maxRetries) {
        console.error('All attempts failed.');
        throw error;
      }
      console.log('Retrying...');
    } finally {
      if (client) {
        console.log('Closing connection.');
        client.destroy(); // Assure la fermeture de la connexion TCP
      }
    }
  }
};

export default RNSendTCPPayload;