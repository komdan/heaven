import stripe from '@heaven/react-native/payment/tipsi-stripe/stripe'

const tokenToPaymentMethod = async (token) => {
  await stripe.createPaymentMethod({
    card: {
      token,
    },
  })
}

export default tokenToPaymentMethod
