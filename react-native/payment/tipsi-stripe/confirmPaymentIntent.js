import stripe from './stripe'
import error from '../../logging/error'
// import {
//   setDocument,
// } from '@heaven/react-native'

const confirmPaymentIntent = async (userId, { client_secret: clientSecret }) => {
  // confirmPaymentIntent && confirmSetupIntent
  try {
    const {
      // paymentIntent: paymentIntentResult,
      error: err,
    } = await stripe.confirmPaymentIntent(
      { clientSecret },
      null,
      { handleActions: false }, // https://stripe.com/docs/js/deprecated/confirm_setup_intent && https://stripe.com/docs/js/deprecated/confirm_payment_intent
    )
    // await setDocument('clients', userId, { lastPaymentIntent: paymentIntentResult })
    if (err) {
      throw new Error(err)
    }
  } catch (e) {
    error(new Error('confirmPaymentIntent.js\\error-follows'))
    error(e)
  }
}

export default confirmPaymentIntent
