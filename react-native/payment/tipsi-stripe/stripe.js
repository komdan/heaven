// import stripe from 'tipsi-stripe'
const stripe = {
  cancelNativePayRequest: () => ({ }),
  completeNativePayRequest: () => ({ }),
  confirmCardPayment: () => ({ }),
  confirmPaymentIntent: () => ({ }),
  createPaymentMethod: () => ({ }),
  openNativePaySetup: () => ({ }),
  paymentRequest: () => ({ }),
  paymentRequestWithNativePay: () => ({ }),
  setOptions: () => ({ }),
  deviceSupportsNativePay: () => false,
  canMakeNativePayPayments: () => false,
}

const {
  STRIPE_PUBLIC_KEY,
  APPLE_MERCHANT_ID,
} = process.env

// ====== Stripe initialisation
stripe.setOptions({
  // publishableKey: 'pk_test_BJhxo8MRjrJ0ZCy3HLZlLCyj',
  publishableKey: STRIPE_PUBLIC_KEY,
  merchantId: APPLE_MERCHANT_ID, // Optional
  androidPayMode: 'test', // Android only
})

export default stripe
