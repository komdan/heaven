// TODO : migrate this
// import { GooglePay } from 'react-native-google-pay'
import {
  error,
} from '@heaven/react-native'
// import {
//   initGooglePay,
// } from '@stripe/stripe-react-native'

import ALLOWED_CARD_NETWORKS from './ALLOWED_CARD_NETWORKS'
import ALLOWED_CARD_AUTH_METHODS from './ALLOWED_CARD_AUTH_METHODS'

const {
  NODE_ENV,
} = process.env

const isGooglePaySupported = async () => {
  try {
    // // ADDED 14/04/2023 when migrating to stripe sdk
    // // src https://www.callstack.com/blog/stripe-react-native-library-configuration
    // await initGooglePay({
    //   testEnv: NODE_ENV === 'development',
    //   // rest of initGooglePay attributes
    // })
    // // END ADDED
    // GooglePay.setEnvironment(NODE_ENV === 'development'
    //   ? GooglePay.ENVIRONMENT_TEST
    //   : GooglePay.ENVIRONMENT_PRODUCTION)
    // const isReady = await GooglePay.isReadyToPay(ALLOWED_CARD_NETWORKS, ALLOWED_CARD_AUTH_METHODS)
    // return isReady
    return false
  } catch (e) {
    error(new Error('googlePay.js\\isGooglePaySupported():: ERROR FOLLOWS'))
    error(e)
    return false
  }
}

export default isGooglePaySupported
