import stripe from '../tipsi-stripe/stripe'

import isVisaAvailable from './isVisaAvailable'
import isMasterCardAvailable from './isMasterCardAvailable'
import isAmexAvailable from './isAmexAvailable'
import isDiscoverAvailable from './isDiscoverAvailable'

const isApplePayConfigured = async () => {
  const allowed = await stripe.deviceSupportsNativePay()

  const visaAvailable = await isVisaAvailable()
  const masterCardAvailable = await isMasterCardAvailable()
  const amexAvailable = await isAmexAvailable()
  const discoverAvailable = await isDiscoverAvailable()

  return allowed
    && (
      visaAvailable
      || masterCardAvailable
      || amexAvailable
      || discoverAvailable
    )
}

export default isApplePayConfigured
