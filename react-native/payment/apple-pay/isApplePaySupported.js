import stripe from '../tipsi-stripe/stripe'

const isApplePaySupported = () => stripe.deviceSupportsNativePay()

export default isApplePaySupported
