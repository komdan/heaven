import stripe from '../tipsi-stripe/stripe'

const isDiscoverAvailable = () => stripe.canMakeNativePayPayments({
  networks: ['discover'],
})

export default isDiscoverAvailable
