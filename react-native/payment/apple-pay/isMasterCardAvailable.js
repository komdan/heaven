import stripe from '../tipsi-stripe/stripe'

const isMasterCardAvailable = () => stripe.canMakeNativePayPayments({
  networks: ['master_card'],
})

export default isMasterCardAvailable
