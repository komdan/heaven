import stripe from '../tipsi-stripe/stripe'

const isAmexAvailable = () => stripe.canMakeNativePayPayments({
  networks: ['american_express'],
})
export default isAmexAvailable
