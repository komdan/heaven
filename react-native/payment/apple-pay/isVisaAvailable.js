import stripe from '../tipsi-stripe/stripe'

const isVisaAvailable = () => stripe.canMakeNativePayPayments({
  networks: ['visa'],
})

export default isVisaAvailable
