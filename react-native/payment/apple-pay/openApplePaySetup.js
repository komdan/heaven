import stripe from '../tipsi-stripe/stripe'

const openApplePaySetup = () => stripe.openNativePaySetup()

export default openApplePaySetup
