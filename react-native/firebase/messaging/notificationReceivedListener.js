// import firebase from '@react-native-firebase/app'

// const notificationReceivedListener = callBack => firebase
//   .notifications()
//   .onNotification(
//     (notification) => {
//       callBack(notification)
//     },
//   )

import {
  localCache,
} from '@heaven/react-native'

const notificationReceivedListener = callBack => localCache.listen('notificationReceived', callBack)

export default notificationReceivedListener
