import requestNotificationPermission from '../../permissions/requestNotificationPermission'
import getFcmToken from './getFcmToken'

const getPushToken = async ({
  onDone,
  t,
}) => {
  // requestNotificationPermission() // <- c'était comme ça avant le grand ménage, à tester. TODO: tester et supprimer cette ligne
  await requestNotificationPermission({ t })

  const pushToken = await getFcmToken()

  await onDone(pushToken)

  return pushToken
}

export default getPushToken
