// import {
//   log,
// } from '@heaven/js'
// import firebase from '@react-native-firebase/app'

// const notificationOpenedListener = callBack => firebase
//   .notifications()
//   .onNotificationOpened(async ({
//     // action,
//     notification,
//     // results,
//   }) => {
//     // log('notificationOpenedListener.js/notificationOpened :', Object.keys(notification))
//     await callBack(notification)
//   })

// const notificationOpenedListener = () => {}

import {
  localCache,
} from '@heaven/react-native'

const notificationOpenedListener = callBack => localCache.listen('notificationOpened', callBack)

export default notificationOpenedListener
