import firebase from '@react-native-firebase/app'

import {
  // log,
  error,
} from '@heaven/react-native'

const hasNotificationEnabled = async () => {
  if (await firebase.messaging().hasPermission()) {
    // user has permissions
    // log('hasNotificationEnabled.js\\OK ! notifications are enabled')
    return true
  }
  // user doesn't have permission
  error(new Error('hasNotificationEnabled.js\\OUCH (not an real error in app) ! Notifications are disabled, probably by user'))
  return false
}

export default hasNotificationEnabled
