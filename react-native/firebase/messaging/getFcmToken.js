import firebase from '@react-native-firebase/app'

const getFcmToken = async () => {
  const fcmToken = await firebase.messaging().getToken()
  if (fcmToken) {
    return fcmToken
  }
  // user doesn't have a device token yet
  throw Error('getFcmToken.js::error-fetching-device-fcm-token')
}

export default getFcmToken
