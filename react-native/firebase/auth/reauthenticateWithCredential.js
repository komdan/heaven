import firebase from '@react-native-firebase/app'

const reauthenticateWithCredential = (currentPassword) => {
  const { currentUser } = firebase.auth()
  const credential = firebase.auth.EmailAuthProvider.credential(currentUser.email, currentPassword)
  return currentUser.reauthenticateAndRetrieveDataWithCredential(credential)
}

export default reauthenticateWithCredential
