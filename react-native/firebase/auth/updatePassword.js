import firebase from '@react-native-firebase/app'

const updatePassword = password => (
  firebase.auth().currentUser.updatePassword(password)
)

export default updatePassword
