import firebase from '@react-native-firebase/app'

const {
  FIREBASE_AUTH_CONFIRMATION_EMAIL_REDIRECT,
} = process.env

const sendEmailVerification = () => (
  firebase.auth().currentUser.sendEmailVerification({
    url: FIREBASE_AUTH_CONFIRMATION_EMAIL_REDIRECT,
  })
)

export default sendEmailVerification
