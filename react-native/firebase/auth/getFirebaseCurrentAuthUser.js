import firebase from '@react-native-firebase/app'

const getFirebaseCurrentAuthUser = () => firebase.auth().currentUser

export default getFirebaseCurrentAuthUser
