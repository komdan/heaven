import firebase from '@react-native-firebase/app'

const sendPasswordResetEmail = email => (
  firebase.auth().sendPasswordResetEmail(email)
)

export default sendPasswordResetEmail
