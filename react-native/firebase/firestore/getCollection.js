import getFirestoreCollection from './getFirestoreCollection'

const getFirestoreCollectionWithAddedFields = async (path, addFields = async () => ({})) => {
  const collection = await getFirestoreCollection(path)
  const collectionWithAddedFields = await Promise.all(collection.map(
    async doc => ({ ...doc, ...(await addFields(doc)) }),
  ))
  return collectionWithAddedFields
}

// ====== Cache-aware
const getCollection = getFirestoreCollectionWithAddedFields

export default getCollection
