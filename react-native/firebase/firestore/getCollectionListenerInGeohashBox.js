import {
  incrementAlphabetical,
} from '@heaven/js'
import firebase from '@react-native-firebase/app'
import {
  error,
} from '@heaven/react-native'

// TODO: check if it works
const getCollectionListenerInGeohashBox = (
  path,
  geohash = '',
  callBack = () => { },
  onError = () => { },
) => (!geohash ? [] : (
  firebase.firestore()
    .collection(path)
    .where('geohash', '>=', geohash)
    .where('geohash', '<', incrementAlphabetical(geohash))
    .onSnapshot(
      snapshot => callBack(snapshot.docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
      }))),
      e => {
        console.error(e)
        error(new Error(`getCollectionListenerInGeohashBox.js firebase firestore error for path:${path} geohash:${geohash}`))
        error(e)
        onError(e)
      },
    )
))

export default getCollectionListenerInGeohashBox
