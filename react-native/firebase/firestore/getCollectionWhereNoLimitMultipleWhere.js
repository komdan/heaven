import {
  chunk,
  flatten,
} from 'lodash'
import getCollectionMultipleWhere from './getCollectionMultipleWhere'

const FIRESTORE_IN_FILTER_LENGTH_LIMIT = 10 // imposed by firestore

const getCollectionWhereNoLimitMultipleWhere = async (collection, field, operator, value, conditions) => {
  const valueChunks = chunk(value, FIRESTORE_IN_FILTER_LENGTH_LIMIT)

  const collectionChunks = await Promise.all(valueChunks.map(async (valueChunk) => {
    const collectionChunk = await getCollectionMultipleWhere(collection, field, operator, valueChunk, conditions)
    return collectionChunk
  }))

  return flatten(collectionChunks)
}

export default getCollectionWhereNoLimitMultipleWhere
