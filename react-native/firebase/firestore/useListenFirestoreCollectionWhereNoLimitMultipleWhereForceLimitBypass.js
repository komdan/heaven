import {
  useEffect,
  useState,
} from 'react'
import {
  chunk,
  flatten,
  // isArray,
  isEqual,
  reject,
} from 'lodash'
// import {
//   log,
// } from '@heaven/js'
import getCollectionMultipleWhereListener from './getCollectionMultipleWhereListener'

const FIRESTORE_IN_FILTER_LENGTH_LIMIT = 10 // imposed by firestore
let toTreat = []

const useListenFirestoreCollectionWhereNoLimitMultipleWhereForceLimitBypass = (collectionPath, field, operator, value, conditions, {
  enableRequest = true,
} = {}) => {
  // operator should be 'in' or 'not-in'
  // console.log('barsIds.length', value.length)
  const [isCollectionLoaded, setIsLoaded] = useState(false)
  const [collection, setCollection] = useState([])

  const [areCollectionLoadedChunks, setareCollectionLoadedChunks] = useState() // [true, false, true, true, false, ...]
  const [collectionChunks, setCollectionChunks] = useState()

  const [shouldTreatChunks, setshouldTreatChunks] = useState(false)

  const valueChunks = chunk(value, FIRESTORE_IN_FILTER_LENGTH_LIMIT) // [['barId1', .., 'barId10'], ['barId11', ..], ..]
  const initalCollectionChunks = valueChunks.map(() => []) // [[], [], ..]]
  const initalareCollectionLoadedChunks = valueChunks.map(() => false) // [false, false, ..]

  // useEffect(() => {
  //   console.log('areCollectionLoadedChunks', areCollectionLoadedChunks)
  //   console.log('collection.length', collection.length)
  // })

  useEffect(() => {
    if (shouldTreatChunks) {
      const newCollectionChunks = (collectionChunks || initalCollectionChunks)
      const newAreCollectionsChunksLoaded = (areCollectionLoadedChunks || initalareCollectionLoadedChunks)
      const toTreatCopy = [...toTreat]

      toTreatCopy.forEach(({ index, newCollectionChunk }) => {
        newCollectionChunks[index] = newCollectionChunk
        newAreCollectionsChunksLoaded[index] = true
        toTreat = reject(toTreat, { index })
        // console.log(`TREATED ${index}`)
      })

      if (!isEqual(newAreCollectionsChunksLoaded, areCollectionLoadedChunks)) {
        setareCollectionLoadedChunks(newAreCollectionsChunksLoaded)
      }
      if (!isEqual(newCollectionChunks, collectionChunks)) {
        setCollectionChunks(newCollectionChunks)
      }
      setshouldTreatChunks(false)
    }
  }, [shouldTreatChunks])

  useEffect(() => {
    // log('useListenFirestoreCollectionWhereNoLimitMultipleWhere.js useEffect [collectionPath, field, operator, value]')
    if (enableRequest) {
      const listeners = valueChunks.map( // listeners : [collectionListener1, collectionListener2, ..]
        (valueChunk, index) => ( // valueChunk : ['barId1', .., 'barId10']
          getCollectionMultipleWhereListener(
            collectionPath, // 'orders'
            [
              {
                field, // 'barId'
                operator, // 'in'
                value: valueChunk, // barsIds, chunked
              },
              ...conditions,
            ],
            (newCollectionChunk) => {
              console.log(`useListenFirestoreCollectionWhereNoLimitMultipleWhere ${index} ${valueChunk}`)
              toTreat.push({
                newCollectionChunk,
                index,
              })
              setshouldTreatChunks(true)
            },
          )),
      )
      return () => listeners.forEach(listener => listener())
    }
    return () => { }
  }, [collectionPath, field, operator, value, enableRequest])

  useEffect(() => {
    if (areCollectionLoadedChunks && collectionChunks) {
      const isAllChunksLoaded = areCollectionLoadedChunks.reduce(
        (accu, isChunkLoaded) => accu && isChunkLoaded,
        true,
      )
      if (isAllChunksLoaded) {
        setCollection(flatten(collectionChunks))
        setIsLoaded(true)
      } else {
        setIsLoaded(false)
      }
    }
  }, [shouldTreatChunks])

  const loadingProgression = +((
    +(((areCollectionLoadedChunks || []).filter(loaded => loaded) || []).length) / +((areCollectionLoadedChunks || []).length)
  ) * 100).toFixed(0) || 0

  return [
    collection,
    isCollectionLoaded,
    loadingProgression,
  ]
}

export default useListenFirestoreCollectionWhereNoLimitMultipleWhereForceLimitBypass
