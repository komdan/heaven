import firebase from '@react-native-firebase/app'
import {
  error,
} from '@heaven/react-native'

function getCollectionListener(
  path,
  callBack,
  onError = () => { },
) {
  return (
    firebase.firestore()
      .collection(path)
      .onSnapshot(
        snapshot => callBack(snapshot.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        }))),
        e => {
          console.error(e)
          error(new Error(`getCollectionListener.js firebase firestore error for path:${path}`))
          error(e)
          onError(e)
        },
      )
  )
}

export default getCollectionListener
