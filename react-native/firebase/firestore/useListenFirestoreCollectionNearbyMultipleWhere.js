import {
  useEffect,
  useState,
} from 'react'
// import {
//   log,
// } from '@heaven/react-native'
import getCollectionNearbyMultipleWhereListener from './getCollectionNearbyMultipleWhereListener'

const useListenFirestoreCollectionNearbyMultipleWhere = (
  collectionPath,
  baseGeohash,
  maxDistanceInMeter,
  conditions = [],
  {
    enableRequest = true,
    resetIsLoadedOnMaxDistanceInMeterChange = false,
  } = {},
) => {
  // log('useListenFirestoreCollectionNearbyMultipleWhere.js executed')
  const [collection, setCollection] = useState()
  const [isLoaded, setIsLoaded] = useState(false)
  const [isPartlyLoaded, setIsPartlyLoaded] = useState(false)

  useEffect(() => {
    // log('useListenFirestoreCollectionNearbyMultipleWhere.js\\useEffect [collectionPath, conditions]')
    if (enableRequest) {
      const listener = getCollectionNearbyMultipleWhereListener(
        collectionPath,
        baseGeohash,
        maxDistanceInMeter,
        conditions,
        (newCollection) => {
          if (!collection?.length || newCollection?.length) {
            setCollection(newCollection)
          }
          // setIsLoaded(true)
        },
        () => { },
        setIsLoaded,
      )
      return () => listener()
    }
    return () => { }
  }, [collectionPath, baseGeohash, maxDistanceInMeter, JSON.stringify(conditions), enableRequest])

  useEffect(() => { // should be useless
    // log('useListenFirestoreCollectionNearbyMultipleWhere.js\\useEffect [collection]')
    if (collection && collection.length) setIsPartlyLoaded(true)
  }, [collection])
  useEffect(() => {
    if (resetIsLoadedOnMaxDistanceInMeterChange) {
      setIsLoaded(false)
    }
  }, [maxDistanceInMeter])

  return [
    collection,
    isLoaded,
    isPartlyLoaded,
  ]
}

export default useListenFirestoreCollectionNearbyMultipleWhere
