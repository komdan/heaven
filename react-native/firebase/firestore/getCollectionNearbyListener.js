import {
  getDistance,
} from 'geolib'
import {
  neighbors,
  decode,
} from 'ngeohash'
import {
  flattenDeep,
  debounce,
  range,
} from 'lodash'
import {
  computeAdequateGeohashLength,
} from '@heaven/js'

import getCollectionListenerInGeohashBox from './getCollectionListenerInGeohashBox'

// TODO: check if this works
const getCollectionNearbyListener = (
  collectionPath,
  baseGeohash,
  maxDistanceInMeter,
  callBack,
  onError,
) => {
  const collectionChunks = range(0, 9).map(() => []) // array of 9 elements containing 9 collections on the different boxes

  const baseGeohashTruncated = baseGeohash.substr(
    0,
    computeAdequateGeohashLength(maxDistanceInMeter),
  )

  const baseGeohashes = [
    baseGeohashTruncated,
    ...neighbors(baseGeohashTruncated),
  ]

  const debouncedCallBack = debounce(callBack, 200) // debounced setCollection callback here prevents too much react rerenders
  const chunkedCallback = index => (newCollection) => {
    collectionChunks[index] = newCollection
      .filter(({ geohash = '' }) => (
        getDistance(decode(geohash), decode(baseGeohash)) <= maxDistanceInMeter
      ))
    debouncedCallBack(flattenDeep(collectionChunks))
  }

  const listeners = [
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[0], chunkedCallback(0), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[1], chunkedCallback(1), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[2], chunkedCallback(2), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[3], chunkedCallback(3), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[4], chunkedCallback(4), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[5], chunkedCallback(5), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[6], chunkedCallback(6), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[7], chunkedCallback(7), onError),
    getCollectionListenerInGeohashBox(collectionPath, baseGeohashes[8], chunkedCallback(8), onError),
  ]

  const clearListeners = () => listeners.forEach(listener => listener())

  return clearListeners
}

export default getCollectionNearbyListener
