import firebase from '@react-native-firebase/app'
import {
  error,
} from '@heaven/react-native'

function getDocumentListener(
  path,
  docId,
  callBack,
  onError = () => { },
) {
  return (
    firebase.firestore()
      .collection(path)
      .doc(docId)
      .onSnapshot((doc) => {
        if (doc.exists) {
          callBack({ id: doc.id, path, ...doc.data() })
        } else {
          callBack({})
        }
      }, e => {
        console.error(new Error(`getDocumentListener.js firebase firestore error for path:${path} docId:${docId}`))
        console.error(e)
        error(new Error(`getDocumentListener.js firebase firestore error for path:${path} docId:${docId}`))
        error(e)
        onError(e)
      })
  )
}

export default getDocumentListener
