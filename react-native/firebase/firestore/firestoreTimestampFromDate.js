import firebase from '@react-native-firebase/app'

const firestoreTimestampFromDate = date => firebase.firestore.Timestamp.fromDate(date)

export default firestoreTimestampFromDate
