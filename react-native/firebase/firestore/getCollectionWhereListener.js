import firebase from '@react-native-firebase/app'
import {
  isEmptyArray,
} from '@heaven/js'
import {
  error,
} from '@heaven/react-native'

function getCollectionWhereListener(
  path,
  field,
  operator,
  value,
  callBack,
  onError = () => { },
) {
  if (isEmptyArray(value)) return () => { }

  return (
    firebase.firestore()
      .collection(path)
      .where(field, operator, value)
      .onSnapshot(
        snapshot => callBack(snapshot.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        }))),
        e => {
          console.error(e)
          error(new Error(`getCollectionWhereListener.js firebase firestore error for path:${path} field:${field} operator:${operator} value:${JSON.stringify(value)}`))
          error(e)
          onError(e)
        },
      )
  )
}

export default getCollectionWhereListener
