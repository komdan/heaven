import firebase from '@react-native-firebase/app'

function setDocument(path, docId, document) {
  return (
    firebase.firestore()
      .collection(`${path}`)
      .doc(`${docId}`)
      .set(document || {}, { merge: true })
  )
}

export default setDocument
