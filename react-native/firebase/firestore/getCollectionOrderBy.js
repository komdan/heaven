import firebase from '@react-native-firebase/app'

import withOnUpdateListener from './withOnUpdateListener'
import MAX_RESULT_LIMIT from './MAX_RESULT_LIMIT'

function getCollectionOrderBy(path, sortingField, descOrAsc, limit = MAX_RESULT_LIMIT) {
  const data = []
  return (
    firebase.firestore()
      .collection(path)
      .orderBy(sortingField, descOrAsc)
      .limit(limit)
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            data.push(withOnUpdateListener({
              ...doc.data(),
              id: doc.id,
              path,
            }))
          })
        return data
      })
  )
}

export default getCollectionOrderBy
