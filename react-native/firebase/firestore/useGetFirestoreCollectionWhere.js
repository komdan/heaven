import {
  useEffect,
  useState,
  useMemo,
} from 'react'
import {
  // log,
  error,
} from '@heaven/react-native'
import getCollectionWhere from './getCollectionWhere'

const useGetFirestoreCollectionWhere = (
  collectionPath,
  field,
  operator,
  value,
  {
    enableRequest = true,
  } = {},
) => {
  const [collection, setCollection] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    const asyncMethod = async () => {
      try {
        if (enableRequest) {
          setCollection(await getCollectionWhere(collectionPath, field, operator, value))
          setIsLoaded(true)
        }
      } catch (e) {
        console.error(e)
        error(e)
      }
    }
    asyncMethod()
    return () => { }
  }, [collectionPath, field, operator, JSON.stringify(value), enableRequest])

  useEffect(() => { // should be useless
    if (collection && collection.length && !isLoaded) setIsLoaded(true)
  }, [
    JSON.stringify(collection),
    isLoaded,
  ])

  return useMemo(() => [
    collection,
    isLoaded,
  ], [
    JSON.stringify(collection),
    isLoaded,
  ])
}

export default useGetFirestoreCollectionWhere
