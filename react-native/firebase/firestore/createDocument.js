import firebase from '@react-native-firebase/app'

function createDocument(path, docId, document) {
  return (
    firebase.firestore()
      .collection(`${path}`)
      .doc(`${docId}`)
      .set(document || {})
  )
}

export default createDocument
