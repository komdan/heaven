import firebase from '@react-native-firebase/app'

function updateDocument(collection, docId, updatedObject) {
  return (
    firebase.firestore()
      .collection(`${collection}`)
      .doc(`${docId}`)
      .update(updatedObject)
  )
}

export default updateDocument
