import firebase from '@react-native-firebase/app'

import withOnUpdateListener from './withOnUpdateListener'

function getFirestoreCollection(path) {
  // Will putt data here
  const data = []

  // returns the firestore promise
  return (
    firebase.firestore()
      .collection(path)
      .get()
      .then(async (snapshot) => {
        if (snapshot === null) return []
        await Promise.all(
          snapshot
            .docs
            .map(async (doc) => {
              // Add the id, on each object, easier for referencing
              // Add the id, on each object, easier for referencing
              // With a better backend (remote timestam cache)
              // const obj = {
              //   ...(await getDocument(path, doc.id)),
              //   id: doc.id,
              // }

              // With a lesser backend (remote timestamp cache)
              data.push(withOnUpdateListener({
                ...doc.data(),
                id: doc.id, // used everywhere and for onUpdateListener
                path, // used for onUpdateListener
              }))
            }),
        )

        return data
      })
  )
}

export default getFirestoreCollection
