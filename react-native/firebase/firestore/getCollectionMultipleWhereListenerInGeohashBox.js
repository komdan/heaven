import {
  incrementAlphabetical,
} from '@heaven/js'
import firebase from '@react-native-firebase/app'
import {
  error,
} from '@heaven/react-native'

// TODO: check if it works
const getCollectionMultipleWhereListenerInGeohashBox = (
  path = '',
  geohash = '',
  conditions = [], // [{field, operator, value}] // eslint-disable-line
  callBack = () => { },
  onError = () => { },
) => {
  if (!geohash || !path) {
    return () => { }
  }
  try {
    let query = firebase.firestore().collection(path)
      .where('geohash', '>=', geohash)
      .where('geohash', '<', incrementAlphabetical(geohash))

    conditions.forEach(({ field, operator, value }) => {
      query = query.where(field, operator, value)
    })

    return query
      .onSnapshot(
        snapshot => callBack(snapshot.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        }))),
        e => {
          console.error(e)
          error(new Error(`getCollectionMultipleWhereListenerInGeohashBox.js firebase firestore error for path:${path} geohash:${geohash}`))
          error(e)
          onError(e)
        },
      )
  } catch (e) {
    console.error(e)
    error('getCollectionMultipleWhereListenerInGeohashBox.js ERROR FOLLOWS')
    error('🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲')
    error(e)
    return () => { }
  }
}

export default getCollectionMultipleWhereListenerInGeohashBox
