import firebase from '@react-native-firebase/app'

function updateDocumentInSubcollection(collection1, docId1, collection2, docId2, updatedObject) {
  return (
    firebase.firestore()
      .collection(`${collection1}`)
      .doc(`${docId1}`)
      .collection(`${collection2}`)
      .doc(`${docId2}`)
      .update(updatedObject)
  )
}

export default updateDocumentInSubcollection
