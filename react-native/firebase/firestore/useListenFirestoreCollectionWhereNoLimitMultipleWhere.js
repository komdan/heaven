import {
  isEmptyArray,
} from '@heaven/js'
import useListenFirestoreCollectionMultipleWhere from './useListenFirestoreCollectionMultipleWhere'
import useListenFirestoreCollectionWhereNoLimitMultipleWhereForceLimitBypass from './useListenFirestoreCollectionWhereNoLimitMultipleWhereForceLimitBypass'

const useListenFirestoreCollectionWhereNoLimitMultipleWhere = (collectionPath, field, operator, value, conditions) => {
  const shouldBypassFirebaseLimit = isEmptyArray(value) // the ForceLimitBypass allows empty array. The other hooks does not allow empty array. Without this we have a firebase issue.
    || value.length > 10

  const [
    collectionWithLimitBypass,
    isCollectionLoadedWithLimitBypass,
    loadingProgressionWithLimitBypass,
  ] = useListenFirestoreCollectionWhereNoLimitMultipleWhereForceLimitBypass(
    collectionPath,
    field,
    operator,
    value,
    conditions,
    {
      enableRequest: shouldBypassFirebaseLimit,
    },
  )
  const [
    collectionWithoutLimitBypass,
    isCollectionLoadedWithoutLimitBypass,
  ] = useListenFirestoreCollectionMultipleWhere(
    collectionPath,
    [
      {
        field,
        operator,
        value,
      },
      ...conditions,
    ],
    {
      enableRequest: !shouldBypassFirebaseLimit,
    },
  )
  const loadingProgressionWithoutLimitBypass = isCollectionLoadedWithoutLimitBypass ? 100 : 0

  return [
    shouldBypassFirebaseLimit ? collectionWithLimitBypass : collectionWithoutLimitBypass,
    shouldBypassFirebaseLimit ? isCollectionLoadedWithLimitBypass : isCollectionLoadedWithoutLimitBypass,
    shouldBypassFirebaseLimit ? loadingProgressionWithLimitBypass : loadingProgressionWithoutLimitBypass,
  ]
}

export default useListenFirestoreCollectionWhereNoLimitMultipleWhere
