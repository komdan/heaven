import firebase from '@react-native-firebase/app'
import withOnUpdateListener from './withOnUpdateListener'

function getFirestoreDocument(
  path,
  docId,
) {
  return (
    firebase.firestore()
      .collection(path)
      .doc(docId)
      .get()
      .then((doc) => {
        if (!doc.exists) {
          return ({})
        }
        return withOnUpdateListener({
          ...doc.data(),
          id: doc.id,
          path,
        })
      })
  )
}

export default getFirestoreDocument
