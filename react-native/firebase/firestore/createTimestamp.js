import firebase from '@react-native-firebase/app'

const createTimestamp = () => firebase.firestore.FieldValue.serverTimestamp()

export default createTimestamp
