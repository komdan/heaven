import {
  useEffect,
  useState,
} from 'react'
import getDocumentListener from './getDocumentListener'

const useListenFirestoreDocument = (path, docId, {
  enableRequest = true,
} = {}) => {
  // log('useListenFirestoreDocument.js executed')
  const [doc, setDoc] = useState()
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    // log('useListenFirestoreDocument.js useEffect [documentPath]')
    if (enableRequest) {
      const listener = getDocumentListener(
        path,
        docId,
        (newDoc) => {
          setDoc(newDoc)
          setIsLoaded(true)
        },
        (e) => { // eslint-disable-line
          setDoc([])
          setIsLoaded(true)
        },
      )
      return () => listener()
    }
    return () => { }
  }, [path, docId])

  useEffect(() => {
    // log('useListenFirestoreDocument.js useEffect [doc]')

    if (doc && doc.length) setIsLoaded(true)
  }, [doc])

  return [
    doc,
    isLoaded,
  ]
}

export default useListenFirestoreDocument
