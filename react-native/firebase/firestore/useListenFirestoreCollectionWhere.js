import {
  useEffect,
  useState,
  useMemo,
} from 'react'
import {
  log,
} from '@heaven/react-native'
import getCollectionWhereListener from './getCollectionWhereListener'

const useListenFirestoreCollectionWhere = (
  collectionPath,
  field,
  operator,
  value,
  {
    enableRequest = true,
  } = {},
) => {
  const [collection, setCollection] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    try {
      if (enableRequest) {
        const listener = getCollectionWhereListener(
          collectionPath,
          field,
          operator,
          value,
          (newCollection) => {
            log('useListenFirestoreCollectionWhere.js TRIGGERED')
            setCollection(newCollection)
            setIsLoaded(true)
          },
        )
        return () => listener()
      }
    } catch (e) {
      console.error(e)
    }
    return () => { }
  }, [collectionPath, field, operator, JSON.stringify(value), enableRequest])

  useEffect(() => { // should be useless
    if (collection && collection.length && !isLoaded) setIsLoaded(true)
  }, [
    JSON.stringify(collection),
    isLoaded,
  ])

  return useMemo(() => [
    collection,
    isLoaded,
  ], [
    JSON.stringify(collection),
    isLoaded,
  ])
}

export default useListenFirestoreCollectionWhere
