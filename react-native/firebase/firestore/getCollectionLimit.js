import firebase from '@react-native-firebase/app'

import withOnUpdateListener from './withOnUpdateListener'
import MAX_RESULT_LIMIT from './MAX_RESULT_LIMIT'

function getCollectionLimit(path, limit = MAX_RESULT_LIMIT) {
  // Will putt data here
  const data = []

  // return the firestore promise
  return (
    firebase.firestore()
      .collection(path)
      .limit(limit)
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            data.push(withOnUpdateListener({
              ...doc.data(),
              id: doc.id,
              path,
            }))
          })
        return data
      })
  )
}
export default getCollectionLimit
