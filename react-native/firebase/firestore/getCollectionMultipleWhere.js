import firebase from '@react-native-firebase/app'

const getCollectionMultipleWhere = async (
  collection,
  conditions,
) => {
  // Will putt data here
  const data = []

  // return the firestore promise
  let query = firebase.firestore().collection(collection)
  conditions.forEach(({ field, operator, value }) => {
    query = query.where(field, operator, value)
  })

  return query
    .get()
    .then((snapshot) => {
      if (snapshot == null) {
        return []
      }
      snapshot
        .docs
        .forEach((doc) => {
          // Add the id, on each object, easier for referencing
          const obj = doc.data()
          data.push({
            ...obj,
            id: doc.id,
          })
        })
      return data
    })
}

export default getCollectionMultipleWhere
