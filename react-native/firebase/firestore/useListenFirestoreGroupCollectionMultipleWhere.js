import {
  useEffect,
  useState,
} from 'react'
// import {
//   log,
// } from '@heaven/react-native'
import getGroupCollectionMultipleWhereListener from './getGroupCollectionMultipleWhereListener'

const useListenFirestoreGroupCollectionMultipleWhere = (
  collectionPath,
  conditions = [],
  {
    enableRequest = true,
  } = {},
) => {
  // log('useListenFirestoreGroupCollectionMultipleWhere.js executed')
  const [collection, setCollection] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    // log('useListenFirestoreGroupCollectionMultipleWhere.js\\useEffect [collectionPath, conditions]')
    if (enableRequest) {
      const listener = getGroupCollectionMultipleWhereListener(
        collectionPath,
        conditions,
        (newCollection) => {
          setCollection(newCollection)
          setIsLoaded(true)
        },
      )
      return () => listener()
    }
    return () => { }
  }, [collectionPath, JSON.stringify(conditions), enableRequest])

  useEffect(() => { // should be useless
    // log('useListenFirestoreGroupCollectionMultipleWhere.js\\useEffect [collection]')
    if (collection && collection.length && !isLoaded) setIsLoaded(true)
  }, [collection, isLoaded])

  return [
    collection,
    isLoaded,
  ]
}

export default useListenFirestoreGroupCollectionMultipleWhere
