import {
  useEffect,
  useState,
} from 'react'
import getDocument from './getDocument'

const useGetFirestoreDocuments = (path, docIds, {
  enableRequest = true,
} = {}) => {
  // log('useGetFirestoreDocuments.js executed')
  const [docs, setDocs] = useState()
  const [isLoaded, setIsLoaded] = useState()

  useEffect(() => {
    // log('useGetFirestoreDocuments.js useEffect [documentPath]')
    const asyncMethod = async () => {
      if (enableRequest) {
        try {
          const newDocs = await Promise.all(docIds.map(async docId => {
            const doc = await getDocument(path, docId)
            return doc
          }))
          setDocs(newDocs)
          setIsLoaded(true)
        } catch (e) {
          console.error(e)
          // setDocs([])
          // setIsLoaded(true)
        }
      }
    }
    asyncMethod()
  }, [path, JSON.stringify(docIds), enableRequest])

  // useEffect(() => {
  //   // log('useGetFirestoreDocuments.js useEffect [docs]')

  //   if (docs && docs.length) setIsLoaded(true)
  // }, [docs])

  return [
    docs,
    isLoaded,
  ]
}

export default useGetFirestoreDocuments
