import {
  useEffect,
  useState,
} from 'react'
// import {
//   log,
// } from '@heaven/react-native'
import getCollectionMultipleWhereListener from './getCollectionMultipleWhereListener'

// MUTED FOR DEBUG BELOW
// const useListenFirestoreCollectionMultipleWhere = () => [[], true]

const useListenFirestoreCollectionMultipleWhere = (
  collectionPath,
  conditions = [],
  {
    enableRequest = true,
  } = {},
) => {
  // log('useListenFirestoreCollectionMultipleWhere.js executed')
  const [collection, setCollection] = useState([])
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    // log('useListenFirestoreCollectionMultipleWhere.js\\useEffect [collectionPath, conditions]')
    if (enableRequest) {
      const listener = getCollectionMultipleWhereListener(
        collectionPath,
        conditions,
        (newCollection) => {
          setCollection(newCollection)
          setIsLoaded(true)
        },
      )
      return () => listener()
    }
    return () => { }
  }, [collectionPath, JSON.stringify(conditions), enableRequest])

  useEffect(() => { // should be useless
    // log('useListenFirestoreCollectionMultipleWhere.js\\useEffect [collection]')
    if (collection && collection.length && !isLoaded) setIsLoaded(true)
  }, [collection, isLoaded])

  return [
    collection,
    isLoaded,
  ]
}

export default useListenFirestoreCollectionMultipleWhere
