import {
  useEffect,
  useState,
} from 'react'
import getDocument from './getDocument'

const useGetFirestoreDocument = (path, docId, {
  enableRequest = true,
} = {}) => {
  // log('useGetFirestoreDocument.js executed')
  const [doc, setDoc] = useState()
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    // log('useGetFirestoreDocument.js useEffect [documentPath]')
    const asyncMethod = async () => {
      if (enableRequest) {
        try {
          const newDoc = await getDocument(path, docId)
          setDoc(newDoc)
          setIsLoaded(true)
        } catch (e) {
          setDoc([])
          setIsLoaded(true)
        }
      }
    }
    asyncMethod()
  }, [path, docId])

  useEffect(() => {
    // log('useGetFirestoreDocument.js useEffect [doc]')

    if (doc && doc.length) setIsLoaded(true)
  }, [doc])

  return [
    doc,
    isLoaded,
  ]
}

export default useGetFirestoreDocument
