import {
  useEffect,
  useState,
} from 'react'
// import {
//   log,
// } from '@heaven/react-native'
import getCollectionListener from './getCollectionListener'

const useListenFirestoreCollection = (
  collectionPath,
  {
    enableRequest = true,
  } = {},
) => {
  // log('useListenFirestoreCollection.js executed')
  const [collection, setCollection] = useState()
  const [isLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    // log('useListenFirestoreCollection.js useEffect [collectionPath]')
    if (enableRequest) {
      const listener = getCollectionListener(collectionPath, (newCollection) => {
        setCollection(newCollection)
        setIsLoaded(true)
      })
      return () => listener()
    }
    return () => { }
  }, [collectionPath, enableRequest])

  useEffect(() => { // should be useless
    // log('useListenFirestoreCollection.js useEffect [collection]')
    if (collection && collection.length && !isLoaded) setIsLoaded(true)
  }, [collection, isLoaded])

  return [
    collection,
    isLoaded,
  ]
}

export default useListenFirestoreCollection
