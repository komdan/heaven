import firebase from '@react-native-firebase/app'

const firestoreGenerateId = path => firebase.firestore()
  .collection(path)
  .doc()
  .id

export default firestoreGenerateId
