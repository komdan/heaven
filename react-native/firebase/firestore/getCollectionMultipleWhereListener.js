import firebase from '@react-native-firebase/app'
import {
  error,
} from '@heaven/react-native'

const getCollectionMultipleWhereListener = (
  path,
  conditions = [], // [{field, operator, value}] // eslint-disable-line
  callBack,
  onError = () => { },
) => {
  try {
    let query = firebase.firestore().collection(path)
    conditions.forEach(({ field, operator, value }) => {
      query = query.where(field, operator, value)
    })

    return query
      .onSnapshot(
        snapshot => callBack(snapshot.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        }))),
        e => {
          console.error(e)
          error(new Error(`getCollectionMultipleWhereListener.js firebase firestore error for path:${path} conditions:${JSON.stringify(conditions)}`))
          error(e)
          onError(e)
        },
      )
  } catch (e) {
    error('getCollectionMultipleWhereListener.js ERROR FOLLOWS')
    error('🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲🥲')
    error(e)
    return () => { }
  }
}

export default getCollectionMultipleWhereListener
