import {
  incrementAlphabetical,
} from '@heaven/js'
import firebase from '@react-native-firebase/app'

const getCollectionInGeohashBox = (collection, geohash) => {
  const data = []

  return (
    firebase.firestore()
      .collection(collection)
      .where('geohash', '>=', geohash)
      .where('geohash', '<', incrementAlphabetical(geohash))
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            const obj = doc.data()
            data.push({
              ...obj,
              id: doc.id,
            })
          })
        return data
      })
  )
}

export default getCollectionInGeohashBox
