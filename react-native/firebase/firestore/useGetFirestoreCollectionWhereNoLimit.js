import {
  useEffect,
  useState,
} from 'react'
import {
  chunk,
  flatten,
  // isArray,
  isEqual,
  reject,
} from 'lodash'
// import {
//   log,
// } from '@heaven/react-native'
import getCollectionWhere from './getCollectionWhere'

const FIRESTORE_IN_FILTER_LENGTH_LIMIT = 10 // imposed by firestore
let toTreat = []

const useListenFirestoreCollectionWhereNoLimit = (collectionPath, field, operator, value) => {
  // operator should be 'in' or 'not-in'
  // console.log('barsIds.length', value.length)
  const [isCollectionLoaded, setIsLoaded] = useState(false)
  const [collection, setCollection] = useState([])

  const [areCollectionLoadedChunks, setareCollectionLoadedChunks] = useState() // [true, false, true, true, false, ...]
  const [collectionChunks, setCollectionChunks] = useState()

  const [shouldTreatChunks, setshouldTreatChunks] = useState(false)

  const valueChunks = chunk(value, FIRESTORE_IN_FILTER_LENGTH_LIMIT) // [['barId1', .., 'barId10'], ['barId11', ..], ..]
  const initalCollectionChunks = valueChunks.map(() => []) // [[], [], ..]]
  const initalareCollectionLoadedChunks = valueChunks.map(() => false) // [false, false, ..]

  // useEffect(() => {
  //   console.log('areCollectionLoadedChunks', areCollectionLoadedChunks)
  //   console.log('collection.length', collection.length)
  // })

  useEffect(() => {
    if (shouldTreatChunks) {
      const newCollectionChunks = (collectionChunks || initalCollectionChunks)
      const newAreCollectionsChunksLoaded = (areCollectionLoadedChunks || initalareCollectionLoadedChunks)
      const toTreatCopy = [...toTreat]

      toTreatCopy.forEach(({ index, newCollectionChunk }) => {
        newCollectionChunks[index] = newCollectionChunk
        newAreCollectionsChunksLoaded[index] = true
        toTreat = reject(toTreat, { index })
        // console.log(`TREATED ${index}`)
      })

      if (!isEqual(newAreCollectionsChunksLoaded, areCollectionLoadedChunks)) {
        setareCollectionLoadedChunks(newAreCollectionsChunksLoaded)
      }
      if (!isEqual(newCollectionChunks, collectionChunks)) {
        setCollectionChunks(newCollectionChunks)
      }
      setshouldTreatChunks(false)
    }
  }, [shouldTreatChunks])

  useEffect(() => {
    // log('useListenFirestoreCollectionWhereNoLimit.js useEffect [collectionPath, field, operator, value]')
    valueChunks.map( // listeners : [collectionListener1, collectionListener2, ..]
      (valueChunk, index) => ( // valueChunk : ['barId1', .., 'barId10']
        getCollectionWhere(
          collectionPath, // 'orders'
          field, // 'barId'
          operator, // 'in'
          valueChunk, // barsIds, chunked
        ).then(
          (newCollectionChunk) => {
            console.log(`useListenFirestoreCollectionWhereNoLimit ${index} ${valueChunk}`)
            toTreat.push({
              newCollectionChunk,
              index,
            })
            setshouldTreatChunks(true)
          },
        )
      ),
    )
  }, [collectionPath, field, operator, JSON.stringify(value)])

  useEffect(() => {
    if (areCollectionLoadedChunks && collectionChunks) {
      const isAllChunksLoaded = areCollectionLoadedChunks.reduce(
        (accu, isChunkLoaded) => accu && isChunkLoaded,
        true,
      )
      if (isAllChunksLoaded) {
        setCollection(flatten(collectionChunks))
        setIsLoaded(true)
      }
    }
  }, [shouldTreatChunks])

  const loadingProgression = +((
    +(((areCollectionLoadedChunks || []).filter(loaded => loaded) || []).length) / +((areCollectionLoadedChunks || []).length)
  ) * 100).toFixed(0) || 0

  return [
    collection,
    isCollectionLoaded,
    loadingProgression,
  ]
}

export default useListenFirestoreCollectionWhereNoLimit
