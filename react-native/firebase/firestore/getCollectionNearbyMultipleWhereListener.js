import {
  getDistance,
} from 'geolib'
import {
  neighbors,
  decode,
} from 'ngeohash'
import {
  flattenDeep,
  debounce,
  range,
} from 'lodash'
import {
  computeAdequateGeohashLength,
} from '@heaven/js'

import getCollectionMultipleWhereListenerInGeohashBox from './getCollectionMultipleWhereListenerInGeohashBox'

// TODO: check if this works
const getCollectionNearbyMultipleWhereListener = (
  collectionPath,
  baseGeohash,
  maxDistanceInMeter,
  conditions,
  callBack,
  onError,
  parentSetIsLoaded,
) => {
  const collectionChunks = range(0, 9).map(() => []) // array of 9 elements containing 9 collections for the 9 different boxes -> [[], [], [], ...]
  const isLoadedCollectionChunks = range(0, 9).map(() => false) // array of 9 elements containing 9 collections for the 9 different boxes -> [false, false, false, ...]

  const baseGeohashTruncated = baseGeohash.substr(
    0,
    computeAdequateGeohashLength(maxDistanceInMeter),
  )

  const baseGeohashes = [
    baseGeohashTruncated,
    ...neighbors(baseGeohashTruncated),
  ]

  const debouncedCallBack = debounce(callBack, 200) // debounced setCollection callback here prevents too much react rerenders
  const chunkedCallback = index => (newCollection) => {
    collectionChunks[index] = newCollection
      .filter(({ geohash = '' }) => (
        getDistance(decode(geohash), decode(baseGeohash)) <= maxDistanceInMeter
      ))
    isLoadedCollectionChunks[index] = true
    console.log(isLoadedCollectionChunks)
    if (isLoadedCollectionChunks.reduce(
      (accu, isChunkLoaded) => accu && isChunkLoaded,
      true,
    )) {
      parentSetIsLoaded(true)
    }
    debouncedCallBack(flattenDeep(collectionChunks))
  }

  const listeners = [
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[0], conditions, chunkedCallback(0), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[1], conditions, chunkedCallback(1), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[2], conditions, chunkedCallback(2), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[3], conditions, chunkedCallback(3), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[4], conditions, chunkedCallback(4), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[5], conditions, chunkedCallback(5), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[6], conditions, chunkedCallback(6), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[7], conditions, chunkedCallback(7), onError),
    getCollectionMultipleWhereListenerInGeohashBox(collectionPath, baseGeohashes[8], conditions, chunkedCallback(8), onError),
  ]

  const clearListeners = () => listeners.forEach(listener => listener())

  return clearListeners
}

export default getCollectionNearbyMultipleWhereListener
