import firebase from '@react-native-firebase/app'
import {
  error,
} from '@heaven/react-native'

const getGroupCollectionMultipleWhereListener = (
  path,
  conditions = [], // [{field, operator, value}]
  callBack,
  onError = () => { },
) => {
  let query = firebase.firestore().collectionGroup(path)
  conditions.forEach(({ field, operator, value }) => {
    query = query.where(field, operator, value)
  })

  return query
    .onSnapshot(
      snapshot => callBack(snapshot.docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
      }))),
      e => {
        console.error(e)
        error(new Error(`getGroupCollectionMultipleWhereListener.js firebase firestore error for path:${path} conditions:${JSON.stringify(conditions)}`))
        error(e)
        onError(e)
      },
    )
}

export default getGroupCollectionMultipleWhereListener
