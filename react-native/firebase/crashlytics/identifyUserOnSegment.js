import {
  error,
} from '@heaven/react-native'
import {
  debounce,
  pick,
} from 'lodash'

const USER_TRAITS_TO_SEND = [ // only type string below or crash in Android !!!
  '_boundToManagerEmail',
  'displayName',
  'phoneNumber',
  // 'areFlashSaleNotificationsEnabled',
  'currentDeviceId',
  // 'devicePosition',
  'email',
  // 'enterpriseName',
  'geohash',
  // 'hasCreditCardSaved',
  'uid',
  'pushToken',
  'referralCode',
]

const identifyUserOnSegment = async ({
  user = {},
  user: {
    id: userId = '',
  } = {},
  analytics,
} = {}) => {
  if (user && userId && analytics && analytics.alias && analytics.identify) {
    try {
      analytics.alias(userId)
      analytics.identify(userId, pick(user, USER_TRAITS_TO_SEND))
    } catch (e) {
      error(e)
      error(new Error('identifyUserOnSegment.js\\error-follows'))
    }
  }
}

export default debounce(identifyUserOnSegment, 10000)
