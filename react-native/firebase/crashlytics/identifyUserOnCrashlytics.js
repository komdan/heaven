import crashlytics from '@react-native-firebase/crashlytics'
import {
  debounce,
  pick,
} from 'lodash'

const USER_TRAITS_TO_SEND = [
  '_boundToManagerEmail',
  'displayName',
  'phoneNumber',
  // 'areFlashSaleNotificationsEnabled',
  'currentDeviceId',
  // 'devicePosition',
  'email',
  // 'enterpriseName',
  'geohash',
  // 'hasCreditCardSaved',
  'uid',
  'pushToken',
  'referralCode',
]

const identifyUserOnCrashlytics = async ({
  user,
  user: {
    id: userId = '',
  } = {},
} = {}) => {
  crashlytics().log('User signed in')
  await Promise.all([
    crashlytics().setUserId(userId),
    crashlytics().setAttributes({
      ...pick(user, USER_TRAITS_TO_SEND),
    }),
  ])
}

export default debounce(identifyUserOnCrashlytics, 10000)
