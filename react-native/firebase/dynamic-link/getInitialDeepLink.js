import firebase from '@react-native-firebase/app'

import {
  parseGETParams,
} from '@heaven/js'
import {
  log,
} from '@heaven/react-native'

const getInitialDeepLink = () => firebase.dynamicLinks()
  .getInitialLink()
  .then(({
    url = '',
  } = { }) => {
    if (url) {
      log(`getInitialDeepLink.js/received url:${url}`)
      return parseGETParams(url)
    }
    return undefined
  })

export default getInitialDeepLink
