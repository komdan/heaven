import firebase from '@react-native-firebase/app'
import {
  parseGETParams,
} from '@heaven/js'

const onDeepLink = (callBack) => firebase.dynamicLinks()
  .onLink(async ({
    url = '',
  } = {}) => {
    if (url) await callBack(parseGETParams(url))
    await callBack(null)
  })

export default onDeepLink
