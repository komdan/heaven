import {
  I18nManager,
  Platform,
} from 'react-native'

const isAndroidRTL = I18nManager.isRTL && Platform.OS === 'android'

export default isAndroidRTL
