import IdleTimerManager from 'react-native-idle-timer'

const enableIdleTimer = () => IdleTimerManager.setIdleTimerDisabled(false)

export default enableIdleTimer
