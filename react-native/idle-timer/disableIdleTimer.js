import IdleTimerManager from 'react-native-idle-timer'

const disableIdleTimer = () => IdleTimerManager.setIdleTimerDisabled(true)

export default disableIdleTimer
