import {
  Clipboard,
  Alert,
} from 'react-native'
import { t as t_ } from 'react-multi-lang'

const copyString = (string) => {
  Clipboard.setString(string)
  Alert.alert(t_('stringSuccessfullyCopiedInClipboard'), string)
}

export default copyString
