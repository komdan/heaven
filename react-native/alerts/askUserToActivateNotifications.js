import {
  Alert,
} from 'react-native'
import openAppSettings from '../linking/openAppSettings'

const askUserToActivateNotifications = ({
  callBack = () => null,
  t,
}) => {
  Alert.alert(
    // Title
    t('notifications'),
    // Message
    t('pleaseActivateNotifications'),
    // Buttons
    [
      {
        text: t('noThanks'),
        onPress: () => { callBack() },
      },
      {
        text: t('goToSettings'),
        onPress: () => { openAppSettings(); callBack() },
      },
    ],
  )
}

export default askUserToActivateNotifications
