import {
  Alert,
} from 'react-native'
import openAppSettings from '../linking/openAppSettings'

const askUserToActivateLocation = ({
  callBack = () => null,
  t,
}) => {
  Alert.alert(
    // Title
    t('location'),
    // Message
    t('pleaseActivateLocation'),
    // Buttons
    [
      {
        text: t('noThanks'),
        onPress: () => { callBack() },
      },
      {
        text: t('goToSettings'),
        onPress: () => { openAppSettings(); callBack() },
      },
    ],
  )
}

export default askUserToActivateLocation
