import {
  CallbackQueue,
} from '../../../heaven/js'

const ExecutionQueue = new CallbackQueue({
  synchronousDelayMs: 280,
  pollingIntervalMs: 100,
})

const executeAfterAnimation = (
  func,
) => {
  ExecutionQueue.push(func)
}

export default executeAfterAnimation
