import {
  Alert,
} from 'react-native'
import {
  debounce,
} from 'lodash'

import executeAfterAnimation from './executeAfterAnimation'

const AlertalertPromise = ( // promise that returns after an alert button is clicked
  title = '',
  body = '',
  buttons = [{ text: 'Ok' }],
) => new Promise((resolve, reject) => {
  const buttonsResolvingThisPromiseOnPress = buttons.map((button) => {
    const {
      onPress = async () => { },
    } = button
    return ({
      ...button,
      onPress: async () => {
        try {
          const onPressResult = await onPress()
          resolve(onPressResult)
        } catch (e) {
          reject(e)
        }
      },
    })
  })
  if (!buttons.length) { // if no button is explicitly defined, no onPress will be triggered, so we resolve the promise now
    resolve()
  }

  executeAfterAnimation(() => { // avoids modal collision
    Alert.alert(
      title,
      body,
      buttonsResolvingThisPromiseOnPress,
    )
  })
})

const Alertalert = debounce( // debounce to make it robust against un-optimized React states change
  AlertalertPromise,
  1000,
  {
    leading: true,
    trailing: false,
  },
)

export default Alertalert
