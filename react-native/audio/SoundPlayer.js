/*
* Created by Matthieu MARIE-LOUISE
* ================================
* Simple synchronous SoundPlayer based on react-native-sound
*/
import soundAtUrl from './soundAtUrl'
import soundInBundle from './soundInBundle'

class SoundPlayer {
  constructor() {
    this.sound = emptySound
    this.loadedSounds = []
  }

  stop() { this.sound.stop() }

  setVolume(volumeIndex) { this.sound.setVolume(volumeIndex) }

  pause() { this.sound.pause() }

  play(
    name = undefined,
    {
      volume = 1,
      infiniteLoop,
      fadeIn = 500,
    } = {},
  ) {
    try {
      this.sound.stop()

      if (name) {
        this.sound = this.loadedSounds
          .find(({ soundName }) => soundName === name)
          .sound
      }
      if (infiniteLoop) this.sound.setNumberOfLoops(-1)

      this.sound.setVolume(volume)
      if (fadeIn) this.fadeIn(fadeIn)
      this.sound.play()
    } catch (e) {
      throw new Error(`SoundPlayer::sound-not-found-in-loaded-sounds:details:${e.message}`)
    }
  }

  async load(soundName, fileNameOrUri) {
    if (fileNameOrUri && fileNameOrUri.uri) {
      this.loadedSounds.push({
        soundName,
        sound: soundAtUrl(fileNameOrUri.uri),
      })
    } else {
      this.loadedSounds.push({
        soundName,
        sound: soundInBundle(fileNameOrUri),
      })
    }
  }

  fadeOut(duration) {
    const initialVolume = this.sound.getVolume()

    let volume = initialVolume
    const period = 100 // in ms
    const iteration = (duration / period)

    const interval = setInterval(
      () => {
        volume -= initialVolume / iteration
        this.sound.setVolume(volume >= 0 ? volume : 0)
        // console.log('SoundPlayer:🔊🔊 volume', volume >= 0 ? volume : 0)
      },
      period,
    )
    setTimeout(
      () => {
        clearInterval(interval)
        // console.log('SoundPlayer:🔊🔊 volume', 0)
        this.sound.setVolume(0)
      },
      duration,
    )
  }

  fadeIn(duration, finalVolume = 1) {
    let volume = 0
    const period = 100 // in ms
    const iteration = (duration / period)

    const interval = setInterval(
      () => {
        volume += finalVolume / iteration
        this.sound.setVolume(volume <= finalVolume ? volume : finalVolume)
        // console.log('SoundPlayer:🔊🔊 volume', volume <= finalVolume ? volume : finalVolume)
      },
      period,
    )
    setTimeout(
      () => {
        clearInterval(interval)
        // console.log('SoundPlayer:🔊🔊 volume', finalVolume)
        this.sound.setVolume(finalVolume)
      },
      duration,
    )
  }
}

const emptySound = {
  stop: () => null,
  play: () => null,
  pause: () => null,
  getCurrentTime: () => null,
  getNumberOfLoops: () => null,
  getPan: () => null,
  getVolume: () => null,
  release: () => null,
  setCurrentTime: () => null,
  setNumberOfLoops: () => null,
  setPan: () => null,
  setVolume: () => null,
}

export default SoundPlayer
