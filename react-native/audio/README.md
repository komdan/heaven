🕊 heaven / react-native / audio
==============================
🔊 Audio functions based on *react-native-sound* 🔊

Usage
-----
### playFullLengthSoundAtUrl - *async*
Fetch a sound at a given URL and plays it. Returns a **promise**. Very useful to wait for a sound to end before playing another one.

```js
const pleaseSay = (string, options) => playFullLengthSoundAtUrl(
  getSpeechUrl(string),
  options,
)

const speak = async () => {
  await pleaseSay('prepare for')
  await pleaseSay('burpees')
}
```

### SoundPlayer
A class exposing synchronous methods for controling sounds. Sounds should be loaded with a `soundName` before being used.

```js
import {
  SoundPlayer,
} from '@heaven/react-native'

SoundPlayer.load('drum120bpm', { uri: 'https://freesound.org/data/previews/381/381353_1676145-lq.mp3' })
SoundPlayer.load('clock', { uri: 'https://freesound.org/data/previews/360/360941_2958634-lq.mp3' })
SoundPlayer.load('drum120', 'drum120.mp3')

export default SoundPlayer
```

Once loaded, the following methods are available:

```js
SoundPlayer.stop()
SoundPlayer.setVolume(0.5)
SoundPlayer.play('drum120bpm', { infiniteLoop: true, volume: 1 })
SoundPlayer.pause()
SoundPlayer.play()

if (!isMusicMuted) SoundPlayer.play('clock', { infiniteLoop: true, fadeIn: 200 })

const onTimerLastSecond = () => SoundPlayer.fadeOut(500) // decrease volume to 0 in 500ms
```

### Helpers
* **playFullLengthSound** - *async*
Used by playFullLengthSoundAtUrl

* **soundAtUrl**
Used by SoundPlayer

* **soundAtUrlPromise** - *async*
Used by playFullLengthSoundAtUrl

* **soundInBundle**
Used by SoundPlayer
