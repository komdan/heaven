import Sound from './Sound'

const soundAtUrl = url => new Sound(
  url,
  null,
  (e) => {
    if (e) {
      throw e
    }
  },
)

export default soundAtUrl
