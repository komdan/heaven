// promise resolving at the end of the sound
const playFullLengthSound = (
  sound,
  {
    infiniteLoop = false,
    numberOfLoop = 1,
    volume = 1,
  } = {},
) => new Promise((resolve, reject) => {
  try {
    if (numberOfLoop !== 1) {
      sound.setNumberOfLoops(numberOfLoop)
    }
    if (infiniteLoop) {
      sound.setNumberOfLoops(-1)
    }
    sound.setVolume(volume)
    sound.stop(() => sound.play())
    setTimeout(
      () => {
        resolve(sound)
        if (!infiniteLoop) {
          sound.release()
        }
      },
      sound.getDuration() * numberOfLoop * 1000,
    )
  } catch (e) {
    reject(e)
  }
})

export default playFullLengthSound
