import soundAtUrlPromise from './soundAtUrlPromise'
import playFullLengthSound from './playFullLengthSound'

const playFullLengthSoundAtUrl = (url, options) => new Promise(async (resolve, reject) => {
  try {
    const sound = await soundAtUrlPromise(url)
    await playFullLengthSound(sound, options)
    resolve(sound)
  } catch (e) {
    reject(e)
  }
})

export default playFullLengthSoundAtUrl
