import Sound from './Sound'

const soundAtUrlPromise = url => new Promise((resolve, reject) => {
  const sound = new Sound(
    url,
    null,
    e => (e ? reject(e) : resolve(sound)),
  )
})

export default soundAtUrlPromise
