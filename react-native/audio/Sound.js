import Sound from 'react-native-sound'

Sound.setCategory('Ambient', true)

export default Sound
