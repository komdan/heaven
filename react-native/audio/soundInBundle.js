import Sound from './Sound'

const soundInBundle = fileName => new Sound(
  fileName,
  Sound.MAIN_BUNDLE,
  (e) => {
    if (e) throw e
  },
)

export default soundInBundle
