/*
Created by Matthieu ML
------------------------
This is a wrapper around AsyncStorage.
------------------------
TL;DR: JSON.stringify to 'set' and JSON.parse to 'get'
------------------------
Added the possibility to listen to a 'key'
*/

import AsyncStorage from '@react-native-async-storage/async-storage'
import EventEmitter from 'EventEmitter'
import {
  isEqual,
} from 'lodash'
// import {
//   log,
// } from '@heaven/react-native'

const eventEmitter = new EventEmitter()

const localCache = {
  get: key => AsyncStorage.getItem(key).then(value => (value
    ? JSON.parse(value)
    : undefined)),
  getMultiple: keys => Promise.all(keys.map(key => localCache.get(key))),
  set: (key, value) => {
    // log(`heaven/react-native::localCache.js:: set(). key: ${key} value:${JSON.stringify(value)} set in localCache`)
    eventEmitter.emit(`${key} set in localCache`, { key, value })
    return AsyncStorage.setItem(key, JSON.stringify(value))
  },
  setMultiple: obj => Object.keys(obj).forEach(key => localCache.set(key, obj[key])), // obj: { key1: value1, key2: value2 }
  listen: (key, callBack) => {
    localCache
      .get(key)
      .then(initialValue => callBack(initialValue))
    // log('heaven/react-native::localCache.js:: listen()')
    let previousValue
    const handleValueChange = ({ value }) => {
      console.log('heaven/react-native::localCache.js:: listen-handleValueChange()')
      if (!isEqual(previousValue, value)) {
        callBack(value)
        previousValue = value
      }
    }
    eventEmitter.on(
      `${key} set in localCache`,
      handleValueChange,
    )
    return () => eventEmitter.off(
      `${key} set in localCache`,
      handleValueChange,
    )
  },
  remove: async (key) => {
    // log('heaven/react-native::localCache.js:: ❌remove(). key', key)
    await AsyncStorage.removeItem(key)
  },
  clear: () => { AsyncStorage.clear() }, // DEBUG PURPOSE
  clearListeners: (key = '') => eventEmitter.removeAllListeners(key), // DEBUG PURPOSE
}

export default localCache
