import emojify from './emojify'
import updateAllObjectValues from './updateAllObjectValues'

const emojifyObject = obj => updateAllObjectValues(
  obj,
  value => emojify(value),
)

export default emojifyObject
