const replaceAllSlashes = string => string.replace(/\//g, '')

export default replaceAllSlashes
