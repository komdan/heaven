import incrementCharacter from './incrementCharacter'

const incrementAlphabetical = string => string.replace(/.$/, incrementCharacter)

export default incrementAlphabetical
