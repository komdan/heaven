const incrementCharacter = character => String.fromCharCode(character.charCodeAt(0) + 1)

export default incrementCharacter
