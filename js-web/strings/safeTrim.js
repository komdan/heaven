const safeTrim = string => (
  (string && string.trim)
    ? string.trim()
    : ''
)

export default safeTrim
