import latinise from './latinise'

const includesSanitizedString = (input, string) => {
  if (!input || !string) return false
  const sanitizedInput = latinise(input.toLowerCase().trim())
  const sanitizedString = latinise(string.toLowerCase().trim())

  // TODO: use Array#includes instead of indexOf
  return sanitizedString.indexOf(sanitizedInput) >= 0
}

export default includesSanitizedString
