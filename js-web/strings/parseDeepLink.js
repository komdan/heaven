import parseGETParams from './parseGETParams'

const parseDeepLink = (longDeepLink) => {
  const { link } = parseGETParams(longDeepLink)
  return parseGETParams(link)
}

export default parseDeepLink
