import {
  fromPairs,
  toPairs,
} from 'lodash'

const updateAllObjectValues = (obj, method) => fromPairs(
  toPairs(obj).map(
    ([key, value]) => [key, method(value)],
  ),
)

export default updateAllObjectValues
