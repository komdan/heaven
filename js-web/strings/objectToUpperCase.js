import updateAllObjectValues from './updateAllObjectValues'

const objectToUpperCase = obj => updateAllObjectValues(
  obj,
  value => value.toUpperCase(),
)

export default objectToUpperCase
