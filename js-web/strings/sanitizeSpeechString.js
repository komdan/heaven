import {
  words,
  trim,
} from 'lodash'

const sanitizeSpeechString = exerciseName => words(trim(exerciseName.toLowerCase())).join(' ')

export default sanitizeSpeechString
