const escapeQuote = string => (
  (typeof string) === 'string' && string.replace
    ? string.replace('\'', '\\\'')
    : string
)

export default escapeQuote
