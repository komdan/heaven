// TODO: remove and use lodash.capitalize ?
const capitalizeFirstLetter = s => (
  s && s.length > 0
    ? `${s[0].toUpperCase()}${s.substr(1, s.length).toLowerCase()}`
    : ''
)

export default capitalizeFirstLetter
