import {
  post,
} from 'axios'

const {
  REACT_APP_BACKEND_URL,
} = process.env

const creditCardToPaymentMethod = async ({
  card,
}) => {
  const {
    data: {
      paymentMethod,
    },
  } = await post(`${REACT_APP_BACKEND_URL}/stripeCreatePaymentMethod`, {
    card,
  })

  return paymentMethod
}

export default creditCardToPaymentMethod
