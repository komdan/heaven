// const fetch from 'node-fetch')

import error from '../logging/error'

function myFetchPost(url = '', postData) {
  // console.log(url, fetch, postData)
  return new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(
        url,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(postData),
        },
      )
      res.json().then((data) => {
        const { body } = data
        resolve(body)
      })
    } catch (e) {
      error(e)
      console.warn('backendRequests/myFetchPost::error: ', JSON.stringify(e))
      reject(e)
    }
  })
}

export default myFetchPost
