// const fetch from 'node-fetch')
import error from '../logging/error'

const {
  REACT_APP_GOOGLE_API_KEY,
} = process.env

const getGoogleReviews = (googlePlaceId) => {
  const apiKey = REACT_APP_GOOGLE_API_KEY
  const url = `https://maps.googleapis.com/maps/api/place/details/json?placeid=${googlePlaceId}&fields=reviews&key=${apiKey}`

  return (
    fetch(url)
      .then((response) => {
        if (response.status === 200) {
          return response.json()
        }
        return {}
      })
      .catch((e) => {
        error(e)
        console.warn(`common.js\\getGoogleReviews('${googlePlaceId}') ERROR: `, JSON.stringify(e))
      })
  )
}

export default getGoogleReviews
