import {
  post,
} from 'axios'
import postSlackNotification from '../logging/postSlackNotification'

import log from '../logging/log'
import error from '../logging/error'

const {
  REACT_APP_MAILJET_SMS_TOKEN,
  NODE_ENV,
} = process.env

const sendSMS = async ({ To, Text }) => {
  try {
    if (
      NODE_ENV === 'production'
      // || NODE_ENV === 'development' // should be commented while live
    ) {
      const url = 'https://api.mailjet.com/v4/sms-send'
      const data = {
        From: 'Komdab',
        To,
        Text,
      }
      const options = {
        headers: {
          Authorization: `Bearer ${REACT_APP_MAILJET_SMS_TOKEN}`,
          'Content-Type': 'application/json',
        },
      }
      const {
        status,
        statusText,
        // headers,
        // config,
        // request,
        data: postData,
      } = await post(url, data, options)
      log(`sendSMS.js\\sms-sent:${JSON.stringify({ status, statusText, postData })}`)
    } else { // NODE_ENV === 'development'
      await postSlackNotification(`__SMS to:${To} text:${Text}`)
      log(`sendSMS.js\\sms-not-sent-decause-dev:${JSON.stringify({ To, Text })}`)
    }
  } catch (e) {
    error(e)
    error(new Error('sms.js\\sendSMS(): ERROR FOLLOWS'))
  }
}

export default sendSMS

// await sendSMS({
//   To: '+33761782538',
//   Text: 'Une commande est arrivée sur Ubar. Veuillez vérifier que la tablette est bien allumée.',
// })

// await sendSMS({
//   To: `+${mobilePhoneNumber}`,
//   Text: smsText,
// })
