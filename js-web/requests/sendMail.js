/* eslint-disable */
import myFetchPostAuthenticated from './myFetchPostAuthenticated'

const {
  REACT_APP_mailJetAPIURL,
  REACT_APP_mailJetPK,
  REACT_APP_mailJetSK,
  NODE_ENV,
} = process.env


// ======================== Functions ========================
const devString = NODE_ENV === 'development' ? '__DEV__' : ''
const sendMail = ({ toEmail, toName, subject, textBody, htmlBody }) => myFetchPostAuthenticated(
  REACT_APP_mailJetAPIURL,
  {
    basicAuth: {
      username: REACT_APP_mailJetPK,
      password: REACT_APP_mailJetSK,
    },
    data: {
      Messages: [{
        From: {
          Email: 'noreply@komdan.com',
          Name: 'Komdab',
        },
        To: [
          {
            Email: toEmail,
            Name: toName,
          },
        ],
        Bcc: [
          {
            Email: 'dev@komdan.com',
          },
        ],
        Subject: devString + subject,
        TextPart: textBody,
        HTMLPart: htmlBody,
      }],
    },
  },
)

export default sendMail
