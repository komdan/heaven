import fetch from 'node-fetch'

import error from '../logging/error'
import btoa from '../casters/btoa'

function myFetchPostAuthenticated(url = '', { basicAuth, data }) {
  const {
    username,
    password,
  } = basicAuth

  return new Promise(async (resolve, reject) => {
    try {
      const res = await fetch(
        url,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Authorization: `Basic ${btoa(`${username}:${password}`)}`,
          },
          body: JSON.stringify(data),
        },
      )
      res.json().then((result) => {
        resolve(result)
      })
    } catch (e) {
      error('➡️myFetchPostAuthenticated::error: ', JSON.stringify(e))
      reject(e)
    }
  })
}

export default myFetchPostAuthenticated
