/*
  Created by Matthieu MARIE-LOUISE
*/
import blue from '../terminal/blue'

import pushLog from './pushLog'
import LOG_SESSION from './LOG_SESSION'

// const {
//   NODE_ENV,
// } = process.env

// const isDevelopment = NODE_ENV === 'development'

// ======================== Logging ====================================
const log = (event, properties) => {
  const now = (new Date()).toLocaleString('en-US')

  if (typeof properties !== 'undefined') {
    // Regular logging
    // if (isDevelopment) console.log(blue(event), properties)

    // Send to custom logger
    pushLog(`"${now}";"${blue(event)} ${JSON.stringify(properties)}";"${LOG_SESSION}"`, 'info')
  } else {
    // Regular logging
    // if (isDevelopment) console.log(blue(event))

    // Send to custom logger
    pushLog(`"${now}";"${blue(event)}";"${LOG_SESSION}"`, 'info')
  }
}

// ======================== Exports ========================
export default log
