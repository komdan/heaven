/*
  Created by Matthieu MARIE-LOUISE
*/
import log from './log'

const {
  NODE_ENV,
  REACT_APP_NODE_ENV,
} = process.env

const debug = (NODE_ENV === 'development' || REACT_APP_NODE_ENV === 'development')
  ? log
  : () => { }

export default debug
