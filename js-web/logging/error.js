/*
  Created by Matthieu MARIE-LOUISE
*/
import pushLog from './pushLog'
import red from '../terminal/red'
import LOG_SESSION from './LOG_SESSION'

import postSlackNotification from './postSlackNotification'

const {
  NODE_ENV,
} = process.env
const isDevelopment = NODE_ENV === 'development'

// ======================== Logging ====================================
const error = (err, properties) => {
  try {
    const {
      stack = '',
      message = '',
      response: {
        data,
      } = {},
    } = err
    const now = (new Date()).toLocaleString('en-US')

    // Regular logging
    if (isDevelopment) console.warn(red(message))
    if (isDevelopment && data) console.error(red(JSON.stringify(data)))

    // Send to custom logger
    pushLog(`"${now}";"${red(message)}  ${JSON.stringify(properties)}";"${LOG_SESSION}";"${stack}"`, 'error')
    if (data) {
      pushLog(`"${now}";"${red(JSON.stringify(data))}";"${LOG_SESSION}";"${stack}"`, 'error')
    }
    if (
      message.includes('firestore')
      // && !(message.includes('The caller does not have permission to execute the specified operation'))
    ) {
      postSlackNotification(`heaven/js-web:: error.message includes "firestore" - ${message}`)
    }
  } catch (e) {
    error(e)
  }
}

// ======================== Exports ========================
export default error
