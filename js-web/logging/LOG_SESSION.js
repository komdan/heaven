/*
  Created by Matthieu MARIE-LOUISE
*/
import uuid from 'uuid'

const LOG_SESSION = uuid.v4()

export default LOG_SESSION
