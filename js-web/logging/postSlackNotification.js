import {
  post,
} from 'axios'

const {
  REACT_APP_SLACK_MONITORING_URL,
  NODE_ENV,
} = process.env

const DEV_PREFIX = NODE_ENV === 'development'
  ? '_'
  : ''

const postSlackNotification = async (
  text,
  slackUrl = REACT_APP_SLACK_MONITORING_URL,
) => {
  try {
    await post(
      slackUrl,
      { text: DEV_PREFIX + text },
    )
  } catch (e) {
    console.error(e)
  }
}

export default postSlackNotification
