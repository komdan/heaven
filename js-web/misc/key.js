const key = (...args) => JSON.stringify({ ...args })

export default key
