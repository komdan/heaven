import {
  omitBy,
  isEqual,
} from 'lodash'

const objectDiff = (obj1, obj2) => omitBy(
  obj1,
  (value, key) => isEqual(obj2[key], value),
)

export default objectDiff
