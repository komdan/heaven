import {
  SHELL_COLORS,
} from '../../constants-web'

const red = string => `${SHELL_COLORS.FgRed}${string}${SHELL_COLORS.Reset}`

export default red
