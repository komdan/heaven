import {
  SHELL_COLORS,
} from '../../constants-web'

const blue = string => `${SHELL_COLORS.FgCyan}${string}${SHELL_COLORS.Reset}`

export default blue
