const replaceAtIndex = (array, indexInArgs, newValue) => array.map(
  (value, index) => (
    index === indexInArgs ? newValue : value
  ),
)

export default replaceAtIndex

// > replaceAtIndex([0,1,2,3], 2, 'chapeau')
// [ 0, 1, 'chapeau', 3 ]
