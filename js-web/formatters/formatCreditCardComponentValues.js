const formatCreditCardComponentValues = (creditCardComponentValues) => {
  const {
    values: {
      number,
      expiry,
      cvc,
      type,
      // valid,
    },
  } = creditCardComponentValues

  const card = {
    number: number.replace(/ /g, ''),
    cvc,
    expMonth: +expiry.split('/')[0],
    expYear: +`20${expiry.split('/')[1]}`,
    type,
  }

  return card
}

export default formatCreditCardComponentValues
