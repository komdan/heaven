// Used in ExerciseSliders
// -> roundup a distance to 5 secs
import round5 from './round5'

const roundDistance500 = x => (
  x < 400
    ? round5(x)
    : Math.ceil((x - 400) / 500) * 500
)

export default roundDistance500
