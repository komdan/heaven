// wanted format : 06 12 34 56 78

const formatInternationalFrenchPhoneNumber = input => (
  input
    .replace(/\+33/g, '0')
    .replace(/[^\d]/g, '')
    .substr(0, 10)
    .replace(/(.{2})/g, '$1 ')
    .trim()
)

export default formatInternationalFrenchPhoneNumber
