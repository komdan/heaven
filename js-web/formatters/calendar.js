import moment from 'moment'
import 'moment/min/locales' // Import all moment-locales -- it's just 400kb
import 'moment-timezone'
import {
  capitalize,
} from 'lodash'

const calendar = ({
  timestamp = new Date(),
  locale = 'fr',
  t = (s) => s,
}) => {
  moment.locale(locale)
  return capitalize(
    moment(timestamp).calendar(null, {
      sameDay: `[${t('Today')}] dddd DD/MM/YYYY`,
      nextDay: `[${t('Tomorrow')}] dddd DD/MM/YYYY`,
      nextWeek: 'dddd DD/MM/YYYY',
      lastDay: `[${t('Yesterday')}] dddd DD/MM/YYYY`,
      lastWeek: 'dddd DD/MM/YYYY',
      sameElse: 'dddd DD/MM/YYYY',
    }),
  )
}

export default calendar
