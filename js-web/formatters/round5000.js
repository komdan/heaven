// Used in ExerciseSliders
// -> roundup a duration to 5 secs

const round5000 = x => (
  x > 4000
    ? Math.ceil((x - 4000) / 5000) * 5000
    : 0
)

export default round5000
