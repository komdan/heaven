import {
  sortBy,
} from 'lodash'
import {
  WEEK_DAYS,
} from '@heaven/constants-web'
import getCurrentMoment from '../time/getCurrentMoment'
import soonerThan from '../time/soonerThan'

const generateOpenAtText = ({
  openingHours,
  timeZoneId,
  t,
}) => {
  const {
    day,
    hours,
  } = getCurrentMoment(timeZoneId)

  const {
    periods,
    isOpen,
  } = openingHours[WEEK_DAYS[day]]

  const craftOpensAtText = opening => `${t('OPENS AT')} ${opening}`

  const openAtText = sortBy(periods, ['opening']) // periods: [{period:0, opening:"17:00"..]
    .reduce(
      (accu, { opening }) => (
        ((soonerThan(hours, opening) && isOpen) ? craftOpensAtText(opening) : null)
        || accu
      ),
      t('CLOSED'),
    )

  return openAtText
}

export default generateOpenAtText
