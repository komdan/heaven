// wanted format : 06 12 34 56 78

// const formatFrenchPhoneNumber = input => (
//   input
//     .replace(/[^\d]/g, '')
//     .substr(0, 10)
//     .replace(/(.{2})/g, '$1 ')
//     .trim()
// )
// Function killed because users does not respect the format

const { getAsYouType } = require('awesome-phonenumber')

const formatFrenchPhoneNumber = input => {
  // https://www.npmjs.com/package/awesome-phonenumber
  return (getAsYouType('FR')).reset(input)
}

export default formatFrenchPhoneNumber
