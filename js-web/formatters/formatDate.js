import moment from 'moment'
import 'moment/min/locales' // Import all moment-locales -- it's just 400kb
import 'moment-timezone'
import {
  capitalize,
} from 'lodash'

const formatDate = (
  timestamp,
  format = 'll',
  locale = 'fr',
) => {
  moment.locale(locale)
  return capitalize(moment(timestamp).format(format))
}

export default formatDate
