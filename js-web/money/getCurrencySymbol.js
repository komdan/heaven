import {
  CURRENCY_SYMBOLS,
} from '../../constants-web'

const getCurrencySymbol = (currency) => {
  if (typeof currency === 'string') {
    return (CURRENCY_SYMBOLS[currency.toUpperCase()] || currency)
  }
  return currency
}

export default getCurrencySymbol
