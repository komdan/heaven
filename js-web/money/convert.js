import axios from 'axios'

import error from '../logging/error'

// convert: https://dev.to/fleepgeek/react-throwaway-app-1-currency-converter--52c7
const convert = (amount, { from: fromCurrencyInArgs, to: toCurrencyInArgs }) => {
  const [fromCurrency, toCurrency] = [fromCurrencyInArgs, toCurrencyInArgs]
    .map(string => string.toUpperCase())
  return fromCurrency !== toCurrency
    ? (
      axios
        .get(`https://api.openrates.io/latest?base=${fromCurrency}&symbols=${toCurrency}`)
        .then((response) => {
          // DEBUG
          // log('\n', '➡️ 💵 common.js\\convert(..):response: ', JSON.stringify(response), '\n')

          const { data: { rates } } = response
          return +(amount * (rates[toCurrency])).toFixed(5)
        })
        .catch((err) => {
          error(err)
        })
    )
    : (
      new Promise(resolve => resolve(amount))
    )
}

export default convert
