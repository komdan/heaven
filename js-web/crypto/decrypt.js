import CryptoJS from 'crypto-js'

// Inspired from https://www.npmjs.com/package/crypto-js
const decrypt = (cipherText, key) => (
  cipherText && cipherText.length
    ? CryptoJS.AES.decrypt(cipherText, key).toString(CryptoJS.enc.Utf8)
    : ''
)

export default decrypt

// > decrypt('U2FsdGVkX19SY0MB+nj6t9cyFJ/7bb6jYKu9v0Q/EgmpHD3NePBh+mk3TJ0AyfEA', 's3cr3t')
// 'I am your father'
// > decrypt('U2FsdGVkX19SY0MB+nj6t9cyFJ/7bb6jYKu9v0Q/EgmpHD3NePBh+mk3TJ0AyfEA', 'wrong_secret')
// 'c'
