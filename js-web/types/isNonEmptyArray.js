import isArray from './isArray'

const isEmptyArray = variable => (
  isArray(variable)
  && variable.length === 0
)

export default isEmptyArray
