const isFunction = variable => (typeof variable === 'function')

export default isFunction
