import assert from 'assert'
import {
  isObject,
} from 'lodash'

const assertObject = (variable, variableName) => assert(
  isObject(variable),
  `${variableName} is not an object. variable: ${variable} of type ${typeof variable}`,
)

export default assertObject
