const isNonEmptyString = variable => typeof variable === 'string' && !!variable

export default isNonEmptyString
