import {
  isDate,
} from 'lodash'
import {
  equal,
} from 'assert'

const assertDate = (variable, variableName) => {
  equal(isDate(variable), true, `${variableName} should be a date and not ${variable} of type ${typeof variable}`)
}

export default assertDate
