import isNonEmptyString from './isNonEmptyString'
import formatFrenchPhoneNumber from '../formatters/formatFrenchPhoneNumber'

const { parsePhoneNumber } = require('awesome-phonenumber')

const isFrenchPhoneNumber = input => {
  const pn = parsePhoneNumber(input, { regionCode: 'FR' })
  return pn.valid
}

export default isFrenchPhoneNumber
