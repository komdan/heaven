import { equal } from 'assert'

const assertString = (input, variableName) => equal(
  typeof input,
  'string',
  `${variableName} should be a string and not: ${input}`,
)

export default assertString
