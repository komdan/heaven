import { equal } from 'assert'
import assertString from './assertString'
import isEmail from './isEmail'

const assertEmail = (input, variableName) => {
  assertString(input, variableName)
  equal(
    isEmail(input),
    true,
    `${variableName} should be an email and not: ${input}`,
  )
}
export default assertEmail
