import { equal } from 'assert'

const assertPositiveNumber = (input, variableName) => {
  const errorMessage = `${variableName} should be positive and not: ${input}`
  equal(typeof input, 'number', errorMessage)
  equal(Number.isNaN(input), false, errorMessage)
  equal(input >= 0, true, errorMessage)
}

export default assertPositiveNumber
