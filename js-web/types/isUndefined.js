const isUndefined = variable => typeof variable === 'undefined'

export default isUndefined
