import {
  isEqual,
} from 'lodash'

import {
  NULL_COORDS,
} from '../../constants-web'

const areCoordsNull = devicePosition => isEqual(devicePosition, NULL_COORDS.coords)

export default areCoordsNull
