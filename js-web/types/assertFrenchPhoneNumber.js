import { equal } from 'assert'
import assertString from './assertString'
const isFrenchPhoneNumber = require('./isFrenchPhoneNumber').default

const assertFrenchPhoneNumber = (input, variableName = '') => {
  const errorMessage = `${variableName} should be a valid phone number and not: ${input}`

  assertString(input, variableName)
  equal(isFrenchPhoneNumber(input), true, errorMessage)
}

export default assertFrenchPhoneNumber
