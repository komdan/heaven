import {
  DISPOSABLE_EMAIL_DOMAIN_LIST,
} from '../../constants-web'

const isDisposableEmail = email => DISPOSABLE_EMAIL_DOMAIN_LIST.reduce(
  (accumulator, currentDisposableDomain) => accumulator || email.includes(currentDisposableDomain),
  false,
)

export default isDisposableEmail
