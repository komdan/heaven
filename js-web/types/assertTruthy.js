import { equal } from 'assert'

const assertTruthy = (input, variableName) => equal(
  !!input,
  true,
  `!!${variableName} should be 'true'  and not: ${input}`,
)

export default assertTruthy
