const isBool = variable => (typeof variable === 'boolean')

export default isBool
