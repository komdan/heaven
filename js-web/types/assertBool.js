import { equal } from 'assert'

const assertBool = (input, variableName) => equal(
  typeof input === 'boolean',
  true,
  `${variableName} should be a boolean and not: - ${input} - of type - ${typeof input} -`,
)

export default assertBool
