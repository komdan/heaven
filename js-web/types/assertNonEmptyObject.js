import assert from 'assert'
import isNonEmptyObject from './isNonEmptyObject'

const assertNonEmptyObject = (variable, variableName) => assert(
  isNonEmptyObject(variable),
  `${variableName} is not a non-empty-object. variable: ${variable} of type ${typeof variable}`,
)

export default assertNonEmptyObject
