const isFalsy = value => !value

export default isFalsy
