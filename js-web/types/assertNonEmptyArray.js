import { equal } from 'assert'
import assertArray from './assertArray'

const assertNonEmptyArray = (input, variableName) => {
  assertArray(input, variableName)
  equal(!!input.length, true, `!!${variableName}.length should be true not: ${input} of type ${typeof input}`)
}

export default assertNonEmptyArray
