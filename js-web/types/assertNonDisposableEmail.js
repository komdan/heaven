import { equal } from 'assert'
import assertString from './assertString'
import isEmail from './isEmail'
import isDisposableEmail from './isDisposableEmail'

const assertNonDisposableEmail = (input, variableName) => {
  assertString(input, variableName)
  equal(
    isEmail(input),
    true,
    `${variableName} should be an email and not: ${input}`,
  )
  equal(
    isDisposableEmail(input),
    false,
    `${variableName} should not be a disposable email: ${input}`,
  )
}

export default assertNonDisposableEmail
