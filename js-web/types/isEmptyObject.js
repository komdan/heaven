import {
  isEmpty,
} from 'lodash'
import isObject from './isObject'

const isEmptyObject = variable => (
  isObject(variable)
  && isEmpty(variable)
)

export default isEmptyObject
