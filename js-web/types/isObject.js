import isArray from './isArray'

const isObject = variable => (
  typeof variable === 'object'
  && variable !== null
  && !isArray(variable)
  && (typeof variable !== 'undefined') // logically useless
)

export default isObject
