import {
  isEmpty,
} from 'lodash'
import isObject from './isObject'

const isNonEmptyObject = variable => (
  isObject(variable)
  && !isEmpty(variable)
)

export default isNonEmptyObject
