import { equal } from 'assert'

import iban from 'iban'
import assertString from './assertString'

// Helpers
const isIbanValid = iban.isValid

const assertIban = (input, variableName) => {
  assertString(input, variableName)
  equal(
    isIbanValid(input),
    true,
    `${variableName} should be an IBAN and not: ${input}`,
  )
}

export default assertIban
