import isObject from './isObject'
import isArray from './isArray'

const isCollection = variable => (
  isArray(variable)
  && variable.reduce(
    (accumulator, elementOfArray) => accumulator && isObject(elementOfArray),
    true,
  )
)

export default isCollection
