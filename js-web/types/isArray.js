const isArray = variable => Array.isArray(variable)

export default isArray
