import { equal } from 'assert'
import assertString from './assertString'

const assertNonEmptyString = (input, variableName) => {
  assertString(input, variableName)
  equal(!!input.length, true, `${variableName} should be a non-empty string and not: ${input} of type ${typeof input}`)
}

export default assertNonEmptyString
