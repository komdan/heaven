const neverEndingPromise = () => new Promise(() => null)

export default neverEndingPromise
