function btoa(string) {
  return Buffer.from(string).toString('base64')
}

export default btoa

// > btoa('allo')
// 'YWxsbw=='
