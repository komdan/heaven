const fromBase64 = b64string => (Buffer.from(b64string, 'base64')).toString()

export default fromBase64

// > fromBase64('YWxsbw==')
// 'allo'
