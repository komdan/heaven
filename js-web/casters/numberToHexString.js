const numberToHexString = number => number.toString(16)

export default numberToHexString
