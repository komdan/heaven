const toBase64 = string => Buffer.from(string).toString('base64')

export default toBase64

// > toBase64('allo')
// 'YWxsbw=='
