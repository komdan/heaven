const {
  REACT_APP_TIMEDOUTPROMISE_DELAY,
} = process.env

const timedOutPromise = (promise, milliseconds) => {
  const ms = milliseconds || REACT_APP_TIMEDOUTPROMISE_DELAY || 20 * 1000 // 20 secs
  return Promise.race([
    promise,
    new Promise((resolve, reject) => setTimeout(
      () => {
        // log(`common.js\\timedOutPromise():: setTimeout resolved after ${ms}ms`)
        reject(new Error(`common.js\\timedOutPromise():: Timed out after ${ms}ms`))
      },
      ms,
    )),
  ])
}

export default timedOutPromise
