import msToWait from './msToWait'

const executeTodayAt = (hour, minute, timeZone, callBack) => {
  const delay = msToWait(hour, minute, timeZone)
  return setTimeout(
    () => delay > 0 && callBack(),
    delay,
  )
}

export default executeTodayAt
