const msToString = (
  timestamp,
  {
    symbols = true, // displays 30' instead of 30min
  } = {},
) => {
  if (!timestamp) return '0'
  // const ms = timestamp % 1000
  const seconds = Math.floor((timestamp % 60000) / 1000)
  const minutes = Math.floor((timestamp % 3600000) / 60000)
  const hours = Math.floor(timestamp / 3600000)

  const hourString = symbols ? 'h' : 'h'
  const minuteString = symbols ? '\'' : 'min'
  const secondString = symbols ? '"' : 'sec'

  // below 1s
  if (timestamp < 1000) return '0'
  // below 1min
  if (timestamp < 60 * 1000) return `${seconds}${secondString}`
  // below 1hour
  if (timestamp < 60 * 60 * 1000) return `${minutes}${minuteString}${seconds ? `${seconds}${secondString}` : ''}`
  // hours
  return `${hours}${hourString}${minutes ? `${minutes}${minuteString}` : ''}${seconds ? `${seconds}${secondString}` : ''}`
}

export default msToString
