import {
  isEqual,
} from 'lodash'
import roundDateToDay from './roundDateToDay'

const isDateToday = date => isEqual(
  roundDateToDay(date),
  roundDateToDay(Date.now()),
)

export default isDateToday
