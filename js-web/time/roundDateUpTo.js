const roundDateUpTo = (
  roundTo,
  date = new Date(),
) => new Date(Math.ceil(date / roundTo) * roundTo)

export default roundDateUpTo

// Example. Round up to 5 min
// > roundDateUpTo(5*60*1000)
// 2021-01-31T09:30:00.000Z
