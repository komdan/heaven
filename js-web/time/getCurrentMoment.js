import moment from 'moment-timezone'

const getCurrentMoment = (timeZone) => {
  const time = moment.tz(Date.now(), timeZone)
  const day = time.day()
  const month = time.month()
  const year = time.year()
  const hours = `${time.hour()}:${time.minute()}`

  return ({
    time,
    day,
    hours,
    month,
    year,
  })
}

export default getCurrentMoment
