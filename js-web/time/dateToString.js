import moment from 'moment-timezone'

const dateToString = (date, timeZone) => moment.tz(
  date,
  timeZone,
).format('DD.MM.YYYY-HH:mm')

export default dateToString
