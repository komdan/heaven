const timestampToLocalDateString = timestamp => timestamp.toDate().toLocaleDateString()

export default timestampToLocalDateString
