import roundDateDownTo from './roundDateDownTo'

const roundDateToDay = date => roundDateDownTo(
  24 * 3600 * 1000,
  date,
)

export default roundDateToDay
