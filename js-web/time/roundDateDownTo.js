const roundDateDownTo = (
  roundTo,
  date = new Date(),
) => new Date(Math.floor(date / roundTo) * roundTo)

export default roundDateDownTo

// Example. Round down to 5 min
// > roundDateDownTo(5*60*1000)
// 2021-01-31T09:25:00.000Z
