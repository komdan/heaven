import moment from 'moment'

const executeAt = (date, callback) => {
  const now = new moment() // eslint-disable-line
  const executionTime = new moment(date) // eslint-disable-line

  const delay = executionTime.diff(now)
  return setTimeout(
    () => delay > 0 && callback(),
    delay,
  )
}

export default executeAt
