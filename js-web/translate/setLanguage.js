import LANGUAGE from './LANGUAGE'
import translations from './translations'

const setLanguage = (lang) => {
  if (lang.toLowerCase() in translations) {
    LANGUAGE.locale = lang.toLowerCase()
  }
}

export default setLanguage
