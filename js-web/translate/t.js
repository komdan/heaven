import LANGUAGE from './LANGUAGE'
import translations from './translations'

// console.log('LANGUAGE.locale in t.js:', LANGUAGE.locale)

const t = string => (
  (string in translations[LANGUAGE.locale])
    ? translations[LANGUAGE.locale][string]
    : string
)

export default t
