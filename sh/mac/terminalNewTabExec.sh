terminalNewTabExec() {
  COMMAND=$1
  open -a Terminal .
  osascript -e "tell application \"Terminal\"" -e "tell application \"System Events\" to keystroke \"t\" using {command down}" -e "do script \"cd $PWD; $COMMAND\" in front window" -e "end tell" > /dev/null  
}
