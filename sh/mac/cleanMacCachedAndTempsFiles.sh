cleanMacCachedAndTempsFiles() {
  askIfEmpty userName $(whoami)
  step "Cleaning cache for user : $userName"
  rm -rf "/System/Volumes/Data/Users/$userName/Library/Caches/*"

  step "Cleaning temporary files"
  rm -rf "/System/Volumes/Data/private/var/tmp/*"
  rm -rf "/System/Volumes/Data/private/tmp/*"

  step "User-specific application logs"
  rm -rf "/System/Volumes/Data/Users/$userName/Library/Logs/*"

  step "System logs"
  sudo rm -rf "/System/Volumes/Data/private/var/log/*"

  step "iOS device backups (if you use iTunes or Finder for backups)"
  rm -rf "/System/Volumes/Data/Users/$userName/Library/Application Support/MobileSync/Backup/*"

  step "System update files (if you're not planning to revert updates)"
  sudo rm -rf "/System/Volumes/Data/Library/Updates/*"

  step "Old macOS installer files (if you're not planning to revert to an older macOS version)"
  sudo rm -rf "/System/Volumes/Data/Applications/Install macOS*.app"

  step "Xcode : Removing old simulator data"
  rm -rf "/Users/$userName/Library/Developer/Xcode/iOS DeviceSupport/*"

  step "Xcode : Removing old archives"
  rm -rf "~/Library/Developer/Xcode/Archives/*"

  step "Xcode : Cleaning derived data"
  rm -rf "~/Library/Developer/Xcode/DerivedData/*"

  step "Xcode : Cleaning Xcode cache"
  rm -rf "~/Library/Caches/com.apple.dt.Xcode/*"
}
