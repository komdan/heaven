#!/bin/sh
source loadHeaven.sh
BACKEND_FOLDER_PATH=/Users/mml/Documents/01.Komdan/01.Ubar/01.Dev/ubar-backend
MANAGER_FOLDER_PATH=/Users/mml/Documents/01.Komdan/01.Ubar/01.Dev/komdab-manager
WEBAPP_FOLDER_PATH=/Users/mml/Documents/01.Komdan/01.Ubar/01.Dev/komdab-menu-webapp
IOS_APP_NAME=komdabmobile

open -a Terminal -n
# https://stackoverflow.com/questions/14679809/how-can-i-open-multiple-tabs-automatically-on-terminal-in-mac-os-lion-by-executi

terminalNewTabExec ./launchBackend.sh
terminalNewTabExec ./launchManager.sh
terminalNewTabExec ./launchWebapp.sh
