gitFetchAndReset() {
  chmod +x ./$ASKPASS_FILE.sh;

  GIT_ASKPASS=./$ASKPASS_FILE.sh git fetch --all;
  GIT_ASKPASS=./$ASKPASS_FILE.sh git reset --hard origin/master;

  if [ "$HEAVEN_SUBMODULE_PATH" != "" ]; then {
    cd $HEAVEN_SUBMODULE_PATH
    GIT_ASKPASS=../$ASKPASS_FILE.sh git fetch --all;
    GIT_ASKPASS=../$ASKPASS_FILE.sh git reset --hard origin/master;
    cd $HEAVEN_SUBMODULE_PATH_GETBACK
  }; fi;
}
