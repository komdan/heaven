gitAddCommitPullPush() {
  commitMessage=${1:-"$commitMessage"}

  echoWithFolder "git add ."
  git add .;

  echoWithFolder "git status"
  git status;

  echoWithFolder "git commit -m \"$commitMessage\""
  git commit -m "$commitMessage";

  chmod +x ./$ASKPASS_FILE.sh;
  echoWithFolder "git pull"
  GIT_ASKPASS=./$ASKPASS_FILE.sh git pull || {
    ask gitPullFailedShouldWeContinue yes no && {
      git mergetool
      exit 0
    };
  };
  echoWithFolder "git push"
  GIT_ASKPASS=./$ASKPASS_FILE.sh git push || {
    ask gitPushFailedShouldWeGitMergetool yes no && {
      git mergetool
      exit 0
    };
  };
}
