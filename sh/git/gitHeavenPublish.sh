gitHeavenPublish() {
  askIfEmpty shouldWeSyncHeaven yes no
  if [ "$HEAVEN_SUBMODULE_PATH" != "" ] && [ "$shouldWeSyncHeaven" != "no" ];
    then {
      cd "$HEAVEN_SUBMODULE_PATH" || exit;

      gitStatus
      askIfEmpty heavenCommitMessage "..."
      gitAddCommitPullPush "$heavenCommitMessage"

      cd "$HEAVEN_SUBMODULE_PATH_GETBACK" || exit;
    };
    else {
      echo "$ERRORICON heaven will not be synced : HEAVEN_SUBMODULE_PATH is empty or you decided not to"
    };
  fi;
}
