gitSetUser() {
  git remote set-url origin "https://$bitbucketUserName@bitbucket.org/$BITBUCKET_REPO"
  if [ "$HEAVEN_SUBMODULE_PATH" != "" ]; then {
    cd $HEAVEN_SUBMODULE_PATH
    git remote set-url origin https://$bitbucketUserName@bitbucket.org/komdan/heaven.git
    cd $HEAVEN_SUBMODULE_PATH_GETBACK
  }; fi;
}
