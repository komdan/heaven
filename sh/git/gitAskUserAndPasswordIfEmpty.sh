gitAskUserAndPasswordIfEmpty() {
  askIfEmpty bitbucketUserName "$SUGGESTED_BITBUCKET_USER"
  secureAskIfEmpty bitbucketPassword
  gitSetPasswordFile
}
