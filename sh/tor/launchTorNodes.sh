# source tor/createTorConfigurations.sh

launchTorNodes() {
  NB_NODES=$1
  BASE_PORT=$2
  NODES_MAX_INDEX=$(($NB_NODES - 1))
  echo "$INFO launchTorNodes ⚡ on $NB_NODES circuits...$NC"

  killingTorProcess() {
    killall tor
  }
  launchTorCircuits() {
    for i in $(seq 0 $NODES_MAX_INDEX);
    do {
      PORT=$(($BASE_PORT + $i))
      echo "Launching circuit on port $PORT - torNodeIndex: $i"
      tor --quiet -f ./temp/tor/torrc.$i &
    };
    done;
  }

  spinner killingTorProcess
  spinner createTorConfigurations
  spinner launchTorCircuits
}
