createTorConfigurations() {
  # BASE_PORT=$1
  # NODES_MAX_INDEX=$2

  for i in $(seq 0 $NODES_MAX_INDEX);
  do
    PORT=$(($BASE_PORT + $i))
    mkdir -p ./temp/tor/tor.$i
    echo "\
      SocksPort $PORT
      DataDirectory ./temp/tor/tor.$i
    " > ./temp/tor/torrc.$i
  done;
}
