copyDevFiles() {
  printf '%s\n' "$INFO Copying development files "

  cp .buildFiles/development/GoogleService-Info.plist ios/
  cp .buildFiles/development/Info.plist ios/"$IOS_APP_NAME"/
  cp .buildFiles/development/google-services.json android/app/
  cp .buildFiles/development/strings.xml android/app/src/main/res/values/
  cp .buildFiles/development/env.js ./env.js
}
