#!/bin/bash
# TODO refacto in heaven/sh
# USAGE
deliver() {
  gitMasterSave() {
    git checkout master
    FILE_NAME=deliver.sh
    GITCOMMENT="$versionCode"
    gitPublishAll
  }

  pushingToTheRightGitBranch() {
    askIfEmpty versionCode
    git checkout master # clean git state to prevent future errors
    git checkout "v$versionCode" || git checkout -b "v$versionCode" # go to right branch OR creates it
    git pull origin master:"v$versionCode"
    git push --set-upstream origin "v$versionCode"
    git checkout master # return to master branch
  }

  showingMasterBranchStatus() {
    git checkout master
    git status
  }

  showingNewVersionBranchStatus() {
    git checkout "v$versionCode"
    git status
    git checkout master
  }

  buildIosAndPrebuildAndroid() {
    .scripts/build-prod.sh
  }

  showingAndroidAppDate() {
    ls -la android/app/build/outputs/apk/release/
  }

  main() {
    # echo "${INFO} Performing MANDATORY checks"
    # ask hasTestsBeenDoneOnProductionAppInReleaseMode no yes
    # exitOn hasTestsBeenDoneOnProductionAppInReleaseMode no

    # ask isBackendUpToDateInProduction no yes
    # exitOn isBackendUpToDateInProduction no

    # ask hasDbConsistencyChecksBeenLaunchRightNow no yes
    # exitOn hasDbConsistencyChecksBeenLaunchRightNow no

    # ask hasFirebaseFunctionsBeenDeployed no yes
    # exitOn hasFirebaseFunctionsBeenDeployed no

    echo "${INFO} Saving git master branch"
    ask versionCode $BUILD_VERSION
    gitMasterSave
    exitOnEmptyVariable versionCode
    spinner pushingToTheRightGitBranch
    spinner showingMasterBranchStatus
    spinner showingNewVersionBranchStatus
    ask doesEverythingLookFineOnMasterAndSavingBranches no yes
    exitOn doesEverythingLookFineOnMasterAndSavingBranches no

    # echo "$INFO Launching build-prod.sh: Building Android and prebuilding iOS"
    # buildIosAndPrebuildAndroid

    echo "$INFO Prebuiling iOS and building Android done"
    spinner showingAndroidAppDate

    ask isApkJustCreated no yes
    exitOn isApkJustCreated no

    # echo "$INFO FASTLANE: fastlane ios deliver"
    # fastlane ios buildanddeliver
  }
  main  
}
