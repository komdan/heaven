cleanXcodeBuildsAndCache() {
  echo "$INFO> (silent) Closing Xcode...$NC";
  killall Xcode 1>/dev/null 2>/dev/null;

  echo "$INFO> (silent) Deep clean Xcode builds...$NC";
  rm -rf ios/build 1>/dev/null 2>/dev/null;

  (
    cd ios || exit;
    if xcodebuild clean 1>/dev/null 2>/dev/null;
      then echo "$GREEN> Xcode build cleaning SUCCESSFUL 🔥$NC";
      else echo "$RED> Xcode build cleaning FAILED 👎$NC";
    fi;
    echo "$INFO> (silent) Deep clean Xcode workspace...$NC";

    if xcodebuild clean -workspace $IOS_APP_NAME.xcworkspace -scheme $IOS_APP_NAME
      then echo "$GREEN> Xcode workspace build cleaning SUCCESSFUL 🔥$NC";
      else echo "$RED> Xcode workspace build cleaning FAILED 👎$NC";
    fi;

    echo "$INFO> Cleaning xcuserdata directories...$NC";
    find . -name "xcuserdata" -type d -exec rm -rf {} \;
    # cd ..;
  )

  echo "$INFO> (silent) Cleaning Xcode cache...$NC"

  # https://stackoverflow.com/questions/5714372/how-to-empty-caches-and-clean-all-targets-xcode-4-and-later

  if rm -rf ~/Library/Developer/Xcode/DerivedData
    then echo "$GREEN> DerivedData cleaning SUCCESSFUL 🔥$NC";
    else echo "$RED> DerivedData cleaning FAILED 👎 Trying again...$NC";
  fi;

  if rm -rf ~/Library/Caches/com.apple.dt.Xcode
    then echo "$GREEN> com.apple.dt.Xcode cleaning SUCCESSFUL 🔥$NC";
    else echo "$RED> com.apple.dt.Xcode cleaning FAILED 👎$NC";
  fi;

  if rm -rf "$(getconf DARWIN_USER_CACHE_DIR)/org.llvm.clang/ModuleCache"
    then echo "$GREEN> ModuleCache cleaning SUCCESSFUL 🔥$NC";
    else echo "$RED> ModuleCache cleaning FAILED 👎$NC";
  fi;
}