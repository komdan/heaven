cleanPods() {
  echo "$INFO> Cleaning pods$NC"
  cd ios || exit;
  rm -rf "${HOME}/Library/Caches/CocoaPods"
  rm -rf "$(pwd)/Pods/"

  echo "$INFO> pod cache clean$NC"
  pod cache clean --all

  echo "$INFO> pod deintegrate$NC"
  pod deintegrate

  echo "$INFO> rm Podfile.lock"

  if rm Podfile.lock 2>/dev/null;
    then echo "$GREEN> Pods removal SUCCESSFUL 🔥$NC";
    else echo "$RED> Pods removal FAILED 👎$NC";
  fi;

  cd ..;
}
