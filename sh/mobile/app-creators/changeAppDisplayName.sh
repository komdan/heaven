getPbxprojPath() {
  find . -name project.pbxproj -depth 3
}

getXcschemePath() {
  find . -name "*.xcscheme" -depth 5
}

getLaunchScreenXibPath() {
  find . -name "LaunchScreen.xib" -depth 4
}

getLaunchScreenStringsPath() {
  find . -name "LaunchScreen.strings" -depth 4
}

replaceStringsInXcodeProject() {
  replaceInFile "\/\* $oldApplicationDisplayName.app \*\/" "\/\* $newApplicationDisplayName.app \*\/" "$pbxprojPath"
  replaceInFile "path = $oldApplicationDisplayName.app" "path = $newApplicationDisplayName.app" "$pbxprojPath"
  replaceInFile "PRODUCT_NAME = $oldApplicationDisplayName;" "PRODUCT_NAME = $newApplicationDisplayName;" "$pbxprojPath"
  replaceInFile "BuildableName = \"$oldApplicationDisplayName.app\"" "BuildableName = \"$newApplicationDisplayName.app\"" "$xcschemePath"
  replaceInFile "$oldApplicationDisplayName" "$newApplicationDisplayName" "$launchScreenXibPath"
  replaceInFile \
    ".text\" = \"$oldApplicationDisplayName\"" \
    ".text\" = \"$newApplicationDisplayName\"" \
    "$launchScreenStrings"
}

getOldDisplayName() {
  grep -o "/\* [a-zA-Z]*\.app \*/" "$pbxprojPath" | cut -d' ' -f2 | cut -d'.' -f1 | head -n1
}

backupPbxproj() {
  tempfile=$(mktemp)
  cp "$pbxprojPath" "$tempfile"
  export pbxprojBackup=$tempfile
}

backupXcscheme() {
  tempfile=$(mktemp)
  cp "$xcschemePath" "$tempfile"
  export xcschemeBackup=$tempfile
}

changeNameOnIos() {
  backupPbxproj # pbxprojBackup
  backupXcscheme # xcschemeBackup

  spinner replaceStringsInXcodeProject

  echo "$INFO colordiff $pbxprojBackup $pbxprojPath"
  colordiff "$pbxprojBackup" "$pbxprojPath" | less
  ask removePbxprojBackup
  rm "$pbxprojBackup"

  echo "$INFO colordiff $xcschemeBackup $xcschemePath"
  colordiff "$xcschemeBackup" "$xcschemePath" | less
  ask removeXcschemeBackup
  rm "$xcschemeBackup"
}

changeDisplayNameOnAppJson() {
  replaceInFile \
    "displayName\": \"$oldApplicationDisplayName" \
    "displayName\": \"$newApplicationDisplayName" \
    "app.json"
}

changeAppNameInAndroid() {
  step "Change app_name in 3 strings.xml"
  replaceInFile \
    "\<string name\=\"app_name\"\>$oldApplicationDisplayName\<\/string\>" \
    "\<string name\=\"app_name\"\>$newApplicationDisplayName\<\/string\>" \
    "android/app/src/main/res/values/strings.xml"
  replaceInFile \
    "\<string name\=\"app_name\"\>$oldApplicationDisplayName\<\/string\>" \
    "\<string name\=\"app_name\"\>$newApplicationDisplayName\<\/string\>" \
    ".buildFiles/development/strings.xml"
  replaceInFile \
    "\<string name\=\"app_name\"\>$oldApplicationDisplayName\<\/string\>" \
    "\<string name\=\"app_name\"\>$newApplicationDisplayName\<\/string\>" \
    ".buildFiles/production/strings.xml"
}

changeAppDisplayName() {
  # #!/bin/bash
  # # USAGE
  # # ./.scripts/app-creators/changeAppDisplayName.sh

  # . .scripts/common/setColors.sh # RED GREEN ORANGE BLUE PURPLE CYAN GRAY NC QUESTION INFO DOTS ACTION SUCCESSICON
  # . .scripts/common/helpers.sh # camelCaseToSentence ask secureAsk colorize exitOnEmptyVariable drawLine replaceInFile spinner
  # . .scripts/config/setConfig.sh # IOS_APP_NAME

  # # "/* $newApplicationDisplayName.app */"
  # # "path = $newApplicationDisplayName.app"
  # # "PRODUCT_NAME = $newApplicationDisplayName;"

  # # "/* $oldApplicationDisplayName.app */"
  # # "path = $oldApplicationDisplayName.app"
  # # "PRODUCT_NAME = $oldApplicationDisplayName;"

  clear
  pbxprojPath=$(getPbxprojPath)
  xcschemePath=$(getXcschemePath)
  launchScreenXibPath=$(getLaunchScreenXibPath)
  launchScreenStrings=$(getLaunchScreenStringsPath)

  ask shouldWeGitSaveBeforeProceeding yes
  if [ "$shouldWeGitSaveBeforeProceeding" = "yes" ]; then {
    gitPublishAll
  }; fi;

  ask oldApplicationDisplayName "$(getOldDisplayName)"
  ask newApplicationDisplayName
  exitOnEmptyVariable newApplicationDisplayName


  changeNameOnIos
  changeDisplayNameOnAppJson
  changeAppNameInAndroid

  echo "$SUCCESSICON App display name changed successfully"
}
