forkMobileApp() {
  #!/bin/bash
  # USAGE
  # ./.scripts/app-creators/forkMobileApp.sh
  echo "$INFO INFO: Old application name $NC"
  oldApplicationNameSuggestion=$(grep app_name ./android/app/src/main/res/values/strings.xml | cut -d'>' -f2 | cut -d'<' -f1)
  find . -name "strings.xml" -type f -exec grep -H "<string name=\"app_name\">" {} \; | colorize $oldApplicationNameSuggestion
  echo

  echo "$INFO INFO: Old package name $NC"
  oldPackageNameSuggestion=$(grep applicationId android/app/build.gradle | cut -d'"' -f2)
  oldPackageLittleNameSuggestion=$(cut -d"." -f3 <<< $oldPackageNameSuggestion)
  find . -name "BUCK" -type f -exec grep -H "package = \"" {} \; | colorize $oldPackageNameSuggestion
  find . -name "build.gradle" -type f -exec grep "applicationId \"" {} \; | colorize $oldPackageNameSuggestion
  find . -name "R.java" -type f -exec grep "package com.komdan" {} \; | colorize $oldPackageNameSuggestion
  find . -name "AppDelegate.m" -type f -exec grep "deepLinkURLScheme" {} \; | colorize $oldPackageNameSuggestion
  echo

  echo "$INFO INFO: Old project name $NC"
  oldProjectNameSuggestion=$(grep "rootProject.name" android/settings.gradle | cut -d"'" -f2)
  find . -name Podfile -type f -exec grep "target" {} \; | colorize $oldProjectNameSuggestion
  find . -name "settings.gradle" -type f -exec grep rootProject.name {} \; | colorize $oldProjectNameSuggestion
  echo

  ask oldPackageName $oldPackageNameSuggestion
  ask oldPackageLittleName $oldPackageLittleNameSuggestion
  ask oldProjectName $oldProjectNameSuggestion
  ask oldApplicationName $oldApplicationNameSuggestion
  echo

  ask newProjectName komdabmobile "mynewapp"
  exitOnEmptyVariable newProjectName
  ask newProjectPackageName com.komdan.$newProjectName
  ask newProjectPackageLittleName $newProjectName
  ask newProjectAppName $newProjectName
  echo

  echo "$INFO Launching clean all,$ORANGE please clean everything without reinstalling anything when asked$NC";
  cleanAll

  echo "$DOTS Remove e2e/e2e.artifacts ? (Ctrl-C to abort) $NC";
  read
  rm -rf e2e/e2e.artifacts

  echo "$DOTS Remove *.ipa ? (Ctrl-C to abort) $NC";
  read
  rm -rf *.ipa

  echo "$DOTS Copy everything into ../$newProjectName ? (Ctrl-C to abort) $NC";
  read

  if [ -d ../$newProjectName ]; then {
    echo "$INFO ../$newProjectName exists"
    ask shouldWeDeleteIt "no" "yes"
    if [ "$shouldWeDeleteIt" != "no" ]; then {
      rm -rf ../$newProjectName;
    };
    else
      echo "$RED Exit..."
      exit 1;
    fi;
  };
  fi;

  duplicateProject() {
    mkdir ../$newProjectName
    cp -R ./ ../$newProjectName
    echo
  }
  spinner duplicateProject;

  echo "$INFO cd ../$newProjectName$NC";
  cd ../$newProjectName
  echo

  echo "$INFO Unlinking git $NC";
  rm -rf .git/
  echo

  echo "$INFO INFO: files containing $oldApplicationName $NC"
  grep -R $oldApplicationName . --exclude-dir 'src' | colorize $oldApplicationName
  echo

  echo -n "$QUESTION Is it ok to replace $GREEN$oldApplicationName$NC by $GREEN$newProjectName$NC temporarily in previous files ? (defaut: yes, no to quit) $NC";
  read replaceInPreviousFiles
  if [ "$replaceInPreviousFiles" != "no" ];
  then {
    echo "$INFO Replacing... $NC"
    grep -Rl $oldApplicationName . --exclude-dir 'src' | while read filePath;
    do {
      sed -i ".sedBackup" "s/$oldApplicationName/$newProjectName/g" $filePath
    }
    done;
  };
  else {
    exit
  }
  fi;
  echo

  ask launchReactNativeRename yes no
  if [ "$launchReactNativeRename" != "no" ]; then {
    react-native-rename $newProjectName -b $newProjectPackageName
  };
  fi;
  echo

  echo "$DOTS Removing if empty : ./android/app/src/main/java/com/komdan/$oldPackageLittleName ? (Ctrl-C to abort, ANY KEY to continue) $NC";
  read
  removeOldAndroidProjectIfEmpty () {
    rmdir ./android/app/src/main/java/com/komdan/$oldPackageLittleName
  }
  spinner removeOldAndroidProjectIfEmpty
  echo

  # $oldPackageName
  echo "$DOTS Replacing $oldPackageName in files... (Ctrl-C to abort, ANY KEY to continue) $NC"
  read
  replaceOldPackageNameInFiles() {
    sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./.buildFiles/production/Info.plist
    sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./.buildFiles/development/Info.plist
    # sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./ios/"$newProjectName"/Info.plist
    # sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./ios/"$newProjectName"/AppDelegate.m
    # sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./ios/"$newProjectName".xcodeproj/project.pbxproj
    sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./ios/"$oldProjectName"/Info.plist
    sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./ios/"$oldProjectName"/AppDelegate.m
    sed -i ".sedBackup" "s/$oldPackageName/$newProjectPackageName/g" ./ios/"$oldProjectName".xcodeproj/project.pbxproj
  }
  spinner replaceOldPackageNameInFiles
  echo

  # $oldProjectName
  echo "$DOTS Replacing $oldProjectName in files... (Ctrl-C to abort, ANY KEY to continue) $NC"
  read
  replaceOldProjectNameInFiles() {
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./app.json
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./package.json
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./.buildFiles/production/env.js
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./.buildFiles/development/env.js
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./env.js
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/File.swift
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName"/Base.lproj/LaunchScreen.xib
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName"/AppDelegate.m
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/Podfile
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName"/fr.lproj/LaunchScreen.strings
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName"/"$oldProjectName".entitlements
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName".xcodeproj/project.pbxproj
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName".xcodeproj/xcshareddata/xcschemes/$oldProjectName.xcscheme
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName"Tests/"$oldProjectName"Tests.m
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./android/"$oldProjectName".iml
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./android/settings.gradle
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./android/.idea/workspace.xml
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./android/.idea/modules.xml
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName"Tests/"$oldProjectName"Tests.m
    sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" ./ios/"$oldProjectName".xcworkspace/contents.xcworkspacedata

    find . -name "xcschememanagement.plist" -exec sed -i ".sedBackup" "s/$oldProjectName/$newProjectName/g" {} \;
  }
  spinner replaceOldProjectNameInFiles
  echo

  echo "$DOTS Renaming folders containing $oldProjectName ? (Ctrl-C to abort) $NC"
  read
  renameOldProjectNameFolders() {
    find . -name "*$oldProjectName*" -type d | while read folderPath;
    do {
      newFolderPath=$(echo $folderPath | sed "s/$oldProjectName/$newProjectName/g")
      echo "$folderPath -> $newFolderPath $NC"
      mv $folderPath $newFolderPath
    };
    done;
  }
  spinner renameOldProjectNameFolders
  echo


  echo "$DOTS Renaming files containing $oldProjectName ? (Ctrl-C to abort) $NC"
  read
  renameOldProjectNameFiles() {
    find . -name "*$oldProjectName*" -type f | while read filePath;
    do {
      newFilePath=$(echo $filePath | sed "s/$oldProjectName/$newProjectName/g")
      echo "$filePath -> $newFilePath $NC"
      mv $filePath $newFilePath
    };
    done;
  }
  spinner renameOldProjectNameFiles
  echo


  echo "$DOTS Remove firebase files? (Ctrl-C to abort)"
  read
  removeFirebaseFiles() {
    rm ./.buildFiles/production/GoogleService-Info.plist
    rm ./.buildFiles/production/google-services.json
    rm -rf ./.buildFiles/production/firebase

    rm ./.buildFiles/development/GoogleService-Info.plist
    rm ./.buildFiles/development/google-services.json
    rm -rf ./.buildFiles/development/firebase

    rm ./ios/GoogleService-Info.plist
    rm ./android/app/google-services.json
  }
  spinner removeFirebaseFiles
  echo

  # npm install -g firebase-tools
  # firebase login
  # firebase projects:create

  ask handleNewBitbucketRepository yes no
  if [ "$handleNewBitbucketRepository" != "no" ]; then {
    ask bitbucketRepositoryName $newProjectName
    # ask bitbucketUsername wzate
    ask bitbucketOwnerName $bitbucketUsername

    listRepositories
    deleteRepository $bitbucketRepositoryName
    createRepository $bitbucketRepositoryName

    echo "$DOTS Git init and first commit ?"
    read

    git init
    git add .
    git commit -m "First commit"
    git remote add origin  https://$bitbucketUsername@bitbucket.org/$bitbucketOwnerName/$bitbucketRepositoryName.git
    git push --set-upstream origin master
  };
  fi;
  echo

  ask handleFirebaseProject yes no
  if [ "$handleFirebase" != "no" ]; then {
    refreshFirebaseConfig
  };
  fi;
  echo

  ask handleCodePush yes no
  if [ "$handleCodePush" != "no" ]; then {
    changeCodePushKeys
  };
  fi;
  echo

  echo "$INFO Removing *.sedBackup"
  find . -name "*.sedBackup" -exec rm {} \;
  echo

  echo "$INFO OK. Launching copyProdFiles$NC"
  copyDevFiles
  echo

  echo "$GREEN> OK. Now copy these files: $NC"
  echo "$ORANGE> ./.buildFiles/production/GoogleService-Info.plist $NC"
  echo "$ORANGE> ./.buildFiles/development/GoogleService-Info.plist $NC"
  echo "$ORANGE> ./.buildFiles/production/google-services.json $NC"
  echo "$ORANGE> ./.buildFiles/development/google-services.json $NC"
  echo "$ORANGE> ./android/app/google-services.json"

  echo "$GREEN> OK. Check is this files are ok: $NC"
  echo "$ORANGE> ./.buildFiles/production/env.js $NC"
  echo "$ORANGE> ./.buildFiles/development/env.js $NC"
  echo "$ORANGE> ./env.js $NC"

  echo "$RED> CAREFUL: you should 'cd ../$newProjectName' to go into the new arborescence"
  echo "buildProd"
  echo

  echo "$RED> CAREFUL: Visit MainActivity.java to check if getMainComponentName returns the correct value."
  echo "$RED> The value should match what's inside app.json"
  echo

  echo "$RED> CAREFUL: You should create a facebook app here : https://developers.facebook.com/apps"
  echo "$RED> Then change all strings.xml (android) and Info.plist (iOS)"
  echo

  echo "$RED> INFO: You might need to reset heaven submodule (TODO in this script)"
  echo

  echo "$RED> INFO: You will need to change LATESTAPPVERSION_URL in env.js files (TODO in this script)"
  echo

  echo "$RED> INFO: You will need to change where the app sends its logs (TODO in this script)"
  echo

  # grep -rl $oldPackageName . | grep -v "./src/"
  # grep -rl $oldProjectName . | grep -v "./src/"

  # grep -rl $newProjectPackageName .
  # grep -rl $newProjectName .

  # rm -rf .komplete-scripts/
  # rm -rf ./.buildFiles/ubar.prod/
  # rm -rf ./.buildFiles/ubar.dev/

  # cp -R ../komdabmobile/.komplete-scripts/ .komplete-scripts/
  # cp -R ../komdabmobile/.buildFiles/ubar.prod/ .buildFiles/ubar.prod/
  # cp -R ../komdabmobile/.buildFiles/ubar.dev/ .buildFiles/ubar.dev/
}
