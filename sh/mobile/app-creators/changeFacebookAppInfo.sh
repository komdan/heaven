openBrowserToFacebookConsole() {
  ask openFacebookConsole yes no
  if [ "$openFacebookConsole" != "no" ]; then {
    echo "$INFO Opening facebook console..."
    sleep 1
    open https://developers.facebook.com/apps
  }; fi;
}

extractValueFromPlist() {
  key="$1"
  filePath="$2"

  plutil -extract "$key" xml1 -o - "$filePath" | sed -n "s/.*<string>\(.*\)<\/string>.*/\1/p"
}

replaceValueInPlistFiles() {
  key="$1"
  value="$2"

  plutil -replace $key -string $value ios/$IOS_APP_NAME/Info.plist
  plutil -replace $key -string $value ./.buildFiles/development/Info.plist
  plutil -replace $key -string $value ./.buildFiles/production/Info.plist
}

replaceValueInStringsXmlFiles() {
  oldString="$1"
  newString="$2"

  find . -name "strings.xml" | grep -v "node_modules/" | while read file;
  do {
    replaceInFile "$1" "$2" "$file"
    replaceInFile "fb$1" "fb$2" "$file"
  };
  done;
}

listValuesFromPlistFiles() {
  key="$1"

  echo "$INFO Searching for $CYAN$key$NC"
  find . -name "Info.plist" | while read file;
  do {
    grep -l "$key" "$file"| while read filePath;
    do {
      echo -n "$filePath: "
      echo -n "$ORANGE"
      extractValueFromPlist "$key" "$filePath"
      echo -n "$NC"
    }
    done;
  }
  done;
}

listValueFromStringsXmlFiles() {
  key="$1"

  echo "$INFO Searching for $CYAN$key$NC";
  find . -name "strings.xml" | grep -v "node_modules/" | while read file;
  do {
    grep "$key" "$file" | colorize ">.*<" "$ORANGE" "$NC";
  };
  done;
}

showFacebookInfoInPlistFiles() {
  listValuesFromPlistFiles "FacebookAppID"
  listValuesFromPlistFiles "FacebookDisplayName"
}

showFacebookInfoInStringsXmlFiles() {
  listValueFromStringsXmlFiles "facebook_app_id"
  listValueFromStringsXmlFiles "fb_login_protocol_scheme"
}


retrieveFacebookDataSuggestions() {
  export oldFacebookAppIdSuggestion=$(extractValueFromPlist "FacebookAppID" "./.buildFiles/production/Info.plist")
  export oldFacebookDisplayNameSuggestion=$(extractValueFromPlist "FacebookDisplayName" "./.buildFiles/production/Info.plist")  
}

askFacebookInformation() {
  ask oldFacebookAppId $oldFacebookAppIdSuggestion
  ask oldFacebookDisplayName $oldFacebookDisplayNameSuggestion
  ask newFacebookAppId
  ask newFacebookDisplayName

  export oldFacebookAppId=$oldFacebookAppId
  export oldFacebookDisplayName=$oldFacebookDisplayName
  export newFacebookAppId=$newFacebookAppId
  export newFacebookDisplayName=$newFacebookDisplayName
}

replaceRemainingOccurences() {
  # oldFacebookAppId=216348562780848
  # newFacebookAppId=410142136560458

  find . -name "Info.plist"| while read file;
  do {
    grep -l "$oldFacebookAppId" "$file" | while read filePath;
    do {
      echo -n $CYAN
      echo sed -i ".sedBackup" "s/$oldFacebookAppId/$newFacebookAppId/g" $filePath
      echo -n $NC
      sed -i ".sedBackup" "s/$oldFacebookAppId/$newFacebookAppId/g" $filePath
      rm -f "*.sedBackup"
    };
    done;
  };
  done;
}

checkForRemainingOccurences() {
  # oldFacebookAppId=216348562780848

  find . -name "Info.plist"| while read filePath;
  do
    grep -R "$oldFacebookAppId" "$filePath"
  done;
}

changeFacebookAppInfo() {
  openBrowserToFacebookConsole

  spinner showFacebookInfoInPlistFiles
  spinner showFacebookInfoInStringsXmlFiles

  retrieveFacebookDataSuggestions # oldFacebookAppIdSuggestion oldFacebookAppIdSuggestion
  askFacebookInformation # oldFacebookAppId oldFacebookDisplayName newFacebookAppId newFacebookDisplayName

  replaceValueInPlistFiles "FacebookAppID" $newFacebookAppId
  replaceValueInPlistFiles "FacebookDisplayName" $newFacebookDisplayName

  replaceValueInStringsXmlFiles $oldFacebookAppId $newFacebookAppId

  spinner replaceRemainingOccurences
  spinner checkForRemainingOccurences
}
