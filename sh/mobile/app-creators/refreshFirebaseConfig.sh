refreshFirebaseConfig() {
  #!/bin/bash
  # USAGE
  # ./.scripts/app-creators/refreshFirebaseConfig.sh

  clear
  echo "$RED@$NC Go to firebase console ${ORANGE}https://console.firebase.google.com${NC} to create two projects (development & production), initialize ${ORANGE}iOS${NC} and ${ORANGE}Android${NC} applications and Firestore > 'Create database', setting europe-west2 (London). ${ORANGE}PRESS A KEY TO CONTINUE${NC}..."
  read

  echo "$INFO INFO: Project list..."
  firebase projects:list
  echo

  # Firebase : DEV
  rm -rf .buildFiles/development/firebase
  mkdir -p .buildFiles/development/firebase
  cd .buildFiles/development/firebase
  echo "$INFO 'firebase init' -> please choose ${ORANGE}DEVELOPMENT${NC} environment (firestore, storage) $NC"
  sleep 3
  firebase init
  echo

  echo "$INFO Select ${ORANGE}Android${NC} app to automatically write google-services.json $NC"
  firebase apps:sdkconfig -o ../google-services.json
  echo

  echo "$INFO Select ${ORANGE}iOS${NC} app to automatically write GoogleService-Info.plist $NC"
  firebase apps:sdkconfig -o ../GoogleService-Info.plist
  echo

  # Firebase : PROD
  cd ../../../

  rm -rf .buildFiles/production/firebase
  mkdir -p .buildFiles/production/firebase
  cd .buildFiles/production/firebase
  echo "$INFO 'firebase init' -> please choose ${ORANGE}PRODUCTION${NC} environment  (firestore, storage) $NC"
  sleep 3
  firebase init
  echo

  echo "$INFO Select ${ORANGE}Android${NC} app to automatically write google-services.json $NC"
  firebase apps:sdkconfig -o ../google-services.json
  echo

  echo "$INFO Select ${ORANGE}iOS${NC} app to automatically write GoogleService-Info.plist $NC"
  firebase apps:sdkconfig -o ../GoogleService-Info.plist
  echo

  cd ../../../  
}
