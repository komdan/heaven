TEMP_FONTS_FOLDER=./temp/fonts
TARGET_FONTS_FOLDER=./src/assets/fonts

listFilesInTemp() {
  find "$TEMP_FONTS_FOLDER" -type f
}

listFilesInAssets() {
  find "$TARGET_FONTS_FOLDER" -type f
}

copyingFontFiles() {
  find "$TEMP_FONTS_FOLDER" -type f | while read filePath;
  do {
    echo "Copying $filePath..."
    cp "$filePath" "$TARGET_FONTS_FOLDER"/ ;
  };
  done;
}

openReactNativeConfigFile() {
  ask doYouWantToOpenReactNativeConfigFile no yes
  if [ "$doYouWantToOpenReactNativeConfigFile" = "yes" ]; then
    open react-native.config.js;
  fi;
}

linkAssets() {
  # ask launchReactNativeAsset
  react-native-asset

  # ask launchReactNativeLink
  # react-native link
}

deletingFontsInAssets() {
  rm -rf "$TARGET_FONTS_FOLDER"/*
}

addFonts() {
  clear
  mkdir -p "$TEMP_FONTS_FOLDER"

  spinner listFilesInTemp
  spinner listFilesInAssets

  # ask continue

  # echo "$ACTION Make sure .react-native.config.js contains something like this:"
  # echo "
  # module.exports = {
  #   assets: ['./src/assets/fonts/'],
  # };
  # "
  
  # openReactNativeConfigFile

  echo "$INFO All fonts in assets folder ${ORANGE}will be lost$NC if they are not backedup in temporary folder"
  # ask continue

  spinner deletingFontsInAssets
  linkAssets
  spinner copyingFontFiles
  copyProdFiles
  linkAssets
  saveBuildFilesToProdFolder

  spinner deletingFontsInAssets
  linkAssets
  spinner copyingFontFiles
  copyDevFiles
  linkAssets
  saveBuildFilesToDevFolder

  # ask whichEnvironmentDoYouWantToLoad dev prod
  # if [ "$whichEnvironmentDoYouWantToLoad" = "dev" ]; then
  #   .scripts/common/copyDevFiles.sh;

  # echo "$INFO You can drop your ttf at ${ORANGE}https://fontdrop.info/$NC to check its real name"
  # ask openFontDropWebsite no yes
  # if [ "$openFontDropWebsite" = "yes" ]; then open https://fontdrop.info; fi;
}
