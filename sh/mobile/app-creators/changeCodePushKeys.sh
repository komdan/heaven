#!/bin/bash
# USAGE: .scripts/app-creators/changeCodePushKeys.sh

showAndroidKeysInfo() {
  echo "$INFO Code-Push - INFO: Peek the two Android deployment keys from below... $GRAY";
  grep -Ri "codepush" .buildFiles/development/strings.xml | colorize ">.*<" "$CYAN" "$GRAY";
  grep -Ri "codepush" .buildFiles/production/strings.xml | colorize ">.*<" "$CYAN" "$GRAY";
  echo "$NC";
  code-push deployment ls "$oldProjectName"-android -k;
}

showIosKeysInfo() {
  echo "$INFO Code-Push - INFO: Peek the two iOS deployment keys from below... $GRAY"
  grep -Ri -A1 "codepush" .buildFiles/development/Info.plist | colorize "<string>.*<\/string>" "$CYAN" "$GRAY"
  grep -Ri -A1 "codepush" .buildFiles/production/Info.plist | colorize "<string>.*<\/string>" "$CYAN" "$GRAY"
  echo "$NC"
  code-push deployment ls "$oldProjectName"-ios -k
}

showOldAndNewVariables() {
  echo "$DOTS Check below and proceed to replacing ? (Ctrl-C to abort) $NC"
  echo oldIosCodePushStagingKey: "$oldIosCodePushStagingKey"
  echo oldIosCodePushProductionKey: "$oldIosCodePushProductionKey"
  echo oldAndroidCodePushStagingKey: "$oldAndroidCodePushStagingKey"
  echo oldAndroidCodePushProductionKey: "$oldAndroidCodePushProductionKey"
  echo
  echo newIosCodePushProductionKey: "$newIosCodePushProductionKey"
  echo newIosCodePushStagingKey: "$newIosCodePushStagingKey"
  echo newAndroidCodePushProductionKey: "$newAndroidCodePushProductionKey"
  echo newAndroidCodePushStagingKey: "$newAndroidCodePushStagingKey"
}

replaceCodePushKeys() {
  find . -name "Info.plist" -type f | while read filePath;
  do {
    sed -i ".sedBackup" "s/$oldIosCodePushStagingKey/$newIosCodePushStagingKey/g" $filePath
    sed -i ".sedBackup" "s/$oldIosCodePushProductionKey/$newIosCodePushProductionKey/g" $filePath
  }
  done;

  find . -name "strings.xml" -type f | while read filePath;
  do {
    sed -i ".sedBackup" "s/$oldAndroidCodePushStagingKey/$newAndroidCodePushStagingKey/g" $filePath
    sed -i ".sedBackup" "s/$oldAndroidCodePushProductionKey/$newAndroidCodePushProductionKey/g" $filePath
  }
  done;

  find . -name "*.sedBackup" -type f -exec rm {} \;
}

changeCodePushKeys() {
  if [ "$oldProjectName" = "" ]; then
    ask oldProjectName "$IOS_APP_NAME";
  fi;

  if [ "$newProjectName" = "" ]; then
    ask newProjectName "$IOS_APP_NAME";
  fi;

  showAndroidKeysInfo
  ask oldAndroidCodePushProductionKey
  ask oldAndroidCodePushStagingKey

  showIosKeysInfo
  ask oldIosCodePushProductionKey
  ask oldIosCodePushStagingKey

  ask createNewCodePushStreams yes no

  if [ "$createNewCodePushStreams" != "no" ]; then {
    code-push app add "$newProjectName-ios" ios react-native
    code-push app add "$newProjectName-android" android react-native
  };
  else {
    echo "$INFO ANDROID - $newProjectName"
    code-push deployment ls "$newProjectName"-android -k
    echo "$INFO IOS - $newProjectName"
    code-push deployment ls "$newProjectName"-ios -k
  }
  fi;

  ask newIosCodePushProductionKey
  ask newIosCodePushStagingKey

  ask newAndroidCodePushProductionKey
  ask newAndroidCodePushStagingKey

  spinner showOldAndNewVariables
  ask proceedWithReplacing

  spinner replaceCodePushKeys

  ask deleteOldCodePushStreams no yes
  if [ "$deleteOldCodePushStreams" = "yes" ]; then {
    echo "$INFO If you know what you are doing, execute this:"
    echo code-push app rm $oldProjectName-ios ios react-native
    echo code-push app rm $oldProjectName-android android react-native
  };
  fi;
}
