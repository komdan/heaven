#!/bin/bash
# USAGE
# ./.scripts/app-creators/changeLogo.sh

CHANGELOGO_SMALLSIZE="430 x 430"
CHANGELOGO_BIGSIZE="1024 x 1024"
CHANGELOGO_RECTANGLESIZE="999 x 196"

colorizeRelevantSizes() {
  colorize "$CHANGELOGO_SMALLSIZE" "$GREEN" "$GRAY" | colorize "$CHANGELOGO_BIGSIZE" "$GREEN" "$GRAY"  | colorize "$CHANGELOGO_RECTANGLESIZE" "$GREEN" "$GRAY"
}

printPngInfosInFolder() {
  folder=$1
  echo "$INFO Information about images in $folder...$GRAY"
  find "$folder" -name "*.png" -exec file {} \;  | colorizeRelevantSizes
  echo -n $NC
  sleep 2
}

printImagesInfoInTemp() {
  printPngInfosInFolder "./temp"
  sleep 1
}
printImagesInfoInAssets() {
  printPngInfosInFolder "./src/assets/images"
  sleep 1
}

createImages() {
  cp "temp/logo-transparent-1024.png" "src/assets/images/logo-transparent-1024.png";
  cp "temp/side-logo.png" "src/assets/images/side-logo.png";

  rm -f "src/assets/images/app.png";
  convert "temp/logo-transparent-1024.png" -resize "430x430" -quality 100 "src/assets/images/app.png";
}

linkingAssets() {
  react-native-asset
  react-native link
}

changeLogo() {
  mkdir -p ./temp/logos
  spinner printImagesInfoInAssets

  spinner printImagesInfoInTemp

  echo "$DOTS Please place your new ${GREEN}logo-transparent-1024.png$NC and ${GREEN}side-logo.png$NC in ./temp/"
  read

  ask areImagesInTempFolder yes no
  if [ "$areImagesInTempFolder" = "no" ]; then exit 1; fi;

  spinner createImages
  spinner printImagesInfoInAssets
  spinner linkingAssets

  echo "${GREEN}ⓥ${NC} Logo successfully changed"
}
