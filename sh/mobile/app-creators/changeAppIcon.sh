DEFAULT_ICON_PATH="src/assets/images/logo-1024.png"

launchingReactNativeSetIcon() {
  npx react-native set-icon --path $newIconPath
}

changeAppIcon() {
  #!/bin/bash
  # USAGE
  # ./.scripts/app-creators/changeAppIcon.sh

  ask newIconPath $DEFAULT_ICON_PATH
  exitOnEmptyVariable newIconPath
  if [ "$newIconPath" != "$DEFAULT_ICON_PATH" ]; then
    cp $newIconPath $DEFAULT_ICON_PATH;
  fi;

  launchingReactNativeSetIcon
}
