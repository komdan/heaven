envJsToDotEnvFile() {
  input="$1"
  output="$2"

  cp "$input" "$output"
  replaceLineInFile "process.env" '' "$output"
  replaceInFile 'const ' '' "$output"
  replaceInFile ' = ' '=' "$output"
  replaceLineInFile "eslint-disable" '' "$output"
  replaceLineInFile "eslint-enable" '' "$output"
  deleteNewlinesInFile .env.heaven
}
