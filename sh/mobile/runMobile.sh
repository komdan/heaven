runMobile() {
  ask runIosApp yes no && {
    runIosApp
  }
  ask runAndroidApp yes no && {
    runAndroidApp
  }
}
