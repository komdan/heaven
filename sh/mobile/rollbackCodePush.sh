rollbackCodePush() {
  ask rollbackAndroid yes no && {
    code-push rollback "$IOS_APP_NAME"-android Production
  }
  ask rollbackIos yes no && {
    code-push rollback "$IOS_APP_NAME"-ios Production
  }
}
