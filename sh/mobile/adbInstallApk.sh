adbInstallApk() {
  ask apkPath "$1" "android/app/build/outputs/apk/release/app-release.apk"
  ask apkFileName "$2" "app-release.apk"
  adb push "$apkPath" '/data/local/tmp/'
  echo "pm install -r -t '/data/local/tmp/$2'" | adb shell
}

# RELEASE
# adb push 'android/app/build/outputs/apk/release/app-release.apk' '/data/local/tmp/'
# DEBUG
# adb push 'android/app/build/outputs/apk/debug/app-debug.apk' '/data/local/tmp/'

# adb shell

# RELEASE
# pm install -r -t "/data/local/tmp/app-release.apk"
# DEBUG
# pm install -r -t "/data/local/tmp/app-debug.apk"
