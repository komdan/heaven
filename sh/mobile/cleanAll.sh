#!/bin/sh
cleanAll() {
  ask checkIfAllMetroBundlerAreKilledBeforeContinuing

  ask shouldWeCleanNodeModules yes no && {
    ask shouldWeCleanYarn yes no && {
      spinner cleanYarn
    }
    ask shouldWeCleanNpm yes no && {
      spinner cleanNpm
    }
    ask shouldWeDeleteNodeModules yes no && {
      spinner deleteNodeModules
    }
    ask shouldWeReinstallNodeModules yes no && {
      yarnInstall() {
        yarn install --production=false # production false always install dev dependencies
      }
      spinner yarnInstall
    }
  }

  ask shoudlWeCleanAndroidStudioBuilds yes no && {
    cleanAndroidStudioBuilds
  }

  ask shouldWeCleanXcodeBuildsAndCache yes no && {
    cleanXcodeBuildsAndCache
  }

  ask shouldWeCleanPods yes no && {
    cleanPods
  }

  ask shouldWeInstallPods yes no && {
    podInstall
  }

  # ask shoudWeReactNativeLink yes no && {
  #   spinner reactNativeLink
  # }

  echo "$INFO REMOVING THE FUCKING Icon r files"
  find . -name Icon$'\r' -exec rm {} \;

  ask shouldWeWipeEmulators yes no && {
    wipeEmulators
  }

  echo "$SUCCESSICON ALRIGHT 🍾 ! Now please restart the packager! -> You can ./.run.sh and see if it works better.$NC";
}
