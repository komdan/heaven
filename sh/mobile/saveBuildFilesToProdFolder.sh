saveBuildFilesToDevFolder() {
  cp ios/GoogleService-Info.plist .buildFiles/production/GoogleService-Info.plist
  cp ios/"$IOS_APP_NAME"/Info.plist .buildFiles/production/Info.plist 
  cp android/app/google-services.json .buildFiles/production/google-services.json
  cp android/app/src/main/res/values/strings.xml .buildFiles/production/strings.xml
  cp ./env.js .buildFiles/production/env.js  
}
