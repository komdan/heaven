#!/bin/sh
# Matthieu MARIE-LOUISE - 09/09/2019 - refacto in heaven 02/2022
source common/*.sh

wipeEmulators() {  
  # ====== Making sure we get the right emulator
  export ANDROID_HOME=/Users/mml/Library/Android/sdk
  export PATH=$ANDROID_HOME/emulator:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH

  # ====== Globals
  echo "$CYAN> Wipe iOS Simulator?$NC ['no' to avoid]";
  read wipeIOSSimulator;
  if [ "$wipeIOSSimulator" != "no" ]; then {
    echo "$RED> Wiping iOS Simulator...$NC"
    killall "Simulator" 2> /dev/null;
    sleep 2
    xcrun simctl erase all;
    echo "$GREEN> Wiping ok 👍$NC"
    echo "$CYAN> Launch iOS Simulator?$NC ['no' to avoid]";
    read launchIOSSimulator;
    if [ "$launchIOSSimulator" != "no" ]; then {
      echo "$GREEN> Launching clean iOS Simulator 🔥🔥🔥$NC";
      # Launch
      xcrun simctl boot "iPhone 11 Pro"
      # Put window in premier plan baby
      open -a Simulator
    };
    fi
  };
  fi

  echo "$CYAN> Wipe Android Emulator ?$NC ['no' to avoid]$RC";
  read wipeAndroidEmulator;
  if [ "$wipeAndroidEmulator" != "no" ]; then {
    echo "$RED> Wiping Android Emulator...$NC";
    kill -9 $(pgrep -x qemu-system-x86_64) 2>/dev/null;
    sleep 2
    echo "$CYAN> Launch Android Emulator?$NC ['no' to avoid]";
    read launchAndroidEmulator;
    if [ "$launchAndroidEmulator" != "no" ];then {
      echo "$RED> Launching Android Emulator with -wipe-data...$NC";
      emulator -avd $(emulator -list-avds) -wipe-data 2>/dev/null 1>/dev/null &
    }; else {
      echo "$RED> Still launching Android Emulator :) will quit in 45sec max...$NC";
      emulator -avd $(emulator -list-avds) -wipe-data -quit-after-boot 45;
      echo "$GREEN> Wiping ok 👍$NC"
    };
    fi
  };
  fi
}
