installAndroidApp() {
  ask installDebugOrReleaseAndroidApp
  if [ "$installDebugOrReleaseAndroidApp" = "debug" ];
    then {
      adb push 'android/app/build/outputs/apk/debug/app-debug.apk' '/data/local/tmp/'
      echo 'pm install -r -t "/data/local/tmp/app-debug.apk"' | adb shell
    };
    else {
      adb push 'android/app/build/outputs/apk/release/app-release.apk' '/data/local/tmp/'
      echo 'pm install -r -t "/data/local/tmp/app-release.apk"' | adb shell
    };
  fi;
}

# adb push 'android/app/build/outputs/apk/release/app-release.apk' '/sdcard/'
# adb shell
# pm install -r -t "/sdcard/app-release.apk"
