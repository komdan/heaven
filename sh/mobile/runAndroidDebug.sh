runAndroidDebug() {
  npx react-native run-android
  # Under the hood, the command that will be executed follows
  # ./gradlew app:installDebug -PreactNativeDevServerPort=8081
}
