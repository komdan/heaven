deployStaging() {
  # Warning: deployStaging is called by deployProd
  # deployProd sets currentCodePushDeploymentType to 'deployProd'

  if [ "$currentCodePushDeploymentType" = '' ]; then
    copyDevFiles
  else 
    copyProdFiles
  fi

  # ====== Controls
  # GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
  # if [ "$GIT_BRANCH" != "staging" ]; then
  #   echo "$RED>Wrong git branch: $GIT_BRANCH"
  #   exit 0
  # fi

  step "Get mobile bundle infos"
  set -a
  getMobileBundleInfos # iosShortVersion iosBundleVersion androidVersionCode androidVersionName GITCOMMENT
  set +a

  step "Lint and propose fix on error 👩‍💻"
  lintAndProposeFixOnError

  step "Git publish all"
  gitPublishAll

  step "Code pushing - $IOS_APP_NAME"
  ask targetVersion "$iosShortVersion"
  step "Code pushing - $IOS_APP_NAME | $targetVersion"
  echo "$INFO Code pushing to $IOS_APP_NAME | $targetVersion in 3secs."
  sleep 3

  ask deployAndroid yes no && {
    echo "$IOS_APP_NAME"
    code-push release-react "$IOS_APP_NAME"-android android --deploymentName Staging --targetBinaryVersion "$targetVersion"
  }
  ask deployIos yes no && {
    code-push release-react "$IOS_APP_NAME"-ios ios --deploymentName Staging --targetBinaryVersion "$targetVersion"
  }
}
