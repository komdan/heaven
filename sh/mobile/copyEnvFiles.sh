copyEnvFiles() {
  askIfEmpty heavenEnvironment dev prod
  printf '%s\n' "$INFO Copying $heavenEnvironment files "

  cp .buildFiles/"$heavenEnvironment"/GoogleService-Info.plist ios/
  cp .buildFiles/"$heavenEnvironment"/Info.plist ios/"$IOS_APP_NAME"/
  cp .buildFiles/"$heavenEnvironment"/google-services.json android/app/
  cp .buildFiles/"$heavenEnvironment"/strings.xml android/app/src/main/res/values/
  cp .buildFiles/"$heavenEnvironment"/env.js ./env.js
}
