openAndroidBundleFolder() {
  ask openDebugOrRelease
  if [ "$openDebugOrRelease" = "release" ];
    then {
      open android/app/build/outputs/bundle/release
    };
    else {
      open android/app/build/outputs/bundle/debug
    };
  fi;
}
