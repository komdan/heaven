readNewBundleInfos() {
  ask newShortVersion "$iosShortVersion" # 2.0.60
  ask newBundleVersion "$iosBundleVersion" # 60
  ask newVersionName "$androidVersionName" # 2.0.60
  ask newVersionCode "$androidVersionCode" # 60
}
