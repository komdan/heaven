
removeArtifactsFolder() {
  rm -rf e2e/e2e.artifacts/*
}
runningTests() {
  detox test --debug-synchronization 400 -c ios --loglevel trace --artifacts-location ./e2e/e2e.artifacts/ --record-logs all --take-screenshots all --record-videos all 1>./e2e/e2e.artifacts/detox-stdout.txt 2>./e2e/e2e.artifacts/detox-stderr.txt
}
runTests() {
  # #!/bin/bash
  # # Usage: .scripts/tests/run-test.sh


  # . .scripts/common/setColors.sh # RED GREEN ORANGE BLUE PURPLE CYAN NC QUESTION INFO DOTS ACTION SUCCESSICON
  # . .scripts/common/helpers.sh # ask colorize drawLine exitOnEmptyVariable spinner
  # # . .scripts/config/setConfig.sh # IOS_APP_NAME
  spinner removeArtifactsFolder
  echo "$INFO Follow execution with '.scripts/tests/follow-test-logs.sh' or 'tail -n10000 -f ./e2e/e2e.artifacts/detox-stdout.txt'"
  spinner runningTests
  # echo "$SUCCESSICON Test completed with success !"
}
