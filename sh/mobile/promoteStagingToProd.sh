promoteStagingToProd() {
  # echo "$GREEN> If you know what you are doing, this is the command to execute: $NC"
  # echo "code-push promote $IOS_APP_NAME-ios Staging Production"
  # echo "code-push promote $IOS_APP_NAME-android Staging Production"
  echo "$GREEN> 🚨🚨🚨 Promoting just published staging to prod $NC"
  code-push promote $IOS_APP_NAME-ios Staging Production
  code-push promote $IOS_APP_NAME-android Staging Production
}
