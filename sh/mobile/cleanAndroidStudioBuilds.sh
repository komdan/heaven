cleanAndroidStudioBuilds() {
  echo "$INFO> (silent) Hard Removing Android builds...$NC"
  rm -rf android/.gradle
  rm -rf android/.idea
  rm -rf android/build
  rm -rf android/app/build
  rm -rf android/app/build.gradle.old

  echo "$INFO> (silent) Deleting hprof files...$NC"
  find . -name "*.hprof" -type f -exec rm {} \;

  echo "$INFO> (silent) Cleaning Android Studio builds...$NC"
  cd android || exit

  if ./gradlew clean 1>/dev/null 2>/dev/null;
    then echo "$GREEN> Android Studio builds cleaning SUCCESSFUL 🔥$NC";
    else echo "$RED> Android Studio builds cleaning FAILED 👎$NC"
  fi;

  cd ..;
}
