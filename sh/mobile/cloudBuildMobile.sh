cloudBuildMobile() {
  ask FILE_NAME '.build-prod.sh' '.build-staging.sh'

  exitOnEmptyVariable heavenEnvironment
  askIfAnyEmpty heavenEnvironment IOS_APP_NAME

  if [ "$heavenEnvironment" = 'production' ]; then {
    GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    if [ "$GIT_BRANCH" != "master" ]; then
      echo "$RED>Wrong git branch: $GIT_BRANCH"
      exit 0
    fi;
  }; fi;

  step "Lint and propose fix on error 👩‍💻"
  lintAndProposeFixOnError

  step "Update version 👩‍💻"
  getMobileBundleInfos
  # copyEnvFiles
  readNewBundleInfos
  replaceBundleInfos "$newBundleVersion" "$newShortVersion" "$newVersionCode" "$newVersionName"

  # Saving new Info.plist file
  cp ios/"$IOS_APP_NAME"/Info.plist .buildFiles/"$heavenEnvironment"/Info.plist 

  getMobileBundleInfos

  step "Mobile set up 👩‍💻"
  # spinner yarnInstall
  # spinner reactNativeLink
  spinner reactNativeAsset

  step "Git publish all 👩‍💻"
  gitPublishAll

  step "Remote Build - Trigger Xcode Cloud & Bitbucket pipelines build 👩‍💻"
  gitSetReleaseBranchVersionned

  step "JS - Deploy JS bundle to code push 👩‍💻"
  askIfEmpty shouldWePodInstall yes no && {
    spinner podInstall # needed for code push ?
  }
  deployProd
  code-push promote komdabmobile-ios Staging Production
  code-push promote komdabmobile-android Staging Production

  step "Follow build in the cloud ☁️"
  echo "🧷 Xcode Cloud : https://appstoreconnect.apple.com/teams/${APPSTORECONNECT_TEAM_ID}/apps/${APPSTORECONNECT_APP_ID}/ci/groups"
  # echo "🧷 Bitbucket pipelines : https://bitbucket.org/${BITBUCKER_HEAVEN_USERNAME}/${BITBUCKER_HEAVEN_REPONAME}/pipelines/results/page/1"
  open -a Firefox "https://appstoreconnect.apple.com/teams/${APPSTORECONNECT_TEAM_ID}/apps/${APPSTORECONNECT_APP_ID}/ci/groups"
  # open -a Firefox "https://bitbucket.org/${BITBUCKER_HEAVEN_USERNAME}/${BITBUCKER_HEAVEN_REPONAME}/pipelines/results/page/1"

  # Because pipelines does not work
  bundleAndroid
  open android/app/build/outputs/bundle/release
}
