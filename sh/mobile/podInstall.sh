podInstall() {
  cd ios || exit;

  if pod install --repo-update;
    then echo "$GREEN> Pods installation SUCCESSFUL 🔥$NC";
    else echo "$RED> Pods installation FAILED 👎$NC";
  fi;

  cd ..;
}
