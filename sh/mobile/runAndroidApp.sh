runAndroidApp() {
  ask runAndroidAppInDebugOrRelease "$runAndroidAppInDebugOrRelease" debug release
  if [ "$runAndroidAppInDebugOrRelease" = "debug" ];
    then {
      runAndroidDebug;
    };
    else {
      runAndroidRelease;
    };
  fi;
}
