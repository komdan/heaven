runIosApp() {
  ask runIosAppInDebugOrRelease "$runIosAppInDebugOrRelease" debug release

  echo $runningSimulatorName

  if [ "$runIosAppInDebugOrRelease" = "debug" ];
    then {
      ask runningSimulatorName "${runningSimulatorName:-'iPhone 13'}"
      npx react-native run-ios --simulator "$runningSimulatorName" --port 8089
    };
    else {
      ask runningDeviceName "${runningDeviceName:-'iPhone 13'}"
      npx react-native run-ios --configuration Release --device "$runningDeviceName"
    }
  fi;
}
