deploymentHistory() {
  echo "$INFO> code-push deployment history $IOS_APP_NAME-ios Production$NC"
  ask showThisHistory yes no && {
    code-push deployment history "$IOS_APP_NAME"-ios Production  
  }

  echo "$INFO> code-push deployment history $IOS_APP_NAME-android Production$NC"
  ask showThisHistory yes no && {
    code-push deployment history "$IOS_APP_NAME"-android Production
  }

  echo "$INFO> code-push deployment history $IOS_APP_NAME-ios Staging$NC"
  ask showThisHistory yes no && {
    code-push deployment history "$IOS_APP_NAME"-ios Staging
  }

  echo "$INFO> code-push deployment history $IOS_APP_NAME-android Staging$NC"
  ask showThisHistory yes no && {
    code-push deployment history "$IOS_APP_NAME"-android Staging
  }

}
