getMobileBundleInfos() {
  iosShortVersion=$(plutil -extract CFBundleShortVersionString xml1 -o - ios/$IOS_APP_NAME/Info.plist | sed -n "s/.*<string>\(.*\)<\/string>.*/\1/p")
  iosBundleVersion=$(plutil -extract CFBundleVersion xml1 -o - ios/$IOS_APP_NAME/Info.plist | sed -n "s/.*<string>\(.*\)<\/string>.*/\1/p")
  androidVersionCode=$(grep "versionCode " android/app/build.gradle | awk '{print $2}')
  androidVersionName=$(grep "versionName " android/app/build.gradle | awk '{print $2}' | tr -d '"')
  # GITCOMMENT="ios:($iosShortVersion, $iosBundleVersion) android:($androidVersionName, $androidVersionCode)"
  GITCOMMENT="($iosShortVersion, $iosBundleVersion)"

  echo "$INFO INFO - iosShortVersion=$iosShortVersion iosBundleVersion=$iosBundleVersion androidVersionCode=$androidVersionCode androidVersionName=$androidVersionName"
}
