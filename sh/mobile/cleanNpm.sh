cleanNpm() {
  rm package-lock.json;
  npm cache clean --force;
  npm prune;
}
