saveBuildFilesToDevFolder() {
  cp ios/GoogleService-Info.plist .buildFiles/development/GoogleService-Info.plist
  cp ios/"$IOS_APP_NAME"/Info.plist .buildFiles/development/Info.plist 
  cp android/app/google-services.json .buildFiles/development/google-services.json
  cp android/app/src/main/res/values/strings.xml .buildFiles/development/strings.xml
  cp ./env.js .buildFiles/development/env.js  
}
