buildMobile() {
  ask FILE_NAME '.build-prod.sh' '.build-staging.sh'

  exitOnEmptyVariable heavenEnvironment
  askIfAnyEmpty heavenEnvironment IOS_APP_NAME

  if [ "$heavenEnvironment" = 'production' ]; then {
    GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
    if [ "$GIT_BRANCH" != "master" && "$GIT_BRANCH" != "main" ]; then
      echo "$RED>Wrong git branch: $GIT_BRANCH"
      exit 0
    fi;
  }; fi;

  step "Lint and propose fix on error 👩‍💻"
  lintAndProposeFixOnError

  step "Update version 👩‍💻"
  getMobileBundleInfos
  # copyEnvFiles
  readNewBundleInfos
  replaceBundleInfos "$newBundleVersion" "$newShortVersion" "$newVersionCode" "$newVersionName"

  # Saving new Info.plist file
  cp ios/"$IOS_APP_NAME"/Info.plist .buildFiles/"$heavenEnvironment"/Info.plist 

  getMobileBundleInfos

  step "Git publish all 👩‍💻"
  gitPublishAll

  step "Mobile set up 👩‍💻"
  spinner yarnInstall
  # spinner reactNativeLink
  spinner reactNativeAsset
  spinner podInstall

  step "Run android release 👩‍💻"
  runAndroidRelease

  echo "$ACTION> PLEASE BUILD IOS STAGING APP WITH XCODE $NC"
  echo "$ACTION> Do not forget to deploy with codePush - CAREFUL WITH THE VERSION !$NC"

  openXCode
}
