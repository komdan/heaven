#!/bin/sh
launchEmulators() {
  export ANDROID_HOME=/Users/mml/Library/Android/sdk
  export PATH=$ANDROID_HOME/emulator:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$PATH

  ask launchIOSSimulator yes no && {
    askIfEmpty runningSimulatorName "${runningSimulatorName-:'iPhone 11'}"

    xcrun simctl boot "$runningSimulatorName" 2>/dev/null
    open -a Simulator
  }

  ask launchAndroidEmulator yes no && {
    emulator -avd $(emulator -list-avds) 2>/dev/null 1>/dev/null &
  }
}
