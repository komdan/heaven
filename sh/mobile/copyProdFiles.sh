copyProdFiles() {
  printf '%s\n' "$INFO Copying production files "

  cp .buildFiles/production/GoogleService-Info.plist ios/
  cp .buildFiles/production/Info.plist ios/"$IOS_APP_NAME"/
  cp .buildFiles/production/google-services.json android/app/
  cp .buildFiles/production/strings.xml android/app/src/main/res/values/
  cp .buildFiles/production/env.js ./env.js
}
