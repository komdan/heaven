findDuplicateFileNames() {
  ask directoryName $1 "./"
  ask excludedPatternsString "index.js"
  ask includedPatternsString ".js"

  excludedPatternsTmpFile="$directoryName/findDuplicateFileNames.sh_excludedPatternsTmpFile.tmp"
  includedPatternsTmpFile="$directoryName/findDuplicateFileNames.sh_includedPatternsTmpFile.tmp"
  createTemporaryFiles() {
    touch $excludedPatternsTmpFile
    for word in $(echo $excludedPatternsString); do {
      echo $word >> $excludedPatternsTmpFile
    }; done;
    touch $includedPatternsTmpFile
    for word in $(echo $includedPatternsString); do {
      echo $word >> $includedPatternsTmpFile
    }; done;
  }
  removeTemporaryFiles() {
    rm $excludedPatternsTmpFile
    rm $includedPatternsTmpFile
  }

  listAllFilesRecursively() {
    find $directoryName -type f
  }

  stringAfterLastSlash() {
    rev | cut -d'/' -f1 | rev
  }

  peekDuplicates() {
    sort | uniq -d
  }

  removeExcludedPatterns() {
    grep -v -f $excludedPatternsTmpFile
  }

  keepOnlyIncludedPatterns() {
    grep -f $includedPatternsTmpFile
  }

  createTemporaryFiles

  listAllFilesRecursively \
    | stringAfterLastSlash \
    | peekDuplicates \
    | removeExcludedPatterns \
    | keepOnlyIncludedPatterns

  removeTemporaryFiles
}
