runAndroidRelease() {
  # npx react-native run-android --variant=release
  cd android && chmod +x gradlew && ./gradlew app:bundleRelease app:assembleRelease && cd ..
}
