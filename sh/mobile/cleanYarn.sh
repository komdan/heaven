cleanYarn() {
  rm yarn.lock yarn-error.log;
  yarn cache clean;
}
