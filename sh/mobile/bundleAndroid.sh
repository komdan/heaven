bundleAndroid() {
  ask bundleDebugOrRelease
  if [ "$bundleDebugOrRelease" = "release" ];
    then {
      cd android && ./gradlew bundleRelease && cd ..
      # cd android && ./gradlew bundleReleaseJsAndAssets && cd ..
      # cd android && ./gradlew assembleRelease && cd ..
    };
    else {
      cd android && ./gradlew bundleDebug && cd ..
    };
  fi;
}
