openAndroidBuildFolder() {
  ask openDebugOrRelease
  if [ "$openDebugOrRelease" = "release" ];
    then {
      open android/app/build/outputs/apk/release
    };
    else {
      open android/app/build/outputs/apk/debug
    };
  fi;
}
