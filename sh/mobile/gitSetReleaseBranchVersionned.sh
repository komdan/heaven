gitSetReleaseBranchVersionned() {
  set -e # hard fail
  ask newVersionName $newVersionName
  git checkout master # clean git state to prevent future errors
  git checkout "v$newVersionName" || git checkout -b "v$newVersionName" # go to right branch OR creates it
  git pull origin master:"v$newVersionName"
  git push --set-upstream origin "v$newVersionName"
  git checkout master # return to master branch
  set +e # soft fail
}
