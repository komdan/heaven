replaceBundleInfos() {
  newBundleVersion=$1
  newShortVersion=$2
  newVersionCode=$3
  newVersionName=$4

  echo "$BLUE> iOS - Replace shortVersion by $newShortVersion$NC"
  plutil -replace CFBundleShortVersionString -string $newShortVersion ios/$IOS_APP_NAME/Info.plist

  echo "$BLUE> iOS - Replace bundleVersion by $newBundleVersion$NC"
  plutil -replace CFBundleVersion -string $newBundleVersion ios/$IOS_APP_NAME/Info.plist

  echo "$BLUE> Android - Replacing versionName in android/app/build.gradle by $newVersionName$NC";
  sed -i'.old' -e "s/\(.*\) versionName \"\(.*\)\".*/\1 versionName \"$newVersionName\"/g" android/app/build.gradle

  echo "$BLUE> Android - Replacing versionCode in android/app/build.gradle by $newVersionCode$NC";
  sed -i'.old' -e "s/\(.*\) versionCode \(.*\).*/\1 versionCode $newVersionCode/g" android/app/build.gradle

  rm android/app/build.gradle.old

  sed -i'.old' -e "s/const BUILD_VERSION = \(.*\).*/const BUILD_VERSION = '$newVersionName'/g" .buildFiles/development/env.js
  sed -i'.old' -e "s/const APP_VERSION = \(.*\).*/const APP_VERSION = '$newVersionCode'/g" .buildFiles/development/env.js
  rm .buildFiles/development/env.js.old

  sed -i'.old' -e "s/const BUILD_VERSION = \(.*\).*/const BUILD_VERSION = '$newVersionName'/g" .buildFiles/production/env.js
  sed -i'.old' -e "s/const APP_VERSION = \(.*\).*/const APP_VERSION = '$newVersionCode'/g" .buildFiles/production/env.js
  rm .buildFiles/production/env.js.old

  sed -i'.old' -e "s/const BUILD_VERSION = \(.*\).*/const BUILD_VERSION = '$newVersionName'/g" env.js
  sed -i'.old' -e "s/const APP_VERSION = \(.*\).*/const APP_VERSION = '$newVersionCode'/g" env.js
  rm env.js.old

  sed -i'.old' -e "s/app_version: \(.*\).*/app_version: '$newVersionName',/g" fastlane/Fastfile
  sed -i'.old' -e "s/build_number: \(.*\).*/build_number: '$newVersionCode',/g" fastlane/Fastfile
  rm fastlane/Fastfile.old
}
