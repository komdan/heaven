resetAdbServerCertificates() {
  adb kill-server
  rm $HOME/.android/adbkey $HOME/.android/adbkey.pub
  adb start-server
}
