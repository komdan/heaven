#!/bin/bash
# USAGE
# ./.scripts/misc/convertRGBHexToIndexedColorsForLottieAnimations.sh
#
# Sublime text regexp | Match colors in lottie JSON animation
# \[0\.[0-9]*,0\.[0-9]*,0\.[0-9]*,1]
# \[[0-1]\.[0-9]*,[0-1]\.[0-9]*,[0-1]\.[0-9]*,1]
# OR  \[[0-9\.]*,[0-9\.]*,[0-9\.]*,[0-9\.]*]

# Found color: komdab blue in EDIT_INFO_LOTTIE.json
# [0.2549019607843137,0.4549019607843137,0.7529411764705882,1]
# -> bnr yellow [0.867,0.671,0.271,1]
# -> bnr marroon [0.416,0.345,0.110,1]

round() {
  numberToRound=$1
  numberOfDigits=$2
  printf %.$2f $(echo "scale=$numberOfDigits;(((10^$numberOfDigits)*$numberToRound)+0.5)/(10^$numberOfDigits)" | bc)
};

convertHexToIndexedColor() {
  color=$1

  indexedColorNotFormatted=$(echo "scale=5; $((16#$color)) / $((16#FF))" | bc)
  round $indexedColorNotFormatted 3
}

convertRGBHexToIndexedColorsForLottieAnimations() {
  ask hexColor "#4476BE"
  R=${hexColor:1:2}
  G=${hexColor:3:2}
  B=${hexColor:5:2}

  iR=$(convertHexToIndexedColor $R)
  iG=$(convertHexToIndexedColor $G)
  iB=$(convertHexToIndexedColor $B)

  echo "[$iR,$iG,$iB,1]"
}
