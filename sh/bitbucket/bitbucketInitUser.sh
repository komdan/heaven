bitbucketInitUser() {
  if [ "$bitbucketUsername" = "" ]; then
    ask bitbucketUsername
  else
    echo "$INFO Using \$bitbucketUsername"
  fi;


  if [ "$bitbucketPassword" = "" ]; then
    secureAsk bitbucketPassword
  else
    echo "$INFO Using \$bitbucketPassword"
  fi;
}
