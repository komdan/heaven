listRepositories() {
  bitbucketInitUser

  # USAGE: listRepositories
  curlListRepositories () {
    curl --user $bitbucketUsername:$bitbucketPassword https://api.bitbucket.org/2.0/repositories/$bitbucketUsername 2>/dev/null | jq '.values[] | { name, projectKey: .project.key }'
    curl --user $bitbucketUsername:$bitbucketPassword "https://api.bitbucket.org/2.0/repositories/$bitbucketUsername?page=2" 2>/dev/null | jq '.values[] | { name, projectKey: .project.key }'
    curl --user $bitbucketUsername:$bitbucketPassword "https://api.bitbucket.org/2.0/repositories/$bitbucketUsername?page=3" 2>/dev/null | jq '.values[] | { name, projectKey: .project.key }'
    curl --user $bitbucketUsername:$bitbucketPassword "https://api.bitbucket.org/2.0/repositories/$bitbucketUsername?page=4" 2>/dev/null | jq '.values[] | { name, projectKey: .project.key }'
  }
  spinner curlListRepositories
}
