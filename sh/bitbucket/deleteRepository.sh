deleteRepository() {
  bitbucketInitUser
  # USAGE: deleteRepository komdabmobile
  repositoryName=$1

  echo "$INFO About to delete $repositoryName"
  ask confirmRepositoryDeletion no $repositoryName
  if [ "$confirmRepositoryDeletion" = "$repositoryName" ]; then {
    curlDeleteRepository() {
      curl -X DELETE \
        --user $bitbucketUsername:$bitbucketPassword \
        https://api.bitbucket.org/2.0/repositories/$bitbucketUsername/$repositoryName \
        2>/dev/null \
      | jq
    }
    spinner curlDeleteRepository
  }; fi;
}
