createRepository() {
  bitbucketInitUser
  # USAGE: createRepository komdabmobile
  repositoryName=$1
  curlCreateRepo() {
    curl --user $bitbucketUsername:$bitbucketPassword -X POST -H "Content-Type: application/json" -d '{
      "scm": "git",
      "project": {
          "key": "PROJ"
      },
      "is_private": true
    }' https://api.bitbucket.org/2.0/repositories/$bitbucketUsername/$repositoryName 2>/dev/null | jq
  }
  spinner curlCreateRepo
}
