remoteFetchEnvFiles() {
  askIfAnyEmpty VPS_PORT SERVICE_NAME VPS_HOSTNAME REMOTE_FOLDER_PATH

  scp -P $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME:$REMOTE_FOLDER_PATH/.env.development ./.env.development.remote
  scp -P $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME:$REMOTE_FOLDER_PATH/.env.development.local ./.env.development.local.remote
  scp -P $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME:$REMOTE_FOLDER_PATH/.env.production ./.env.production.remote
  scp -P $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME:$REMOTE_FOLDER_PATH/ubar-firebase-dev.service-account-file.json ./ubar-firebase-dev.service-account-file.json.remote
  scp -P $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME:$REMOTE_FOLDER_PATH/ubar-firebase-prod.service-account-file.json ./ubar-firebase-prod.service-account-file.json.remote
}
