remoteSynchronizeEnvFiles() {
  remoteFetchEnvFiles
  diffEnvFiles
  echo "$ACTION The diff should NOT display any '${ORANGE}>${RED}${NC}' !"
  ask continue
  echo "$ACTION ARE YOU SURE THAT THE REMOTE .ENV FILES DOES NOT CONTAIN NEW VALUES ? '${ORANGE}>${RED}' ! $NC"
  ask continue
  remoteDeployEnvFiles
  rmTempEnvFiles
}
