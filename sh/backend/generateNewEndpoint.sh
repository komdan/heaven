generateNewEndpoint() {
  endpointName="$1"
  askIfAnyEmpty endpointName

  basePath=./src/endpoints/$endpointName

  generateFolder() {
    mkdir "$basePath"
  }

  generateFunctionsFolder() {
    mkdir "$basePath/$endpointName.functions"
  }

  generateContantsFolder() {
    mkdir "$basePath/$endpointName.constants"
  }

  generateDbRequestsFolder() {
    mkdir "$basePath/$endpointName.dbRequests"
  }

  generateAssertFile() {
    echo "\
/* eslint-disable */
const {
  // assertNonEmptyString,
} = require('../../../heaven/js')

const ${endpointName}Assert = ({
  body: {
    paramToAssert = '',
  } = {},
} = {}) => {
  // assertNonEmptyString(paramToAssert, 'paramToAssert')
}

exports.default = ${endpointName}Assert\
" > "$basePath"/"$endpointName"Assert.js
  }

  generateDbRequestsFile() {
    echo "\
/* eslint-disable */
// const {
//   getCollection,
//   getDocument,
// } = require('../../../heaven/node')

const ${endpointName}DbRequests = async () => {
  return ({
  })
}

exports.default = ${endpointName}DbRequests\
" > "$basePath"/"$endpointName"DbRequests.js
  }

  generateExecuteFile() {
    echo "\
const ${endpointName}Execute = async ({
  body,
  dbResults,
} = {}) => {

  const responseToClient = {
    success: true,
  }

  const analyticsData = {
    endpointTriggered: '${endpointName}',
  }

  return ({
    responseToClient,
    analyticsData,
  })
}

exports.default = ${endpointName}Execute\
" > "$basePath"/"$endpointName"Execute.js
  }

  generateHandleFile() {
    echo "\
const {
  log,
  error,
} = require('../../../heaven/js')

const ${endpointName}Assert = require('./${endpointName}Assert').default
const ${endpointName}Verify = require('./${endpointName}Verify').default
const ${endpointName}DbRequests = require('./${endpointName}DbRequests').default
const ${endpointName}Execute = require('./${endpointName}Execute').default

const ${endpointName}Handle = async (req, res) => {
  try {
    log('${endpointName}Handle.js TRIGGERED')
    const {
      body,
    } = req

    ${endpointName}Assert({
      body,
    })

    const dbResults = await ${endpointName}DbRequests({
      body,
    })

    await ${endpointName}Verify({
      body,
      dbResults,
    })

    const {
      responseToClient,
    } = await ${endpointName}Execute({
      body,
      dbResults,
    })

    res.status(200).send(responseToClient)
  } catch (e) {
    error(e)
    const {
      message = '',
      response: {
        data = {},
      } = {},
    } = e || {}
    res.status(400).send({
      message,
      responseData: data,
    })
  }
}

exports.default = ${endpointName}Handle\
" > "$basePath"/"$endpointName"Handle.js
  }

  generateVerifyFile() {
    echo "\
/* eslint-disable */
// const {
//   equal,
// } = require('assert')

const ${endpointName}Verify = ({
  body,
  dbResults,
} = {}) => {
  // equal(BACKEND_SECRET_KEY === backendSecretKey, true, 'Wrong secret key provided')
}

exports.default = ${endpointName}Verify\
" > "$basePath"/"$endpointName"Verify.js
  } 

  generateIndexFile() {
    echo "const ${endpointName}Handle = require('./${endpointName}Handle').default

exports.${endpointName}Handle = ${endpointName}Handle\
" > "$basePath"/index.js
  }

  # set -e

  if [ -z "$endpointName" ]
  then
      echo "$ERRORICON Missing endpoint name"
  else
    generateFolder
    generateFunctionsFolder
    generateContantsFolder
    generateDbRequestsFolder
    generateAssertFile
    generateDbRequestsFile
    generateExecuteFile
    generateHandleFile
    generateVerifyFile
    generateIndexFile
    echo "[-] DONE"
  fi

  # set +e
}
