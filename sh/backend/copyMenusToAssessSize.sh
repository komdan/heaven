copyMenusToAssessSize() {
  askIfAnyEmpty folderPath
  #!/bin/sh
  # ./scripts/copyMenusToAssessSize.sh
  # Result goes in TEMP_FOLDER
  TEMP_FOLDER="./temp"

  rm -r $TEMP_FOLDER
  find ./backups/prod/bars/*/drinks -type d -maxdepth 0 | while read folderPath; do {
    cp -R $folderPath $TEMP_FOLDER
  }; done;
  find ./backups/prod/bars/*/foods -type d -maxdepth 0 | while read folderPath; do {
    cp -R $folderPath $TEMP_FOLDER
  }; done;
  find ./backups/prod/bars/*/menus -type d -maxdepth 0 | while read folderPath; do {
    cp -R $folderPath $TEMP_FOLDER
  }; done;
  du -hsc $TEMP_FOLDER
  # rm -r $TEMP_FOLDER
}
