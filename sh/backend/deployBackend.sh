deployBackend() {
  step "🖲 DB Checks"
  canIDeployBackend

  # step "Deploy config 👩‍💻" # All prompts that user will have after
  # ask shouldWeRunBackendLocally no yes
  # ask shouldWeLint yes no
  # ask autoFixLint yes no
  # ask wannaContinueWithErrorsAfterLintAndFixMyLove no yes
  # ask shouldWeSyncHeaven yes no
  # ask heavenCommitMessage "..."
  # ask commitMessage "..."
  # ask reinstallNodeModules no yes
  # ask shouldWeRestartRemoteService yes no
  # askIfEmpty bitbucketUserName "$SUGGESTED_BITBUCKET_USER"
  # secureAskIfEmpty bitbucketPassword

  step "Set git user and password 👩‍💻"
  gitAskUserAndPasswordIfEmpty

  step "Pre Deployment Test 👩‍💻"
  askAndRunBackendLocally

  step "Lint and propose fix on error 👩‍💻"
  lintAndProposeFixOnError

  step "Git Publish All 👩‍💻"
  gitPublishAll

  step "Build Backend 👩‍💻"
  :

  step "🌎 Synchronize .env.* files"
  remoteSynchronizeEnvFiles
  # spinner remoteFixPermissions
  remoteFixPermissions

  step "🌎 Remote Git Update Repository"
  remoteGitUpdateRepository

  step "🌎 Reinstall node modules"
  askIfEmpty reinstallNodeModules no yes && {
    remoteResetNodeModules
  }

  step "🌎 Remote Yarn Build"
  askIfEmpty shouldWeYarnBuildRemotely yes no && {
    remoteYarnBuild
  }

  step "🌎 Remote Restart Service"
  askIfEmpty shouldWeRestartRemoteService yes no && {
    remoteRestartService
  }

  step "🌎 Post deployment test"
  ask isSlackNotificationWellReceived yes no || {
    step "⚠ Debug $SERVICE_NAME service ⚠"
    remoteRootSSH
  }

  step "$SUCCESSICON Done"
}
