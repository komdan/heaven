runTestThenKillBackendLocally() {
  BACKEND_TEST_PORT=1330

  killProcessListeningToPort "$BACKEND_TEST_PORT" 2>/dev/null

  mkdir -p ./temp
  echo "$GTICON http://localhost:$BACKEND_TEST_PORT $NC"
  # exec './start.dev.local.sh' 1>./temp/backend.stdout.temp 2>./temp/backend.stderr.temp &
  exec './start.dev.local.sh' &
  backendPid=$!

  open -a "Google Chrome" "http://127.0.0.1:$BACKEND_TEST_PORT"
  sleep 10
  ask isLocalBackendRunning
  # curl http://127.0.0.1:$BACKEND_TEST_PORT
  kill $backendPid 2>/dev/null
  # ask okOkIsEverythingAlright

  killProcessListeningToPort "$BACKEND_TEST_PORT" 2>/dev/null
  sleep 1
}
