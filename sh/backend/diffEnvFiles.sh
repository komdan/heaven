diffEnvFiles() {
  askIfAnyEmpty ENV_FILE

  echo "$GTICON diff $ENV_FILE $ENV_FILE.remote"
  diff $ENV_FILE "$ENV_FILE".remote
}
