askAndRunBackendLocally() {
  ask shouldWeRunBackendLocally yes no && {
    yarn build
    runTestThenKillBackendLocally
  }
}
