canIDeployBackend() {
  echo "============================================================="
  echo " Will check executeAtInfos in database. Press Ctrl+C after..."
  echo "============================================================="
  echo
  echo "$(cat .env.$heavenEnvironment | tr '\n' ' ') npx babel-node scripts/dbOperations/getFutureExecuteAts.js" | /bin/bash
  ask continue yes no || exit
}
