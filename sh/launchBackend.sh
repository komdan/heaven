#!/bin/sh
source loadHeaven.sh
BACKEND_FOLDER_PATH=/Users/mml/Documents/01.Komdan/01.Ubar/01.Dev/ubar-backend
MANAGER_FOLDER_PATH=/Users/mml/Documents/01.Komdan/01.Ubar/01.Dev/komdab-manager
WEBAPP_FOLDER_PATH=/Users/mml/Documents/01.Komdan/01.Ubar/01.Dev/komdab-menu-webapp
IOS_APP_NAME=komdabmobile

killProcessListeningToPort 1330
cd $BACKEND_FOLDER_PATH
echo "$(cat .env.development.local | tr '\n' ' ')  npx babel-node start.js" | /bin/bash # ./scripts/run-devLocal.sh &
