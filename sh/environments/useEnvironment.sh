useEnvironmentScriptPath=$(cd "$(dirname "${BASH_SOURCE[0]:-$0}")" ; pwd -P)

useEnvironment() {
  heavenEnvironment=${1:-"$heavenEnvironment"}

  case "$heavenEnvironmentType" in
    "web") HEAVEN_SUBMODULE_PATH_GETBACK="../.." ;;
    "webSSR") HEAVEN_SUBMODULE_PATH_GETBACK=".." ;;
    "mobile") HEAVEN_SUBMODULE_PATH_GETBACK=".." ;;
    "backend") HEAVEN_SUBMODULE_PATH_GETBACK=".." ;;
  esac

  projectRootFolder="$heavenShBasePath/../$HEAVEN_SUBMODULE_PATH_GETBACK"

  if [ "$heavenEnvironmentType" = "mobile" ];
    then {
      copyEnvFiles
      # for mobile build, we replace 'localhost' to allow building on same WiFi real devices.
      # "127.0.0.1" will not be tampered with
      replaceInFile "localhost" $(lanip) 'env.js'
      envJsToDotEnvFile 'env.js' '.env.heaven'
      set -a
      source "$projectRootFolder/.env.heaven"
      set +a
    };
    else {
      set -a # export next variables : https://unix.stackexchange.com/questions/79064/how-to-export-variables-from-a-file
      source "$projectRootFolder/.env.$heavenEnvironment"
      set +a

      echo "$INFO 'source .env.$heavenEnvironment' executed."
    };
  fi;
}
