askEnvironment() {
  if ([ "$LOAD_SILENTLY" != true ]) || ([ "$FORCE_ENV" = "" ] || [ "$FORCE_ENV_TYPE" = "" ]);
    then {
      ask heavenEnvironment "" "development" "production" "development.local" "production.local"
      ask heavenEnvironmentType "" "backend" "web" "mobile"
    };
    else {
      heavenEnvironment="${FORCE_ENV:-"$heavenEnvironment"}"
      heavenEnvironmentType="${FORCE_ENV_TYPE:-"$heavenEnvironmentType"}"
    };
  fi;
}
