# https://stackoverflow.com/questions/4774054/reliable-way-for-a-bash-script-to-get-the-full-path-to-itself
HEAVEN_LOCAL_CACHE="$HOME/.heaven.localCache"
old_path=$(pwd -P)
heavenShBasePath=$(cd "$(dirname "${BASH_SOURCE[0]:-$0}")" ; pwd -P)

cd "$heavenShBasePath" || exit;
trap "
  cd $old_path;
  trap - INT;
  kill -2 $$;
" INT # trap insight : http://kb.mit.edu/confluence/pages/viewpage.action?pageId=3907156

sourceHeavenLocalCache() {
  if test -f "$HEAVEN_LOCAL_CACHE"; then
    source "$HEAVEN_LOCAL_CACHE"
  fi
}
sourceHeavenLocalCache

sourceHeavenModules() {
  heavenSourcedModules=()
  for module in $(find */*.sh); do {
    set -a # export next variables : https://unix.stackexchange.com/questions/79064/how-to-export-variables-from-a-file
    source "$module"
    heavenSourcedModules+=("$module")
    set +a
  }; done;
  for module in $(find */*/*.sh); do {
    set -a # export next variables : https://unix.stackexchange.com/questions/79064/how-to-export-variables-from-a-file
    source "$module"
    heavenSourcedModules+=("$module")
    set +a
  }; done;
}
sourceHeavenModules

cd "$old_path";

askEnvironment
useEnvironment
step "heaven/sh loaded $SUCCESSICON"

printf "
heavenEnvironment=%s
heavenEnvironmentType=%s
" $heavenEnvironment $heavenEnvironmentType > "$HEAVEN_LOCAL_CACHE"
# env > "$HEAVEN_LOCAL_CACHE"
