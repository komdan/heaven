exitOnEmptyVariable() {
  variableName=$1
  variableValue=$(getVariableValue $1)

  if [ "$variableValue" = "" ] || [ "$variableValue" = " " ]; then 
    echo "${RED}$(camelCaseToSentence $variableName) ($variableName) cannot be empty$NC"
    exit 1;
  fi
}
