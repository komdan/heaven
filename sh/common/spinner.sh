spinner() {
  _SPINNER_POS=0
  _TASK_OUTPUT=""
  CYAN=$(tput setaf 6)
  NORMAL=$(tput sgr0)
  BOLD=$(tput bold)

  stty -echo && tput civis

  FUNCTION_NAME="$1"
  DISPLAY_STRING=" $(camelCaseToSentence "$FUNCTION_NAME")"

  VARIABLE_NAME="${2:-}"

    _TASK_OUTPUT=""
    local delay=0.05
    local list=( $(printf '\xe2\xa0\x8b')
                 $(printf '\xe2\xa0\x99')
                 $(printf '\xe2\xa0\xb9')
                 $(printf '\xe2\xa0\xb8')
                 $(printf '\xe2\xa0\xbc')
                 $(printf '\xe2\xa0\xb4')
                 $(printf '\xe2\xa0\xa6')
                 $(printf '\xe2\xa0\xa7')
                 $(printf '\xe2\xa0\x87')
                 $(printf '\xe2\xa0\x8f'))
    local i=$_SPINNER_POS
    local tempfile
    tempfile=$(mktemp)
    errorfile=$(mktemp)

    eval $FUNCTION_NAME >> $tempfile 2>/dev/null &
    # eval $FUNCTION_NAME >> $tempfile 2>"$errorfile" &
    local pid=$!

    tput sc
    printf "%s %s" "${list[i]}" "$DISPLAY_STRING"
    tput el
    tput rc

    i=$(($i+1))
    i=$(($i%10))

    while [ "$(ps a | awk '{print $1}' | grep $pid)" ]; do
        printf "%s" "${CYAN}${list[i]}${NORMAL}"
        i=$(($i+1))
        i=$(($i%10))
        sleep $delay
        printf "\b\b\b"
    done

    _TASK_OUTPUT="$(cat -e $tempfile)"
    _SPINNER_POS=$i

    echo "🔥 $DISPLAY_STRING $NC"

    if [ -z $VARIABLE_NAME ]; then {
      echo "$CYAN $(cat $tempfile) $NC"
    }; else {
      eval $VARIABLE_NAME=\'"$_TASK_OUTPUT"\'
    };
    fi

    wait $pid
    resultingCode="$?"
    if [ "$resultingCode" = "0" ]; then
        grep -i "error" $tempfile > /dev/null
        if [ "$?" = "0" ]; then
          echo "${ORANGE}! error found$NC"
        else
          echo "${GREEN}✔$NC"
        fi;
    else
      echo "${ORANGE}$(cat "$errorfile")$NC"
      echo "${RED}x$NC"
    fi;
    echo

    rm -f $tempfile $errofile
  tput el
  stty sane
  return $resultingCode
}
