reloadHeaven() {
  reloadHeaven_oldPath=$(pwd -P)

  cd "$heavenShBasePath" || exit;
  FORCE_ENV_TYPE=
  FORCE_ENV=
  LOAD_SILENTLY=false
  source ./loadHeaven.sh
  cd "$reloadHeaven_oldPath"
}
