replaceLineInFile() {
  oldString="$1"
  newString="$2"
  filePath="$3"

  askIfAnyEmpty oldString newString filePath

  sed -i ".sedBackup" "/$oldString/s/.*/$newString/" "$filePath"
  rm "${filePath}.sedBackup"
}
