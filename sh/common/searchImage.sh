searchImage() {
  #!/bin/bash
  # USAGE: scripts/searchImage.sh virgin+mojito
  SEARCH_STRING=$1
  open /Applications/Firefox.app "https://www.google.com/search?as_st=y\
  &tbm=isch\
  &hl=en-GB\
  &as_q=$SEARCH_STRING\
  &as_epq=\
  &as_oq=\
  &as_eq=\
  &cr=\
  &as_sitesearch=\
  &safe=images\
  &tbs=itp:clipart,ic:specific,isc:white,iar:s"
}
