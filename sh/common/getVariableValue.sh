getVariableValue() {
  currentShell=$(getCurrentShell)
  # echo "$currentShell"
  if [ "$currentShell" = "/bin/zsh" ] || [ "$currentShell" = "zsh" ] || [ "$currentShell" = "-zsh" ];
    then {
      printf '%s' "${(P)1}"
    };
    else {
      printf '%s' "${!1}"
    };
  fi;
}

# USAGE: getVariableValue toto
# outputs the value of toto, as "$toto" would do
# useful when using getVariableValue $variableName, as "$!variableName" would do (but not in zsh)
