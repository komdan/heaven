exitOn() {
  variableName="$1"
  variableValue=$(getVariableValue $1)
  variableExitingValue="$2"

  if [ "$variableValue" = "$variableExitingValue" ]; then 
    echo "${RED}$(camelCaseToSentence $variableName) ($variableName) cannot be \"${variableExitingValue}\"$NC"
    exit 1;
  fi
}
