askIfAnyEmpty() {
  variables=("$1")

  for variable in $variables; do {
    askIfEmpty "$variable"
  }; done;
}
