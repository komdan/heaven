onSIGINT() {
  callback="$1"
  trap "
    $(declare -f $callback);
    $callback;
    trap - INT;
    kill -2 $$;
  " INT
}

# trap infos : http://kb.mit.edu/confluence/pages/viewpage.action?pageId=3907156
