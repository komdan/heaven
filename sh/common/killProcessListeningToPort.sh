# USAGE
# killProcessListeningToPort 1330

killProcessListeningToPort() {
  port="$1"

  for port in $(netstat -anv | grep $port | awk '{print $9}'); do {
    kill -9 $port;
  }; done
}
