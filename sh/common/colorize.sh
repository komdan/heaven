colorize() {
  string="$1"
  color="$2"
  defaultColor="$3"

  if [ "$color" = "" ]; then color="$ORANGE"; fi;
  if [ "$defaultColor" = "" ]; then defaultColor="$NC"; fi;

  # sed -e "s/\($string\)/$color\1$defaultColor/p"
  sed -e "s/\($string\)/$color\1$defaultColor/"
}
