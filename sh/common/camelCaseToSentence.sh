camelCaseToSentence() {
  string=$1
  lowerCaseString=$(echo "$string" | sed -e 's/\([A-Z]\)/ \1/g' | tr '[:upper:]' '[:lower:]')

  echo $lowerCaseString | awk '{print toupper(substr($0,0,1))tolower(substr($0,2))}'
}
