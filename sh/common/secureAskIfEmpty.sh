secureAskIfEmpty() {
  variableName=$1
  variableValue=$(getVariableValue $1)
  defaultValue=$2
  otherPossibilities=$3

  if [ "$variableValue" = "" ]; then {
    secureAsk $1 $2 $3
    # export $1
  }; fi;

  variableValue=$(getVariableValue $1)
  export $variableName=$variableValue
}
