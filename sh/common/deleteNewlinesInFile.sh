deleteNewlinesInFile() {
  filePath="$1"

  askIfAnyEmpty filePath

  sed -i ".sedBackup" '/^[[:space:]]*$/d' "$filePath"
  rm "${filePath}.sedBackup"
}
