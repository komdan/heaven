oldStep="heaven/sh"

step() {
  newStep=${1:-$oldStep}
  oldStep=$newStep

  echo ""
  clear

  echo "$REPO_NAME $GEARICONS $heavenEnvironment $GEARICONS $heavenEnvironmentType $GEARICONS $ORANGE $newStep $NC"
  # echo "$(basename $0) $GEARICONS $heavenEnvironment $GEARICONS $heavenEnvironmentType $GEARICONS $ORANGE $newStep $NC"
  # echo "$(basename $0) $GEARICONS $newStep $NC"
  echo ""

  export oldStep
}
