lintFolder() {
  lintTargetFolder=${1:-"$lintTargetFolder"}
  node node_modules/eslint/bin/eslint.js "$lintTargetFolder" --ext .js --rule 'no-unused-vars: off'
}
