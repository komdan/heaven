getCurrentShell() {
  ps -p $$ | tr '\n' ' ' | awk '{print $8}'
}
