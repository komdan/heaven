removeLineContaining() {
  searchedString="$1"
  filePath="$2"

  askIfAnyEmpty searchedString filePath

  sed -i ".sedBackup" "/$searchedString/d" "$filePath"
  rm "${filePath}.sedBackup"
}
