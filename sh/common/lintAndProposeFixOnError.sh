lintAndProposeFixOnError() {
  askIfEmpty shouldWeLint yes no
  if [ "$shouldWeLint" = "yes" ]; then {
    spinner lintFolder
    if [ "$?" != "0" ]; then {
      askIfEmpty autoFixLint yes no
      if [ "$autoFixLint" = "yes" ]; then { 
        spinner lintAndFixFolder
        if [ "$?" != "0" ]; then {
          askIfEmpty wannaContinueWithErrorsAfterLintAndFixMyLove
        }; fi;
      }; fi;
    }; fi;
  }; fi;
}
