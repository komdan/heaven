replaceInFile() {
  oldString="$1"
  newString="$2"
  filePath="$3"

  askIfAnyEmpty oldString newString filePath

  sed -i ".sedBackup" "s/$oldString/$newString/g" "$filePath"
  rm "${filePath}.sedBackup"
}
