ask() {
  variableName=$1
  variableValue=$(getVariableValue $1)
  # variableValue=${!1} # bash, sh
  # variableValue="${(@k)1}" # zsh - https://unix.stackexchange.com/questions/150039/bad-substitution-error-in-zsh-shell
  defaultValue=$2
  # otherPossibilities=$3
  otherPossibilities="${@:3}"

  printf %s "$QUESTION $(camelCaseToSentence $variableName)? ";
  if [ "$defaultValue" = "" ] && [ "$variableValue" != "" ]; then
    defaultValue="$variableValue";
  fi;
  if [ "$defaultValue" != "" ]; then
    printf %s "[default: $GREEN$defaultValue$NC] ";
  fi;

  if [ "$otherPossibilities" != "" ]; then
    printf %s "(other: $otherPossibilities) ";
  fi;

  read input;
  if [ "$input" = "" ]; then 
    input=$defaultValue
  fi;
  echo "$CYAN$input$NC"
  export $variableName=$input

  if [ "$input" = "no" ] || [ "$input" = "n" ];
    then {
      return 1;
    };
    else {
      return 0;
    };
  fi;
}
