askIfEmpty() {
  variableName="$1"
  variableValue=$(getVariableValue "$1")
  defaultValue="$2"
  otherPossibilities="$3"

  if [ "$variableValue" = "" ]; then {
    ask "$variableName" "$defaultValue" "$otherPossibilities"
    # export $1
  }; fi;

  variableValue=$(getVariableValue "$1")

  # echo "$variableName : $CYAN$variableValue$NC"
  export "$variableName"="$variableValue"
  if [ "$variableValue" = "no" ] || [ "$variableValue" = "n" ];
    then {
      return 1;
    };
    else {
      return 0;
    };
  fi;
}
