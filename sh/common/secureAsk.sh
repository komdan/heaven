secureAsk() {
  variableName=$1

  echo "🗝  $(camelCaseToSentence $variableName)? ";

  read -s input;
  export "$variableName"="$input"
}
