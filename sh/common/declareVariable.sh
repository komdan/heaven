# USAGE: declareVariable toto
# outputs "toto=123", "toto=" if $toto was empty

declareVariable() {
  currentShell=$(getCurrentShell)
  if [ "$currentShell" = "/bin/zsh" ];
    then {
      printf '%s=%s\n' "$1" "${(P)1}"
    };
    else {
      printf '%s=%s\n' "$1" "${!1}"
    };
  fi;
}
