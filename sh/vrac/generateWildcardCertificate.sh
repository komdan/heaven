generateWildcardCertificate() {
  service apache2 stop
  sudo certbot certonly --manual -d $EXTERNAL_URL --preferred-challenges=dns --server https://acme-v02.api.letsencrypt.org/directory --agree-tos
  service apache2 start
}
