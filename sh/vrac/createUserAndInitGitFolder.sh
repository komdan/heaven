createUserAndInitGitFolder() {
  useradd --create-home $SERVICE_NAME
  passwd $SERVICE_NAME
  cd /home/$SERVICE_NAME
  su $SERVICE_NAME <<EOSU
    git clone https://$BITBUCKET_USER@bitbucket.org/$BITBUCKET_REPO
    cd $BITBUCKET_FOLDER
    git submodule init
    git submodule update --init --recursive
EOSU
}
