addMmlSshKey() {
  su $SERVICE_NAME <<EOSU
  mkdir ~/.ssh 2>/dev/null
  echo "\
# MML macbook
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCoBMWQsLwA5oM/m12AHMzHghpdRId2hGTKzwEnKyNelVJMrAWHHa3RslucQf8/mBltK6lHwTSCImPNLZrn/eC0ovOfs2fjNT7L/Zf84gUjoVAFRVJO+scnnd0sQXNSdwTIjxi0JxcS5jVf2Jf2gTnRNtJlWVWFam8DRcCSh9HXLo09qksavQuhUA7FhBBZ2cG7i7PN9/LpDBRk4YeG1zf4IXm6mxO7PjaIS+LkK337St2JVMv1H+4GzehfJmdqe+oTOJyoksGtVKeQZZbaYYp2evspnlpnl9ng3BpMm9zeIBtOQysns1PYDZLEUN49z5W7ylF095tzqPiAX/LPA/iF+gtAIu+Kg6w6hi4AhNekAvECZ9hzrNe6e7SdpFAXngB9jsghPNIKS0AU0il1ZAgMlwWX0x6LDYT8uGRxMVg/pOgd+rfOMnmxiFoxOz/iOglkOrqWfXDlydfbmBm1HZ3TERzBKWXCPGNeWj4n8A3wOSyekdxkA7SuViO7LKA94NE= mml@cibmx23000003
" >>  ~/.ssh/authorized_keys

EOSU
}
