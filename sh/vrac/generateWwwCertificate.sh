generateWwwCertificate() {
  service apache2 stop
  sudo certbot certonly --standalone -d www.$EXTERNAL_URL
  service apache2 start
}
