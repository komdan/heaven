deleteLastLineOfFile() {
  file="$1"

  EXT=.sedBackup

  sed -i $EXT '$d' "$file";
  rm "$file"$EXT
}
