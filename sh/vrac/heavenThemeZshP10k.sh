heavenThemeZshP10k() {
  if [[ -f "$HOME/.p10k.zsh" ]]; then {
    # typeset -g POWERLEVEL9K_LEFT_PROMPT_ELEMENTS=(
    #   # os_icon               # os identifier
    #   dir                     # current directory
    #   vcs                     # git status
    #   prompt_char             # prompt symbol
    # )
    POWERLEVEL9K_LEFT_PROMPT_ELEMENTS+=(
      newline
      heavenEnvironment
      prompt_char
    )

    function prompt_heavenEnvironment() {
      # red=1 # green=2 # orange=3 white=9
      # p10k segment -f 3 -i '🕊' -t "$heavenEnvironment $heavenEnvironmentType $REPO_NAME $GTICON"
      p10k segment -f 3 -i '⚙'
      p10k segment -f 9 -t "$heavenEnvironment ⚙ $heavenEnvironmentType ⚙ $REPO_NAME"
      # echo "$GEARICON $heavenEnvironment $heavenEnvironmentType $REPO_NAME $GTICON"
    }
  }; fi;

  stty sane # delete if pbm
  clear
}
