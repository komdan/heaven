yarnHeavenZsh() {
  # Prefered line > packageJson."scripts"."heavenzsh"
  # LOAD_SILENTLY=true source src/@heaven/sh/loadHeaven.sh && LOAD_SILENTLY= yarnHeavenZsh
  # set +e; (echo 'source $HEAVEN_SUBMODULE_PATH/sh/loadHeaven.sh && heavenThemeZshP10k' >> $HOME/.zshrc); (zsh); (sed -i .sedBackup /loadHeaven.sh/d $HOME/.zshrc && rm $HOME/.zshrc.sedBackup); set -e

  set +e; # soft fail
  echo 'source $HEAVEN_SUBMODULE_PATH/sh/loadHeaven.sh && heavenThemeZshP10k' >> $HOME/.zshrc;
  {
    sleep 4
    sed -i .sedBackup /loadHeaven.sh/d $HOME/.zshrc && rm $HOME/.zshrc.sedBackup;
  } &
  zsh;
  set -e # hard fail

  # Standalone one liner below > packageJson."scripts"."heavenzsh"
  # set +e; (echo 'source heaven/sh/loadHeaven.sh && heavenThemeZshP10k' >> $HOME/.zshrc); (zsh); (sed -i .sedBackup /loadHeaven.sh/d $HOME/.zshrc && rm $HOME/.zshrc.sedBackup); set -e
}
