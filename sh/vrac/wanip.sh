wanip() {
  # Get external IP address
  # https://unix.stackexchange.com/questions/22615/how-can-i-get-my-external-ip-address-in-a-shell-script
  dig @resolver4.opendns.com myip.opendns.com +short
}
