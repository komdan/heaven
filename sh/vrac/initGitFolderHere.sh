initGitFolderHere() {
    git clone https://$BITBUCKET_USER@bitbucket.org/$BITBUCKET_REPO
    cd $BITBUCKET_FOLDER
    git submodule init
    git submodule update --init --recursive
}
