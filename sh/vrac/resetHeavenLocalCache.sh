HEAVEN_LOCAL_CACHE="$HOME/.heaven.localCache"

resetHeavenLocalCache() {
  if test -f "$HEAVEN_LOCAL_CACHE"; then
    rm "$HEAVEN_LOCAL_CACHE"
  fi
}
