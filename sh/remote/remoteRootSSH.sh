remoteRootSSH() {
  ssh -t -p $VPS_PORT root@$VPS_HOSTNAME "
    cd $REMOTE_FOLDER_PATH;
    exec \$SHELL # or just 'zsh' if problem appears
  "
}
