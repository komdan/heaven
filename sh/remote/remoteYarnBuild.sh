remoteYarnBuild() {
  ssh -p $VPS_PORT root@$VPS_HOSTNAME "
    cd $REMOTE_FOLDER_PATH
    yarn build
  "
}
