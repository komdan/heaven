remoteResetNodeModules() {
  ssh -p $VPS_PORT root@$VPS_HOSTNAME "
    cd $REMOTE_FOLDER_PATH
    $(declare -f resetNodeModules)
    resetNodeModules
  "
}
