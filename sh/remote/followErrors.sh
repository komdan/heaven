followErrors() {
  echo "$GTICON ssh -p $VPS_PORT root@$LOGGER_IP"
  echo "$GTICON $REMOTE_ERROR_LOG_PATH"
  
  ssh -p $VPS_PORT root@$LOGGER_IP "
    tail -f -n5000 $REMOTE_ERROR_LOG_PATH
  "
}
