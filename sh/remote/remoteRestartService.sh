remoteRestartService() {
  restartingService() {
    sleep 2 # if error, give a little time to abort
    ssh -p $VPS_PORT root@$VPS_HOSTNAME "
      cd $REMOTE_FOLDER_PATH || exit;

      service $SERVICE_NAME stop
      sleep 4
      service $SERVICE_NAME start
      sleep 8
      service $SERVICE_NAME status;
      if [ \"\$?\" = \"0\" ];
        then echo \"$SUCCESSICON Service restarted successfully 👌$NC\";
        else echo \"$ERRORICON Failure restarting service ✋$NC\";
      fi;
    "
  }
  spinner restartingService
}
