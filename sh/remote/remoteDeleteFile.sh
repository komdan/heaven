remoteDeleteFile() {
  fileToDelete=${1:-"$fileToDelete"}

  ssh -p $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME "
    rm -f \"$REMOTE_FOLDER_PATH\"/\"$fileToDelete\"
  "
}
