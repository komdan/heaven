remoteLoggerRootSSH() {
  ssh -t -p $VPS_PORT root@$LOGGER_IP "
    cd "$REMOTE_LOG_PATH"/..;
    exec \$SHELL # or just 'zsh' if problem appears
  "
}
