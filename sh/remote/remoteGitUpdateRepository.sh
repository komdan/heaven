remoteGitUpdateRepository() {
  remoteGitDeployPasswordFile
  ssh -p $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME "
    cd $REMOTE_FOLDER_PATH || exit;
    $(cat "$ENV_FILE")
    bitbucketPassword=$bitbucketPassword
    bitbucketUserName=$bitbucketUserName
    # echo $bitbucketUserName
    # echo $bitbucketPassword
    $(declare -f gitSetUser)
    $(declare -f gitFetchAndReset)

    gitSetUser
    gitFetchAndReset
  "
  remoteGitDeletePasswordFile
}
