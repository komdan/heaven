followLogs() {
  echo "$GTICON ssh -p $VPS_PORT root@$LOGGER_IP"
  echo "$GTICON $REMOTE_LOG_PATH"
  
  ssh -p $VPS_PORT root@$LOGGER_IP "
    tail -f -n3000 $REMOTE_LOG_PATH
  "
}
