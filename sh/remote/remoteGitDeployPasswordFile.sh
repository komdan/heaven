remoteGitDeployPasswordFile() {
  remoteDeployFile "$ASKPASS_FILE.sh"

  if [ "$HEAVEN_SUBMODULE_PATH" != "" ]; then {
    remoteDeployFile "$ASKPASS_FILE.sh" "$REMOTE_FOLDER_PATH/$HEAVEN_SUBMODULE_PATH"
  }; fi;
}
