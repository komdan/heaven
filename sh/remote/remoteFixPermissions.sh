remoteFixPermissions() {
  ssh -p $VPS_PORT root@$VPS_HOSTNAME "
    cd $REMOTE_FOLDER_PATH;
    chown -R $SERVICE_NAME:$SERVICE_NAME .
  "
}
