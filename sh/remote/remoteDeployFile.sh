remoteDeployFile() {
  fileToDeploy=${1:-"$fileToDeploy"}
  folderPath=${2:-"$REMOTE_FOLDER_PATH"}

  scp -P $VPS_PORT "$fileToDeploy" $SERVICE_NAME@$VPS_HOSTNAME:$folderPath/
}
