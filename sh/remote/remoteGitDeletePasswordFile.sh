remoteGitDeletePasswordFile() {
  remoteDeleteFile "$ASKPASS_FILE.sh"

  if [ "$HEAVEN_SUBMODULE_PATH" != "" ]; then {
    remoteDeleteFile "$HEAVEN_SUBMODULE_PATH/$ASKPASS_FILE.sh"
  }; fi;
}
