buildSSRWeb() {
  replaceEnvProductionFile
  onSIGINT restoreEnvProductionFile

  rm -rf build/*
  yarn install
  yarn buildProd

  restoreEnvProductionFile
}
