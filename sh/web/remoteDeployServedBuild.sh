remoteDeployServedBuild() {
  echo "$BLUE> Server: Copying all build files except meta.json which triggers the app reload$NC"
  rsync -av -e "ssh -p $VPS_PORT" --exclude='meta.json' build/* $SERVICE_NAME@$VPS_HOSTNAME:/home/$SERVICE_NAME/$REPO_NAME/servedBuild/
}
