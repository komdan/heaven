deploySSRWeb() {
  step "Set git user and password 👩‍💻"
  gitAskUserAndPasswordIfEmpty

  step "Pre Deployment Test 👩‍💻"
  :

  step "Lint and propose fix on error 👩‍💻"
  lintAndProposeFixOnError

  step "Git Publish All 👩‍💻"
  gitPublishAll

  step "Build Web 👩‍💻"
  buildSSRWeb

  step "🌎 Synchronize .env.* files"
  :
  :

  step "🌎 Remote Git Update Repository"
  remoteGitUpdateRepository

  step "🌎 Deploy build"
  remoteDeleteBuild
  remoteDeployBuild

  step "🌎 Reinstall node modules"
  askIfEmpty reinstallNodeModules no yes && {
    remoteResetNodeModules
  }

  step "🌎 Remote Restart Service"
  ask shouldWeRestartRemoteService yes no && {
    remoteRestartService
  }

  step "🌎 Post deployment test"
  step "$SUCCESSICON Done"
}
