buildWeb() {
  replaceEnvProductionFile
  onSIGINT restoreEnvProductionFile

  rm -rf build
  yarn install
  yarn build
  rm build/static/*/*.map

  restoreEnvProductionFile
}
