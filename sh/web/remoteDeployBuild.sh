remoteDeployBuild() {
  echo "$BLUE> Server: Copying all build files$NC"
  rsync -av -e "ssh -p $VPS_PORT" build/* $SERVICE_NAME@$VPS_HOSTNAME:/home/$SERVICE_NAME/$REPO_NAME/build/
}
