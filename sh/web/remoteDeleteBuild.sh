remoteDeleteBuild() {
  ask enterToDeleteRemoteBuild
  ask youSureTheBuildWasSuccessfulLocally
  ask ifItDidNotBuildLocallyYouWillCrashTheAppForEveryone
  ask itHasAlreadyHappenTwice
  ask thisIsTheLastWarningBeforeDeletingRemoteBuild
  ssh -p $VPS_PORT $SERVICE_NAME@$VPS_HOSTNAME "
    rm -rf /home/$SERVICE_NAME/$REPO_NAME/build/*
  "
}
