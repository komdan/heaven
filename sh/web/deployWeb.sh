deployWeb() {
  step "Set git user and password 👩‍💻"
  gitAskUserAndPasswordIfEmpty

  step "Pre Deployment Test 👩‍💻"
  :

  step "Lint and propose fix on error 👩‍💻"
  lintAndProposeFixOnError

  step "Git Publish All 👩‍💻"
  gitPublishAll

  step "Build Web 👩‍💻"
  buildWeb

  step "🌎 Synchronize .env.* files"
  :
  :

  step "🌎 Remote Git Update Repository"
  remoteGitUpdateRepository

  step "🌎 Deploy build"
  remoteDeleteServedBuild
  remoteDeployServedBuild

  step "🌎 Remote Restart Service"
  ask shouldWeRestartRemoteService yes no && {
    remoteRestartService
    remoteDeployMetaJson
  }

  step "🌎 Post deployment test"
  step "$SUCCESSICON Done"
}
