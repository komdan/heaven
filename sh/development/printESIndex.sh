printESIndex() {
  MAXDEPTH="$1"
  if [ "$MAXDEPTH" = "" ]; then
    MAXDEPTH=99999999;
  fi;

  deleteJSExtension() {
    sed -e 's/\.js//g'
  }

  lastElementAfterSlash() {
    rev | cut -d'/' -f1 | rev
  }

  fileName() {
    filePath="$1"

    printf '%s\n' "$filePath" | lastElementAfterSlash
  }

  # CORE
  generateImports() {
    filePaths="$1"

    for filePath in $(printf '%s' "$filePaths");
    do {
      name=$(fileName "$filePath")
      if [ "$name" != "index" ]; then
        printf '%s\n' "import $name from '$filePath'";
      fi;
    };
    done;
  }

  generateExports() {
    filePaths=$1

    printf '%s\n' ''
    printf '%s\n' "export {"
    for filePath in $(printf '%s' "$filePaths");
    do {
      name=$(fileName "$filePath")
      if [ "$name" != "index" ]; then
        printf '%s\n' "  $name,";
      fi;
    };
    done;
    printf '%s\n' "}"
  }

  filePaths=$(find . -name "*.js" -maxdepth $MAXDEPTH -type f | deleteJSExtension)

  generateImports "$filePaths"
  generateExports "$filePaths"
}
