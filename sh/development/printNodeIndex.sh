printNodeIndex() {
  MAXDEPTH="$1"
  if [ "$MAXDEPTH" = "" ]; then
    MAXDEPTH=99999999;
  fi;
  # HELPERS
  deleteJSExtension() {
    sed -e 's/\.js//g'
  }

  lastElementAfterSlash() {
    rev | cut -d'/' -f1 | rev
  }

  fileName() {
    filePath="$1"

    printf '%s\n' "$filePath" | lastElementAfterSlash
  }

  # CORE
  generateImports() {
    filePaths="$1"

    for filePath in $(printf '%s' "$filePaths");
    do {
      name=$(fileName "$filePath")
      if [ "$name" != "index" ]; then
        printf '%s\n' "const $name = require('$filePath').default";
      fi;
    };
    done;
  }

  generateExports() {
    filePaths="$1"

    printf '%s\n' ''
    for filePath in $(printf '%s' "$filePaths");
    do {
      name=$(fileName "$filePath")
      if [ "$name" != "index" ]; then
        printf '%s\n' "exports.$name = $name";
      fi;
    };
    done;
  }

  filePaths=$(find . -name "*.js" -maxdepth $MAXDEPTH -type f | deleteJSExtension)

  generateImports "$filePaths"
  generateExports "$filePaths"
}
