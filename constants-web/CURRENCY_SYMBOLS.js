// Supported stripe currencies
// https://stripe.com/docs/currencies#presentment-currencies
export default {
  USD: '$',
  AED: 'AED',
  AFN: '؋',
  ALL: 'Lek',
  AMD: 'AMD',
  ANG: 'ƒ',
  AOA: 'AOA',
  ARS: '$',
  AUD: '$',
  AWG: 'ƒ',
  AZN: '₼',
  BAM: 'KM',
  BBD: '$',
  BDT: 'BDT',
  BGN: 'лв',
  BIF: 'BIF',
  BMD: '$',
  BND: '$',
  BOB: '$b',
  BRL: 'R$',
  BSD: '$',
  BWP: 'P',
  BZD: 'BZ$',
  CAD: '$',
  CDF: 'CDF',
  CHF: 'CHF',
  CLP: '$',
  CNY: '¥',
  COP: '$',
  CRC: '₡',
  CVE: 'CVE',
  CZK: 'Kč',
  DJF: 'DJF',
  DKK: 'kr',
  DOP: 'RD$',
  DZD: 'DZD',
  EGP: '£',
  ETB: 'ETB',
  EUR: '€',
  FJD: '$',
  FKP: '£',
  GBP: '£',
  GEL: 'GEL',
  GIP: '£',
  GMD: 'GMD',
  GNF: 'GNF',
  GTQ: 'Q',
  GYD: '$',
  HKD: '$',
  HNL: 'L',
  HRK: 'kn',
  HTG: 'HTG',
  HUF: 'Ft',
  IDR: 'Rp',
  ILS: '₪',
  INR: 'INR',
  ISK: 'kr',
  JMD: 'J$',
  JPY: '¥',
  KES: 'KES',
  KGS: 'лв',
  KHR: '៛',
  KMF: 'KMF',
  KRW: '₩',
  KYD: '$',
  KZT: 'лв',
  LAK: '₭',
  LBP: '£',
  LKR: '₨',
  LRD: '$',
  LSL: 'LSL',
  MAD: 'MAD',
  MDL: 'MDL',
  MGA: 'MGA',
  MKD: 'ден',
  MMK: 'MMK',
  MNT: '₮',
  MOP: 'MOP',
  MRO: 'MRO',
  MUR: '₨',
  MVR: 'MVR',
  MWK: 'MWK',
  MXN: '$',
  MYR: 'RM',
  MZN: 'MT',
  NAD: '$',
  NGN: '₦',
  NIO: 'C$',
  NOK: 'kr',
  NPR: '₨',
  NZD: '$',
  PAB: 'B/.',
  PEN: 'S/.',
  PGK: 'PGK',
  PHP: '₱',
  PKR: '₨',
  PLN: 'zł',
  PYG: 'Gs',
  QAR: '﷼',
  RON: 'lei',
  RSD: 'Дин.',
  RUB: '₽',
  RWF: 'RWF',
  SAR: '﷼',
  SBD: '$',
  SCR: '₨',
  SEK: 'kr',
  SGD: '$',
  SHP: '£',
  SLL: 'SLL',
  SOS: 'S',
  SRD: '$',
  STD: 'STD',
  SZL: 'SZL',
  THB: '฿',
  TJS: 'TJS',
  TOP: 'TOP',
  TRY: 'TRY',
  TTD: 'TT$',
  TWD: 'NT$',
  TZS: 'TZS',
  UAH: '₴',
  UGX: 'UGX',
  UYU: '$U',
  UZS: 'лв',
  VND: '₫',
  VUV: 'VUV',
  WST: 'WST',
  XAF: 'XAF',
  XCD: '$',
  XOF: 'XOF',
  XPF: 'XPF',
  YER: '﷼',
  ZAR: 'R',
  ZMW: 'ZMW',
}
