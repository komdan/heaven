import firebase from '../firebase'

function setDocument(path, docId, document) {
  return (
    firebase.firestore()
      .collection(`${path}`)
      .doc(`${docId}`)
      .set(document || {}, { merge: true })
  )
}

export default setDocument
