import firebase from '../firebase'

const getCollectionMultipleWhereListener = (
  path,
  conditions = [], // [{field, operator, value}]
  callBack,
) => {
  let query = firebase.firestore().collection(path)
  conditions.forEach(({ field, operator, value }) => {
    query = query.where(field, operator, value)
  })

  return query
    .onSnapshot(
      snapshot => callBack(snapshot.docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
      }))),
      e => console.error(e),
    )
}

export default getCollectionMultipleWhereListener
