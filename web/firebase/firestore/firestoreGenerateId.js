import firebase from '../firebase'

const firestoreGenerateId = path => firebase.firestore()
  .collection(path)
  .doc()
  .id

export default firestoreGenerateId
