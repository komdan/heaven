import {
  useEffect,
  useState,
} from 'react'
import {
  reject,
  find,
  flattenDeep,
  isEmpty,
} from 'lodash'
import useListenFirestoreCollection from './useListenFirestoreCollection'
import getCollectionListener from './getCollectionListener'

const clearAllListeners = listeners => listeners.forEach((listener) => {
  listener()
})

const useListenFirestoreNestedCollection = (collectionPath, l1CollectionNames) => {
  const [populatedCollection, setPopulatedCollection] = useState([])
  const [isPopulatedCollectionLoaded, setisPopulatedCollectionLoaded] = useState(false)

  const [
    collection,
    isCollectionLoaded,
  ] = useListenFirestoreCollection(collectionPath)

  useEffect(() => {
    if (isCollectionLoaded) {
      let currentPopulatedCollection = collection
      const listeners = flattenDeep(
        collection.map(({ id }) => l1CollectionNames.map(l1CollectionName => getCollectionListener(
          `${collectionPath}/${id}/${l1CollectionName}`, // exemple: /bars/.../menus/afMpIUCowuw44o5KojBV/items
          (newL1Collection) => {
            currentPopulatedCollection = [
              ...reject(currentPopulatedCollection, { id }),
              {
                ...find(currentPopulatedCollection, { id }),
                [l1CollectionName]: newL1Collection,
              },
            ]
            setPopulatedCollection(currentPopulatedCollection)
          },
        ))),
      )
      return () => clearAllListeners(listeners)
    }
    return () => { }
  }, [isCollectionLoaded, collection, collectionPath]) // eslint-disable-line

  useEffect(() => {
    const areCollectionFetched = !isEmpty(l1CollectionNames) && !isEmpty(populatedCollection) && flattenDeep(
      l1CollectionNames.map(l1CollectionName => (
        populatedCollection.map(collectionItems => collectionItems[l1CollectionName] !== undefined)
      )),
    ).reduce((accu, value) => accu && value, true)
    // console.log('heaven-useListenFirestoreNestedCollection/areCollectionFetched', areCollectionFetched)

    setisPopulatedCollectionLoaded(areCollectionFetched)
  }, [populatedCollection, collectionPath]) // eslint-disable-line

  return [
    populatedCollection,
    isPopulatedCollectionLoaded,
  ]
}

export default useListenFirestoreNestedCollection
