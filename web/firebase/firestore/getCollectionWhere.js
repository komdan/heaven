import firebase from '../firebase'

import withOnUpdateListener from './withOnUpdateListener'

function getCollectionWhere(path, field, operator, value) {
  // Will putt data here
  const data = []

  // return the firestore promise
  return (
    firebase.firestore()
      .collection(path)
      .where(field, operator, value)
      .get()
      .then((snapshot) => {
        if (snapshot == null) {
          return []
        }
        snapshot
          .docs
          .forEach((doc) => {
            // Add the id, on each object, easier for referencing
            data.push(withOnUpdateListener({
              ...doc.data(),
              id: doc.id,
              path,
            }))
          })
        return data
      })
  )
}
export default getCollectionWhere
