import getFirestoreDocument from './getFirestoreDocument'

const getDocument = getFirestoreDocument

export default getDocument
