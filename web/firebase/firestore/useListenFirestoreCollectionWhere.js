import {
  useEffect,
  useState,
} from 'react'
import getCollectionWhereListener from './getCollectionWhereListener'

const useListenFirestoreCollectionWhere = (collectionPath, field, operator, value) => {
  const [collection, setCollection] = useState([])
  const [isCollectionLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    const listener = getCollectionWhereListener(
      collectionPath,
      field,
      operator,
      value,
      (newCollection) => {
        setCollection(newCollection)
      },
    )
    return () => listener()
  }, [collectionPath, field, operator, value])

  useEffect(() => {
    setIsLoaded(!!collection.length)
  }, [collection])

  return [
    collection,
    isCollectionLoaded,
  ]
}

export default useListenFirestoreCollectionWhere
