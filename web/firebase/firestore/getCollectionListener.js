import firebase from '../firebase'

function getCollectionListener(path, callBack) {
  return (
    firebase.firestore()
      .collection(path)
      .onSnapshot(
        snapshot => callBack(snapshot.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        }))),
        e => console.error(e),
      )
  )
}

export default getCollectionListener
