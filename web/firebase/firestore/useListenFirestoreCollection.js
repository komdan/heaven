import {
  useEffect,
  useState,
} from 'react'
import getCollectionListener from './getCollectionListener'

const useListenFirestoreCollection = (collectionPath) => {
  const [collection, setCollection] = useState([])
  const [isCollectionLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    const listener = getCollectionListener(collectionPath, (newCollection) => {
      setCollection(newCollection)
    })
    return () => listener()
  }, [collectionPath])

  useEffect(() => {
    setIsLoaded(!!collection.length)
  }, [collection])

  return [
    collection,
    isCollectionLoaded,
  ]
}

export default useListenFirestoreCollection
