import {
  useEffect,
  useState,
} from 'react'
import {
  log,
} from '@heaven/js-web'
import getCollectionMultipleWhereListener from './getCollectionMultipleWhereListener'

const useListenFirestoreCollectionMultipleWhere = (
  collectionPath,
  conditions = [],
) => {
  const [collection, setCollection] = useState([])
  const [isCollectionLoaded, setIsLoaded] = useState(false)

  useEffect(() => {
    log('useListenFirestoreCollectionMultipleWhere.js\\useEffect [collectionPath, conditions]')
    const listener = getCollectionMultipleWhereListener(
      collectionPath,
      conditions,
      (newCollection) => {
        setCollection(newCollection)
      },
    )
    return () => listener()
  }, [collectionPath, JSON.stringify(conditions)])

  useEffect(() => {
    log('useListenFirestoreCollectionMultipleWhere.js\\useEffect [collection]')
    setIsLoaded(!!collection.length)
  }, [collection])

  return [
    collection,
    isCollectionLoaded,
  ]
}

export default useListenFirestoreCollectionMultipleWhere
