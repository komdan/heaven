import firebase from '../firebase'

function getDocumentListener(
  path,
  docId,
  callBack,
  onError = () => { },
) {
  return (
    firebase.firestore()
      .collection(path)
      .doc(docId)
      .onSnapshot((doc) => {
        if (doc.exists) {
          callBack({ id: doc.id, path, ...doc.data() })
        }
      }, e => onError(e))
  )
}

export default getDocumentListener
