import getDocumentListener from './getDocumentListener'

const withOnUpdateListener = (doc) => {
  if (!doc) return doc
  if (doc.onUpdate) return doc
  const onUpdate = ((callBack) => {
    let callCounter = 0
    return getDocumentListener(
      doc.path,
      doc.id,
      newDoc => (callCounter++ !== 0) && callBack(newDoc),
    )
  })
  return ({
    ...doc,
    onUpdate,
  })
}

export default withOnUpdateListener
