import firebase from '../firebase'

function getCollectionWhereListener(path, field, operator, value, callBack) {
  return (
    firebase.firestore()
      .collection(path)
      .where(field, operator, value)
      .onSnapshot(
        snapshot => callBack(snapshot.docs.map(doc => ({
          ...doc.data(),
          id: doc.id,
        }))),
        e => console.error(e),
      )
  )
}

export default getCollectionWhereListener
