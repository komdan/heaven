import {
  useState,
  useContext,
  useEffect,
} from 'react'

import EventEmitter from 'EventEmitter'

import {
  error,
  formatCreditCardComponentValues,
  creditCardToPaymentMethod,
} from '@heaven/js-web'
// import {
//   Alert,
// } from '@heaven/web'
import {
  AuthContext,
} from '@contexts'
import {
  useTranslation,
} from 'react-multi-lang'

const event = new EventEmitter()

const useCreditCardModal = ({
  saveCreditCardInformation,
}) => {
  const {
    user: {
      id: userId,
      anonCreditCardInformation: {
        last4 = '',
      } = {},
    },
  } = useContext(AuthContext)
  const t = useTranslation()

  const [creditCardInformation, setCreditCardInformation] = useState({
    values: {
      cvc: '',
      expiry: '',
      name: '',
      number: '',
    },
  })
  const [isCreditCardModalVisible, setisCreditCardModalVisible] = useState(false)
  const [isCreditCardImprintModalVisible, setisCreditCardImprintModalVisible] = useState(false)
  const [creditCardModalError, setcreditCardModalError] = useState('')
  const [rememberCard, setRememberCard] = useState(false)

  // CHANGED BY FRANCOIS
  /* web version */
  const [number, setNumber] = useState('')
  const [expiry, setExpiry] = useState('')
  const [cvc, setCvc] = useState('')

  const creditCardComponentValues = {
    values: {
      number,
      expiry: expiry.replace(/ /g, ''),
      cvc,
      type: 'card',
    },
  }

  useEffect(
    () => {
      setCreditCardInformation(formatCreditCardComponentValues(creditCardComponentValues))
    },
    [number, expiry, cvc],
  )

  // END CHANGE I GUESS ?

  const showCreditCardModal = () => {
    setTimeout(() => setisCreditCardModalVisible(true), 600)
  }
  const onCreditCardModalError = setcreditCardModalError
  const onCreditCardModalValidatePress = async () => {
    try {
      const paymentMethod = await creditCardToPaymentMethod({
        card: creditCardInformation,
      })
      event.emit('credit-card-payment-method', paymentMethod)
      setisCreditCardModalVisible(false)
    } catch (e) {
      error(e)
      setcreditCardModalError(e.response?.data?.message || e.message)
    }
    try {
      if (rememberCard) {
        await saveCreditCardInformation({
          creditCardInformation,
          userId,
        })
      }
    } catch (e) {
      error(e)
      error(new Error('useCreditCardModal.js\\error-follows-in-saveCreditCardInformation'))
    }
  }
  const onCreditCardModalClosePress = () => {
    setisCreditCardModalVisible(false)
    event.emit('credit-card-modal-closed', true)
  }
  const onCreditCardValueChange = (newCreditCardComponentValues) => {
    setcreditCardModalError()
    setCreditCardInformation(formatCreditCardComponentValues(newCreditCardComponentValues))
  }
  const onRememberCardChange = (value) => {
    setRememberCard(value)
  }

  const payWithCreditCard = () => new Promise((resolve) => {
    event.off('credit-card-payment-method')
    event.off('credit-card-modal-closed')
    event.on('credit-card-payment-method', paymentMethod => resolve(paymentMethod))
    // CHANGED BY FRANCOIS
    // event.on('credit-card-modal-closed', closed => closed && reject(new Error('credit-card-modal-closed')))
    showCreditCardModal()
  })

  // const payOnSiteSavingCard = ({
  //   payWithSavedCard,
  //   amount,
  //   currencyCode,
  //   user,
  // }) => new Promise((resolve, reject) => {
  //   Alert.alert(
  //     t('Votre empreinte de carte'),
  //     t(`En cas d'oubli de votre moyen de paiement, nous préleverons le montant de la commande sur votre carte bleue${last4 ? ` ••••-${last4}` : ''}. Selon votre banque, cette caution peut entraîner un débit. Il sera remboursé sous 24h.`),
  //     [
  //       {
  //         text: t('Cancel'),
  //         style: 'destructive',
  //         onPress: () => reject(),
  //       },
  //       {
  //         text: last4 ? t('Confirmer ma commande') : t('Renseigner ma CB'),
  //         onPress: last4
  //           ? async () => {
  //             const paymentMethod = await payWithSavedCard({ amount, currencyCode, user, t, captureMethod: 'manual' })
  //             resolve({ ...paymentMethod, captureMethod: 'manual' })
  //           }
  //           : async () => {
  //             const paymentMethod = await payWithCreditCard()
  //             resolve({ ...paymentMethod, captureMethod: 'manual' })
  //           },
  //       },
  //     ],
  //   )
  // })

  const [creditCardImprintModalOk, setCreditCardImprintModalOkAction] = useState(() => () => {})

  const payOnSiteSavingCard = ({
    payWithSavedCard,
    amount,
    currencyCode,
    user,
  }) => new Promise((resolve) => {
    setisCreditCardImprintModalVisible(true)
    setCreditCardImprintModalOkAction(
      last4
        ? () => async () => payOnSiteWithSavedCard({
          payWithSavedCard,
          amount,
          currencyCode,
          user,
          resolve,
        })
        : () => async () => payOnSiteAskCardInfos({
          resolve,
        }),
    )
  })

  const payOnSiteAskCardInfos = async ({ resolve }) => {
    setisCreditCardImprintModalVisible(false)
    setCreditCardImprintModalOkAction(
      () => () => {},
    )
    const paymentMethod = await payWithCreditCard()
    resolve({ ...paymentMethod, captureMethod: 'manual' })
  }

  const payOnSiteWithSavedCard = async ({
    payWithSavedCard,
    amount,
    currencyCode,
    user,
    resolve,
  }) => {
    setisCreditCardImprintModalVisible(false)
    setCreditCardImprintModalOkAction(
      () => () => {},
    )
    const paymentMethod = await payWithSavedCard({ amount, currencyCode, user, t, captureMethod: 'manual' })
    resolve({ ...paymentMethod, captureMethod: 'manual' })
  }

  return ({
    payWithCreditCard,
    payOnSiteSavingCard,
    isCreditCardModalVisible,
    onCreditCardModalClosePress,
    onCreditCardModalValidatePress,
    creditCardModalError,
    showCreditCardModal,
    onCreditCardModalError,
    creditCardInformation,
    onCreditCardValueChange,
    onRememberCardChange,
    isCreditCardImprintModalVisible,
    setisCreditCardImprintModalVisible,
    creditCardImprintModalOk,
    setCreditCardImprintModalOkAction,
    rememberCard,
    number,
    setNumber,
    cvc,
    setCvc,
    expiry,
    setExpiry,
    last4,
  })
}

export default useCreditCardModal
