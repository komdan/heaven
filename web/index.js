import getLocation from './location/getLocation'
import payWithGooglePay from './payment/google-pay/payWithGooglePay'
import isGooglePaySupported from './payment/google-pay/isGooglePaySupported'
import payWithApplePay from './payment/apple-pay/payWithApplePay'
import isApplePaySupported from './payment/apple-pay/isApplePaySupported'
import isApplePayConfigured from './payment/apple-pay/isApplePayConfigured'
import openApplePaySetup from './payment/apple-pay/openApplePaySetup'
import confirmPaymentIntent from './payment/tipsi-stripe/confirmPaymentIntent'
import localCache from './storage/localCache'
import setDocument from './firebase/firestore/setDocument'
import getDocumentListener from './firebase/firestore/getDocumentListener'
import getDocument from './firebase/firestore/getDocument'
import getCollection from './firebase/firestore/getCollection'
import getCollectionWhere from './firebase/firestore/getCollectionWhere'
import useListenFirestoreCollectionWhere from './firebase/firestore/useListenFirestoreCollectionWhere'
import useListenFirestoreNestedCollection from './firebase/firestore/useListenFirestoreNestedCollection'
import getCollectionListener from './firebase/firestore/getCollectionListener'
import firestoreTimestampToDate from './firebase/firestore/firestoreTimestampToDate'
import getCollectionWhereListener from './firebase/firestore/getCollectionWhereListener'
import useListenFirestoreCollection from './firebase/firestore/useListenFirestoreCollection'
import useListenFirestoreCollectionMultipleWhere from './firebase/firestore/useListenFirestoreCollectionMultipleWhere'
import getFirestoreCollection from './firebase/firestore/getFirestoreCollection'
import getCollectionMultipleWhereListener from './firebase/firestore/getCollectionMultipleWhereListener'
import withOnUpdateListener from './firebase/firestore/withOnUpdateListener'
import getFirestoreDocument from './firebase/firestore/getFirestoreDocument'
import firestoreGenerateId from './firebase/firestore/firestoreGenerateId'
import getPushToken from './firebase/messaging/getPushToken'
import firebase from './firebase/firebase'
import useCreditCardModal from './hooks/useCreditCardModal'
import showFlashMessage from './notifications/showFlashMessage'
import Alert from './notifications/Alert'
import AlertalertProvider from './alert/AlertalertProvider'
import AlertalertContext from './alert/AlertalertContext'
import CreditCardModal from './modals/CreditCardModal'
import CreditCardImprintModal from './modals/CreditCardImprintModal'

export {
  getLocation,
  payWithGooglePay,
  isGooglePaySupported,
  payWithApplePay,
  isApplePaySupported,
  isApplePayConfigured,
  openApplePaySetup,
  confirmPaymentIntent,
  localCache,
  setDocument,
  getDocumentListener,
  getDocument,
  getCollection,
  useListenFirestoreCollectionWhere,
  useListenFirestoreNestedCollection,
  getCollectionListener,
  firestoreTimestampToDate,
  getCollectionWhereListener,
  useListenFirestoreCollection,
  useListenFirestoreCollectionMultipleWhere,
  getFirestoreCollection,
  getCollectionMultipleWhereListener,
  withOnUpdateListener,
  getFirestoreDocument,
  firestoreGenerateId,
  getPushToken,
  firebase,
  useCreditCardModal,
  showFlashMessage,
  Alert,
  getCollectionWhere,
  CreditCardModal,
  CreditCardImprintModal,
  AlertalertProvider,
  AlertalertContext,
}
