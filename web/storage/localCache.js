/* eslint no-undef: 0 */
/*
Created by Matthieu ML
------------------------
ADAPTED FROM @heaven/react-native for the web
------------------------
This is a wrapper around localStorage.
------------------------
TL;DR: JSON.stringify to 'set' and JSON.parse to 'get'
------------------------
Added the possibility to listen to a 'key'
*/

import EventEmitter from 'EventEmitter'

const eventEmitter = new EventEmitter()

const localCache = {
  get: (key) => {
    const value = localStorage.getItem(key)
    return (value
      ? JSON.parse(value)
      : null)
  },
  getMultiple: keys => keys.map(key => localCache.get(key)),
  set: (key, value) => {
    eventEmitter.emit(`${key} set in localCache`, { key, value })
    return localStorage.setItem(key, JSON.stringify(value))
  },
  setMultiple: obj => Object.keys(obj).forEach(key => localCache.set(key, obj[key])), // obj: { key1: value1, key2: value2 }
  listen: (key, callBack) => {
    const initialValue = localCache.get(key)
    callBack(initialValue)

    const handleValueChange = ({ value }) => callBack(value)
    eventEmitter.on(
      `${key} set in localCache`,
      handleValueChange,
    )
    return () => eventEmitter.off(
      `${key} set in localCache`,
      handleValueChange,
    )
  },
  remove: async (key) => {
    await localStorage.removeItem(key)
  },
  clear: () => { // DEBUG PURPOSE
    localStorage.clear()
  },
  clearListeners: (key = '') => eventEmitter.removeAllListeners(key), // DEBUG PURPOSE
}

export default localCache
