// import stripe from '@heaven/react-native/payment/tipsi-stripe/stripe'
// import { GooglePay } from 'react-native-google-pay'
// import {
//   // log,
//   error,
// } from '@heaven/js'

// import ALLOWED_CARD_NETWORKS from './ALLOWED_CARD_NETWORKS'
// import ALLOWED_CARD_AUTH_METHODS from './ALLOWED_CARD_AUTH_METHODS'
// import isGooglePaySupported from './isGooglePaySupported'

// const {
//   STRIPE_PUBLIC_KEY,
//   GOOGLE_PAY_DISPLAYED_LABEL,
// } = process.env

// const payWithGooglePay = async ({ amount, currencyCode }) => {
//   try {
//     const stringAmountNotInSmallestUnit = JSON.stringify(amount / 100)
//     console.log(stringAmountNotInSmallestUnit)
//     // Set the environment before the payment request
//     // GooglePay.setEnvironment(GooglePay.ENVIRONMENT_TEST)

//     const requestData = {
//       cardPaymentMethod: {
//         tokenizationSpecification: {
//           type: 'PAYMENT_GATEWAY',
//           // stripe (see Example):
//           gateway: 'stripe',
//           gatewayMerchantId: '',
//           stripe: {
//             publishableKey: STRIPE_PUBLIC_KEY,
//             version: '2018-11-08',
//           },
//           // other:
//           // gateway: 'example',
//           // gatewayMerchantId: 'exampleGatewayMerchantId',
//         },
//         allowedCardNetworks: ALLOWED_CARD_NETWORKS,
//         allowedCardAuthMethods: ALLOWED_CARD_AUTH_METHODS,
//         billingAddressRequired: false,
//       },
//       transaction: {
//         totalPrice: stringAmountNotInSmallestUnit,
//         totalPriceStatus: 'FINAL',
//         currencyCode,
//       },
//       merchantName: GOOGLE_PAY_DISPLAYED_LABEL,
//     }

//     // Check if Google Pay is available
//     const isReady = await isGooglePaySupported()
//     if (isReady) {
//       // Request payment token
//       const token = JSON.parse(await GooglePay.requestPayment(requestData))
//       // if (!token) throw new Error('googlePay.js\\payWithGooglePay.js:: ERROR: request.id ("tok_...") from requestPayment token is falsy')
//       // Send a token to your payment gateway
//       // log(token)
//       // log(token.id)
//       // log(typeof token)
//       const paymentMethod = await stripe.createPaymentMethod({
//         card: {
//           token: token.id,
//         },
//       })

//       return paymentMethod
//     }
//     return null
//   } catch (e) {
//     error(new Error('googlePay.js\\payWithGooglePay(..):: ERROR FOLLOWS'))
//     error(e)
//     return null
//   }
// }

const payWithGooglePay = async () => { }

export default payWithGooglePay
