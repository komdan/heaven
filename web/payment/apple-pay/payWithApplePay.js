// TODO

import {
  error,
} from '@heaven/js-web'

const {
  APPLE_PAY_DISPLAYED_LABEL,
} = process.env

const payWithApplePay = async ({
  amount,
  currencyCode,
  stripe,
}) => {
  try {
    const stringAmountInSmallestUnit = (+amount / 100).toFixed(2)
    const { tokenId } = await stripe.paymentRequest(
      {
        country: 'FR',
        currency: currencyCode,
        total: {
          label: APPLE_PAY_DISPLAYED_LABEL,
          amount: stringAmountInSmallestUnit,
        },
      },
    )
    await stripe.completeNativePayRequest()
    const paymentMethod = await stripe.createPaymentMethod({
      card: {
        token: tokenId,
      },
    })

    return paymentMethod
  } catch (e) {
    error(e)
    await stripe.cancelNativePayRequest()
    throw e
  }
}

export default payWithApplePay
