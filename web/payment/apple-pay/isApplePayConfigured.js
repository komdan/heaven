// src : https://developer.apple.com/documentation/apple_pay_on_the_web/apple_pay_js_api/checking_for_apple_pay_availability

const isApplePayConfigured = async () => {
  const {
    ApplePaySession,
    ApplePaySession: {
      canMakePaymentsWithActiveCard = () => { },
    } = {},
  } = window || {}

  if (ApplePaySession) {
    const merchantIdentifier = 'example.com.store'
    const canMakePayments = await canMakePaymentsWithActiveCard(
      merchantIdentifier,
    )

    return !!canMakePayments
  }
  return false
}

export default isApplePayConfigured
