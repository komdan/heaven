/* eslint camelcase: 0 */
import {
  error,
} from '@heaven/js-web'

const confirmPaymentIntent = async (
  userId,
  {
    client_secret: clientSecret,
    payment_method,
  },
  stripe,
) => {
  // confirmPaymentIntent && confirmSetupIntent
  console.log('⚠️')
  try {
    const {
      // paymentIntent: paymentIntentResult,
      error: err,
    } = await stripe.confirmCardPayment(
      clientSecret,
      {
        payment_method,
      },
      // { handleActions: false }, // https://stripe.com/docs/js/deprecated/confirm_setup_intent && https://stripe.com/docs/js/deprecated/confirm_payment_intent
    )
    console.log('⚠️⚠️⚠️')
    // await setDocument('clients', userId, { lastPaymentIntent: paymentIntentResult })
    if (err) {
      throw new Error(err)
    }
  } catch (e) {
    error(new Error('confirmPaymentIntent.js\\error-follows'))
    error(e)
  }
}

export default confirmPaymentIntent
