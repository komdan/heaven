// DOCS
// just add useCreditCardModal in your "GlobalContext"

import React, {
  useContext,
} from 'react'
import {
  useTranslation,
} from 'react-multi-lang'
import {
  Container,
  Message,
} from 'semantic-ui-react'
import {
  IsMobileContext,
  FullScreenModal,
  OkAndCancelButtonsInModal,
} from 'paradise/react-web'
import {
  GlobalContext,
} from '@contexts'

const CreditCardImprintModal = () => {
  const t = useTranslation()
  const {
    isCreditCardImprintModalVisible,
    setisCreditCardImprintModalVisible,
    creditCardImprintModalOk,
    setCreditCardImprintModalOkAction,
  } = useContext(GlobalContext)
  const isMobile = useContext(IsMobileContext)
  return (
    <FullScreenModal
      isShowingModal={isCreditCardImprintModalVisible}
      navigationFunction={() => {
        setisCreditCardImprintModalVisible(false)
        setCreditCardImprintModalOkAction(() => {})
      }}
      headerText={`💳 ${t('L\'établissement demande une caution')}`}
      isMobile={isMobile}
    >
      <Container text textAlign="center">
        <Message
          warning
          text
          className="margin-top-2em"
          header={t('La caution peut générer un débit temporaire, restitué après votre paiement sur place')}
        />
        <OkAndCancelButtonsInModal
          onOkPress={creditCardImprintModalOk}
          okText={t('Renseigner une CB')}
          onCancelPress={() => {
            setisCreditCardImprintModalVisible(false)
            setCreditCardImprintModalOkAction(() => {})
          }}
        />
      </Container>
    </FullScreenModal>
  )
}

export default CreditCardImprintModal
