// DOCS
// just add useCreditCardModal in your "GlobalContext"

import React, {
  useContext,
} from 'react'
import {
  useTranslation,
} from 'react-multi-lang'
import CreditCardInput from 'react-credit-card-input'
import {
  Divider,
  Header,
  Container,
  Message,
} from 'semantic-ui-react'
import {
  IsMobileContext,
  FullScreenModal,
  OkAndCancelButtonsInModal,
  CheckboxInput,
} from 'paradise/react-web'
import {
  GlobalContext,
} from '@contexts'

const CreditCardModal = () => {
  const t = useTranslation()
  const {
    isCreditCardModalVisible,
    onCreditCardModalValidatePress,
    onCreditCardModalClosePress,
    creditCardModalError,
    // onCreditCardValueChange,
    onRememberCardChange,
    rememberCard,
    number,
    setNumber,
    cvc,
    setCvc,
    expiry,
    setExpiry,
  } = useContext(GlobalContext)
  const isMobile = useContext(IsMobileContext)

  return (
    <FullScreenModal
      isShowingModal={isCreditCardModalVisible}
      navigationFunction={() => onCreditCardModalClosePress()}
      headerText={t('Payment information')}
      isMobile={isMobile}
    >
      <Divider />
      <Container text textAlign="center">
        <Message
          warning
          header={`💳 ${t('Credit card only')}`}
          content={`🚫 ${t('Restaurant titles card not accepted here')}`}
        />
        <CreditCardInput
          cardNumberInputProps={{ value: number, onChange: e => setNumber(e.target.value) }}
          cardExpiryInputProps={{ value: expiry, onChange: e => setExpiry(e.target.value) }}
          cardCVCInputProps={{ value: cvc, onChange: e => setCvc(e.target.value) }}
          fieldClassName="credit-card-container"
          inputClassName="credit-card-input"
          dangerTextClassName="font-size-1p2em"
        />
        <Divider />
        {!!creditCardModalError && (
          <Header as="h4" textAlign="center" className="full-width" color="red">{`${creditCardModalError}`}</Header>
        )}
        <CheckboxInput
          t={t}
          label={t('Remember this card')}
          isChecked={rememberCard}
          setIsChecked={onRememberCardChange}
          toggle
        />
        <Divider />
        <OkAndCancelButtonsInModal
          onOkPress={onCreditCardModalValidatePress}
          onCancelPress={onCreditCardModalClosePress}
        />
      </Container>
    </FullScreenModal>
  )
}

export default CreditCardModal
