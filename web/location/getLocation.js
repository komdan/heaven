import {
  NULL_COORDS,
} from '@heaven/constants-web'

const getLocation = async () => NULL_COORDS

export default getLocation
