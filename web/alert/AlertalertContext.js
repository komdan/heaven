import { createContext } from 'react'

const AlertalertContext = createContext(null)

export default AlertalertContext
