import React, {
  useState,
  Fragment,
  useContext,
  useEffect,
  useMemo,
} from 'react'
import {
  Button,
  Container,
  Message,
} from 'semantic-ui-react'
// import createPersistedState from 'use-persisted-state'
import {
  IsMobileContext,
  FullScreenModal,
} from 'paradise/react-web'
import AlertalertContext from './AlertalertContext'

// const useIsShowingState = createPersistedState('alert-is-showing');
// const useIsLoadingState = createPersistedState('alert-is-loading');
// const useTitleState = createPersistedState('alert-title');
// const useMessageState = createPersistedState('alert-message');
// const useButtonsState = createPersistedState('alert-buttons');

const AlertalertProvider = ({
  children = <Fragment />,
}) => {
  const isMobile = useContext(IsMobileContext)
  const [isShowingAlert, setIsShowingAlert] = useState(false)
  const [isLoading, setAlertIsLoading] = useState(false)
  const [title, setTitle] = useState('')
  const [message, setMessage] = useState('')
  const [buttons, setButtons] = useState([{ text: 'OK' }])

  const Alertalert = (
    newTitle = '',
    newMessage = '',
    newButtons = [{ text: 'OK' }],
  ) => {
    setTitle(newTitle)
    setMessage(newMessage)
    setButtons(newButtons)
    setIsShowingAlert(true)
  }
  const AlertalertMemo = useMemo(() => Alertalert, [])

  const hideAlertalert = () => {
    setTitle('')
    setMessage('')
    setButtons([])
    setIsShowingAlert(false)
  }

  useEffect(() => {
    if (!message || message === '' || !title || title === '') {
      setIsShowingAlert(false)
    }
  }, [message, title])

  return (
    <AlertalertContext.Provider value={AlertalertMemo}>
      {children}
      <FullScreenModal
        showReturnButton={false}
        isShowingModal={isShowingAlert}
        isMobile={isMobile}
      >
        <Container text textAlign="center">
          <Message
            warning
            info
            className="margin-top-2em"
            header={title}
            content={message}
          />
          {buttons.map(({
            text = '',
            style = '',
            onPress = () => {},
          }, buttonIndex) => (
            <Button
              key={`buttonalertalert${buttonIndex}`}
              color={(style === 'destructive') ? 'red' : 'green'}
              size={isMobile ? 'medium' : 'huge'}
              content={text}
              className="full-width"
              onClick={async () => {
                setAlertIsLoading(true)
                await onPress()
                setAlertIsLoading(false)
                hideAlertalert()
              }}
              loading={isLoading}
              disabled={isLoading}
            />
          ))}
        </Container>
      </FullScreenModal>
    </AlertalertContext.Provider>
  )
}

export default AlertalertProvider
