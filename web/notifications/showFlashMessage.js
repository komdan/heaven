import { toast } from 'react-toastify'

const showFlashMessage = (
  title,
  message = '',
  {
    onPress = () => { },
    position = 'top-right',
    duration = 'long',
    autoHide = true,
  } = {},
) => {
  let flashMessageDuration
  if (duration === 'veryShort') { flashMessageDuration = 1000 }
  if (duration === 'short') { flashMessageDuration = 4000 }
  if (duration === 'long') { flashMessageDuration = 8000 }

  toast(`${title} ${message}`, {
    position,
    autoClose: autoHide ? flashMessageDuration : 99999999999,
    closeOnClick: !onPress,
    pauseOnHover: true,
    onClick: onPress,
    hideProgressBar: true,
    type: toast.TYPE.INFO,
    // hideOnPress: ,
    // backgroundColor: FLASH_MESSAGE_BACKGROUND_COLOR || 'black',
    // color: FLASH_MESSAGE_TEXT_COLOR || 'white',
    // autoHide,
  })
}

export default showFlashMessage
